#ifndef __CONSTANTS__
#define __CONSTANTS__

template <typename E>
constexpr auto to_underlying(E e) noexcept
{
    return static_cast<std::underlying_type_t<E>>(e);
}

/**
 * @brief The IntVarDomain class represents the domain, which
 * is a list of distinct values, of an integer variable
 */
class Domain {
public:
	static const int NONE = 0;
	static const int INTEGER_LIST = 1;
	static const int INTEGER_INTERVAL = 2;
	static const int BOOLEAN = 3;
	std::list<int> domain;

	/**
	 * @brief Return the type of the domain
	 * @return A integer which is related to the constants listed before
	 */
	virtual int type() const { return Domain::INTEGER_LIST; }

	Domain() { }
	Domain(std::initializer_list< int > l) : domain(l) { }

	// Wrapper functions to domain
	unsigned int size() const { return domain.size(); }
	int max() const { return *std::max_element(domain.begin(), domain.end()); }
	int min() const { return *std::min_element(domain.begin(), domain.end()); }

	// CHR needed overloaded functions
	bool operator==(const Domain& o) { return (*this == o); }
	std::string to_string() const { return chr::TIW::to_string(domain); }
};

namespace std {
	template< >
	struct hash<Domain>
	{
		typedef unsigned int argument_type;
		typedef hash_result_type result_type;

		result_type operator()(const Domain& e) const
		{
			hash< std::list< int > > hasher;
			return hasher(e.domain);
		}
	};
}


using IntVarDomain = Domain;


/**
 * @brief The Flag enum class list all possible flags for a
 * vertice of the graph
 */
enum Flag : unsigned long
{
	// NODE TYPE
	VARIABLE			= 1UL << 0,
	RELATION			= 1UL << 1,
	TRANSFORMATION		= 1UL << 2,

	// VARIABLE DATATYPE
	INTEGER				= 1UL << 3,
	BOOLEAN				= 1UL << 4,
	AUXILIARY			= 1UL << 5,
	PROP_INTEGER		= 1UL << 35,	// integer to branch (it requires interval variables)

	// VARIABLE TRANSFORMATIONS
	DIRECT_ENCODING		= 1UL << 6,
	LOG_ENCODING		= 1UL << 7,
	VAR_INTERVAL        = 1UL << 34,
	HYBRID_ENCODING     = 1UL << 21,

	// RELATIONS
	ALL_DIFFERENT		= 1UL << 8,
	ADD_EQUAL			= 1UL << 9,
	SUB_EQUAL			= 1UL << 10,
	ADD_SUB_EQUAL		= 1UL << 11,
	MULT_EQUAL			= 1UL << 12,
	BINARY_DIFF			= 1UL << 13,
	BINARY_DISJUNCTION	= 1UL << 14,
	BINARY_NOT			= 1UL << 15,

	LESS_THAN			= 1UL << 16,
	GREATER_THAN		= 1UL << 17,
	LESS_EQUAL_THAN     = 1UL << 18,
	GREATER_EQUAL_THAN	= 1UL << 19,

	EQUAL				= 1UL << 20,
	//ELEMENT				= 1UL << 21,	// to-fix: ID in use by HYBRID_ENCODING
	IF_EQUAL_THEN		= 1UL << 22,
	IF_THEN         	= 1UL << 23,
	IF_THEN_NOT     	= 1UL << 24,
	IFTHEN_GREATER      = 1UL << 25,
	//STORED_EQUAL		= 1UL << 26,
	AT_LEAST			= 1UL << 27,
	AT_MOST				= 1UL << 28,
	AND					= 1UL << 29,
	IF_THEN_OR      	= 1UL << 30,
	CARDINALITY         = 1UL << 31,
	CARDINALITY_LEQ     = 1UL << 32,

//	CSPBOOLEAN          = 1UL << 33,	// 2019-01-09 new vartype ??
	SUBINTERVAL         = 1UL << 33,	// 2019-11-13 --> to create sub-interval clauses

	PROP_ADD_EQUAL      = 1UL << 26,	// addition to propagate

	INTERVAL_BRANCHING  = 1UL << 36,	// CVG 2019-02-14
	LINEAR_EQ_SEQ		= 1UL << 37		// CVG 20191008

};

struct Flags
{
	unsigned long value;

	Flags() : value(0) {}
	Flags(Flag f) : value(f) {}
	Flags(unsigned long v) : value(v) {}
	operator unsigned long() const { return value; }
};

inline bool check(Flags value, Flags flags) { return ((value.value & flags.value) == flags.value); }

namespace chr {
template<>
struct Type_instruction_wrapper< Flags, false >
{
	/**
	 * Convert a Flag object \a s to a string.
	 * @param s The set to convert
	 * @return A new string
	 */
	static std::string to_string(Flags flags) {
		std::string str, sep;
		unsigned int mask = 1;

		for (unsigned int i = 0; i < 40; ++i)	//before:32
		{
			switch (flags.value & mask)
			{
				case Flag::VARIABLE        : str += sep + "Variable"; break;
				case Flag::RELATION        : str += sep + "Relation"; break;
				case Flag::TRANSFORMATION  : str += sep + "Transformation"; break;
				case Flag::INTEGER         : str += sep + "Integer"; break;
				case Flag::BOOLEAN         : str += sep + "Boolean"; break;
				//case Flag::CSPBOOLEAN      : str += sep + "Boolean"; break;   //20190109
				case Flag::DIRECT_ENCODING : str += sep + "DirectEncoding" ; break;
				case Flag::LOG_ENCODING       : str += sep + "LogEncoding" ; break;
				case Flag::AUXILIARY          : str += sep + "Auxiliary"   ; break;
				case Flag::ALL_DIFFERENT      : str += sep + "AllDifferent"; break;
				case Flag::BINARY_DIFF        : str += sep + "BinaryDiff"  ; break;
				case Flag::ADD_EQUAL          : str += sep + "AddEqual"    ; break;
				case Flag::SUB_EQUAL          : str += sep + "SubEqual"    ; break;
				case Flag::ADD_SUB_EQUAL      : str += sep + "AddSubEqual" ; break;
				case Flag::MULT_EQUAL         : str += sep + "MultEqual"   ; break;
				case Flag::BINARY_DISJUNCTION : str += sep + "Disjunction"; break;
				case Flag::BINARY_NOT         : str += sep + "BinaryNot"   ; break;
				case Flag::LESS_THAN          : str += sep + "LessThan"     ; break;
				case Flag::LESS_EQUAL_THAN    : str += sep + "LessEqualThan"; break;
				case Flag::GREATER_THAN       : str += sep + "GreaterThan"  ; break;
				case Flag::EQUAL              : str += sep + "Equal"        ; break;
				//case Flag::ELEMENT            : str += sep + "Element"      ; break;
			}
			if (!str.empty()) sep = " | ";
			mask <<= 1;
		}
		return str;
	}
};
}

namespace std {
	template< >
	struct hash< Flags >
	{
		typedef unsigned int argument_type;
		typedef hash_result_type result_type;

		result_type operator()(const Flags& e) const
		{
			hash< unsigned int > hasher;
			return hasher(e.value);
		}
	};
}

#endif // __CONSTANTS__
