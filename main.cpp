//#include <iostream>
//#include <ostream>
//#include <fstream>
//#include <sstream>
//#include <array>
//#include <list>
//#include <vector>
//#include <queue>
//#include <string>
//#include <graph_chr_header.hpp>

#include <examples/tests.hpp>
//#include <examples/golomb.hpp>
//#include <examples/sudoku.hpp>
#include <examples/magic_square.hpp>

#include <examples/test_propagation.hpp>
//#include <examples/relations_tests.hpp>
//#include <examples/garam.hpp>
//#include <examples/car_sequencing.hpp>


#define N_STR std::string()



// ==========================================================================
// Test Double Boites
// boites LOG + boites DIRECT
//
// X + Y = Z
// [0,3] + [4,7] = [8,15]
// ==========================================================================

void test_double_boites(GRAPH& space)
{
	space.encoding_sequence.push_back(Flag::LOG_ENCODING);
	space.encoding_sequence.push_back(Flag::DIRECT_ENCODING);

	unsigned int x = space._first_free_id++;
	space.vertice(x, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, IntVarDomain({0,1,2,3}));
	unsigned int y = space._first_free_id++;
	space.vertice(y, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, IntVarDomain({4,5,6,7}));
	unsigned int z = space._first_free_id++;
	space.vertice(z, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, IntVarDomain({8,9,10,11,12,13,14,15}));

	unsigned int add = space._first_free_id++;
	space.vertice(add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain());
	space.edge(add, x, 0);
	space.edge(add, y, 1);
	space.edge(add, z, 2);
	std::cout << add << ": " << x << " + " << y << " = " << z << std::endl;

	unsigned int alldiff = space._first_free_id++;
	space.vertice(alldiff, Flags(Flag::RELATION | Flag::ALL_DIFFERENT), N_STR, IntVarDomain() );
	space.edge(alldiff, x, -1);
	space.edge(alldiff, y, -1);
	space.edge(alldiff, z, -1);
	std::cout << alldiff << ": alldiff(" << x << "," << y << "," << z << ")" << std::endl;

	std::cout << std::endl;
	//CHR_RUN( space.apply_all_transformation(); )

	// output
	//std::string filename = "testdb_dirlog.cnf";
	//std::unordered_map<unsigned int, unsigned int> map_cnf_graph;
	//space.print_cnf(filename, map_cnf_graph, true);
}



void test_prop_integer()
{
	std::cout << "test_prop_integer()" << std::endl;
	GRAPH space;

	// -- add_equal
	unsigned int x = space._first_free_id++;
	space.vertice(x, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, IntVarDomain({0,1,2,3,4}));

	unsigned int y = space._first_free_id++;
	space.vertice(y, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, IntVarDomain({0,1,2}));

	unsigned int z = space._first_free_id++;
	space.vertice(z, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, IntVarDomain({0,1,2,3,4,5}));

	unsigned int add = space._first_free_id++;
	space.vertice(add, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain());
	space.edge(add, x, 0);
	space.edge(add, y, 1);
	space.edge(add, z, 2);

	// -- less_than
	unsigned int aux = space._first_free_id++;
	space.vertice(aux, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({3}));

	unsigned int lessthan = space._first_free_id++;
	space.vertice(lessthan, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, IntVarDomain());
	space.edge(lessthan, z, 0);
	space.edge(lessthan, aux, 1);

	CHR_RUN( space.apply_all_transformation(); )

	// output
	std::string filename = "lala";
	std::unordered_map<unsigned int, unsigned int> map_cnf_graph;
	space.print_cnf(filename, map_cnf_graph, true);
}



// ===========================================================================
// MAIN
// ===========================================================================

int main(int argc, const char *argv[])
{
    //time_t my_time = time(NULL);
	//std::cout << "main()      : start\t\t" << ctime(&my_time);

	TRACE( chr::Log::register_flags(chr::Log::ALL); )
	Options options = parse_options(argc, argv, {
		// I/O options
		{ "print_stats",       "s",     false, "Print the statistics (computing times, triggered rules, ...)"},
		{ "print_store",       "p",     false, "Print the store of constraint"},
		{ "print_dot",         "dot",   false, "Print the graph in dot format"},
		{ "print_cnf",         "cnf",   false, "Print the disjunction of the store in CNF format"},
		{ "print_cnf_scale",   "cnf_s", false, "Print the disjunction of the store in CNF format with rescaled variable numbers"},
		{ "load_cnf_solution", "sol",   true,  "Load the solution file created by a sat solver"},

		// Tests ------------------------------------------

		{ "test",             "test",   true,  "Test a model using a SimpleFile"},
		{ "test_add",         "badd",   false, "Generate all add benchmark files"},
		{ "test_sub",         "bsub",   false, "Generate all sub benchmark files"},
		{ "test_mult",		  "bmult",  false, "Generate all mult benchmark files"},
		{ "test_eq",          "beq",    false, "Generate all equal benchmark files"},
		{ "test_lt",          "blt",    false, "Generate all less than benchmark files"},
		{ "test_gt",          "bgt",    false, "Generate all greater than benchmark files"},
		{ "test_elt",         "belt",   false, "Generate all element benchmark files"},
		{ "test_condition",   "bcond",  false, "Generate benchmarks for the CONDITION"},
		{ "test_lessthan",    "blthan", false, "Test LESS_THAN relation"},
		{ "test_propagation", "tprop",  true,  "Test propagation (0,1,2)"},

		// Models -----------------------------------------

		{ "sudoku_m0",       "sudoku0", true,  "Sudoku (order N), model 0 : Classical"},
		{ "sudoku_m1",       "sudoku1", false, "Sudoku (order 3), model 1 : Magic-Sudoku"},
		{ "magic_square_m0", "ms0",     true,  "Magic Square (size N), model 0 : v1"},
		{ "magic_square_m1", "ms1",     true,  "Magic Square (size N), model 1 : MS propagated"},
		{ "magic_square_m2", "ms2",     true,  "Magic Square (size N), model 2 : MS using LINEAR_EQUAL_SEQ global constraint"},
		{ "magic_square_m3", "ms3",     true,  "Magic Square (size N), model 3 : basic, not factorized, arborescent decomp."},
		{ "magic_square_m4", "ms4",     true,  "Magic Square (size N), model 4 : basic, not factorized, sequential decomp."},

//		{ "magic_square_continue",   "msc",   true, "Magic Square (size n) with continue values between 1 to n*n"},
//		{ "bimagic_square",          "bms",   true, "Bimagic Square (size N)"},
//		{ "bimagic_square_continue", "bmsc",  true, "Bimagic Square (size N) with continue values between 1 to n*n"},
//		{ "garam",                   "garam", true, "Garam problem with initial grid given as a parameter"},
//		{ "carsequencing_m0",        "cars0", true, "The Car Sequencing, model 0"},
//		{ "carsequencing_m1",        "cars1", true, "The Car Sequencing, model 1"},
//		{ "carsequencing_m2",        "cars2", true, "The Car Sequencing, model 2"},
		{ "golomb",        "golomb", true, "Golomb rulers"},
	});

	Options_values values;


	// Tests ----------------------------------------------

//	if (has_option("benchmarks_add", options))      { create_add_equal_benchmark_files(); return 0; }
//	if (has_option("benchmarks_sub", options))      { create_sub_equal_benchmark_files(); return 0;	}
//	if (has_option("benchmarks_mult", options))     { create_mult_equal_benchmark_files();return 0;	}
//	if (has_option("benchmarks_condition", options)){ create_condition_benchmark_files(); return 0;	}
//	if (has_option("test_lessthan", options))       { test_lessthan(); return 0; }
	if (has_option("test_propagation", options, values))
	{
		if (values[0].i == 0) {
			// test 1 propagation
			tp1_addequal();
			tp1_propaddequal();
			tp1_propall();
		}
		else if (values[0].i == 1) {
			// test 2 propagation
			tp2_addequal();
			tp2_propaddequal();
			tp2_propall();
		}
		return 0;
	}


    // Models ---------------------------------------------

    GRAPH space;

    if (has_option("test", options, values)) {
		//run_test(space, values[0].str);
		//test_hybrid(space);
		test_addition_hybrid(space);
	}

/*	if (has_option("sudoku_m0", options, values)) {
		run_sudoku(space, "sudoku_GordonRoyle.txt", values[0].i);
	}
	*/
////	else if (has_option("sudoku_m1", options, values)) {
////		run_magic_sudoku(space, "magic_sudoku.txt", 0);
////	}
//	else if (has_option("magic_square_m0", options, values)) {
//		generate_magic_square_problem(space, values[0].i, 0, true);
//	}
//	else if (has_option("magic_square_m1", options, values)) {
//		run_magic_square_propagated(space, values[0].i);
//	}
//	else if (has_option("magic_square_m2", options, values)) {
//		run_magic_square_m2(space, values[0].i);
//	}

	else if (has_option("magic_square_m3", options, values)) {
		run_magic_square_m3(space, values[0].i);
	}
	else if (has_option("magic_square_m4", options, values))
	{
		space.encoding_sequence.push_back(Flag::HYBRID_ENCODING);
		space.HBASE = 2;
		run_magic_square_m4(space, values[0].i);
	}

////	else if (has_option("garam", options, values)) {
////		generate_garam_problem(space,values[0].str);
////	}
//	else if (has_option("bimagic_square", options, values)) {
//		//generate_bi_magic_square_problem(space, values[0].i, 72);
//		run_bimagic_square(space, values[0].i, true, 0);
//		return 0;
//	}
////	else if (has_option("bimagic_square_continue", options, values)) {
////		int N  = values[0].i;
////		generate_bi_magic_square_problem(space, N, N*N, true);
////	}
////	else if (has_option("magic_square_continue", options, values)) {
////		generate_magic_square_problem(space, values[0].i,255);
////	}
////	else if (has_option("carsequencing", options, values)) {
////		generate_car_sequencing(space, values[0].str);
////	}
//	else if (has_option("golomb", options, values)) {
//		run_golomb(space, values[0].i);
//	}


	//test_double_boites(space);

    //space.collect_graph_stats(N_STR);
    CHR_RUN( space.apply_all_transformation(); )
    values.clear();
	//space.collect_graph_stats(N_STR);


	// CNF Output -----------------------------------------

	if (has_option("print_cnf", options)) {
		space.print_cnf(std::cout);
	}

	std::unordered_map<unsigned int,unsigned int> map_cnf_graph;

	if (has_option("print_cnf_scale", options)) {
		std::string filename = "output.cnf";
		space.print_cnf(filename, map_cnf_graph, true);
		space.print_variable_path(filename, map_cnf_graph);
	}


	//
	// HOWTO: ./graph <model> -cnf_s -sol <solution_file>
	//

	if (has_option("load_cnf_solution", options, values))
	{
		std::cout << "loading CNF solution from \'" << values[0].str << "\'" << std::endl;
		std::ifstream solution_file (values[0].str);

		if (solution_file.is_open()) {
			std::string result;
			solution_file >> result;

			if (result == "SAT")
			{
				int value;
				while (solution_file >> value) {
					if (value == 0) break;
					space.update_boolean_domain(map_cnf_graph.at(std::abs(value)), IntVarDomain({ ((value<0)?0:1) }) );
                }
			}
		} else {
			std::cerr << "File " << values[0].str << " not found." << std::endl;
			exit(1);
		}
		space.propagate_boolean_solution_to_integer_variables();
		std::string filename = "output.cnf";
		space.print_csp_solution(filename);
		values.clear();
	}

	if (has_option("print_stats", options)) chr::Statistics::print(std::cout);
	if (has_option("print_store", options)) space.print_store(std::cout);
	if (has_option("print_dot", options))   space.print_graph_dot(std::cout);

    //my_time = time(NULL);
	//std::cout << "main()      : end\t\t" << ctime(&my_time);

	return 0;
}


