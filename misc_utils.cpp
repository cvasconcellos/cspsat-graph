#include "misc_utils.hpp"


// =======================================================================
// HYBRID ENCODING rules
//
// HBASE	A positive integer to be used as encoding base.
// Offset	The minimum value attainable in a certain encoding.
//
//
// =======================================================================



// =======================================================================
// h_offset()
//
// x		Integer value
// base		Hybrid base
//
// examples:
// h_offset(10,3)  = 1
// h_offset(5,3)   = 0
// h_offset(-10,3) = -2
// =======================================================================

int h_offset(int x, unsigned int base)
{
	//return ((int)(x/pow(2,base)) - ((x<0) ? 1:0));

    int ox = ((int)(x/pow(2,base)) - ((x<0) ? 1:0));
	if (ox == 0)
		return 1;
	return ox;
}



// =======================================================================
// h_domain_offset()
//
// Returns the offset required to represent a domain/interval.
//
// l,u	The interval
// base Hybrid base
//
// example:
// h_domain_offset(21,21,3) = 2		--> offset of 16 (2*2^3)
// h_domain_offset(0,32,3)  = 0		--> no-offset
// h_domain_offset(-10,0,3) = -2	--> offset of -16 (-2*2^3)
// =======================================================================

int h_domain_offset(int l, int u, unsigned int base)
{
	//return std::min(h_offset(l,base), h_offset(u,base));

	int lo = h_offset(l,base);
	lo = (lo == 0) ? 1 : lo;
	return std::min(lo, h_offset(u,base));
}



// =======================================================================
// h_domain_length()
//
// Returns the length of a domain. It uses the maximum offset possible,
// skipping the bzaines not required.
// It doesn't consider a 0-bzaine.
//
// lower,upper	The interval
// base			Hybrid base
//
// example:
// h_domain_length(21,21,3) = 4 --> 3 log + 1 order (1 bzaine skipped)
// =======================================================================

unsigned int h_domain_length(int lower, int upper, int base)
{
	int ol = h_offset(lower, base);
	int ou = h_offset(upper, base);

////	//int r = abs(ou-ol) + base; 	// domain length
////
////	// if domain includes 0, then
////	//return abs(ou-ol) + base + (((dmin<=0) && (0<dmax)) ? 1 : 0);
////
////	return (abs(ou - ol)+base);
//
//	if (ol != ou)
//		return (abs(ou - ol)+base);
//	return (abs(ol)+base);

	if ((lower < 0) && (upper <= 0))
        return (base + abs(ol));
	else if ((lower < 0) && (upper > 0))
		return (base + abs(ol) + abs(ou));
	return (base + abs(ou));
}



// =======================================================================
// bzvalue()
//
// Returns the bzaine's value.
//
// index	Array index (the whole vector's index)
// base		Hybrid encoding base (ex: B=2 => (2^0, 2^1, 2^B-1) & (step=2^B) )
// offset	Value offset (differs on each variable)
//
// examples:
// bzvalue(3,3,0)  = 0  --> 1st order-side index, base=8
// bzvalue(2,3,3)  = 0   --> no bzaine, this index is in the log-side
// =======================================================================

int bzvalue(unsigned int index, unsigned int base, int offset)
{
	/*int bz = 0;
	for(int i=base; i<index; i++)

	unsigned int step = pow(2,base);
	int bz=0;
	if (index>=base) {
		if (offset<0)
			bz = step * (offset-index+base-1);
		else
			bz = step * (offset+index-base+1);
	}
	return bz;
	*/
	if (index>=base)
		return ((int)(index-base+offset) * pow(2,base));
	return 0;
}



// =======================================================================
// bzindex()
//
// Returns the bzaine's bit index, from LSB to MSB (log to order).
// The index considers an array starting in 0.
//
// value	Bzaine absolute value
// base		Hybrid base
// offset	Value offset (differs on each variable)
//
// examples:
// bzindex(24,3,0) = 6  --> 6th bit in a value with base=3 and offset=0.
// bzindex(-24,3,0)= 6  --> 6th bit in a value with base=3 and offset=0.
// bzindex(16,2,0) = 6  --> 6th bit in a value with base=2 and offset=0.
// bzindex(16,3,2) = 3  --> 3rd bit in a value with base=3 and offset=2.
// =======================================================================

unsigned int bzindex(int value, unsigned int base, int offset)
{
	return (base + (int) abs(value)/pow(2,base) - abs(offset));
}




// =======================================================================
// hybrid2value()
//
// s		Binary string, from LSB to MSB
// base		Hybrid base
// offset	Hybrid offset
// =======================================================================

int hybrid2value(std::string s, unsigned int base, unsigned int offset)
{
	int value = 0;

	// log
	for (int i=0; i < std::min((int) base,(int) s.length()) ; i++) {
		value += (s[i] == '1') ? pow(2,i) : 0 ;
	}

	// order
	int index = 0;
	for (unsigned int i=base; i<s.length(); i++) {
		value += (s[i] == '1') ? (index+offset)*(int)pow(2,base) : 0;
		if (offset > 0)
			index++;
		else
			index--;
	}

	return value;
}



// =======================================================================
// value2hybrid()
//
// Returns the hybrid encoding using the given base.
// The value returned goes from LSB to MSB (log+order) and removes offset.
//
// value	Integer value
// base		Hybrid base
// offset	Offset to use (0 by default)
//
// examples:
// value2hybrid(21,3,2) = 101 1
// value2hybrid(10,3,1) = 010 1
// =======================================================================

/*std::vector<bool> value2hybrid(int value, unsigned int base, int offset, unsigned int length)
{
	std::vector<bool> r;	// result

	// order --------------------------------------
	unsigned int step = std::pow(2,base);		// step: size of intervals

	if (value < 0) {
		while (value <= 0){
			r.push_back(true);
			value +=step;
		}
	}
	else {
		while (abs(value) >= abs(step)) {
			r.push_back(true);
			value-=step;
		}
	}
	r.push_back(true);

	// remove offset
	if (offset != 0) {
		for (int i=0; i<offset; i++)
			r.pop_back();
	}

	// log ----------------------------------------
	value = abs(value);

	for (int i=(base-1); i>=0; --i)
	{
		if (value == 0) {
			r.push_back(false);
		}
		else if (value >= std::pow(2,i)) {
			r.push_back(true);
			value -= std::pow(2,i);
		}
		else {
			r.push_back(false);
		}
	}
	// reverse vector to place log-encoding first
	std::reverse(r.begin(),r.end());

	// if length isn't the required, add zeros at the end
	while (r.size() < length)
        r.push_back(false);

	return r;
}
*/
/*
std::vector<bool> value2hybrid(int value, unsigned int base, int offset, unsigned int length)
	std::vector<bool> r;

	// order --------------------------------------
	unsigned int step = std::pow(2,base);

	while ((value <0) || (abs(value) >= step)) {
		r.push_back(true);
		value += (value < 0) ? (step) : (-1*step);
	}

	// remove offset
	offset += (offset<0) ? 1 : 0;
	if (offset > 1) {
		for (unsigned int i=0; i<abs(offset); i++)
			r.pop_back();
	}

	// add a minimal order part
	if (r.size() < 1)
		r.push_back(false);


	// log ----------------------------------------
	value = abs(value);

	for (int i=(base-1); i>=0; --i)
	{
		if (value == 0) {
			r.push_back(false);
		}
		else if (value >= std::pow(2,i)) {
			r.push_back(true);
			value -= std::pow(2,i);
		}
		else {
			r.push_back(false);
		}
	}

	// reverse vector to place log-encoding first
	std::reverse(r.begin(),r.end());

	// if length isn't the required, add zeros at the end
	while (r.size() < length)
        r.push_back(false);

	return r;
}
*/

std::vector<bool> value2hybrid(int value, unsigned int base, int offset, unsigned int length)
{
	std::vector<bool> r;
	int B = pow(2,base);

	// order --------------------------------------
	int olength     = (int) ((abs(value)+((value<0) ? B : 0))/B);	// order-part length
	int olength_adj = olength - (abs(offset) -1);		// length adjusted by offset

	//std::cout << "order-length: " << olength << "  ~ " << olength_adj << std::endl;

	for (unsigned int i=0; i<olength_adj; i++)
		r.push_back(true);

	// adjust to length required
	for (unsigned int i=(r.size()+base); i<length; i++)
		r.push_back(false);

	// reverse vector (output: msb->lsb)
	std::reverse(r.begin(),r.end());

	// log ----------------------------------------
	value = abs(abs(value) - (olength*B)) ;

	for (int i=(base-1); i>=0; --i)
	{
		if (value == 0) {
			r.push_back(false);
		}
		else if (value >= std::pow(2,i)) {
			r.push_back(true);
			value -= std::pow(2,i);
		}
		else {
			r.push_back(false);
		}
	}

	// return the reversed vector (output: lsb->msb)
	std::reverse(r.begin(),r.end());
	return r;
}











std::vector<unsigned int> dec2binary(int n, int nbits)
{
    std::vector<unsigned int> output;
    for (int i=(nbits-1); i >= 0; i--) {
        int k = n >> i;
        if (k & 1)
            output.push_back(1);
        else
            output.push_back(0);
    }
    return output;
}



// ==========================================================================
// value -> hybrid encodage
// input: value decimal value, a base (base>0)
// output: A boolean vector, from MSB to LSB
// ==========================================================================
/*
std::vector<bool> value2hybrid(int value, unsigned int base)
{
	std::vector<bool> r;

	// order ------------------------------------
	unsigned int step = std::pow(2,base);

	if (value < 0) {
		while (value <= 0){
			r.push_back(true);
			value +=step;
		}
	}
	else {
		while (abs(value) >= abs(step)) {
			r.push_back(true);
			value-=step;
		}
	}

	// log --------------------------------------
	value = abs(value);

	for (int i=(base-1); i>=0; --i)
	{
		if (value == 0) {
			r.push_back(false);
		}
		else if (value >= std::pow(2,i)) {
			r.push_back(true);
			value -= std::pow(2,i);
		}
		else {
			r.push_back(false);
		}
	}

	std::reverse(r.begin(),r.end());
    return r;
}
*/


// ==========================================================================
// POWER CEIL
//
// Ex: base=2, this can be replaced by:
//   pow(2, logb(x)+1)
//   pow(2, ceil(log(x)/log(2)))
// ==========================================================================
/*
unsigned int power_ceil(unsigned int num, int base=2)
{
	// num : Number to cover
	// base: Power base to use
	unsigned int value=1;
	while (value <= num) value *=base;
	return(value);
}
*/



// ==========================================================================
// SUB_INTERVALS 2
// Returns a collection of subintervals in [a,b] based on power of 2 values.
//
// a: Interval lower bound
// b: Interval upper bound
// ==========================================================================

std::vector<Interval> subintervals2(int a, int b)
{
	std::vector<unsigned int> cutpoint;
	int i_from = (a == 0) ? 1 : logb(a)+1;	// adjustment in case of a==0

	for(unsigned int i = i_from; pow(2,i)<=b; ++i) {
		cutpoint.push_back(pow(2,i));
	}

	std::vector<Interval> boundaries;
	int l = a;
	int u = cutpoint[0];
	for(unsigned int i=0; i<cutpoint.size(); ++i) {
		u = (i == cutpoint.size()) ? a : cutpoint[i]-1;
		Interval x(l,u);
		boundaries.push_back(x);
		l = cutpoint[i];
	}
	u = b;
	Interval x(l,u);
	boundaries.push_back(x);

	return boundaries;
}



/* SUBINTERVALS WIP

// Example program
#include <iostream>
#include <cmath>
#include <queue>
#include <vector>

int main()
{
    //
    // std::vector<int> subintervals(int a,int b,int ncuts);
    //
    int a = 4;
    int b = 46;
    int ncuts = 2;      // ncuts

    std::queue<int> lbound;
    lbound.push(a);
    std::queue<int> ubound;
    ubound.push(b);
    std::queue<int> cutlevel;
    cutlevel.push(0);

    for (unsigned int k=0; ((k<ncuts) || (lbound.size()>0)); k++)
    {
        int l = lbound.front(); lbound.pop();
        int u = ubound.front(); ubound.pop();

        int center = (int)((u-l)/2);
        int cl = logb(center);
        int cu = logb(center)+1;

        std::cout << "[" << l << "," << u << "]\t" << center << "\t" << cl << "\t" << cu << std::endl;
    }

}


*/




/* ==========================================================================
 * SPLIT DOMAIN
 *
 * Takes an integer domain (x \in {minv,maxv}) and cuts it in several intervals.
 * Returns a vector with the cut values.
 *
 * Arguments
 *   a,b  : The interval low and upper bound .
 *   ncuts: The number of cuts to do over the interval.
 *
 * (TODO) Cases:
 *  1) Both positive: All OK
 *  2) Both negative: Converted to positive and reconverted at the end.
 *  3) a(-), b(+)   :
 * ==========================================================================
 */




/*
std::vector<int> interval_cutpoints(int a, int b, unsigned int ncuts, bool all_levels=true)
{
	//if (a>b) return {-1};
	bool reverse_interval = false;
	if ((a<0) && (b<0) ) {
		int aux = -1*a;
		a = -1*b;
		b = aux;
		reverse_interval = true;
	}

//	// interval type
//	unsigned int type=1 ;
//	if (a<0 && 0<=b) {
//		type = 2;	// -+: swap bounds
//		int aux = (-1)*a;
//		a = (-1)*b;
//		b = (-1)*aux;
//	} else {
//		type = 3;	// --: TODO
//	}

	std::queue<int> lbound, ubound, level;
	lbound.push(a); ubound.push(b);
	std::vector<int> output;
	unsigned int k=0;	//cuts count

	while (k<ncuts && !lbound.empty()) {
		int lower = lbound.front(); lbound.pop();
		int upper = ubound.front(); ubound.pop();
		int mid   = trunc((lower+upper)/2);
		int cut   = ilogb(upper-1);

		// The cut-point is valid only if it's INSIDE the interval
		if ((lower<=pow(2,cut)) && (pow(2,cut)<=upper)) {
			unsigned int right = abs(mid-pow(2,cut));	// distance |-->
			unsigned int left  = abs(mid-pow(2,cut-1));	// distance <--|

			// choose the cut closest to the interval center.
			if ( (lower<pow(2,cut-1)) && (left<=right) ) cut--;

			// add the new intervals to queue
			lbound.push(lower); ubound.push(pow(2,cut)); level.push(k+1);
			lbound.push(pow(2,cut)+1); ubound.push(upper); level.push(k+1);

			// add the new cut point to output
			bool c1 = ((a != (int)(pow(2,cut))) && ( (int)(pow(2,cut)) != b));
			bool c2 = ((all_levels) || ((k+1)==ncuts));
			//if ((all_levels) || ((k+1)==ncuts))
			if (c1 && c2)
				output.push_back(pow(2,cut));
		}

		// pop the next cut-level
		k = level.front(); level.pop();
	}
	if (reverse_interval) {
		for(unsigned int i=0; i<output.size(); i++)
			output[i] *= -1;
	}

	//output.push_back(a);
	std::sort(output.begin(), output.end());
	//output.push_back(b);
	return output;
}
*/


/*
std::vector<int> interval_cutpoints(int a, int b, unsigned int ncuts)
{
	bool negative_interval = false;
	if ((a<0) && (b<=0)) {
		int aux = -1*a;
		a = -1*b;
		b = aux;
		negative_interval = true;
	}

	std::queue<int> lbound, ubound, level;
	lbound.push(a); ubound.push(b);
	unsigned int k=0;	//cuts count
	std::vector<int> output;

	while (k<ncuts && !lbound.empty())
	{
		int lower = lbound.front(); lbound.pop();
		int upper = ubound.front(); ubound.pop();
		int cut = (int) pow(2,ilogb(upper));

		if ((lower <= cut) && (cut <= upper))
		{
			// add the cut-point to output
			output.push_back(cut);

			// enqueue new intervals
			if (lower <= (cut-1)) {
				lbound.push(lower);
				ubound.push(cut-1);
				level.push(k+1);
			}
			if (cut <= upper) {
				lbound.push(cut);
				ubound.push(upper);
				level.push(k+1);
			}
		}
		// pop the next level
		k = level.front(); level.pop();
	}
	if (negative_interval) {
		for(unsigned int i=0; i<output.size(); i++)
			output[i] *= -1;
	}

	std::sort(output.begin(), output.end());
	output.erase( std::unique( output.begin(), output.end() ), output.end() );
	return output;
}
*/



/*
// The simplest version.. it cuts everything

#include <iostream>
#include <cmath>
#include <vector>

std::vector<int> cutpoints (int a, int b)
{
    int vmin = 4;
    int vmax = 46;

    std::vector<int> cutpoints;
    for(unsigned int i = logb(vmin)+1; pow(2,i)<vmax; ++i) {
        std::cout << "i  : " << i << "\t2^i: " << pow(2,i) << std::endl;
        cutpoints.push_back(pow(2,i));
    }
    return(cutpoints);
}
// to build the intervals
    int lbound = vmin;
    int ubound = cutpoints[0];
    for(unsigned int i=0; i<=cutpoints.size(); ++i) {
        lbound = (i == 0) ? vmin : lbound;
        ubound = (i == cutpoints.size()) ? vmax : cutpoints[i]-1;
        std::cout << "b" << i << ": (" << lbound << "," << ubound << ")" << std::endl;
        lbound = ubound+1;
    }
*/

