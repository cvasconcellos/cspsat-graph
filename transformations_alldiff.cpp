/*
 * ALL_DIFFERENT
 *
 * decompose_alldifferent()
 * transform_different_to_disjunction()
 * encode_constraint_different_direct()
 * encode_constraint_different_log
 *
 */


#include <graph_chr_header.hpp>

#define N_STR std::string()

// ===========================================================================
// ALL_DIFFERENT to DIFF
// ===========================================================================

void GRAPH::decompose_alldifferent(unsigned int id)
{
	std::cout << "Graph::decompose_alldiff(" << id << ")" << std::endl;

	// Check transformation; if it does not exist, create it
	chr::Logical_var< unsigned int > l_id_trans;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::ALL_DIFFERENT | Flag::BINARY_DIFF), l_id_trans);
	if (l_id_trans.ground())
		return;

	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::ALL_DIFFERENT | Flag::BINARY_DIFF), N_STR, IntVarDomain()); num_transformations++;

	// Get relation's variables (l_relation_vars)
	chr::Logical_var_mutable< std::vector<unsigned int> > l_relation_vars;
	get_all_children_with_flags(id, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), l_relation_vars);
	get_all_children_with_flags(id, Flags(Flag::VARIABLE | Flag::INTEGER), l_relation_vars);
	get_all_children_with_flags(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), l_relation_vars);

	// For each value in the domain of the integer variable, create a SAT boolean variable
	auto it0 = (*l_relation_vars).begin();
	auto it_end = (*l_relation_vars).end();
	for ( ; it0 != it_end ; ++it0)
	{
		auto it1 = it0;
		++it1;
		for ( ; it1 != it_end ; ++it1)
		{
			unsigned int id_diff = _first_free_id++;
			vertice(id_diff, Flags(Flag::RELATION | Flag::BINARY_DIFF), N_STR, IntVarDomain()); num_relations++;
			edge(id_transformation, id_diff, -1);
			edge(id_diff, *it0, 0);
			edge(id_diff, *it1, 1);
		}
	}
}



// ===========================================================================
// DIFF to CNF (direct encoding)
// ===========================================================================


void GRAPH::transform_different_to_disjunction(unsigned int id)
{
    std::vector<unsigned long>::iterator it = std::find(encoding_sequence.begin(),
														encoding_sequence.end(),
														Flag::DIRECT_ENCODING);

    if (it != encoding_sequence.end()) {
        // Prioritize constraint encoding under DIRECT encoded variables
        encode_constraint_different_direct(id);

    } else {
    	// By now, there is no another one
    	encode_constraint_different_log(id);

    	// Hybrid encoding works just like log.. no modifications needed.
    }
}


/* 2019-12-03: Force log encoding
void GRAPH::transform_different_to_disjunction(unsigned int id)
{
   	encode_constraint_different_log(id);
}
*/


void GRAPH::encode_constraint_different_direct(unsigned int id)
{
	std::cout << "Graph::encode_constraint_different_direct(" << id << ")" << std::endl;

	// Check transformation; if it does not exist, create it
	chr::Logical_var< unsigned int > l_id_trans;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::BINARY_DIFF | Flag::BINARY_DISJUNCTION), l_id_trans);
	if (l_id_trans.ground())
        return;


	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::BINARY_DIFF | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	// Récupération des fils de type VARIABLE de la relation
	chr::Logical_var_mutable< std::vector< unsigned int > > l_relation_vars;
	get_all_children_with_flags(id, Flags(Flag::VARIABLE | Flag::INTEGER), l_relation_vars);
	get_all_children_with_flags(id, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), l_relation_vars);
	get_all_children_with_flags(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), l_relation_vars);
	assert((*l_relation_vars).size() == 2);

	chr::Logical_var< unsigned int > l_id_trans0_boolean, l_id_trans1_boolean;
	// Vérifie pour chaque variable de la relation
	// que la transformation de la variable entière en booléenne existe
	get_child_with_flags((*l_relation_vars)[0], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::DIRECT_ENCODING), l_id_trans0_boolean);
	if (!l_id_trans0_boolean.ground()) {
		encode_variable((*l_relation_vars)[0], Flag::DIRECT_ENCODING);
		get_child_with_flags((*l_relation_vars)[0], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::DIRECT_ENCODING), l_id_trans0_boolean);
	}
	assert(l_id_trans0_boolean.ground());
	get_child_with_flags((*l_relation_vars)[1], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::DIRECT_ENCODING), l_id_trans1_boolean);
	if (!l_id_trans1_boolean.ground())
	{
		encode_variable((*l_relation_vars)[1], Flag::DIRECT_ENCODING);
		get_child_with_flags((*l_relation_vars)[1], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::DIRECT_ENCODING), l_id_trans1_boolean);
	}
	assert(l_id_trans1_boolean.ground());

	// get X's boolean variables
	chr::Logical_var< Domain > l_domain_0;
	get_domain((*l_relation_vars)[0],l_domain_0);
	const IntVarDomain& domain_0 = dynamic_cast<const IntVarDomain&>(*l_domain_0);
	chr::Logical_var_mutable< std::vector< unsigned int > > l_boolean_vars_0;
	get_all_children_with_flags(*l_id_trans0_boolean, Flags(Flag::VARIABLE | Flag::BOOLEAN), l_boolean_vars_0);
	get_all_children_with_flags(*l_id_trans0_boolean, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), l_boolean_vars_0);

	// second integer var
	chr::Logical_var< Domain > l_domain_1;
	get_domain((*l_relation_vars)[1],l_domain_1);
	const IntVarDomain& domain_1 = dynamic_cast<const IntVarDomain&>(*l_domain_1);
	chr::Logical_var_mutable< std::vector< unsigned int > > l_boolean_vars_1;
	get_all_children_with_flags(*l_id_trans1_boolean, Flags(Flag::VARIABLE | Flag::BOOLEAN), l_boolean_vars_1);
	get_all_children_with_flags(*l_id_trans1_boolean, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), l_boolean_vars_1);

	unsigned int i = 0;
	for (unsigned int x : domain_0.domain)
	{
		unsigned int j = 0;
		for (unsigned int y : domain_1.domain)
		{
			if (x == y)
			{
				unsigned int id_disjunction = _first_free_id++;
				vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
				edge(id_transformation, id_disjunction, -1);

				chr::Logical_var< unsigned int > l_neg_0, l_neg_1;
				get_opposite_var((*l_boolean_vars_0)[i], l_neg_0);
				assert(l_neg_0.ground());
				get_opposite_var((*l_boolean_vars_1)[j], l_neg_1);
				assert(l_neg_1.ground());
				edge(id_disjunction, *l_neg_0, 0);
				edge(id_disjunction, *l_neg_1, 1);
			}
			++j;
		}
		++i;
	}
}



// ===========================================================================
// DIFF to CNF (log encoding)
// ===========================================================================

void GRAPH::encode_constraint_different_log(unsigned int id)
{
	std::cout << "Graph::encode_constraint_different_log(" << id << ")" << std::endl;

	// Check transformation; if it does not exist, create it
	chr::Logical_var< unsigned int > l_id_trans;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::BINARY_DIFF | Flag::BINARY_DISJUNCTION), l_id_trans);
	if (l_id_trans.ground())
        return;

	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::BINARY_DIFF | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;


	// check the relation has more than 3 children (3 vars + this transformation)
	chr::Logical_var_mutable< std::vector<unsigned int> > relation_children;
	get_all_children(id, relation_children);
	assert((*relation_children).size() > 2);

	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolean_vars_0;
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolean_vars_1;

	for (unsigned int i=0; i<2; i++)
	{
		chr::Logical_var<unsigned int> rvar;
		get_child_with_order(id, i, rvar);

		// get node information
		chr::Logical_var<Flags>       vflags;
		chr::Logical_var<std::string> vlabel;
		chr::Logical_var<Domain>      vdomain;
		get_vertice((*rvar), vflags, vlabel, vdomain);

		// based on variable type, retrieve boolean variables associated
		bool is_decision_var = (check(vflags, Flags(Flag::VARIABLE | Flag::INTEGER)) ||
								check(vflags, Flags(Flag::VARIABLE | Flag::PROP_INTEGER)));
		bool is_auxiliary_var = check(vflags, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY));
		assert(is_decision_var || is_auxiliary_var);

		if (is_decision_var)
		{
			chr::Logical_var<unsigned int> var_transformation;
			get_child_with_flags((*rvar), Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), var_transformation);
			if (!var_transformation.ground()) {
				//encode_variable((*rvar), Flag::LOG_ENCODING);
				//get_child_with_flags((*rvar), Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), var_transformation);
				encode_variable((*rvar), Flag::HYBRID_ENCODING);
				get_child_with_flags((*rvar), Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::HYBRID_ENCODING), var_transformation);
			}
			// 2019-12-18: Added tag AUXILIARY bcos LOG encoded vars is auxiliary in case of bridge
			if (i==0) get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_0);
			if (i==1) get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_1);
		}
		else
		{
			chr::Logical_var<unsigned int> var_transformation;
			get_child_with_flags((*rvar), Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), var_transformation);
			if (!var_transformation.ground()) {
				//encode_variable((*rvar), Flag::LOG_ENCODING);
				//get_child_with_flags((*rvar), Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), var_transformation);

				encode_variable((*rvar), Flag::HYBRID_ENCODING);
				get_child_with_flags((*rvar), Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::HYBRID_ENCODING), var_transformation);
			}
			if (i==0) get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_0);
			if (i==1) get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_1);
		}
	}

	// vectors containing XOR results (auxiliary vars)
	std::vector< unsigned int > xor_variables;

	if (((*l_boolean_vars_0).size() != 0) and ((*l_boolean_vars_1).size()!= 0))
	{
		unsigned int max_bitsize = std::max((*l_boolean_vars_0).size(),(*l_boolean_vars_1).size());
		unsigned int l_0 = (*l_boolean_vars_0)[0];
		unsigned int l_1 = (*l_boolean_vars_1)[0];

		// one XOR per bit
		for (unsigned int i = 0; i < max_bitsize; ++i)
		{
			chr::Logical_var< unsigned int > l_neg_0, l_neg_1;
			if (i<(*l_boolean_vars_0).size())
				l_0 = (*l_boolean_vars_0)[i];
			else
				l_0 = (*l_boolean_vars_0)[(*l_boolean_vars_0).size()-1];
			get_opposite_var(l_0, l_neg_0);
			assert(l_neg_0.ground());

			if (i<(*l_boolean_vars_1).size())
				l_1 = (*l_boolean_vars_1)[i];
			else
				l_1 = (*l_boolean_vars_1)[(*l_boolean_vars_1).size()-1];
			get_opposite_var(l_1, l_neg_1);
			assert(l_neg_1.ground());

			// Création de la variable auxiliaire
			unsigned int a1   = _first_free_id++;
			unsigned int n_a1 = _first_free_id++;
			vertice(a1, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0,1} )); num_variables++;
			vertice(n_a1, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0,1} )); num_variables++;
			vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
			edge(id_transformation, _first_free_id-1, -1);
			edge(id_transformation, _first_free_id-3, -1);
			edge(_first_free_id-1, a1, -1);
			edge(_first_free_id-1, n_a1, -1);
			xor_variables.push_back(a1);

			clause(id_transformation,l_neg_0,l_1,a1);
			clause(id_transformation,l_0,l_neg_1,a1);
			clause(id_transformation,l_0,l_1,n_a1);
			clause(id_transformation,l_neg_0,l_neg_1,n_a1);

		}

		// clause contenant tous les resultats des xor
		unsigned int id_disjunction = _first_free_id++;
		vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, id_disjunction, -1);
		for (unsigned int i = 0; i < max_bitsize; ++i) {
			edge(id_disjunction, xor_variables[i], -1);
		}
	}
	std::cout << std::endl ;
}

