/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

#include <graph_chr_header.hpp>

#define N_STR std::string()

void GRAPH::collect_graph_stats(std::string txt)
{
	// TODO: add graph "numbers" to a vector

	// for now, print values
	std::cout << "Nb nodes: " ;
    std::cout << num_variables << "\t" ;
    std::cout << num_relations << "\t" ;
    std::cout << num_transformations << "\t" ;
    std::cout << "(" <<  _first_free_id << ")";
    std::cout << std::endl ;
}

void GRAPH::apply_transformation(Flags flags, std::function<void (unsigned int)> transformation)
{
	for (auto& c : get_vertice_store()) {
		if (check(*c._p1, flags))
			transformation(*c._p0);
	}
}



// ========================================================
// apply_all_transformation
// ========================================================
void GRAPH::apply_all_transformation()
{
    assert(encoding_sequence.size()>0);

	//std::cout << "GRAPH::apply_transformations()" << std::endl;

	// Constraint decomposition
	apply_transformation(Flags(Flag::RELATION | Flag::LINEAR_EQ_SEQ), [this](unsigned int id) { this->decompose_linear_seq(id); });
	apply_transformation(Flags(Flag::RELATION | Flag::ALL_DIFFERENT), [this](unsigned int id) { this->decompose_alldifferent(id); });
	apply_transformation(Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL),[this](unsigned int id) { this->propagate_addition(id); });

    //collect_graph_stats(N_STR);

	// to SAT(CNF)
	apply_transformation(Flags(Flag::RELATION | Flag::BINARY_DIFF), [this](unsigned int id) { this->transform_different_to_disjunction(id);  });
	apply_transformation(Flags(Flag::RELATION | Flag::ADD_EQUAL),   [this](unsigned int id) { this->transform_add_equal_cnf(id);  }); //{ this->transform_add_equal_to_disjunction(id);  });

/*	apply_transformation(Flags(Flag::RELATION | Flag::SUB_EQUAL),         [this](unsigned int id) { this->transform_add_sub_equal_to_disjunction(id,false); });
	apply_transformation(Flags(Flag::RELATION | Flag::MULT_EQUAL),        [this](unsigned int id) { this->transform_mult_equal_to_disjunction(id); });
*/
	apply_transformation(Flags(Flag::RELATION | Flag::LESS_THAN),         [this](unsigned int id) { this->transform_less_than_to_disjunction(id);          });
/*	apply_transformation(Flags(Flag::RELATION | Flag::GREATER_THAN),      [this](unsigned int id) { this->transform_greater_than_to_disjunction(id);       });
	apply_transformation(Flags(Flag::RELATION | Flag::LESS_EQUAL_THAN),   [this](unsigned int id) { this->transform_less_equal_than_to_disjunction(id);    });
	apply_transformation(Flags(Flag::RELATION | Flag::GREATER_EQUAL_THAN),[this](unsigned int id) { this->transform_greater_equal_than_to_disjunction(id); });

	apply_transformation(Flags(Flag::RELATION | Flag::EQUAL),             [this](unsigned int id) { this->transform_equal_to_disjunction(id);     });
	apply_transformation(Flags(Flag::RELATION | Flag::ELEMENT),           [this](unsigned int id) { this->transform_element_to_disjunction(id);   });
	apply_transformation(Flags(Flag::RELATION | Flag::IF_THEN),           [this](unsigned int id) { this->transform_ifthen_to_disjunction(id);    });
	apply_transformation(Flags(Flag::RELATION | Flag::IF_THEN_OR),        [this](unsigned int id) { this->transform_ifthenor_to_disjunction(id);  });
	apply_transformation(Flags(Flag::RELATION | Flag::IF_THEN_NOT),       [this](unsigned int id) { this->transform_ifthennot_to_disjunction(id); });
	apply_transformation(Flags(Flag::RELATION | Flag::IFTHEN_GREATER),    [this](unsigned int id) { this->transform_IfThenGreater_toCNF(id);      });
	apply_transformation(Flags(Flag::RELATION | Flag::AT_LEAST),          [this](unsigned int id) { this->transform_atleast_to_disjunction(id);   });
	apply_transformation(Flags(Flag::RELATION | Flag::AT_MOST),           [this](unsigned int id) { this->transform_atmost_to_disjunction(id);    });
	apply_transformation(Flags(Flag::RELATION | Flag::AND),               [this](unsigned int id) { this->transform_and_to_disjunction(id);       });
	apply_transformation(Flags(Flag::RELATION | Flag::CARDINALITY),          [this](unsigned int id) { this->transform_cardinality_to_cnf(id,0);     });
	apply_transformation(Flags(Flag::RELATION | Flag::CARDINALITY_LEQ),      [this](unsigned int id) { this->transform_cardinality_to_cnf(id,1);     });
	apply_transformation(Flags(Flag::RELATION | Flag::INTERVAL_BRANCHING),   [this](unsigned int id) { this->transform_interval_branching_to_cnf(id); });
	//apply_transformation(Flags(Flag::RELATION | Flag::INTERVAL_BRANCHING),   [this](unsigned int id) { this->transform_interval_branching_to_cnf(id, Flag::INTERVAL_BRANCHING); });
	//apply_transformation(Flags(Flag::RELATION | Flag::INTERVAL_BRANCH_LOWER),[this](unsigned int id) { this->transform_interval_branching_to_cnf(id, Flag::INTERVAL_BRANCH_LOWER); });
	//apply_transformation(Flags(Flag::RELATION | Flag::INTERVAL_BRANCH_UPPER),[this](unsigned int id) { this->transform_interval_branching_to_cnf(id, Flag::INTERVAL_BRANCH_UPPER); });
	*/
}


void GRAPH::propagate_boolean_solution_to_integer_variables()
{
    std::cout << "propagate_boolean_solution_to_integer_variables()\t" << std::endl;

	std::vector<unsigned int> integer_variables_ids;
	for (auto& c : get_vertice_store()) {
		if (check(*c._p1, Flag::VARIABLE | Flag::INTEGER))
			integer_variables_ids.push_back(*c._p0);
	}

	for (auto& var_id : integer_variables_ids)
	{
		// Direct encoding ------------------------------------------

		chr::Logical_var<unsigned int> l_trans_direct_id;
		get_child_with_flags(var_id, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::DIRECT_ENCODING), l_trans_direct_id);
		if (l_trans_direct_id.ground())
		{
			bool is_instanced = true;
			// Get integer variable's domain
			chr::Logical_var<Domain> l_integer_variable_domain;
			get_domain(var_id,l_integer_variable_domain);
			const IntVarDomain& integer_variable_domain = *l_integer_variable_domain;
			int int_value = 0;

			// Retrieve boolean variables
			chr::Logical_var_mutable< std::vector< unsigned int > > l_boolean_vars;
			get_all_children_with_flags_ordered(*l_trans_direct_id, Flags(Flag::VARIABLE | Flag::BOOLEAN ), 0, l_boolean_vars);
			get_all_children_with_flags_ordered(*l_trans_direct_id, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY ), 0, l_boolean_vars);
			auto it = integer_variable_domain.domain.begin();
			for (auto& e : *l_boolean_vars)
			{
				chr::Logical_var< Domain > l_domain;
				get_domain(e,l_domain);
				const IntVarDomain& domain = *l_domain;

				assert(l_domain.ground());
				if (domain.size() > 1) {
					is_instanced = false;
					break;
				}

				if (domain.domain.front() == 1) {
					int_value = *it;
					break;
				}
				++it;
			}

			// update domain
			if (is_instanced)
				update_integer_domain(var_id, IntVarDomain({int_value}));
		}


		// Log encoding ---------------------------------------------

		chr::Logical_var<unsigned int> l_trans_log_id;
		get_child_with_flags(var_id, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_trans_log_id);
		if (l_trans_log_id.ground())
		{
			// I guess this is hardcoded with testing purposes (??)
//			if (*l_trans_log_id == 6498)
//			{
//				std::cout << "MATCH" << std::endl;
//			}
			bool is_instanced = true;
			// Récupération des variables booléennes et de leurs valeurs
			chr::Logical_var_mutable< std::vector<unsigned int> > l_boolean_vars;
			get_all_children_with_flags_ordered(*l_trans_log_id, Flags(Flag::VARIABLE | Flag::BOOLEAN ), 0, l_boolean_vars);
			get_all_children_with_flags_ordered(*l_trans_log_id, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY ), 0, l_boolean_vars);
			std::vector< unsigned int > boolean_values;
			for (auto& e : *l_boolean_vars)
			{
				chr::Logical_var< Domain > l_domain;
				get_domain(e,l_domain);
				const IntVarDomain& domain = *l_domain;

				assert(l_domain.ground());
				if (domain.size() > 1) {
					is_instanced = false;
					break;
				}
				boolean_values.push_back( domain.domain.front() );
			}

			if (is_instanced)
			{
				// Calculate integer (LOG ENCODING)
				int int_value = 0;
				unsigned int mask = 0;
				auto it = boolean_values.rbegin();
				auto it_end = boolean_values.rend();
				for ( ; it != it_end; ++it )
				{
					int_value <<= 1;
					int_value += *it;
					mask <<= 1;
					mask += 1;
				}
				if (boolean_values.back() == 1) // For negative values, use 2-complement
				{
					int_value -= 1;
					int_value = ~int_value;
					int_value &= mask;
					int_value *= -1;
				}

				// Bridge treatment ---------------------------------

				if (l_trans_direct_id.ground())
				{
					// Get du domaine de la variable entières
					chr::Logical_var<Domain> l_integer_variable_domain;
					get_domain(var_id,l_integer_variable_domain);
					const IntVarDomain& integer_variable_domain = *l_integer_variable_domain;

					assert(l_integer_variable_domain.ground());
					std::cout << "log: " << l_trans_log_id << " " << integer_variable_domain.domain.front() << "\tdir: " << l_trans_direct_id << " " << int_value << std::endl;

					assert(integer_variable_domain.domain.front() == int_value);
				} else {
					update_integer_domain(var_id, IntVarDomain({int_value}));
				}
			}
		}
	}
}
