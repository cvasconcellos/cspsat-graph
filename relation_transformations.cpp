/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */


#include <graph_chr_header.hpp>
//#include "utils.hh"

#define N_STR std::string()



// ===========================================================================
// CLAUSE ("disjunctions")
// ===========================================================================

unsigned int GRAPH::clause(unsigned int id_transformation)
{
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	return (_first_free_id-1);
}

unsigned int GRAPH::clause(unsigned int id_transformation, unsigned int x_id)
{
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	edge(_first_free_id-1, x_id, -1);
	return (_first_free_id-1);
}

unsigned int GRAPH::clause(unsigned int id_transformation, unsigned int x_id, unsigned int y_id)
{
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	edge(_first_free_id-1, x_id, -1);
	edge(_first_free_id-1, y_id, -1);
	return (_first_free_id-1);
}

unsigned int GRAPH::clause(unsigned int id_transformation, unsigned int x_id, unsigned int y_id, unsigned int z_id)
{
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	edge(_first_free_id-1, x_id, -1);
	edge(_first_free_id-1, y_id, -1);
	edge(_first_free_id-1, z_id, -1);
	return (_first_free_id-1);
}

unsigned int GRAPH::clause(unsigned int id_transformation, unsigned int w_id, unsigned int x_id, unsigned int y_id, unsigned int z_id)
{
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	edge(_first_free_id-1, w_id, -1);
	edge(_first_free_id-1, x_id, -1);
	edge(_first_free_id-1, y_id, -1);
	edge(_first_free_id-1, z_id, -1);
	return (_first_free_id-1);
}



// ===========================================================================
// LITERAL
// Links the two variables to represent that l=~nl
// l : Literal's ID
// nl: Negation of l's ID
// ===========================================================================

unsigned int GRAPH::literals(unsigned int id_transformation, unsigned int l, unsigned int nl)
{
	unsigned int rid = _first_free_id++;
	vertice(rid, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, l, -1);		// links the positive literal
	edge(id_transformation, rid, -1);	// links the relation
	edge(rid, l, -1);
	edge(rid, nl, -1);
	return rid;
}

unsigned int GRAPH::literals(unsigned int id_transformation, unsigned int l, unsigned int nl, unsigned int order)
{
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, l, order);					// links the positive literal
	edge(id_transformation, _first_free_id-1, -1);	// links the relation
	edge(_first_free_id-1, l, -1);
	edge(_first_free_id-1, nl, -1);
	return (_first_free_id-1);
}



// ===========================================================================
// FULL ADDER
// ===========================================================================

void GRAPH::full_add(unsigned int id_transformation, long x, long y, long c_in, long n_c_in, long z, long& c_out, long& n_c_out)
{
    //std::cout << "GRAPH::full_adder(" << x << "," << y << ")" << std::endl;

	// Retrieve negatives variables
	chr::Logical_var< unsigned int > l_neg_x, l_neg_y, l_neg_z;
	get_opposite_var(x, l_neg_x);
	assert(l_neg_x.ground());
	long n_x = *l_neg_x;

	get_opposite_var(y, l_neg_y);
	assert(l_neg_y.ground());
	long n_y = *l_neg_y;

	get_opposite_var(z, l_neg_z);
	assert(l_neg_z.ground());
	long n_z = *l_neg_z;

	// auxiliary vars (a1, a2, a3)
	unsigned int a1 = _first_free_id++;
	unsigned int n_a1 = _first_free_id++;
	vertice(a1, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0,1} )); num_variables++;
	vertice(n_a1, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0,1} )); num_variables++;
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	edge(id_transformation, _first_free_id-3, -1);
	edge(_first_free_id-1, a1, -1);
	edge(_first_free_id-1, n_a1, -1);

	unsigned int a2 = _first_free_id++;
	unsigned int n_a2 = _first_free_id++;
	vertice(a2, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0,1} )); num_variables++;
	vertice(n_a2, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0,1} )); num_variables++;
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	edge(id_transformation, _first_free_id-3, -1);
	edge(_first_free_id-1, a2, -1);
	edge(_first_free_id-1, n_a2, -1);

	unsigned int a3 = _first_free_id++;
	unsigned int n_a3 = _first_free_id++;
	vertice(a3, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0,1} )); num_variables++;
	vertice(n_a3, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0,1} )); num_variables++;
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	edge(id_transformation, _first_free_id-3, -1);
	edge(_first_free_id-1, a3, -1);
	edge(_first_free_id-1, n_a3, -1);

//	std::cout << "----> add     : ";
//	std::cout << " ("<< a1 << "," << n_a1 << ") " ;
//	std::cout << " ("<< a2 << "," << n_a2 << ") " ;
//	std::cout << " ("<< a3 << "," << n_a3 << ") " ;
//	std::cout << std::endl;

	// On traite d'abord le cas ou x et y sont -1 (le bit x et le bit y n'existent pas dans le problème)
	c_out   = -1;	// It starts at -1 au cas où on sortirait avant la création de c_out
	n_c_out = -1;	// It starts at -1 au cas où on sortirait avant la création de n_c_out

	// output variables
	c_out = _first_free_id++;
	n_c_out = _first_free_id++;
	vertice(c_out, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
	vertice(n_c_out, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	edge(id_transformation, _first_free_id-3, -1);
	edge(_first_free_id-1, c_out, -1);
	edge(_first_free_id-1, n_c_out, -1);

	// XOR_1
	clause(id_transformation, x, y, n_a1);
	clause(id_transformation, n_x, n_y, n_a1);
	clause(id_transformation, n_x, y, a1);
	clause(id_transformation, x, n_y, a1);

	// AND_1
	clause(id_transformation, n_x, n_y, a2);
	clause(id_transformation, x, n_a2);
	clause(id_transformation, y, n_a2);

	// XOR_2
	if (c_in != -1) {
		clause(id_transformation, a1, c_in, n_z);
		clause(id_transformation, n_a1, n_c_in, n_z);
		clause(id_transformation, n_a1, c_in, z);
		clause(id_transformation, a1, n_c_in, z);

	} else {
		clause(id_transformation, a1, n_z);
		clause(id_transformation, n_a1, z);
	}

	// AND_2
	if (c_in != -1) {
		clause(id_transformation, n_a1, n_c_in, a3);
		clause(id_transformation, a1, n_a3);
		clause(id_transformation, c_in, n_a3);
	}

	// OR_1
	if (c_in != -1) {
		clause(id_transformation, n_a3, c_out);
		clause(id_transformation, n_a2, c_out);
		clause(id_transformation, a2, a3, n_c_out);
	} else {
		clause(id_transformation, n_a2, c_out);
		clause(id_transformation, a2, n_c_out);
	}
}


// ==========================================================================
// FULL SUBSTRACTOR
/* ==========================================================================

void GRAPH::full_sub(unsigned int id_transformation, long x, long y, long c_in, long n_c_in, long z, long& c_out, long& n_c_out)
{

	// Récupération des variables négatives
	chr::Logical_var< unsigned int > l_neg_x, l_neg_y, l_neg_z;
	get_opposite_var(x, l_neg_x);
	assert(l_neg_x.ground());
	long n_x = *l_neg_x;

	get_opposite_var(y, l_neg_y);
	assert(l_neg_y.ground());
	long n_y = *l_neg_y;

	get_opposite_var(z, l_neg_z);
	assert(l_neg_z.ground());
	long n_z = *l_neg_z;

	// Création des variables auxiliaires
	unsigned int y_xor   = _first_free_id++;
	unsigned int n_y_xor = _first_free_id++;
	vertice(y_xor, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
	vertice(n_y_xor, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	edge(id_transformation, _first_free_id-3, -1);
	edge(_first_free_id-1, y_xor, -1);
	edge(_first_free_id-1, n_y_xor, -1);

	unsigned int a1   = _first_free_id++;
	unsigned int n_a1 = _first_free_id++;
	vertice(a1, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
	vertice(n_a1, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	edge(id_transformation, _first_free_id-3, -1);
	edge(_first_free_id-1, a1, -1);
	edge(_first_free_id-1, n_a1, -1);

	unsigned int a2   = _first_free_id++;
	unsigned int n_a2 = _first_free_id++;
	vertice(a2,   Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0,1} )); num_variables++;
	vertice(n_a2, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0,1} )); num_variables++;
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	edge(id_transformation, _first_free_id-3, -1);
	edge(_first_free_id-1, a2, -1);
	edge(_first_free_id-1, n_a2, -1);

	unsigned int a3 = _first_free_id++;
	unsigned int n_a3 = _first_free_id++;
	vertice(a3,   Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0,1} )); num_variables++;
	vertice(n_a3, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0,1} )); num_variables++;
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	edge(id_transformation, _first_free_id-3, -1);
	edge(_first_free_id-1, a3, -1);
	edge(_first_free_id-1, n_a3, -1);

	// On traite d'abord le cas ou x et y sont -1 (le bit x et le bit y n'existent pas dans le problème)
	c_out   = -1; // Initialisation à -1 au cas où on sortirait avant la création de c_out
	n_c_out = -1; // Initialisation à -1 au cas où on sortirait avant la création de n_c_out

	// Création des variables de sortie
	c_out   = _first_free_id++;
	n_c_out = _first_free_id++;
	vertice(c_out,   Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0,1} )); num_variables++;
	vertice(n_c_out, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0,1} )); num_variables++;
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	edge(id_transformation, _first_free_id-3, -1);
	edge(_first_free_id-1, c_out, -1);
	edge(_first_free_id-1, n_c_out, -1);

	// XOR_1
	clause(id_transformation, x, n_y, n_a1);
	clause(id_transformation, n_x, y, n_a1);
	clause(id_transformation, n_x, n_y, a1);
	clause(id_transformation, x, y, a1);

	// AND_1
	clause(id_transformation, n_x, y, a2);
	clause(id_transformation, x, n_a2);
	clause(id_transformation, n_y, n_a2);

	// XOR_2
	if (c_in != -1) {
		clause(id_transformation, a1, c_in, n_z);
		clause(id_transformation, n_a1, n_c_in, n_z);
		clause(id_transformation, n_a1, c_in, z);
		clause(id_transformation, a1, n_c_in, z);
	} else {
		// Pas de retenue en entrée
		clause(id_transformation, n_a1, n_z);
		clause(id_transformation, a1, z);
	}

	// AND_2
	if (c_in != -1) {
		clause(id_transformation, n_a1, n_c_in, a3);
		clause(id_transformation, a1, n_a3);
		clause(id_transformation, c_in, n_a3);
	} else {
		clause(id_transformation, n_a1);
		clause(id_transformation, n_a3);
	}

	// OR_1
	clause(id_transformation, n_a3, c_out);
	clause(id_transformation, n_a2, c_out);
	clause(id_transformation, a2, a3, n_c_out);
}
*/


// ===========================================================================
// FULL SUBSTRACTOR
// ===========================================================================

void GRAPH::full_sub_add(unsigned int id_transformation, long x, long y, long& previous_y_xor, long c_in, long n_c_in, long z, long& c_out, long& n_c_out, unsigned int id_operation_choice, unsigned int id_n_operation_choice)
{
	// Récupération des variables négatives
	chr::Logical_var< unsigned int > l_neg_x, l_neg_y, l_neg_z;
	get_opposite_var(x, l_neg_x);
	assert(l_neg_x.ground());
	long n_x = *l_neg_x;

	get_opposite_var(y, l_neg_y);
	assert(l_neg_y.ground());
	long n_y = *l_neg_y;

	get_opposite_var(z, l_neg_z);
	assert(l_neg_z.ground());
	long n_z = *l_neg_z;

	// auxiliary vars
	unsigned int y_xor   = _first_free_id++;
	unsigned int n_y_xor = _first_free_id++;
	vertice(y_xor, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
	vertice(n_y_xor, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain());num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	edge(id_transformation, _first_free_id-3, -1);
	edge(_first_free_id-1, y_xor, -1);
	edge(_first_free_id-1, n_y_xor, -1);
	previous_y_xor = y_xor;

	unsigned int a1   = _first_free_id++;
	unsigned int n_a1 = _first_free_id++;
	vertice(a1, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
	vertice(n_a1, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain());num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	edge(id_transformation, _first_free_id-3, -1);
	edge(_first_free_id-1, a1, -1);
	edge(_first_free_id-1, n_a1, -1);

	unsigned int a2   = _first_free_id++;
	unsigned int n_a2 = _first_free_id++;
	vertice(a2, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
	vertice(n_a2, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain());num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	edge(id_transformation, _first_free_id-3, -1);
	edge(_first_free_id-1, a2, -1);
	edge(_first_free_id-1, n_a2, -1);

	unsigned int a3   = _first_free_id++;
	unsigned int n_a3 = _first_free_id++;
	vertice(a3, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
	vertice(n_a3, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	edge(id_transformation, _first_free_id-3, -1);
	edge(_first_free_id-1, a3, -1);
	edge(_first_free_id-1, n_a3, -1);

	// On traite d'abord le cas ou x et y sont -1 (le bit x et le bit y n'existent pas dans le problème)
	c_out   = -1; // Initialisation à -1 au cas où on sortirait avant la création de c_out
	n_c_out = -1; // Initialisation à -1 au cas où on sortirait avant la création de n_c_out

	// Création des variables de sortie
	c_out   = _first_free_id++;
	n_c_out = _first_free_id++;
	vertice(c_out, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
	vertice(n_c_out, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
	vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, _first_free_id-1, -1);
	edge(id_transformation, _first_free_id-3, -1);
	edge(_first_free_id-1, c_out, -1);
	edge(_first_free_id-1, n_c_out, -1);

	// Clauses sur (y XOR id_operation_choice) <=> y_xor
	clause(id_transformation, n_y, id_operation_choice,  y_xor);
	clause(id_transformation, y,   id_n_operation_choice,y_xor);
	clause(id_transformation, y,   id_operation_choice,  n_y_xor);
	clause(id_transformation, n_y, id_n_operation_choice,n_y_xor);

	// XOR_1
	clause(id_transformation, x,   y_xor, n_a1);
	clause(id_transformation, n_x, n_y_xor, n_a1);
	clause(id_transformation, n_x, y_xor, a1);
	clause(id_transformation, x,   n_y_xor, a1);

	// AND_1
	clause(id_transformation, n_x, n_y_xor, a2);
	clause(id_transformation, x, n_a2);
	clause(id_transformation, y_xor, n_a2);

	// XOR_2
	if (c_in != -1) {
		clause(id_transformation, a1, c_in, n_z);
		clause(id_transformation, n_a1, n_c_in, n_z);
		clause(id_transformation, n_a1, c_in, z);
		clause(id_transformation, a1, n_c_in, z);
	} else {
		// Pas de retenue en entrée
		clause(id_transformation, a1, n_z);
		clause(id_transformation, n_a1, z);
	}

	// AND_2
	if (c_in != -1) {
		clause(id_transformation, n_a1, n_c_in, a3);
		clause(id_transformation, a1, n_a3);
		clause(id_transformation, c_in, n_a3);
	}

	// OR_1
	if (c_in != -1) {
		clause(id_transformation, n_a3, c_out);
		clause(id_transformation, n_a2, c_out);
		clause(id_transformation, a2, a3, n_c_out);
	} else {
		clause(id_transformation, n_a2, c_out);
		clause(id_transformation, a2, n_c_out);
	}
}



// ===========================================================================
// ADD_EQUAL to CNF
//
// Only available in LOG encoding
//
// ===========================================================================

void GRAPH::transform_add_equal_to_disjunction(unsigned int id)
{
    std::cout << std::endl << "Graph::transform_add_equal_to_disjunction(" << id << ")" << std::endl;

    // does transformation already exists?
	chr::Logical_var<unsigned int> l_id_trans;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::ADD_EQUAL | Flag::BINARY_DISJUNCTION), l_id_trans);
	if (l_id_trans.ground())
        return;

	// check if relation has 3 children
	chr::Logical_var_mutable< std::vector<unsigned int> > relation_children;
	get_all_children(id, relation_children);
	assert((*relation_children).size() == 3);

	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::ADD_EQUAL | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	std::vector<unsigned int> l_boolvars_x;
	std::vector<unsigned int> l_boolvars_y;
	std::vector<unsigned int> l_boolvars_z;

	for (unsigned int i=0; i<3; i++)
	{
		chr::Logical_var<unsigned int> rvar;
		get_child_with_order(id, i, rvar);

		// variable information
		chr::Logical_var<Flags>       vflags;
		chr::Logical_var<std::string> vlabel;
		chr::Logical_var<Domain>      vdomain;
		get_vertice((*rvar), vflags, vlabel, vdomain);

		// get transformation associated to variable encoding
		chr::Logical_var<unsigned int> var_transformation;
		get_child_with_flags((*rvar), Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), var_transformation);
		if (!var_transformation.ground()) {
			encode_variable((*rvar), Flag::LOG_ENCODING); // encode_variable((*rvar));
			get_child_with_flags((*rvar), Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), var_transformation);
		}

		// get boolean variables
		chr::Logical_var_mutable< std::vector<unsigned int> > boolvars;

		get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, boolvars);
		if ((*boolvars).size()==0) get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, boolvars);
		assert((*boolvars).size()>0);

		for(unsigned int j=0; j<(*boolvars).size(); ++j) {
			if (i==0) l_boolvars_x.push_back( (*boolvars)[j] );
			if (i==1) l_boolvars_y.push_back( (*boolvars)[j] );
			if (i==2) l_boolvars_z.push_back( (*boolvars)[j] );
		}
	}

    long carry, n_carry, previous_x, previous_y, previous_z;
	full_add(id_transformation, l_boolvars_x[0], l_boolvars_y[0], -1, -1, l_boolvars_z[0], carry, n_carry);
	previous_x = l_boolvars_x[0];
	previous_y = l_boolvars_y[0];
	previous_z = l_boolvars_z[0];

	unsigned int max_domain_size = std::max(l_boolvars_x.size(), std::max(l_boolvars_y.size(), l_boolvars_z.size()));
	for (unsigned int i=1; i<max_domain_size; ++i)
	{
		long x = (i < l_boolvars_x.size()) ? l_boolvars_x[i] : previous_x;
		long y = (i < l_boolvars_y.size()) ? l_boolvars_y[i] : previous_y;
		long z = (i < l_boolvars_z.size()) ? l_boolvars_z[i] : previous_z;
		full_add(id_transformation, x, y, carry, n_carry, z, carry, n_carry);
		previous_x = x;
		previous_y = y;
		previous_z = z;
	}

	// If (x,y) signs are different, the final carry is not relevant
	// If (x,y) signs are equal, then final carry must have the same sign as z-sign
	// (previous_x == previous_y) -> (carry == previous_z)

	// retrieve negative variables
	chr::Logical_var<unsigned int> l_neg_x, l_neg_y, l_neg_z;
	get_opposite_var(previous_x, l_neg_x);
	assert(l_neg_x.ground());
	get_opposite_var(previous_y, l_neg_y);
	assert(l_neg_y.ground());
	get_opposite_var(previous_z, l_neg_z);
	assert(l_neg_z.ground());

	clause(id_transformation,previous_x,previous_y,carry,*l_neg_z);
	clause(id_transformation,previous_x,previous_y,n_carry,previous_z);
	clause(id_transformation,*l_neg_x,*l_neg_y,carry,*l_neg_z);
	clause(id_transformation,*l_neg_x,*l_neg_y,n_carry,previous_z);

}



// ===========================================================================
// SUB EQUAL to DISJUNCTION
/* ===========================================================================

void GRAPH::transform_sub_equal_to_disjunction(unsigned int id)
{
    std::cout << std::endl << "GRAPH::transform_sub_equal_to_disjunction(" << id << ")" << std::endl ;

    // Check transformation; if it does not exist, create it
	chr::Logical_var< unsigned int > l_id_trans;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::SUB_EQUAL | Flag::BINARY_DISJUNCTION), l_id_trans);
	if (l_id_trans.ground())
		return;

	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::SUB_EQUAL | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	// Get relation's variables (l_relation_vars)
	chr::Logical_var_mutable< std::vector<unsigned int> > l_relation_vars;
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER), 0, l_relation_vars);
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), 0, l_relation_vars);
	assert((*l_relation_vars).size() == 3);

	// Vérifie pour chaque variable de la relation
	// que la transformation de la variable entière en booléenne existe
	std::vector< unsigned int > transformations_for_integer_variables;
	for (unsigned int i=0; i < (*l_relation_vars).size(); ++i)
	{
		chr::Logical_var< unsigned int > l_id_trans_boolean;
		get_child_with_flags((*l_relation_vars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		if (!l_id_trans_boolean.ground())
		{
			build_log_encoding_from((*l_relation_vars)[i]);
			get_child_with_flags((*l_relation_vars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		}
		assert(l_id_trans_boolean.ground());
		transformations_for_integer_variables.push_back(*l_id_trans_boolean);
	}

	// Récupération des variables booléennes et du domaine de la première variable entière
	chr::Logical_var_mutable< std::vector< unsigned int > > l_boolean_vars_x;
	get_all_children_with_flags_ordered(transformations_for_integer_variables[0], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolean_vars_x);
	get_all_children_with_flags_ordered(transformations_for_integer_variables[0], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_x);

	// Récupération des variables booléennes et du domaine de la deuxième variable entière
	chr::Logical_var_mutable< std::vector< unsigned int > > l_boolean_vars_y;
	get_all_children_with_flags_ordered(transformations_for_integer_variables[1], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolean_vars_y);
	get_all_children_with_flags_ordered(transformations_for_integer_variables[1], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_y);

	// Récupération des variables booléennes et du domaine de la troisième variable entière
	chr::Logical_var_mutable< std::vector< unsigned int > > l_boolean_vars_z;
	get_all_children_with_flags_ordered(transformations_for_integer_variables[2], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolean_vars_z);
	get_all_children_with_flags_ordered(transformations_for_integer_variables[2], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_z);

	long carry, n_carry, previous_x, previous_y, previous_z;
	assert((*l_boolean_vars_x).size() > 0);
	assert((*l_boolean_vars_y).size() > 0);
	assert((*l_boolean_vars_z).size() > 0);
	full_sub(id_transformation,(*l_boolean_vars_x)[0],(*l_boolean_vars_y)[0],-1,-1,(*l_boolean_vars_z)[0],carry,n_carry);
	previous_x = (*l_boolean_vars_x)[0];
	previous_y = (*l_boolean_vars_y)[0];
	previous_z = (*l_boolean_vars_z)[0];

	unsigned int max_domain_size = std::max((*l_boolean_vars_x).size(), std::max((*l_boolean_vars_y).size(), (*l_boolean_vars_z).size()));
	for (unsigned int i = 0; i < max_domain_size; ++i)
	{
		long x = (i < (*l_boolean_vars_x).size()) ? (*l_boolean_vars_x)[i] : previous_x;
		long y = (i < (*l_boolean_vars_y).size()) ? (*l_boolean_vars_y)[i] : previous_y;
		long z = (i < (*l_boolean_vars_z).size()) ? (*l_boolean_vars_z)[i] : previous_z;
		full_sub(id_transformation,x,y,carry,n_carry,z,carry,n_carry);
		previous_x = x;
		previous_y = y;
		previous_z = z;
	}
	// Si les deux bits de signes finaux de	x et y sont différents, alors la carry finale n'a pas d'importance
	// Si les deux bits de signes finaux de	x et y sont identiques, alors la carry finale doit être identique au bit de signe de z
	// (previous_x == neg previous_y) -> (carry == previous_z)
	// On prend neg previous_y car y est inversé dans le full_sub
	// Récupération des variables négatives
	chr::Logical_var<unsigned int> l_neg_x, l_neg_y, l_neg_z;
	get_opposite_var(previous_x, l_neg_x);
	assert(l_neg_x.ground());
	get_opposite_var(previous_y, l_neg_y);
	assert(l_neg_y.ground());
	get_opposite_var(previous_z, l_neg_z);
	assert(l_neg_z.ground());

	clause(id_transformation,previous_x,*l_neg_y,carry,*l_neg_z);
	clause(id_transformation,previous_x,*l_neg_y,n_carry,previous_z);
	clause(id_transformation,*l_neg_x,previous_y,carry,*l_neg_z);
	clause(id_transformation,*l_neg_x,previous_y,n_carry,previous_z);
}
*/


// =========================================================
// ADD_SUB_EQUAL to CNF
// =========================================================

void GRAPH::transform_add_sub_equal_to_disjunction(unsigned int id, bool add)
{
    std::cout << std::endl << "GRAPH::transform_add_sub_equal_to_disjunction(" << id << ")" << std::endl;

    // Check transformation; if it does not exist, create it
	chr::Logical_var< unsigned int > l_id_trans;
	if (add)
		get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::ADD_EQUAL | Flag::BINARY_DISJUNCTION), l_id_trans);
	else
		get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::SUB_EQUAL | Flag::BINARY_DISJUNCTION), l_id_trans);
	if (l_id_trans.ground())
		return;

	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	if (add) {
		vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::ADD_SUB_EQUAL | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;
	} else {
		vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::ADD_SUB_EQUAL | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;
	}

	// Get relation's variables (l_relation_vars)
	chr::Logical_var_mutable< std::vector< unsigned int > > l_relation_vars;
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER), 0, l_relation_vars);
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), 0, l_relation_vars);
	assert((*l_relation_vars).size() == 3);

	// Vérifie pour chaque variable de la relation
	// que la transformation de la variable entière en booléenne existe
	std::vector< unsigned int > transformations_for_integer_variables;
	for (unsigned int i=0; i < (*l_relation_vars).size(); ++i)
	{
		chr::Logical_var< unsigned int > l_id_trans_boolean;
		get_child_with_flags((*l_relation_vars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		if (!l_id_trans_boolean.ground())
		{
			build_log_encoding_from((*l_relation_vars)[i], false);
			get_child_with_flags((*l_relation_vars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		}
		assert(l_id_trans_boolean.ground());
		transformations_for_integer_variables.push_back(*l_id_trans_boolean);
	}

	//std::cout << "transformations_for_integer_variables : " << transformations_for_integer_variables.capacity() << std::endl;

	// Récupération des variables booléennes et du domaine de la première variable entière
	chr::Logical_var_mutable< std::vector< unsigned int > > l_boolean_vars_x;
	get_all_children_with_flags_ordered(transformations_for_integer_variables[0], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolean_vars_x);
	get_all_children_with_flags_ordered(transformations_for_integer_variables[0], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_x);

	// Récupération des variables booléennes et du domaine de la deuxième variable entière
	chr::Logical_var_mutable< std::vector< unsigned int > > l_boolean_vars_y;
	get_all_children_with_flags_ordered(transformations_for_integer_variables[1], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolean_vars_y);
	get_all_children_with_flags_ordered(transformations_for_integer_variables[1], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_y);

	// Récupération des variables booléennes et du domaine de la troisième variable entière
	chr::Logical_var_mutable< std::vector< unsigned int > > l_boolean_vars_z;
	get_all_children_with_flags_ordered(transformations_for_integer_variables[2], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolean_vars_z);
	get_all_children_with_flags_ordered(transformations_for_integer_variables[2], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_z);

	// Variable de choix entre additionneur et soustracteur
	unsigned int id_operation_choice          = _first_free_id++;
	unsigned int id_n_operation_choice        = _first_free_id++;
	unsigned int id_operation_choice_node_not = _first_free_id++;
	vertice(id_operation_choice, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( { 0, 1 } )); num_variables++;
	edge(id_transformation, id_operation_choice, -1);
	vertice(id_n_operation_choice, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( { 0, 1 } )); num_variables++;
	vertice(id_operation_choice_node_not, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, id_operation_choice_node_not, -1);
	edge(id_operation_choice_node_not, id_operation_choice, -1);
	edge(id_operation_choice_node_not, id_n_operation_choice, -1);

	if (add) {
		clause(id_transformation, id_n_operation_choice);	// force adder
	} else {
		clause(id_transformation, id_operation_choice);		// force subtractor
	}

	long carry = id_operation_choice, n_carry = id_n_operation_choice;
	long previous_x = -1, previous_y = -1, previous_z = -1, previous_y_xor; // le -1 ne sert jamais car les vecteurs ont une taille > 0
	assert((*l_boolean_vars_x).size() > 0);
	assert((*l_boolean_vars_y).size() > 0);
	assert((*l_boolean_vars_z).size() > 0);

	unsigned int max_domain_size = std::max((*l_boolean_vars_x).size(), std::max((*l_boolean_vars_y).size(), (*l_boolean_vars_z).size()));
	for (unsigned int i=0; i<max_domain_size; ++i)
	{
		long x = (i < (*l_boolean_vars_x).size())?(*l_boolean_vars_x)[i]:previous_x;
		long y = (i < (*l_boolean_vars_y).size())?(*l_boolean_vars_y)[i]:previous_y;
		long z = (i < (*l_boolean_vars_z).size())?(*l_boolean_vars_z)[i]:previous_z;
		full_sub_add(id_transformation,x,y,previous_y_xor,carry,n_carry,z,carry,n_carry,id_operation_choice,id_n_operation_choice);
		previous_x = x;
		previous_y = y;
		previous_z = z;
	}
	// Si les deux bits de signes finaux de	x et y sont différents, alors la carry finale n'a pas d'importance
	// Si les deux bits de signes finaux de	x et y sont identiques, alors la carry (modulo le Xor à faire) finale doit être identique au bit de signe de z
	// (previous_x == previous_y_xor) -> (carry == previous_z)
	// Récupération des variables négatives
	chr::Logical_var< unsigned int > l_neg_x, l_neg_y_xor, l_neg_z;
	get_opposite_var(previous_x, l_neg_x);
	assert(l_neg_x.ground());
	get_opposite_var(previous_y_xor, l_neg_y_xor);
	assert(l_neg_y_xor.ground());
	get_opposite_var(previous_z, l_neg_z);
	assert(l_neg_z.ground());

	clause(id_transformation,previous_x,previous_y_xor,carry,*l_neg_z);
	clause(id_transformation,previous_x,previous_y_xor,n_carry,previous_z);
	clause(id_transformation,*l_neg_x,*l_neg_y_xor,carry,*l_neg_z);
	clause(id_transformation,*l_neg_x,*l_neg_y_xor,n_carry,previous_z);
}



// ===========================================================================
// MULT_EQUAL to DISJUNCTION
/* ===========================================================================

void GRAPH::transform_mult_equal_to_disjunction(unsigned int id)
{
    std::cerr << std::endl << "GRAPH::transform_mult_equal_to_disjunction()" << std::endl;

    // Check transformation; if it does not exist, create it
	chr::Logical_var< unsigned int > l_id_trans;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::MULT_EQUAL | Flag:: BINARY_DISJUNCTION), l_id_trans);
	if (l_id_trans.ground())
		return;

	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::MULT_EQUAL | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	// Get relation's variables (l_relation_vars)
	chr::Logical_var_mutable< std::vector<unsigned int> > l_relation_vars;
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER), 0, l_relation_vars);
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), 0, l_relation_vars);
	assert((*l_relation_vars).size() == 3);

	// Vérifie pour chaque variable de la relation
	// que la transformation de la variable entière en booléenne existe
	std::vector< unsigned int > transformations_for_integer_variables;
	for (unsigned int i=0; i < (*l_relation_vars).size(); ++i)
	{
		chr::Logical_var< unsigned int > l_id_trans_boolean;
		get_child_with_flags((*l_relation_vars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		if (!l_id_trans_boolean.ground())
		{
			build_log_encoding_from((*l_relation_vars)[i]);
			get_child_with_flags((*l_relation_vars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		}
		assert(l_id_trans_boolean.ground());
		transformations_for_integer_variables.push_back(*l_id_trans_boolean);
	}

    //std::cout << "transformations_for_integer_variables : " << transformations_for_integer_variables.capacity() << "\t";

	// Récupération des variables booléennes et du domaine de la première variable entière
	chr::Logical_var_mutable< std::vector< unsigned int > > l_boolean_vars_x;
	get_all_children_with_flags_ordered(transformations_for_integer_variables[0], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolean_vars_x);
	get_all_children_with_flags_ordered(transformations_for_integer_variables[0], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_x);

	// Récupération des variables booléennes et du domaine de la deuxième variable entière
	chr::Logical_var_mutable< std::vector< unsigned int > > l_boolean_vars_y;
	get_all_children_with_flags_ordered(transformations_for_integer_variables[1], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolean_vars_y);
	get_all_children_with_flags_ordered(transformations_for_integer_variables[1], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_y);

	// Récupération des variables booléennes et du domaine de la troisième variable entière
	chr::Logical_var_mutable< std::vector< unsigned int > > l_boolean_vars_z;
	get_all_children_with_flags_ordered(transformations_for_integer_variables[2], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolean_vars_z);
	get_all_children_with_flags_ordered(transformations_for_integer_variables[2], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_z);
	std::vector< unsigned int > previous_aux_boolean;

	assert(_first_free_id > 0);
	// Nombre de bits maximum pour tenir le résultat, modulo la troncature de la multiplication
	unsigned int nb_bits_for_result = (*l_boolean_vars_x).size() + (*l_boolean_vars_y).size();
	unsigned int max_step           = nb_bits_for_result;   // Nombre d'additions intermédiaires <=> nombre de step
	unsigned int id_constante_one   = 0;                    // 0 veut dire que rien n'a été encore fait
	unsigned int id_constante_zero  = 0;                    // 0 veut dire que rien n'a été encore fait

	for (unsigned int step = 0; step < max_step ; ++step)
	{
		unsigned id_y = (*l_boolean_vars_y)[ std::min(step, static_cast<unsigned int>((*l_boolean_vars_y).size()) - 1) ];

		// Création du nombre résultat de la multiplication de x par un bit de y
		// On limite le nombre produit au maximum au nombre de bits de z
		std::vector< unsigned int > current_aux_boolean;
		for (unsigned int i = 0; i < std::min( static_cast< unsigned int >((*l_boolean_vars_x).size()) + step, nb_bits_for_result ) ; ++i)
		{
			if (i < step)
			{
				// Les premiers bits sont à 0
				if (id_constante_zero == 0)
				{
					id_constante_one  = _first_free_id++;
					id_constante_zero = _first_free_id++;
					unsigned int id_node_not = _first_free_id++;
					vertice(id_constante_one, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( { 1 } )); num_variables++;
					edge(id_transformation, id_constante_one, i);
					vertice(id_constante_zero, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( { 0 } )); num_variables++;
					vertice(id_node_not, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
					edge(id_transformation, id_node_not, -1);
					edge(id_node_not, id_constante_one, -1);
					edge(id_node_not, id_constante_zero, -1);

					// Fix la constante à 0
					clause(id_transformation,id_constante_zero);
				}
				current_aux_boolean.push_back(id_constante_one);
			} else
			{
				// Multiplication par le bit courant de y
				unsigned int id_pos_var  = _first_free_id++;
				unsigned int id_neg_var  = _first_free_id++;
				unsigned int id_node_not = _first_free_id++;
				vertice(id_pos_var, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( { 0, 1 } )); num_variables++;
				edge(id_transformation, id_pos_var, i);
				current_aux_boolean.push_back(id_pos_var);
				vertice(id_neg_var, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( { 0, 1 } )); num_variables++;
				vertice(id_node_not, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
				edge(id_transformation, id_node_not, -1);
				edge(id_node_not, id_pos_var, -1);
				edge(id_node_not, id_neg_var, -1);

				unsigned id_x = (*l_boolean_vars_x)[i-step];
				chr::Logical_var< unsigned int > l_neg_x, l_neg_y;
				get_opposite_var(id_x, l_neg_x);
				assert(l_neg_x.ground());
				get_opposite_var(id_y, l_neg_y);
				assert(l_neg_y.ground());

				clause(id_transformation,*l_neg_y,*l_neg_x,id_pos_var);
				clause(id_transformation,*l_neg_y,id_x,id_neg_var);
				clause(id_transformation,id_y,id_neg_var);
			}
		}

		if (step == 0) {
			// Premier cas, on ne fait pas l'addition, on récupère juste le chiffre binaire intermédiaire
			previous_aux_boolean.swap(current_aux_boolean);
		} else {
			std::vector< unsigned int > futur_aux_boolean;
			if (step == (max_step - 1))
			{
				// Pour le dernier tour, le futur nombre est égale à z
				for (auto e : *l_boolean_vars_z)
					futur_aux_boolean.push_back( e );
			} else {
				// Pour chaque tour intermédiaire on génère un nombre auxiliaire pour stocker
				// le résultat intermédiaire (modulo la troncature du faite de la multiplication)
				for (unsigned int i = 0; i < std::min( static_cast<unsigned int>(current_aux_boolean.size()) + 1, nb_bits_for_result ) ; ++i)
				{
					unsigned int id_pos_var = _first_free_id++;
					unsigned int id_neg_var = _first_free_id++;
					unsigned int id_node_not = _first_free_id++;
					vertice(id_pos_var, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( { 0, 1 } )); num_variables++;
					edge(id_transformation, id_pos_var, i);
					futur_aux_boolean.push_back(id_pos_var);
					vertice(id_neg_var, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( { 0, 1 } )); num_variables++;
					vertice(id_node_not, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
					edge(id_transformation, id_node_not, -1);
					edge(id_node_not, id_pos_var, -1);
					edge(id_node_not, id_neg_var, -1);
				}
			}

			// FULL ADD RECOPIE DEPUIS LE ADD_EQUAL ------------
			long carry, n_carry, previous_x, previous_y, previous_z;
			full_add(id_transformation,previous_aux_boolean[0],current_aux_boolean[0],-1,-1,futur_aux_boolean[0],carry,n_carry);
			previous_x = previous_aux_boolean[0];
			previous_y = current_aux_boolean[0];
			previous_z = futur_aux_boolean[0];

			unsigned int max_domain_size = std::max(previous_aux_boolean.size(), std::max(current_aux_boolean.size(), futur_aux_boolean.size()));
			for (unsigned int i = 1; i < max_domain_size; ++i)
			{
				long x = (i < previous_aux_boolean.size())?previous_aux_boolean[i]:previous_x;
				long y = (i < current_aux_boolean.size())?current_aux_boolean[i]:previous_y;
				long z = (i < futur_aux_boolean.size())?futur_aux_boolean[i]:previous_z;
				full_add(id_transformation,x,y,carry,n_carry,z,carry,n_carry);
				previous_x = x;
				previous_y = y;
				previous_z = z;
			}
			// On oublie totalement les bits qui dépassent le nombre de bits de z
			// On tronc le chiffre résultat
			// FIN DU FULL ADD ------------------------------

			previous_aux_boolean.swap(futur_aux_boolean);
		}
	}
}
*/


// ==========================================================================
// LESS GREATER EQUAL to CNF
// id   : Relations's ID
// flag : LESS_THAN, GREATER_THAN, EQUAL
//
// version sent on Jan 22, 2019 (flardeux)
// ==========================================================================

void GRAPH::less_greater_equal_to_disjunction(unsigned int id, unsigned long int flag)
{
	std::cout << std::endl << "GRAPH::less_greater_equal_to_disjunction(" << id << ")" << std::endl;

    // Check if transformation exists
  	chr::Logical_var< unsigned int > l_id_trans;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | flag | Flag::BINARY_DISJUNCTION), l_id_trans);
	if (l_id_trans.ground()) {
		std::cout << "transformation already exists" << std::endl;
		return;
	}

	// check the relation has 2 children/vars
	chr::Logical_var_mutable< std::vector<unsigned int> > relation_children;
	get_all_children(id, relation_children);
	assert((*relation_children).size() == 2);

	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | flag | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolean_vars_x;
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolean_vars_y;


	// retrieve variables -------------------------------------------

	for (unsigned int i=0; i<2; i++)
	{
		// CSP variable + info
		chr::Logical_var<unsigned int> rvar;
		get_child_with_order(id, i, rvar);

		chr::Logical_var<Flags> vflags;
		chr::Logical_var<std::string> vlabel;
		chr::Logical_var<Domain> vdomain;
		get_vertice((*rvar), vflags, vlabel, vdomain);

		// encoding transformation + boolean variables associated
		bool is_decision_var = check(vflags, Flags(Flag::VARIABLE | Flag::INTEGER)) ||
							   check(vflags, Flags(Flag::VARIABLE | Flag::PROP_INTEGER)) ;

		if (is_decision_var)
		{
			chr::Logical_var<unsigned int> var_transformation;
			get_child_with_flags((*rvar), Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), var_transformation);
			if (!var_transformation.ground()) {
				encode_variable((*rvar), Flag::LOG_ENCODING);
				get_child_with_flags((*rvar), Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), var_transformation);
			}
			// 2019-12-18: Added tag AUXILIARY bcos LOG encoded vars is auxiliary in case of bridge
			if (i==0) get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_x);
			if (i==1) get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_y);

		}
		else
		{
			chr::Logical_var<unsigned int> var_transformation;
			get_child_with_flags((*rvar), Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), var_transformation);
			if (!var_transformation.ground()) {
				encode_variable((*rvar), Flag::LOG_ENCODING);
				get_child_with_flags((*rvar), Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), var_transformation);
			}
			if (i==0) get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_x);
			if (i==1) get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_y);
		}
	}

	// vecteurs contenant les variables auxiliare (résultat du or)
	std::vector< unsigned int > or_variables;
	//
	if (((*l_boolean_vars_x).size() != 0) and ((*l_boolean_vars_y).size()!= 0))
	{
        unsigned int max_bit_size = std::max((*l_boolean_vars_x).size(),(*l_boolean_vars_y).size());
        unsigned int l_x = (*l_boolean_vars_x)[0];
        unsigned int l_y = (*l_boolean_vars_y)[0];

        std::vector<unsigned int> less_variables;
        std::vector<unsigned int> neg_less_variables;
        std::vector<unsigned int> greater_variables;
        std::vector<unsigned int> neg_greater_variables;
        std::vector<unsigned int> eq_variables;
        std::vector<unsigned int> neg_eq_variables;

        for (unsigned int i=0; i<max_bit_size; ++i)
        {
            chr::Logical_var< unsigned int > l_neg_x, l_neg_y;
            if (i<(*l_boolean_vars_x).size())
                l_x = (*l_boolean_vars_x)[i];
            else
                l_x = (*l_boolean_vars_x)[(*l_boolean_vars_x).size()-1];
            get_opposite_var(l_x, l_neg_x);
            assert(l_neg_x.ground());

            if (i<(*l_boolean_vars_y).size())
                l_y = (*l_boolean_vars_y)[i];
            else
                l_y = (*l_boolean_vars_y)[(*l_boolean_vars_y).size()-1];
            get_opposite_var(l_y, l_neg_y);
            assert(l_neg_y.ground());

			// auxiliary variable for LESS
            if ((flag == Flag::LESS_THAN) or (flag == Flag::LESS_EQUAL_THAN))
            {
                unsigned int less = _first_free_id++;
                unsigned int n_less = _first_free_id++;
                vertice(less, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0,1} )); num_variables++;
                vertice(n_less, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0,1} )); num_variables++;
                vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
                edge(id_transformation, _first_free_id-1, -1);
                edge(id_transformation, _first_free_id-3, -1);
                edge(_first_free_id-1, less, -1);
                edge(_first_free_id-1, n_less, -1);
                less_variables.push_back(less);
                neg_less_variables.push_back(n_less);

                // sign bit: + est codé en 0 et - en 1 donc on doit faire greater
                if (i == max_bit_size-1) {
                    clause(id_transformation,l_neg_x,l_y,less);	// x=1 and y=0 -> greater=1
                    clause(id_transformation,n_less,l_x);		//greater=1 -> x=1
                    clause(id_transformation,n_less,l_neg_y);	//greater=1 -> y=0
                }
                else {
                    clause(id_transformation,l_x,l_neg_y,less);	// x=0 and y=1 -> less=1
                    clause(id_transformation,n_less,l_neg_x);	// less=1 -> x=0
                    clause(id_transformation,n_less,l_y);		// less=1 -> y=1
                }
			}

            // auxiliary variable for EQUAL
			unsigned int equal = _first_free_id++;
			unsigned int n_equal = _first_free_id++;
			vertice(equal, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
			vertice(n_equal, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
			vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
			edge(id_transformation, _first_free_id-1, -1);
			edge(id_transformation, _first_free_id-3, -1);
			edge(_first_free_id-1, equal, -1);
			edge(_first_free_id-1, n_equal, -1);
			eq_variables.push_back(equal);
			neg_eq_variables.push_back(n_equal);
			//(a<=>b) <=> eq
			clause(id_transformation,l_x,l_y,equal);			//a^b v -a^-b <-> eq
			clause(id_transformation,l_neg_x,l_neg_y,equal);	//...
			clause(id_transformation,l_neg_x,l_y,n_equal);		//av-b ^ -avb <-> eq
			clause(id_transformation,l_x,l_neg_y,n_equal);		//...

            // auxiliary variable for GREATER
            if ((flag == Flag::GREATER_THAN) or (flag == Flag::GREATER_EQUAL_THAN))
            {
                unsigned int greater   = _first_free_id++;
                unsigned int n_greater = _first_free_id++;
                vertice(greater, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
                vertice(n_greater, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
                vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
                edge(id_transformation, _first_free_id-1, -1);
                edge(id_transformation, _first_free_id-3, -1);
                edge(_first_free_id-1, greater, -1);
                edge(_first_free_id-1, n_greater, -1);
                greater_variables.push_back(greater);
                neg_greater_variables.push_back(n_greater);

                //sign bit: + est codé en 0 et - en 1 donc on doit faire less
                if (i == max_bit_size-1) {
                    clause(id_transformation,l_x,l_neg_y,greater);	// x=0 and y=1 -> less=1
                    clause(id_transformation,n_greater,l_neg_x);	// less=1 -> x=0
                    clause(id_transformation,n_greater,l_y);		// less=1 -> y=1
                } else {
                    clause(id_transformation,l_neg_x,l_y,greater);	// x=1 and y=0 -> greater=1
                    clause(id_transformation,n_greater,l_x);		//greater=1 -> x=1
                    clause(id_transformation,n_greater,l_neg_y);	//greater=1 -> y=0
                }
            }

		}

        for (int i = max_bit_size-1; i >= 0; --i)
        {
			// auxiliary variables for the Global ORs (example)
			// EXP: l3 | (e3 & l2) | (e3 & e2 & l1) | (e3 & e2 & e1 & l0)
			// aux3 = l3, aux2 = (e3 & l2), aux1 = (e3 & e2 & l1), aux0 = (e3 & e2 & e1 & l0)
            unsigned int aux;
            unsigned int n_aux;
			aux   = _first_free_id++;
			n_aux = _first_free_id++;
			vertice(aux, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
			vertice(n_aux, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
			vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
			edge(id_transformation, _first_free_id-1, -1);
			edge(id_transformation, _first_free_id-3, -1);
			edge(_first_free_id-1, aux, -1);
			edge(_first_free_id-1, n_aux, -1);

            switch (flag)
            {
                case Flag::GREATER_THAN : case Flag::GREATER_EQUAL_THAN :
                {
                    //greater AND eq AND ... -> aux
                    unsigned int id_disjunction = _first_free_id++;
                    vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
                    edge(id_transformation, id_disjunction, -1);
                    edge(id_disjunction, aux, -1);
                    edge(id_disjunction, neg_greater_variables[i], -1);
                    for (unsigned int j = i+1; j < max_bit_size; ++j)
                        edge(id_disjunction, neg_eq_variables[j], -1);

                    //greater AND eq AND ... <- aux
                    for (unsigned int j = i+1; j < max_bit_size; ++j)
                    {
                        unsigned int id_disjunction = _first_free_id++;
                        vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
                        edge(id_transformation, id_disjunction, -1);
                        edge(id_disjunction, n_aux, -1);
                        edge(id_disjunction, eq_variables[j], -1);
                    }
                    id_disjunction = _first_free_id++;
                    vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
                    edge(id_transformation, id_disjunction, -1);
                    edge(id_disjunction, n_aux, -1);
                    edge(id_disjunction, greater_variables[i], -1);
                    break;
                }

                case Flag::LESS_THAN :case Flag::LESS_EQUAL_THAN :
                {
                    //less AND eq AND ... -> aux
                    unsigned int id_disjunction = _first_free_id++;
                    vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
                    edge(id_transformation, id_disjunction, -1);
                    edge(id_disjunction, aux, -1);
                    edge(id_disjunction, neg_less_variables[i], -1);
                    for (unsigned int j = i+1; j < max_bit_size; ++j)
                        edge(id_disjunction, neg_eq_variables[j], -1);

                    //less AND eq AND ... <- aux
                    for (unsigned int j = i+1; j < max_bit_size; ++j)
                    {
                        unsigned int id_disjunction = _first_free_id++;
                        vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
                        edge(id_transformation, id_disjunction, -1);
                        edge(id_disjunction, n_aux, -1);
                        edge(id_disjunction, eq_variables[j], -1);
                    }
                    id_disjunction = _first_free_id++;
                    vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
                    edge(id_transformation, id_disjunction, -1);
                    edge(id_disjunction, n_aux, -1);
                    edge(id_disjunction, less_variables[i], -1);
                    break;
                }
                default : std::cerr << "flag " << flag << " unknown for this relation." << std::endl;
            }
            or_variables.push_back(aux);
        }

         if ((flag == Flag::LESS_EQUAL_THAN) or (flag == Flag::GREATER_EQUAL_THAN))
        {
            //auxiliary variable for equal
            unsigned int global_equal   = _first_free_id++;
            unsigned int n_global_equal = _first_free_id++;
            vertice(global_equal, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0,1} )); num_variables++;
            vertice(n_global_equal, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0,1} )); num_variables++;
            vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
            edge(id_transformation, _first_free_id-1, -1);
            edge(id_transformation, _first_free_id-3, -1);
            edge(_first_free_id-1, global_equal, -1);
            edge(_first_free_id-1, n_global_equal, -1);
            //equal[0] and  ... and equal[n] -> global_equal
            unsigned int id_disjunction = _first_free_id++;
            vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
            edge(id_transformation, id_disjunction, -1);
            edge(id_disjunction, global_equal, -1);
           for (unsigned int i = 0; i < max_bit_size; ++i)
                edge(id_disjunction, neg_eq_variables[i],-1);
            //equal[0] and  ... and equal[n] <- global_equal
            for (unsigned int i = 0; i < max_bit_size; ++i)
                clause(id_transformation,eq_variables[i],n_global_equal);
            // clause contenant tous les resultats des or
            id_disjunction = _first_free_id++;
            vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
            edge(id_transformation, id_disjunction, -1);
            for (unsigned int i = 0; i < max_bit_size; ++i)
                edge(id_disjunction, or_variables[i], -1);
            edge(id_disjunction, global_equal, -1);
        }
        else {
            // clause contenant tous les resultats des or
            unsigned int id_disjunction = _first_free_id++;
            vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
            edge(id_transformation, id_disjunction, -1);
            for (unsigned int i = 0; i < max_bit_size; ++i)
                edge(id_disjunction, or_variables[i], -1);
	    }
	}

	//std::cout << "end" << std::endl;
}



// ==========================================================================
// COMPARATORS to CNF
//   LESS_THAN (<)
//   GREATER_THAN (>)
//   LESS_EQUAL_THAN (<=)
//   GREATER_EQUAL_THAN (>=)
// ==========================================================================

void GRAPH::transform_less_than_to_disjunction(unsigned int id)
{
	less_greater_equal_to_disjunction(id, Flag::LESS_THAN);
	//comparators_to_cnf(id, Flag::LESS_THAN);
}
/*
void GRAPH::transform_greater_than_to_disjunction(unsigned int id)
{
	less_greater_equal_to_disjunction(id, Flag::GREATER_THAN);
	//comparators_to_cnf(id, Flag::GREATER_THAN);
}

void GRAPH::transform_less_equal_than_to_disjunction(unsigned int id)
{
	less_greater_equal_to_disjunction(id, Flag::LESS_EQUAL_THAN);
	//comparators_to_cnf(id, Flag::LESS_EQUAL_THAN);
}

void GRAPH::transform_greater_equal_than_to_disjunction(unsigned int id)
{
	less_greater_equal_to_disjunction(id, Flag::GREATER_EQUAL_THAN);
	//comparators_to_cnf(id, Flag::GREATER_EQUAL_THAN);
}
*/

// ==========================================================================
// EQUAL (=) to DISJUNCTION
/* ==========================================================================

void GRAPH::transform_equal_to_disjunction(unsigned int id)
{
    std::cout << std::endl << "GRAPH::transform_equal_to_disjunction()" << std::endl;

    // Vérifie que la transformation n'existe pas déjà
  	chr::Logical_var< unsigned int > l_id_trans;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::EQUAL | Flag::BINARY_DISJUNCTION), l_id_trans);
  	if (l_id_trans.ground())
		return;

	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::EQUAL | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	// Get relation's variables (l_relation_vars)
	chr::Logical_var_mutable< std::vector< unsigned int > > l_relation_vars;
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), 0, l_relation_vars);
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER), 0, l_relation_vars);
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), 0, l_relation_vars);
	assert((*l_relation_vars).size() == 2);

	// Vérifie pour chaque variable de la relation
	// que la transformation de la variable entière en booléenne existe
	std::vector< unsigned int > transformations_for_integer_variables;
	for (unsigned int i=0; i < (*l_relation_vars).size(); ++i)
	{
		chr::Logical_var< unsigned int > l_id_trans_boolean;
		get_child_with_flags((*l_relation_vars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		if (!l_id_trans_boolean.ground())
		{
			build_log_encoding_from((*l_relation_vars)[i]);
			get_child_with_flags((*l_relation_vars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		}
		assert(l_id_trans_boolean.ground());
		transformations_for_integer_variables.push_back(*l_id_trans_boolean);
	}

	//std::cout << "\ttransformations_for_integer_variables : " << transformations_for_integer_variables.capacity() << std::endl;

	// Récupération des boolean variables et du domaine de la première variable entière
	chr::Logical_var_mutable< std::vector< unsigned int > > l_boolean_vars_x;
	get_all_children_with_flags_ordered(transformations_for_integer_variables[0], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolean_vars_x);
	get_all_children_with_flags_ordered(transformations_for_integer_variables[0], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_x);

	// Récupération des variables booléennes et du domaine de la deuxième variable entière
	chr::Logical_var_mutable< std::vector< unsigned int > > l_boolean_vars_y;
	get_all_children_with_flags_ordered(transformations_for_integer_variables[1], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolean_vars_y);
	get_all_children_with_flags_ordered(transformations_for_integer_variables[1], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolean_vars_y);
	assert((*l_boolean_vars_x).size() > 0);
	assert((*l_boolean_vars_y).size() > 0);


	if (((*l_boolean_vars_x).size() != 0) and ((*l_boolean_vars_y).size()!= 0))
	{
        unsigned int max_bit_size = std::max((*l_boolean_vars_x).size(),(*l_boolean_vars_y).size());
        unsigned int l_x = (*l_boolean_vars_x)[0];
        unsigned int l_y = (*l_boolean_vars_y)[0];

        for (unsigned int i = 0; i < max_bit_size; ++i)
        {
            chr::Logical_var< unsigned int > l_neg_x, l_neg_y;
            if (i<(*l_boolean_vars_x).size())
                l_x = (*l_boolean_vars_x)[i];
            else
                l_x = (*l_boolean_vars_x)[(*l_boolean_vars_x).size()-1];

            get_opposite_var(l_x, l_neg_x);
            assert(l_neg_x.ground());

            if (i<(*l_boolean_vars_y).size())
                l_y = (*l_boolean_vars_y)[i];
            else
                l_y = (*l_boolean_vars_y)[(*l_boolean_vars_y).size()-1];

            get_opposite_var(l_y, l_neg_y);
            assert(l_neg_y.ground());

            clause(id_transformation,l_x,l_neg_y);
            clause(id_transformation,l_neg_x,l_y);
        }
	}
}
*/


// ===========================================================================
// EQUAL_TO_DISJUNCTION
// TODO: vectors by value !!!
//  * Transform an equality  relation node ( a = b <-> c) of id \a id
//  * into a bunch of disjunction relations and add them to the graph.
//  * It creates the boolean variables if needed.
//  * @param id The id of the inequality relation node
//  * @param a Vector id of the first var node
//  * @param b Vector id of the second var node
//  * @param c The id of the result var node
//  * @param n_c The id of the result neg var node
// =========================================================================
/*
void GRAPH::equal_to_disjunction(unsigned int id, std::vector<unsigned int> a, std::vector<unsigned int> b, unsigned int c, unsigned int n_c)
{
    std::cout << std::endl << "GRAPH::equal_to_disjunction(" << id << ")" << std::endl ;

    // TODO : Transformation node missing ??

    if ((a.size() != 0) and (b.size()!= 0))
	{
        unsigned int max_bit_size = std::max(a.size(),b.size());
        unsigned int l_a = a[0];
        unsigned int l_b = b[0];
        std::vector<unsigned int> var_aux;
        std::vector<unsigned int> var_n_aux;

        for (unsigned int i = 0; i < max_bit_size; ++i)
        {
            chr::Logical_var< unsigned int > l_neg_a, l_neg_b;
            if (i<a.size())
                l_a = a[i];
            else
                l_a = a[a.size()-1];
            get_opposite_var(l_a, l_neg_a);
            assert(l_neg_a.ground());

            if (i<b.size())
                l_b = b[i];
            else
                l_b = b[b.size()-1];
            get_opposite_var(l_b, l_neg_b);
            assert(l_neg_b.ground());

            // Création d'une variable auxiliaire
            unsigned int aux   = _first_free_id++;
            unsigned int n_aux = _first_free_id++;
            vertice(aux, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
            vertice(n_aux, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
            vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
            edge(id, _first_free_id-1, -1);
            edge(id, _first_free_id-3, -1);
            edge(_first_free_id-1, aux, -1);
            edge(_first_free_id-1, n_aux, -1);
            var_aux.push_back(aux);
            var_n_aux.push_back(n_aux);

            clause(id,l_neg_a,l_neg_b,aux);	// ~a | ~b | aux
            clause(id,l_a,l_b,aux);			//  a |  b | aux
            clause(id,l_a,l_neg_b,n_aux);	//  a | ~b | ~aux
            clause(id,l_neg_a,l_b,n_aux);	// ~a |  b | ~aux
        }

        // clauses contenant tous les resultats des aux
        unsigned int id_disjunction = _first_free_id++;
        vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
        edge(id, id_disjunction, -1);
        edge(id_disjunction, c, -1);
        for (unsigned int i = 0; i < var_aux.size(); ++i)
        {
            edge(id_disjunction, var_n_aux[i], -1);
            clause(id,var_aux[i],n_c);
        }
	}
}
*/


// ===========================================================================
// AND
// http://sofdem.github.io/gccat/gccat/Cand.html
//
// x, y1..yn : All CSP Booleans
// AND(x,y1,y2..yn) => OR(nlx,ly1) OR(nlx,ly2) .. OR(lx,nly1,nly2 .. nlyn)
// ===========================================================================
/*
void GRAPH::transform_and_to_disjunction(unsigned int id)
{
    // Check if transformation already exists
  	chr::Logical_var< unsigned int > l_check;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::AND | Flag::BINARY_DISJUNCTION), l_check);
  	if (l_check.ground())
		return;

	// Create transformation
	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::AND | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	// Get relation's variables
	chr::Logical_var_mutable< std::vector<unsigned int> > l_rvars;
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER), 0, l_rvars);
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), 0, l_rvars);
	assert((*l_rvars).size() > 1);

	// If needed, encode variables; then, get the transformations' IDs.
	std::vector<unsigned int> var_transformation;
	for (unsigned int i=0; i < (*l_rvars).size(); ++i)
	{
		chr::Logical_var< unsigned int > l_id_trans_boolean;
		get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		if (!l_id_trans_boolean.ground())
		{
			build_log_encoding_from((*l_rvars)[i]);
			get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		}
		assert(l_id_trans_boolean.ground());
		var_transformation.push_back(*l_id_trans_boolean);
	}

	// literal X
	chr::Logical_var_mutable< std::vector<unsigned int> > lx_boolvars;
	get_all_children_with_flags_ordered(var_transformation[0], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, lx_boolvars);
	get_all_children_with_flags_ordered(var_transformation[0], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, lx_boolvars);
	assert((*lx_boolvars).size() > 0);

	unsigned int lx = (*lx_boolvars)[0];
	chr::Logical_var< unsigned int> lnx;
	get_opposite_var(lx, lnx);

	// n-ary disjunction (single clause)
	unsigned int nary = _first_free_id++;
	vertice(nary, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, nary, -1);
	edge(nary, lnx, -1);


	for (unsigned int i=1; i< var_transformation.size(); ++i)
	{
		// obtain log-encoded literals
		chr::Logical_var_mutable< std::vector<unsigned int> > ly_boolvars;
		get_all_children_with_flags_ordered(var_transformation[i], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, ly_boolvars);
		get_all_children_with_flags_ordered(var_transformation[i], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, ly_boolvars);
		assert((*ly_boolvars).size() > 0);

		unsigned int ly = (*ly_boolvars)[0];
		chr::Logical_var< unsigned int> lny;
		get_opposite_var(ly, lny);

		// binary disjunction (multiple clauses: ~x|y )
		unsigned int binary = _first_free_id++;
		vertice(binary, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, binary, -1);
		edge(binary, lx, -1);
		edge(binary, lny, -1);

		// n-ary disjunction (single clause: x|~y1|..|~yn )
		edge(nary, ly, -1);
	}
}
*/


// ===========================================================================
// IF_THEN_OR
//
// IN :	x => y1 v y2 v ... v yn (CSP Boolean variables)
// OUT:	~x v y1 v y2 ... yn
// ===========================================================================
/*
void GRAPH::transform_ifthenor_to_disjunction(unsigned int id)
{
    // Check transformation; if it does not exist, create it
  	chr::Logical_var< unsigned int > l_check;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::IF_THEN_OR | Flag::BINARY_DISJUNCTION), l_check);
  	if (l_check.ground())
		return;

	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::IF_THEN_OR | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	// Get relation's variables
	chr::Logical_var_mutable< std::vector<unsigned int> > l_rvars;
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER), 0, l_rvars);
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), 0, l_rvars);
	assert((*l_rvars).size() > 1);

	// If needed, encode variables; then, get the transformations' IDs.
	std::vector<unsigned int> var_transformation;
	for (unsigned int i=0; i < (*l_rvars).size(); ++i)
	{
		chr::Logical_var< unsigned int > l_id_trans_boolean;
		get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		if (!l_id_trans_boolean.ground())
		{
			build_log_encoding_from((*l_rvars)[i]);
			get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		}
		assert(l_id_trans_boolean.ground());
		var_transformation.push_back(*l_id_trans_boolean);
	}

	// x-literals
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_x;
	get_all_children_with_flags_ordered(var_transformation[0], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_x);
	get_all_children_with_flags_ordered(var_transformation[0], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_x);
	assert((*l_boolvars_x).size() > 0);
	unsigned int lx = (*l_boolvars_x)[0];	// only the 1st bit

	chr::Logical_var< unsigned int> lnx;
	get_opposite_var(lx, lnx);
	assert(lnx.ground());

	unsigned int ifthenor = _first_free_id++;
	vertice(ifthenor, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, ifthenor, -1);
	edge(ifthenor, lnx, -1);

	// y-literals
	for (unsigned int i=1; i<var_transformation.size(); i++) {

		chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_y;
		get_all_children_with_flags_ordered(var_transformation[i], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_y);
		get_all_children_with_flags_ordered(var_transformation[i], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_y);
		assert((*l_boolvars_y).size() > 0);

		unsigned int ly = (*l_boolvars_y)[0];
		edge(ifthenor, ly, -1);
	}
}
*/



// ===========================================================================
// AT LEAST
// AT_LEAST(x,y,z) => OR(lx,ly,lz)
// ===========================================================================
/*
void GRAPH::transform_atleast_to_disjunction(unsigned int id)
{
	//std::cout << "transform_atleast(" << id << ") -----------------------------" << std::endl;

    // Check if transformation already exists
  	chr::Logical_var< unsigned int > l_check;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::AT_LEAST | Flag::BINARY_DISJUNCTION), l_check);
  	if (l_check.ground())
		return;

	// Create transformation
	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::AT_LEAST | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	// Get relation's variables
	chr::Logical_var_mutable< std::vector<unsigned int> > l_rvars;
	get_all_children_with_flags(id, Flags(Flag::VARIABLE | Flag::INTEGER), l_rvars);
	get_all_children_with_flags(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), l_rvars);
	assert((*l_rvars).size() > 1);

	// If needed, encode variables; then, get the transformations' IDs.
	std::vector<unsigned int> var_transformation;
	for (unsigned int i=0; i < (*l_rvars).size(); ++i)
	{
		chr::Logical_var< unsigned int > l_id_trans_boolean;
		get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		if (!l_id_trans_boolean.ground())
		{
			build_log_encoding_from((*l_rvars)[i]);
			get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		}
		assert(l_id_trans_boolean.ground());
		var_transformation.push_back(*l_id_trans_boolean);
	}

	// at-least clause
	unsigned int atleast = _first_free_id++;
	vertice(atleast, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, atleast, -1);


	for (unsigned int i=0; i < var_transformation.size(); ++i)
	{
		// obtain log-encoded literals
		chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars;
		get_all_children_with_flags_ordered(var_transformation[i], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars);
		get_all_children_with_flags_ordered(var_transformation[i], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars);
		assert((*l_boolvars).size() > 0);

		// add them to the clause
		edge(atleast, (*l_boolvars)[0], -1);
	}
}
*/



// THIS VERSION IS FOR THE CSPBOOLEAN !
/*
void GRAPH::transform_atleast_to_disjunction(unsigned int id)
{
	//std::cout << "transform_atleast(" << id << ") -----------------------------" << std::endl;

    // Check if transformation already exists
  	chr::Logical_var< unsigned int > l_check;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::AT_LEAST | Flag::BINARY_DISJUNCTION), l_check);
  	if (l_check.ground())
		return;

	// Create transformation
	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::AT_LEAST | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	// RVARS_INT: (r)elation's Integer (var)iables
	chr::Logical_var_mutable< std::vector<unsigned int> > rvars_int;
	get_all_children_with_flags(id, Flags(Flag::VARIABLE | Flag::INTEGER), rvars_int);
	get_all_children_with_flags(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), rvars_int);

	chr::Logical_var_mutable< std::vector<unsigned int> > rvars_bool;
	get_all_children_with_flags(id, Flags(Flag::VARIABLE | Flag::INTEGER), rvars_bool);
	get_all_children_with_flags(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), rvars_bool);

	// For the AT-LEAST function or all variables are boolean or all are INTEGER


	//TODO: assert((*rvars_int).size() > 1 (*rvars_int).size() > 1);

	// If needed, encode variables; then, get the transformations' IDs.
	std::vector<unsigned int> var_transformation;
	for (unsigned int i=0; i < (*rvars_int).size(); ++i)
	{
		chr::Logical_var< unsigned int > l_id_trans_boolean;
		get_child_with_flags((*rvars_int)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		if (!l_id_trans_boolean.ground())
		{
			build_log_encoding_from((*rvars_int)[i], true);
			get_child_with_flags((*rvars_int)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		}
		assert(l_id_trans_boolean.ground());
		var_transformation.push_back(*l_id_trans_boolean);
	}

	// at-least clause
	unsigned int atleast = _first_free_id++;
	vertice(atleast, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, atleast, -1);


	for (unsigned int i=0; i < var_transformation.size(); ++i)
	{
		// obtain log-encoded literals
		chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars;
		get_all_children_with_flags_ordered(var_transformation[i], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars);
		get_all_children_with_flags_ordered(var_transformation[i], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars);
		assert((*l_boolvars).size() > 0);

		// add them to the clause
		edge(atleast, (*l_boolvars)[0], -1);
	}

}
*/


// ===========================================================================
// AT MOST
// AT_MOST(x1,x2,x3) => AND( OR(l1,l2), OR(l1,l3)..)
// ===========================================================================
/*
void GRAPH::transform_atmost_to_disjunction(unsigned int id)
{
	//std::cout << "transform_atmost(" << id << ") -----------------------------" << std::endl;

    // Check if transformation already exists
  	chr::Logical_var< unsigned int > l_check;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::AT_MOST | Flag::BINARY_DISJUNCTION), l_check);
  	if (l_check.ground())
		return;

	// Create transformation
	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::AT_MOST | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	// Get relation's variables
	chr::Logical_var_mutable< std::vector<unsigned int> > l_rvars;
	get_all_children_with_flags(id, Flags(Flag::VARIABLE | Flag::INTEGER), l_rvars);
	get_all_children_with_flags(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), l_rvars);
	assert((*l_rvars).size() > 1);

	// If needed, encode variables; then, get the transformations' IDs.
	std::vector<unsigned int> var_transformation;
	for (unsigned int i=0; i < (*l_rvars).size(); ++i)
	{
		chr::Logical_var< unsigned int > l_id_trans_boolean;
		get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		if (!l_id_trans_boolean.ground())
		{
			build_log_encoding_from((*l_rvars)[i]);
			get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		}
		assert(l_id_trans_boolean.ground());
		var_transformation.push_back(*l_id_trans_boolean);
	}

	for (unsigned int i=0; i <(var_transformation.size()-1); ++i)
	{
		// first-literal (log-encoded)
		chr::Logical_var_mutable< std::vector<unsigned int> > lx_boolvars;
		get_all_children_with_flags_ordered(var_transformation[i], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, lx_boolvars);
		get_all_children_with_flags_ordered(var_transformation[i], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, lx_boolvars);
		assert((*lx_boolvars).size() > 0);

		unsigned int lx = (*lx_boolvars)[0];
		chr::Logical_var< unsigned int> l_nx;
		get_opposite_var(lx, l_nx);

		for (unsigned int j=(i+1); j<var_transformation.size(); ++j)
		{
			// second-literal (log-encoded)
			chr::Logical_var_mutable< std::vector<unsigned int> > ly_boolvars;
			get_all_children_with_flags_ordered(var_transformation[j], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, ly_boolvars);
			get_all_children_with_flags_ordered(var_transformation[j], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, ly_boolvars);
			assert((*ly_boolvars).size() > 0);

			// TODO: here I should add the loop to run over all bits.

			// clause
			//unsigned int atmost = _first_free_id++;
			//vertice(atmost, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
			//edge(id_transformation, atmost, -1);

			unsigned int ly = (*ly_boolvars)[0];
			chr::Logical_var< unsigned int> l_ny;
			get_opposite_var(ly, l_ny);

			clause(id_transformation, l_nx, l_ny);
		}
	}
}
*/


// ===========================================================================
// IF <condition> THEN <assignment>
// x => (y <=> z)
// x, y, z : Booleans (TODO: this is hardcoded!!)
// ===========================================================================
/*
void GRAPH::transform_ifthen_to_disjunction(unsigned int id)
{
    // Check if transformation already exists
  	chr::Logical_var< unsigned int > l_check;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::IF_THEN | Flag::BINARY_DISJUNCTION), l_check);
  	if (l_check.ground())
		return;

	// Create transformation
	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::IF_THEN | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	// Get relation's variables
	chr::Logical_var_mutable< std::vector<unsigned int> > l_rvars;
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER), 0, l_rvars);
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), 0, l_rvars);
	assert((*l_rvars).size() == 3);

	// If needed, encode variables; then, get the transformations' IDs.
	std::vector<unsigned int> var_transformation;
	for (unsigned int i=0; i < (*l_rvars).size(); ++i)
	{
		chr::Logical_var< unsigned int > l_id_trans_boolean;
		get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		if (!l_id_trans_boolean.ground())
		{
			build_log_encoding_from((*l_rvars)[i]);
			get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		}
		assert(l_id_trans_boolean.ground());
		var_transformation.push_back(*l_id_trans_boolean);
	}

	// x-literals
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_x;
	get_all_children_with_flags_ordered(var_transformation[0], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_x);
	get_all_children_with_flags_ordered(var_transformation[0], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_x);
	assert((*l_boolvars_x).size() > 0);

	// y-literals
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_y;
	get_all_children_with_flags_ordered(var_transformation[1], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_y);
	get_all_children_with_flags_ordered(var_transformation[1], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_y);
	assert((*l_boolvars_y).size() > 0);

	// z-literals
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_z;
	get_all_children_with_flags_ordered(var_transformation[2], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_z);
	get_all_children_with_flags_ordered(var_transformation[2], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_z);
	assert((*l_boolvars_z).size() > 0);

	unsigned int l_x = (*l_boolvars_x)[0];
	unsigned int l_y = (*l_boolvars_y)[0];
	unsigned int l_z = (*l_boolvars_z)[0];

	chr::Logical_var< unsigned int> l_nx, l_ny, l_nz;
	get_opposite_var(l_x, l_nx);
	get_opposite_var(l_y, l_ny);
	get_opposite_var(l_z, l_nz);
	assert(l_nx.ground());
	assert(l_ny.ground());
	assert(l_nz.ground());

	clause(id_transformation, l_nx, l_ny, l_z);	// (~x | ~y | z)
	clause(id_transformation, l_nx, l_y, l_nz);	// (~x | y | ~z)

//	std::cout << "---------> ";
//	std::cout << "(" << (*l_rvars)[0] << ") " << l_nx << " ";
//	std::cout << "(" << (*l_rvars)[1] << ") " << l_y << "," << l_ny << " ";
//	std::cout << "(" << (*l_rvars)[2] << ") " << l_z << "," << l_nz << std::endl;

}
*/


// ===========================================================================
// IF <condition> THEN <different>
// x => (y != z)
// x, y, z : Booleans (TODO: this is hardcoded!!)
// ===========================================================================
/*
void GRAPH::transform_ifthennot_to_disjunction(unsigned int id)
{
    // Check if transformation already exists
  	chr::Logical_var< unsigned int > l_check;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::IF_THEN_NOT | Flag::BINARY_DISJUNCTION), l_check);
  	if (l_check.ground())
		return;

	// Create transformation
	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::IF_THEN_NOT | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	// Get relation's variables
	chr::Logical_var_mutable< std::vector<unsigned int> > l_rvars;
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER), 0, l_rvars);
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), 0, l_rvars);
	assert((*l_rvars).size() == 3);

	// If needed, encode variables; then, get the transformations' IDs.
	std::vector<unsigned int> var_transformation;
	for (unsigned int i=0; i < (*l_rvars).size(); ++i)
	{
		chr::Logical_var< unsigned int > l_id_trans_boolean;
		get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		if (!l_id_trans_boolean.ground())
		{
			build_log_encoding_from((*l_rvars)[i]);
			get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		}
		assert(l_id_trans_boolean.ground());
		var_transformation.push_back(*l_id_trans_boolean);
	}

	// x-literals
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_x;
	get_all_children_with_flags_ordered(var_transformation[0], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_x);
	get_all_children_with_flags_ordered(var_transformation[0], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_x);
	assert((*l_boolvars_x).size() > 0);

	// y-literals
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_y;
	get_all_children_with_flags_ordered(var_transformation[1], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_y);
	get_all_children_with_flags_ordered(var_transformation[1], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_y);
	assert((*l_boolvars_y).size() > 0);

	// z-literals
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_z;
	get_all_children_with_flags_ordered(var_transformation[2], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_z);
	get_all_children_with_flags_ordered(var_transformation[2], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_z);
	assert((*l_boolvars_z).size() > 0);

		unsigned int l_x = (*l_boolvars_x)[0];
		unsigned int l_y = (*l_boolvars_y)[0];
		unsigned int l_z = (*l_boolvars_z)[0];

		chr::Logical_var< unsigned int> l_nx, l_ny, l_nz;
		get_opposite_var(l_x, l_nx);
		get_opposite_var(l_y, l_ny);
		get_opposite_var(l_z, l_nz);
		assert(l_nx.ground());
		assert(l_ny.ground());
		assert(l_nz.ground());

		clause(id_transformation, l_nx, l_y, l_z);	    // (~x | y | z)
		clause(id_transformation, l_nx, l_ny, l_nz);	// (~x | ~y| ~z )
}
*/



//// ===========================================================================
//// LINK
//// x, y, z : Booleans (TODO: this is hardcoded!!)
//// ===========================================================================
//void GRAPH::transform_link_to_disjunction(unsigned int id)
//{
//    // Check if transformation already exists
//  	chr::Logical_var< unsigned int > l_check;
//	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::LINK | Flag::BINARY_DISJUNCTION), l_check);
//  	if (l_check.ground())
//		return;
//
//	// Create transformation
//	unsigned int id_transformation = _first_free_id++;
//	edge(id, id_transformation, -1);
//	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::LINK | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;
//
//	// Get relation's variables
//	chr::Logical_var_mutable< std::vector<unsigned int> > l_rvars;
//	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER), 0, l_rvars);
//	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), 0, l_rvars);
//	assert((*l_rvars).size() == 3);
//
//	// If needed, encode variables; then, get the transformations' IDs.
//	std::vector<unsigned int> var_transformation;
//	for (unsigned int i=0; i < (*l_rvars).size(); ++i)
//	{
//		chr::Logical_var< unsigned int > l_id_trans_boolean;
//		get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
//		if (!l_id_trans_boolean.ground())
//		{
//			build_log_encoding_from((*l_rvars)[i]);
//			get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
//		}
//		assert(l_id_trans_boolean.ground());
//		var_transformation.push_back(*l_id_trans_boolean);
//	}
//
//	// x-literals
//	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_x;
//	get_all_children_with_flags_ordered(var_transformation[0], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_x);
//	get_all_children_with_flags_ordered(var_transformation[0], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_x);
//	assert((*l_boolvars_x).size() > 0);
//
//	// y-literals
//	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_y;
//	get_all_children_with_flags_ordered(var_transformation[1], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_y);
//	get_all_children_with_flags_ordered(var_transformation[1], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_y);
//	assert((*l_boolvars_y).size() > 0);
//
//	// z-literals : Check domain !!
//	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_z;
//	get_all_children_with_flags_ordered(var_transformation[2], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_z);
//	get_all_children_with_flags_ordered(var_transformation[2], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_z);
//	assert((*l_boolvars_z).size() > 0);
//
//	chr::Logical_var< Domain > l_domain_z;
//	get_domain(var_transformation[2],l_domain_z);
//	const IntVarDomain& domain_z = dynamic_cast<const IntVarDomain&>(*l_domain_z);
//
//	unsigned int l_x = (*l_boolvars_x)[0];
//	unsigned int l_y = (*l_boolvars_y)[0];
//
//	chr::Logical_var< unsigned int> l_nx, l_ny;
//	get_opposite_var(l_x, l_nx);
//	get_opposite_var(l_y, l_ny);
//	assert(l_nx.ground());
//	assert(l_ny.ground());
//
//
//	for (unsigned int z : domain_z.domain) {
//		if (z == 0) {
//			clause(id_transformation, l_nx, l_ny);
//		} else {
//			clause(id_transformation, l_nx, l_y);
//		}
//	}
//}


/* ===========================================================================
// IF EQUAL THEN --> CNF
// ===========================================================================

void GRAPH::transform_ifequalthen_to_disjunction(unsigned int id)
{
	//std::cout << "ifequalthen_to_disjunction()\n" ;

    // Check if transformation already exists
  	chr::Logical_var< unsigned int > l_check;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::IF_EQUAL_THEN | Flag::BINARY_DISJUNCTION), l_check);
  	if (l_check.ground())
		return;

	// Create transformation
	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::EQUAL | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	// Get relation's variables
	chr::Logical_var_mutable< std::vector<unsigned int> > l_rvars;
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER), 0, l_rvars);
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), 0, l_rvars);
	assert((*l_rvars).size() == 4);

	// If needed, encode variables; then, get the transformations' IDs.
	std::vector<unsigned int> var_transformation;
	for (unsigned int i=0; i < (*l_rvars).size(); ++i)
	{
		chr::Logical_var< unsigned int > l_id_trans_boolean;
		get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		if (!l_id_trans_boolean.ground())
		{
			build_log_encoding_from((*l_rvars)[i]);
			get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		}
		assert(l_id_trans_boolean.ground());
		var_transformation.push_back(*l_id_trans_boolean);
	}

	// x-literals
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_x;
	get_all_children_with_flags_ordered(var_transformation[0], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_x);
	get_all_children_with_flags_ordered(var_transformation[0], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_x);
	assert((*l_boolvars_x).size() > 0);

	// y-literals
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_y;
	get_all_children_with_flags_ordered(var_transformation[1], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_y);
	get_all_children_with_flags_ordered(var_transformation[1], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_y);
	assert((*l_boolvars_y).size() > 0);

	// u-literals
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_u;
	get_all_children_with_flags_ordered(var_transformation[2], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_u);
	get_all_children_with_flags_ordered(var_transformation[2], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_u);
	assert((*l_boolvars_u).size() > 0);

	// v-literals
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_v;
	get_all_children_with_flags_ordered(var_transformation[3], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_v);
	get_all_children_with_flags_ordered(var_transformation[3], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_v);
	assert((*l_boolvars_v).size() > 0);

	unsigned int max_bitsize = std::max(std::max((*l_boolvars_x).size(),(*l_boolvars_y).size()), std::max((*l_boolvars_u).size(), (*l_boolvars_v).size()));
	unsigned int max_x = (*l_boolvars_x).size();
	unsigned int max_y = (*l_boolvars_y).size();
	unsigned int max_u = (*l_boolvars_u).size();
	unsigned int max_v = (*l_boolvars_v).size();

	for (unsigned int i=0; i<max_bitsize; i++)
	{
		unsigned int l_x = (i < max_x) ? (*l_boolvars_x)[i] : (*l_boolvars_x)[max_x-1];
		unsigned int l_y = (i < max_y) ? (*l_boolvars_y)[i] : (*l_boolvars_y)[max_y-1];	//(*l_boolvars_y)[i];
		unsigned int l_u = (i < max_u) ? (*l_boolvars_u)[i] : (*l_boolvars_u)[max_u-1];	//(*l_boolvars_u)[i];
		unsigned int l_v = (i < max_v) ? (*l_boolvars_v)[i] : (*l_boolvars_v)[max_v-1];	//(*l_boolvars_v)[i];

		chr::Logical_var< unsigned int> l_nx, l_ny, l_nu, l_nv;
		get_opposite_var(l_x, l_nx);
		get_opposite_var(l_y, l_ny);
		get_opposite_var(l_u, l_nu);
		get_opposite_var(l_v, l_nv);
		assert(l_nx.ground());
		assert(l_ny.ground());
		assert(l_nu.ground());
		assert(l_nv.ground());

		clause(id_transformation, l_x,  l_y,  l_nu, l_v);	// (x | y | ~u | v)
		clause(id_transformation, l_x,  l_y,  l_u,  l_nv);	// (x | y | u | ~v)
		clause(id_transformation, l_nx, l_ny, l_nu, l_v);	// (~x | ~y | ~u | v)
		clause(id_transformation, l_nx, l_ny, l_u,  l_nv);	// (~x | ~y | u | ~v)
	}
}
*/

/* ===========================================================================
// STORED EQUAL
// (X <=> Y) <=> Z
//
// 2018-10-25
// x,y : Integers (and treated like them)
// z   : Integer BUT it will be used as BOOLEAN; this means, that only the
//       1st bit will be used.
//       This will work ONLY if Z-domain has been correctly declared in {0,1}
// ===========================================================================
void GRAPH::transform_stored_equal_to_disjunction(unsigned int id)
{
	//std::cout << "stored_equal_to_disjunction()\n" ;

    // Check if transformation already exists
  	chr::Logical_var< unsigned int > l_check;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::STORED_EQUAL | Flag::BINARY_DISJUNCTION), l_check);
  	if (l_check.ground())
		return;

	// Create transformation
	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::STORED_EQUAL | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	// Get relation's variables
	chr::Logical_var_mutable< std::vector<unsigned int> > l_rvars;
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER), 0, l_rvars);
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), 0, l_rvars);
	assert((*l_rvars).size() == 3);

	// If needed, encode variables; then, get the transformations' IDs.
	std::vector<unsigned int> var_transformation;
	for (unsigned int i=0; i < (*l_rvars).size(); ++i)
	{
		chr::Logical_var< unsigned int > l_id_trans_boolean;
		get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		if (!l_id_trans_boolean.ground())
		{
			build_log_encoding_from((*l_rvars)[i]);
			get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		}
		assert(l_id_trans_boolean.ground());
		var_transformation.push_back(*l_id_trans_boolean);
	}

	// x-literals
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_x;
	get_all_children_with_flags_ordered(var_transformation[0], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_x);
	get_all_children_with_flags_ordered(var_transformation[0], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_x);
	assert((*l_boolvars_x).size() > 0);

	// y-literals
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_y;
	get_all_children_with_flags_ordered(var_transformation[1], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_y);
	get_all_children_with_flags_ordered(var_transformation[1], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_y);
	assert((*l_boolvars_y).size() > 0);

	// z-literals
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_z;
	get_all_children_with_flags_ordered(var_transformation[2], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_z);
	get_all_children_with_flags_ordered(var_transformation[2], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_z);
	assert((*l_boolvars_z).size() > 0);

	unsigned int max_bitsize = std::max(std::max((*l_boolvars_x).size(),(*l_boolvars_y).size()), (*l_boolvars_z).size());
	unsigned int max_x = (*l_boolvars_x).size();
	unsigned int max_y = (*l_boolvars_y).size();
	//unsigned int max_z = (*l_boolvars_z).size();


	for (unsigned int i=0; i<max_bitsize; i++)
	{
		unsigned int l_x = (i > max_x-1) ? (*l_boolvars_x)[max_x-1] : (*l_boolvars_x)[i] ;	//(*l_boolvars_x)[i];
		unsigned int l_y = (i > max_y-1) ? (*l_boolvars_y)[max_y-1] : (*l_boolvars_y)[i] ;	//(*l_boolvars_y)[i];
		unsigned int l_z = (*l_boolvars_z)[0]; //(i < max_z) ? (*l_boolvars_z)[i] : (*l_boolvars_z)[max_z-1];	//(*l_boolvars_z)[i];

		chr::Logical_var< unsigned int> l_nx, l_ny; //, l_nz;
		get_opposite_var(l_x, l_nx);
		get_opposite_var(l_y, l_ny);
		//get_opposite_var(l_z, l_nz);
		assert(l_nx.ground());
		assert(l_ny.ground());
		//assert(l_nz.ground());

		// (X <=> Y) <=> Z
		//clause(id_transformation, l_x,  l_y,  l_z);		// (x | y | z)
		//clause(id_transformation, l_nx, l_ny, l_z);		// (~x | ~y | z)
		//clause(id_transformation, l_nx, l_y,  l_nz);	// (~x | y | ~z)
		//clause(id_transformation, l_x,  l_ny, l_nz);	// (x | ~y | ~z)

		//(X <=> Y) => Z
		clause(id_transformation, l_x,  l_y,  l_z);		// (x | y | z)
		clause(id_transformation, l_nx, l_ny, l_z);		// (~x | ~y | z)
	}
}
*/

// ==========================================================================
// WIP: IFTHEN to disjunction
/* ==========================================================================

void GRAPH::ifthen_to_disjunction(unsigned int id, std::vector<unsigned int>& a, int b, unsigned int c, unsigned int n_c)
{
    unsigned int int_value = static_cast<unsigned int>(abs(b));
    if (b < 0) // Pour une valeur négative du domaine, on prend le complément à deux
    {
        int_value = ~int_value;
        int_value += 1;
    }
    std::vector<unsigned int> var_aux;
    std::vector<unsigned int> var_n_aux;
    for (unsigned int i = 0; i < a.size(); ++i)
    {
        // Création d'une variable auxiliaire
        unsigned int aux = _first_free_id++;
        unsigned int n_aux = _first_free_id++;
        vertice(aux, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
        vertice(n_aux, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
        vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
        edge(id, _first_free_id-1, -1);
        edge(id, _first_free_id-3, -1);
        edge(_first_free_id-1, aux, -1);
        edge(_first_free_id-1, n_aux, -1);
        var_aux.push_back(aux);
        var_n_aux.push_back(n_aux);

        chr::Logical_var< unsigned int > l_neg_a;
        get_opposite_var(a[i], l_neg_a);
        assert(l_neg_a.ground());
        unsigned int bit_value = int_value & 0x01;
        if (bit_value == 1)
        {
            clause(id,l_neg_a,n_aux);// (a!=b<-c)
            clause(id,a[i],aux);// (a!=b->c)
        }
        else
        {
            clause(id,a[i],n_aux);// (a!=b<-c)
            clause(id,l_neg_a,aux);// // (a!=b->c)
        }
        int_value >>= 1;
    }

    //std::cout << "var_aux   : " << var_aux.capacity() << "\t";
    //std::cout << "var_n_aux : " << var_n_aux.capacity() << std::endl;

    // clauses contenant tous les resultats des aux
    unsigned int id_disjunction = _first_free_id++;
    vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain());
    edge(id, id_disjunction, -1);
    edge(id_disjunction, n_c, -1);
    for (unsigned int i = 0; i < var_aux.size(); ++i)
    {
        edge(id_disjunction, var_aux[i], -1);
        clause(id,var_n_aux[i],c);
    }

    if (((b<0) && (int_value == 0)) || ((b>=0) && (int_value == 1)))
    std::cerr <<"assert(b<0?int_value==1:int_value==0)" << std::endl;
    assert(b<0?int_value==1:int_value==0);
}

*/



// ===========================================================================
// DIFF to_int_disjunction ?????
// ===========================================================================
/*
void GRAPH::diff_to_int_disjunction(unsigned int id, std::vector<unsigned int> a, int b, unsigned int c, unsigned int n_c)
{
    unsigned int int_value = static_cast<unsigned int>(abs(b));
    if (b < 0) // Pour une valeur négative du domaine, on prend le complément à deux
    {
        int_value = ~int_value;
        int_value += 1;
    }
    std::vector<unsigned int> var_aux;
    std::vector<unsigned int> var_n_aux;
    for (unsigned int i = 0; i < a.size(); ++i)
    {
        // Création d'une variable auxiliaire
        unsigned int aux = _first_free_id++;
        unsigned int n_aux = _first_free_id++;
        vertice(aux, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
        vertice(n_aux, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
        vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
        edge(id, _first_free_id-1, -1);
        edge(id, _first_free_id-3, -1);
        edge(_first_free_id-1, aux, -1);
        edge(_first_free_id-1, n_aux, -1);
        var_aux.push_back(aux);
        var_n_aux.push_back(n_aux);

        chr::Logical_var< unsigned int > l_neg_a;
        get_opposite_var(a[i], l_neg_a);
        assert(l_neg_a.ground());
        unsigned int bit_value = int_value & 0x01;
        if (bit_value == 1)
        {
            clause(id,l_neg_a,n_aux);	// (c => a!=b)
            clause(id,a[i],aux);		// (a!=b => c)
        }
        else
        {
            clause(id,a[i],n_aux);	// (c => a!=b)
            clause(id,l_neg_a,aux);	// (a!=b => c)
        }
        int_value >>= 1;
    }

    // clauses contenant tous les resultats des aux
    unsigned int id_disjunction = _first_free_id++;
    vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain());
    edge(id, id_disjunction, -1);
    edge(id_disjunction, n_c, -1);
    for (unsigned int i = 0; i < var_aux.size(); ++i)
    {
        edge(id_disjunction, var_aux[i], -1);
        clause(id,var_n_aux[i],c);
    }

    if (((b<0) && (int_value == 0)) || ((b>=0) && (int_value == 1)))
    std::cerr <<"assert(b<0?int_value==1:int_value==0)" << std::endl;
    assert(b<0?int_value==1:int_value==0);
}
*/

// ===========================================================================
// ELEMENT to DISJUNCTION
// ===========================================================================
/*
void GRAPH::transform_element_to_disjunction(unsigned int id)
{
    //std::cout << "GRAPH::transform_element_to_disjunction(" << id << ")" << std::endl ;

    // Vérifie que la transformation n'existe pas déjà
    chr::Logical_var< unsigned int > l_id_trans;
    get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::ELEMENT | Flag::BINARY_DISJUNCTION), l_id_trans);
	if (l_id_trans.ground())
		return;

	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::ELEMENT | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	// Récupération des fils de type VARIABLE de la relation
	chr::Logical_var_mutable< std::vector< unsigned int > > l_relation_vars;
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER), 0, l_relation_vars);
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), 0, l_relation_vars);
	assert((*l_relation_vars).size() >= 3);

	// Element (X, index, Tab)
	// Vérifie pour chaque variable de la relation
	// que la transformation de la variable entière en booléenne existe
	std::vector< unsigned int > transformations_for_integer_variables;
	for (unsigned int i=0; i < (*l_relation_vars).size(); ++i)
	{
		chr::Logical_var< unsigned int > l_id_trans_boolean;
		get_child_with_flags((*l_relation_vars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		if (!l_id_trans_boolean.ground())
		{
			build_log_encoding_from((*l_relation_vars)[i]);
			get_child_with_flags((*l_relation_vars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		}
		assert(l_id_trans_boolean.ground());
		transformations_for_integer_variables.push_back(*l_id_trans_boolean);
	}

	//std::cout << "transformations_for_integer_variables : " << transformations_for_integer_variables.capacity() << "\t";

	// Récupération des variables booléennes et du domaine de la première variable entière : X
	chr::Logical_var_mutable< std::vector< unsigned int > > var_x;
	get_all_children_with_flags_ordered(transformations_for_integer_variables[0], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, var_x);
	get_all_children_with_flags_ordered(transformations_for_integer_variables[0], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, var_x);

	// Récupération des variables booléennes et du domaine de la seconde variable entière : index
	chr::Logical_var_mutable< std::vector< unsigned int > > var_index;
	get_all_children_with_flags_ordered(transformations_for_integer_variables[1], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, var_index);
	get_all_children_with_flags_ordered(transformations_for_integer_variables[1], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, var_index);

	chr::Logical_var< Flags > l_flags;
	chr::Logical_var< std::string > l_label;
	chr::Logical_var< Domain > l_domain_index;
	get_vertice((*l_relation_vars)[1],l_flags,l_label,l_domain_index);
	const IntVarDomain& domain_index = dynamic_cast<const IntVarDomain&>(*l_domain_index);

	// Récupération des boolean variables et du domaine de la troisième variable entière : tableau
	std::vector<chr::Logical_var_mutable<std::vector< unsigned int > >>var_tab;
	var_tab.resize(transformations_for_integer_variables.size()-2);
	for (unsigned int i=2; i < transformations_for_integer_variables.size(); ++i)
	{
	  get_all_children_with_flags_ordered(transformations_for_integer_variables[i], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, var_tab[i-2]);
	  get_all_children_with_flags_ordered(transformations_for_integer_variables[i], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, var_tab[i-2]);
	}

	// error case
	if ((domain_index.min() >= static_cast <int> (var_tab.size())) ||           //toutes les valeurs de l'index sont supérieures à la taille du tableau
        (domain_index.max() < 0))                                               //toutes les valeurs de l'index sont inférieures à 0
	{
        // auxiliary var
        unsigned int aux_index_i   = _first_free_id++;
        unsigned int n_aux_index_i = _first_free_id++;
        vertice(aux_index_i, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
        vertice(n_aux_index_i, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
        vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
        edge(id_transformation, _first_free_id-1, -1);
        edge(id_transformation, _first_free_id-3, -1);
        edge(_first_free_id-1, aux_index_i, -1);
        edge(_first_free_id-1, n_aux_index_i, -1);
        clause(id_transformation,aux_index_i);
        clause(id_transformation,n_aux_index_i);
        return;
	}

	// suppression des index supérieur à la taille du tableau
	if ((unsigned int) domain_index.max() >= var_tab.size())
	{
		unsigned int aux_integer = _first_free_id++;
		vertice(aux_integer, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, IntVarDomain({static_cast<int> (var_tab.size())}) ); num_variables++;
		unsigned int aux_relation = _first_free_id++;
		vertice(aux_relation, Flags(Flag::RELATION | Flag::GREATER_THAN), N_STR, IntVarDomain() ); num_relations++;
		edge(aux_relation,aux_integer,0);
		edge(aux_relation,(*l_relation_vars)[1],1);
		transform_greater_than_to_disjunction(aux_relation);
	}

	// suppression des index inférieurs à 0
	if (domain_index.min() < 0)
	{
		unsigned int aux_integer = _first_free_id++;
		vertice(aux_integer, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, IntVarDomain({-1}) ); num_variables++;
		unsigned int aux_relation = _first_free_id++;
		vertice(aux_relation, Flags(Flag::RELATION | Flag::GREATER_THAN), N_STR, IntVarDomain() ); num_relations++;
		edge(aux_relation,(*l_relation_vars)[1],0);
		edge(aux_relation,aux_integer,1);
		transform_greater_than_to_disjunction(aux_relation);
	}


	for (unsigned int i=0; i<var_tab.size(); ++i)
	{
		// index != i
		unsigned int aux_index_i = _first_free_id++;
		unsigned int n_aux_index_i = _first_free_id++;
		vertice(aux_index_i, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
		vertice(n_aux_index_i, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
		vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, _first_free_id-1, -1);
		edge(id_transformation, _first_free_id-3, -1);
		edge(_first_free_id-1, aux_index_i, -1);
		edge(_first_free_id-1, n_aux_index_i, -1);
		diff_to_int_disjunction(id_transformation,var_index,i,aux_index_i,n_aux_index_i);

		// var_x = var_tab[i]
		unsigned int aux_x_tab = _first_free_id++;
		unsigned int n_aux_x_tab = _first_free_id++;
		vertice(aux_x_tab, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
		vertice(n_aux_x_tab, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
		vertice(_first_free_id++, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, _first_free_id-1, -1);
		edge(id_transformation, _first_free_id-3, -1);
		edge(_first_free_id-1, aux_x_tab, -1);
		edge(_first_free_id-1, n_aux_x_tab, -1);
		equal_to_disjunction(id_transformation,var_x,var_tab[i],aux_x_tab,n_aux_x_tab);

		// index!=i OR var_x=var_tab[i] (pour index==i -> var_x=var_tab[i])
		clause(id_transformation,aux_index_i,aux_x_tab);
	}
}
*/






// ===========================================================================
// IF_THEN_GREATER to DISJUNCTION
// ITG(var, array)
// ===========================================================================
/*
void GRAPH::transform_IfThenGreater_toCNF(unsigned int id)
{
    // Check if transformation already exists
    chr::Logical_var< unsigned int > l_id_trans;
    get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::IFTHEN_GREATER | Flag::BINARY_DISJUNCTION), l_id_trans);
	if (l_id_trans.ground())
		return;

	// Create transformation
	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::IF_THEN_NOT | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	// Get relation's variables
	chr::Logical_var_mutable< std::vector<unsigned int> > l_rvars;
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER), 0, l_rvars);
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), 0, l_rvars);
	assert((*l_rvars).size() >= 2);

	// If needed, encode variables; then, get the transformations' IDs.
	std::vector<unsigned int> var_transformation;
	for (unsigned int i=0; i < (*l_rvars).size(); ++i)
	{
		chr::Logical_var< unsigned int > l_id_trans_boolean;
		get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		if (!l_id_trans_boolean.ground())
		{
			build_log_encoding_from((*l_rvars)[i]);
			get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		}
		assert(l_id_trans_boolean.ground());
		var_transformation.push_back(*l_id_trans_boolean);
	}

	// TODO: FOR EACH BIT... (like this, it's more suitable for Direct Encoding)

	// x-literals
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_x;
	get_all_children_with_flags_ordered(var_transformation[0], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_x);
	get_all_children_with_flags_ordered(var_transformation[0], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_x);
	assert((*l_boolvars_x).size() > 0);

	unsigned int l_x = (*l_boolvars_x)[0];
	chr::Logical_var< unsigned int> l_nx;
	get_opposite_var(l_x, l_nx);
	assert(l_nx.ground());

	// y-literals
	for (unsigned int i=1; i < (*l_rvars).size(); ++i) {
		chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_y;
		get_all_children_with_flags_ordered(var_transformation[i], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_y);
		get_all_children_with_flags_ordered(var_transformation[i], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_y);
		assert((*l_boolvars_y).size() > 0);

		unsigned int l_y = (*l_boolvars_y)[0];
		chr::Logical_var< unsigned int> l_ny;
		get_opposite_var(l_y, l_ny);
		assert(l_ny.ground());

		clause(id_transformation, l_nx, l_ny);	// (~x | ~y)
	}
}
*/



// ===========================================================================
// CARDINALITY (with Sequential counter)
// cardinality_type: 0=equal, 1=lessthan
//
// SUM(Xi) = D
// Xi: Integer variables
// D : Integer (it's a constant, but declared as variable, so, always AUXILIARY)
//
// CARDINALITY(D,x1,x2,..,xn)
// ===========================================================================
/*
void GRAPH::transform_cardinality_to_cnf(unsigned int id, unsigned int cardinality_type)
{
	//std::cout << "GRAPH::transform_cardinality_to_cnf(" << id << ")" << std::endl;

    // Check transformation; if it does not exist, create it
  	chr::Logical_var< unsigned int > l_check;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::CARDINALITY | Flag::BINARY_DISJUNCTION), l_check);
  	if (l_check.ground())
		return;

	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::CARDINALITY | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	// Relation's variables (l_rvars)
	// TODO: replace INTEGER by CSPBOOLEAN
	//       (useful to differentiate value (D) from variables (Xi) )
	// TODO: The swap of AUXILIARY and DECISION call should be temporary
	chr::Logical_var_mutable< std::vector<unsigned int> > l_rvars;
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), 0, l_rvars);
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER), 0, l_rvars);
	assert((*l_rvars).size() > 1);

	// If needed, encode variables; then, get the transformations' IDs.
	std::vector<unsigned int> var_transformation;
	for (unsigned int i=0; i< (*l_rvars).size(); ++i)
	{
		chr::Logical_var< unsigned int > l_id_trans_boolean;
		get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		if (!l_id_trans_boolean.ground())
		{
			build_log_encoding_from((*l_rvars)[i]);
			get_child_with_flags((*l_rvars)[i], Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		}
		assert(l_id_trans_boolean.ground());
		var_transformation.push_back(*l_id_trans_boolean);
	}


	// demand-value
	chr::Logical_var< Flags >       ld_flags;
	chr::Logical_var< std::string > ld_label;
	chr::Logical_var< Domain >      ld_domain;
	get_vertice((*l_rvars)[0], ld_flags, ld_label, ld_domain);
	unsigned int d = (*ld_domain).max();
	assert(d > 0);

	// literals (X)
	std::vector<unsigned int> xpos, xneg;
	for (unsigned int i=1; i<var_transformation.size(); i++) {
		chr::Logical_var_mutable< std::vector<unsigned int> > lx_boolvars;
		get_all_children_with_flags_ordered(var_transformation[i], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, lx_boolvars);
		get_all_children_with_flags_ordered(var_transformation[i], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, lx_boolvars);
		assert((*lx_boolvars).size() > 0);

		unsigned int lx = (*lx_boolvars)[0];	// ENCODING: using only the first bit
		chr::Logical_var< unsigned int> lnx;
		get_opposite_var(lx, lnx);
		assert(lnx.ground());
		xpos.push_back(lx);
		xneg.push_back(lnx);
	}

	// auxiliary variables (S)
	unsigned int n = xpos.size();
	std::vector< std::vector<unsigned int> > spos;
	std::vector< std::vector<unsigned int> > sneg;

	for (unsigned int i=0; i<=n; i++) {
		std::vector<unsigned int> srowpos, srowneg;

		for(unsigned int j=0; j<(d+2); j++) {
			unsigned int s    = _first_free_id++;
			unsigned int ns   = _first_free_id++;
			unsigned int bnot = _first_free_id++;      // binary not
			vertice(s, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
			vertice(ns, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), N_STR, IntVarDomain( {0, 1} )); num_variables++;
			vertice(bnot, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;

			edge(id_transformation, bnot, -1);
			edge(id_transformation, s, -1);
			edge(bnot, s, -1);
			edge(bnot, ns, -1);

			srowpos.push_back(s);
			srowneg.push_back(ns);
		}
		spos.push_back(srowpos);
		sneg.push_back(srowneg);
	}

//	// TEST ----------------------------
//	std::cout << "X, S :"  << std::endl;
//	for (unsigned int i=0; i<=n; i++) {
//
//		if (i<n)
//			std::cout << "(" << xpos[i] << "," << xneg[i] << ")\t" ;
//		else
//			std::cout << "\t\t";
//
//		for(unsigned int j=0; j<=(d+1); j++) {
//			std::cout << "(" << spos[i][j] << "," << sneg[i][j] << ")  ";
//		}
//		std::cout << std::endl;
//	}
//	std::cout << std::endl;


	// clauses
	for(unsigned int i=1; i<=n; i++) {
		for(unsigned int j=0; j<(d+2); j++) {

			clause(id_transformation, sneg[i-1][j], spos[i][j]);
			clause(id_transformation, xpos[i-1],    sneg[i][j], spos[i-1][j]);

//			std::cout << i << "," << j << ":\t";
//			std::cout << sneg[i-1][j] << "," << spos[i][j] << "\t";
//			std::cout << xpos[i-1]    << "," << sneg[i][j] << "," << spos[i-1][j] << "\t";

			if (j>0) {
				clause(id_transformation, sneg[i][j], spos[i-1][j-1]);
				clause(id_transformation, xneg[i-1],  sneg[i-1][j-1], spos[i][j]);

//				std::cout << sneg[i][j] << "," << spos[i-1][j-1] << "\t";
//				std::cout << xneg[i-1]  << "," << sneg[i-1][j-1] << "," << spos[i][j] ;
			}
//			std::cout << std::endl;
		}
	}

	// unitary clauses
	clause(id_transformation, spos[0][0]);
	clause(id_transformation, sneg[0][1]);
	if (cardinality_type==0) {					//equality
        clause(id_transformation, spos[n][d]);
	}
	clause(id_transformation, sneg[n][d+1]);

    //std::cout << "--------------- ----------------------------------------" << std::endl;
}
*/







// ==================================================================
// Decompose linear (eq) sequential
//
//   Decomposes a LINEAR constraint into a series of simple
//   additions in the form A+B=Z, using a sequential schema.
//
// Constraint:
//   X1 + X2 + X3 + X4 + X5 = Y
//   LINEAR_EQ_SEQ(Y, X1, X2, X3, X4, X5)
//
// Decomposition:
//   X1+X2=A1; A1+X3=A2; A2+X4=A3; A3+X5=Y;
//   ADD_EQUAL(X1,X2,A1); ADD_EQUAL(A1,X3,A2) ...
// ==================================================================

void GRAPH::decompose_linear_seq(unsigned int rid)
{
	std::cout << std::endl << "Graph::decompose_linear_seq(" << rid << ") " << std::endl;

	// check if transformation already exists
	chr::Logical_var< unsigned int > l_check;
	get_child_with_flags(rid, Flags(Flag::TRANSFORMATION | Flag::LINEAR_EQ_SEQ | Flag::ADD_EQUAL), l_check);
	if (l_check.ground())
		return;

	// check the right number of variables (3+)
	chr::Logical_var_mutable< std::vector<unsigned int> > check_rvars;
	get_all_children(rid, check_rvars);
	assert((*check_rvars).size() > 2);

	// create transformation node
	unsigned int id_transformation = _first_free_id++;
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::LINEAR_EQ_SEQ | Flag::ADD_EQUAL), N_STR, IntVarDomain()); num_transformations++;
	edge(rid, id_transformation, -1);

	// get relation variables (ordered) -----------------------------

	std::vector<unsigned int> relation_vars;
	for (unsigned int i=0; i<(*check_rvars).size(); ++i)
	{
		chr::Logical_var<unsigned int> rvar;
		get_child_with_order(rid, i , rvar);
		assert(rvar.ground());

		chr::Logical_var<Flags>       flags;
		chr::Logical_var<std::string> label;
		chr::Logical_var<Domain>      domain;
		get_vertice(rvar, flags,label,domain);

		// check variables are type-valid
		bool c1 = check(flags,Flags(Flag::VARIABLE | Flag::INTEGER));
		bool c2 = check(flags,Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY));
		bool c3 = check(flags,Flags(Flag::VARIABLE | Flag::PROP_INTEGER));
		assert(c1 || c2 || c3);
		relation_vars.push_back((*rvar));
	}

	// get result (variable Y) domain
	int result_dmin = 0;
	int result_dmax = 0;
	{
		chr::Logical_var<Flags>       yflags;
		chr::Logical_var<std::string> ylabel;
		chr::Logical_var<Domain>      ydomain;
		get_vertice(relation_vars[0], yflags, ylabel, ydomain);
		result_dmin = (*ydomain).min();
		result_dmax = (*ydomain).max();
	}


	// decompose constraint (a+b=z) ---------------------------------

	unsigned int a = relation_vars[1];
	for (unsigned int i=2; i<relation_vars.size(); ++i)
	{
		unsigned int b = relation_vars[i];

		unsigned int z;
		int zmin, zmax;
		if (i == relation_vars.size()-1) {
			// use the right-side of linear constraint (first node)
			z = relation_vars[0];
			zmin = result_dmin;
			zmax = result_dmax;
		} else {
			// create a new auxiliary variable; use Dom(a+b) as Dom(z)
			// (domain can not go further the result's domain)
			chr::Logical_var<Flags>       a_flags,  b_flags;
			chr::Logical_var<std::string> a_label,  b_label;
			chr::Logical_var<Domain>      a_domain, b_domain;
			get_vertice(a, a_flags, a_label, a_domain);
			get_vertice(b, b_flags, b_label, b_domain);
			zmin = (*a_domain).min() + (*b_domain).min();
			zmax = (*a_domain).max() + (*b_domain).max();
			zmax = std::min(zmax,result_dmax);

			IntVarDomain z_domain;
			for (int v=zmin; v<=zmax; ++v)
				z_domain.domain.push_back(v);

			z = _first_free_id++;
			vertice(z, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, z_domain); num_variables++;
		}

		// new addition
		unsigned int addition = _first_free_id++;
		vertice(addition, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, addition, -1);
		edge(addition, a, 0);
		edge(addition, b, 1);
		edge(addition, z, 2);
		std::cout << addition << ":\t" << a << " + " << b << " = " << z ;
		std::cout << "\t[" << zmin << "," << zmax << "] ";
		std::cout << std::endl;

		// update A (to continue sequence)
		a = z;
	}
}



// ==================================================================
// Decompose linear (eq) arborescent
//
//   Decomposes a LINEAR constraint into a series of simple
//   additions in the form A+B=Z, using an arborescent schema.
//
// Constraint:
//   X1 + X2 + X3 + X4 + X5 = Y
//   LINEAR_EQ_ARB(Y, X1, X2, X3, X4, X5)
//
// Decomposition:
//   X1+X2=A1; X3+X4=A2; A1+A2=A3; X5+A3=Y;
//   ADD_EQUAL(X1,X2,A1); ADD_EQUAL(X3,X4,A2) ...
// ==================================================================

void GRAPH::decompose_linear_arb(unsigned int rid)
{
	std::cout << std::endl << "Graph::decompose_linear_arb(" << rid << ") " << std::endl;

	// check if transformation already exists
	chr::Logical_var< unsigned int > l_check;
	get_child_with_flags(rid, Flags(Flag::TRANSFORMATION | Flag::LINEAR_EQ_SEQ | Flag::ADD_EQUAL), l_check);
	if (l_check.ground())
		return;

	// check the right number of variables (3+)
	chr::Logical_var_mutable< std::vector<unsigned int> > check_rvars;
	get_all_children(rid, check_rvars);
	assert((*check_rvars).size() > 3);

	// create transformation node
	unsigned int id_transformation = _first_free_id++;
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::LINEAR_EQ_SEQ | Flag::ADD_EQUAL), N_STR, IntVarDomain()); num_transformations++;
	edge(rid, id_transformation, -1);

	// get relation variables (ordered) -----------------------------

	std::vector<unsigned int> relation_vars;
	for (unsigned int i=0; i<(*check_rvars).size(); ++i)
	{
		chr::Logical_var<unsigned int> rvar;
		get_child_with_order(rid, i , rvar);
		assert(rvar.ground());

		chr::Logical_var<Flags>       flags;
		chr::Logical_var<std::string> label;
		chr::Logical_var<Domain>      domain;
		get_vertice(rvar, flags,label,domain);

		// check variables are type-valid
		bool c1 = check(flags,Flags(Flag::VARIABLE | Flag::INTEGER));
		bool c2 = check(flags,Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY));
		bool c3 = check(flags,Flags(Flag::VARIABLE | Flag::PROP_INTEGER));
		assert(c1 || c2 || c3);
		relation_vars.push_back((*rvar));
	}

	// get result (variable Y) domain
	int result_dmin = 0;
	int result_dmax = 0;
	{
		chr::Logical_var<Flags>       yflags;
		chr::Logical_var<std::string> ylabel;
		chr::Logical_var<Domain>      ydomain;
		get_vertice(relation_vars[0], yflags, ylabel, ydomain);
		result_dmin = (*ydomain).min();
		result_dmax = (*ydomain).max();
	}


	// decompose constraint (x1+x2=r) ---------------------------------

	std::queue<unsigned int> qdec;	// queue for decision vars
	std::queue<unsigned int> qaux;	// queue for auxiliary vars

	for (unsigned int i=2; i<relation_vars.size(); ++i)
		qdec.push(relation_vars[i]);

	while (qdec.size()>0 || qaux.size()>0)
	{
		int x1, x2;
		int r = relation_vars[0];

		// addends + result
		if (qdec.size() > 1) {
			std::cout << "case 1\t";
            x1 = qdec.front(); qdec.pop();
            x2 = qdec.front(); qdec.pop();
            r = _first_free_id++;
		}
		else if (qdec.size() == 1) {
			std::cout << "case 2\t";
            x1 = qdec.front(); qdec.pop();
            x2 = qaux.front(); qaux.pop();

			if (qaux.size() > 0) {
                r = _first_free_id++;
			}
		} else {
			std::cout << "case 3\t";
            x1 = qaux.front(); qaux.pop();
            x2 = qaux.front(); qaux.pop();

            if (qaux.size() > 1)
                r = _first_free_id++;
		}

		// domain
		chr::Logical_var<Flags>       x1_flags;
		chr::Logical_var<std::string> x1_label;
		chr::Logical_var<Domain>      x1_domain;
		get_vertice(x1, x1_flags, x1_label, x1_domain);
		int x1min = (*x1_domain).min();
		int x1max = (*x1_domain).max();

		chr::Logical_var<Flags>       x2_flags;
		chr::Logical_var<std::string> x2_label;
		chr::Logical_var<Domain>      x2_domain;
		get_vertice(x2, x2_flags, x2_label, x2_domain);
		int x2min = (*x2_domain).min();
		int x2max = (*x2_domain).max();

		int rmin, rmax;
		if (result_dmin < result_dmax) {
			rmin = std::max((x1min+x2min), result_dmin);
			rmax = std::min((x1max+x2max), result_dmax);
		} else {
			rmin = x1min+x2min;
			rmax = std::min((x1max+x2max), result_dmax);
		}

		IntVarDomain new_aux_domain;
		for (int k=rmin; k<=rmax; k++)
			new_aux_domain.domain.push_back(k);

		// new auxiliary variable
		if (r != relation_vars[0]) {
			vertice(r, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, new_aux_domain); num_variables++;
            qaux.push(r);
		}

		// addition
		unsigned int addition = _first_free_id++;
		vertice(addition, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); num_relations++;
		edge(addition, x1, 0);
		edge(addition, x2, 1);
		edge(addition, r, 2);
		std::cout << addition << ": " << x1 << " + " << x2 << " = " << r << "\t";
		std::cout << "[" <<  x1min << "," << x2max << "] + ";
		std::cout << "[" <<  x2min << "," << x2max << "] = ";
		std::cout << "[" <<  rmin << "," << rmax << "] " << std::endl;

	}

}
