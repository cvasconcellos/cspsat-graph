/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

#include <graph_chr_header.hpp>
#include <ostream>
#include <fstream>
#include <unordered_map>
#include <time.h>

#define N_STR std::string()


// ==================================================================
// PRINT STORE
// ==================================================================

void GRAPH::print_store(std::ostream& s_out)
{
	auto it = chr_store_begin();
	while (!it.at_end())
	{
		s_out << it.string() << std::endl;
		++it;
	}
}


// ==================================================================
// PRINT SOLUTION STORE
// ==================================================================

void GRAPH::print_csp_solution(std::string filename)
{
	std::cout << std::endl << "print_solution_store()" << std::endl;

	filename += ".cspsol";
	std::ofstream f;
	f.open(filename);

	std::cout << "VAR\tVALUE" << std::endl;
	f << "VAR\tVALUE" << std::endl;
	for (auto& node : get_vertice_store())
	{
		if ( ((*node._p3).size()==1) && (!check(*node._p1, Flag::BOOLEAN))) {
			std::cout << *node._p0 << "\t" << (*node._p3).min() << std::endl;
			f << *node._p0 << "\t" << (*node._p3).min() << std::endl;
		}
	}
	f.close();
}



// ==================================================================
// PRINT VARIABLE PATH
// ==================================================================

void GRAPH::print_variable_path(std::ostream& s_out, std::unordered_map<unsigned int,unsigned int> & map_cnf_graph_ids)
{
	std::cout << std::endl << "print_variable_path()" << std::endl;
	assert (map_cnf_graph_ids.size()>0);
	s_out << "NAME\tCNF\tVAR1\tTYPE1\tVAR2\tTYPE2\tVAR3\tTYPE3\tVAR4\tTYPE4\tVAR5\tTYPE5" << std::endl;

	for (auto& id : map_cnf_graph_ids)
	{
		// node information
		chr::Logical_var<Flags>       flags;
		chr::Logical_var<std::string> label;
		chr::Logical_var<Domain>      domain;
		get_vertice(id.second, flags, label, domain);

        if ( N_STR.compare(label) == 0 ) {
            s_out << "VAR" << "\t" << id.first << "\t" << id.second << "\t";	// cnf, graph
        } else {
            s_out << label << "\t" << id.first << "\t" << id.second << "\t";	// cnf, graph
        }
		if (check(*flags, Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY))	s_out << "BooleanAux\t";
		else																	s_out << "Boolean\t";

		// go back until find the root
		int node_id = id.second;
		while (node_id > -1) {

			chr::Logical_var<unsigned int> child(node_id);
			chr::Logical_var<unsigned int> parent;
			get_parent_node(child, parent);
			// to use get_all_parents(), the precedence is: transformation -> variable -> relation

			if (parent.ground()) {
				chr::Logical_var<Flags>       flags;
				chr::Logical_var<std::string> label;
				chr::Logical_var<Domain>      domain;
				get_vertice(parent, flags, label, domain);

				s_out << parent << "\t";
				if (check(*flags, Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY))					s_out << "BooleanAux\t";
				else if (check(*flags, Flag::VARIABLE | Flag::BOOLEAN))									s_out << "Boolean\t";
				else if (check(*flags, Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY))				s_out << "IntegerAux\t";
				else if (check(*flags, Flag::VARIABLE | Flag::INTEGER))									s_out << "Integer\t";
				else if (check(*flags, Flag::VARIABLE | Flag::PROP_INTEGER))							s_out << "PropInteger\t";

				else if (check(*flags, Flag::RELATION | Flag::PROP_ADD_EQUAL))							s_out << "r_PropAddEqual\t";
				else if (check(*flags, Flag::RELATION | Flag::ADD_EQUAL))								s_out << "r_AddEqual\t";
				else if (check(*flags, Flag::RELATION | Flag::ADD_SUB_EQUAL))							s_out << "r_AddSubEqual\t";
				else if (check(*flags, Flag::RELATION | Flag::SUB_EQUAL))								s_out << "r_SubEqual\t";
				else if (check(*flags, Flag::RELATION | Flag::MULT_EQUAL))								s_out << "r_MultEqual\t";
				else if (check(*flags, Flag::RELATION | Flag::LESS_THAN))								s_out << "r_LessThan\t";
				else if (check(*flags, Flag::RELATION | Flag::LESS_EQUAL_THAN))							s_out << "r_LessEqual\t";
				else if (check(*flags, Flag::RELATION | Flag::GREATER_THAN))							s_out << "r_GreaterThan\t";
				else if (check(*flags, Flag::RELATION | Flag::GREATER_EQUAL_THAN))						s_out << "r_GreaterEqual\t";
				else if (check(*flags, Flag::RELATION | Flag::ALL_DIFFERENT))							s_out << "r_AllDifferent\t";
				else if (check(*flags, Flag::RELATION | Flag::BINARY_DIFF))		    					s_out << "r_BinaryDiff\t";
				else if (check(*flags, Flag::RELATION | Flag::CARDINALITY))								s_out << "r_Cardinality\t";
				else if (check(*flags, Flag::RELATION | Flag::CARDINALITY_LEQ))							s_out << "r_CardinalityLEQ\t";
				else if (check(*flags, Flag::RELATION | Flag::IF_THEN))							        s_out << "r_IfThen\t";
				else if (check(*flags, Flag::RELATION | Flag::IF_THEN_NOT))							    s_out << "r_IfThenNot\t";
				else if (check(*flags, Flag::RELATION | Flag::IF_THEN_OR))							    s_out << "r_IfThenOR\t";
				else if (check(*flags, Flag::RELATION | Flag::IF_EQUAL_THEN))					        s_out << "r_IfEqualThen\t";
				else if (check(*flags, Flag::RELATION | Flag::INTERVAL_BRANCHING))						s_out << "r_IntervalBranching\t";
				else if (check(*flags, Flag::RELATION | Flag::BINARY_DISJUNCTION))						s_out << "r_Disjunction\t";
				else if (check(*flags, Flag::RELATION | Flag::BINARY_NOT))						        s_out << "r_LiteralNot\t";

				else if (check(*flags, Flag::TRANSFORMATION | Flag::INTEGER | Flag::VAR_INTERVAL))		s_out << "t_Interval\t";
				else if (check(*flags, Flag::TRANSFORMATION | Flag::INTEGER | Flag::DIRECT_ENCODING))	s_out << "tcnf_Direct\t";
				else if (check(*flags, Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING))		s_out << "tcnf_Log\t";

				else if (check(*flags, Flag::TRANSFORMATION | Flag::PROP_ADD_EQUAL | Flag::ADD_EQUAL))	s_out << "t_PropAddition\t";
				else if (check(*flags, Flag::TRANSFORMATION | Flag::INTERVAL_BRANCHING | Flag::BINARY_DISJUNCTION))	s_out << "tcnf_IB\t";
				else																					s_out << "NA\t";

				node_id = parent;
			} else {
				node_id = -1;
			}
		}
		s_out << std::endl;
	}
}

void GRAPH::print_variable_path(std::string filename, std::unordered_map<unsigned int,unsigned int> & map_cnf_graph_ids)
{
	filename += ".varpath";
	std::ofstream f;
	f.open(filename);
	print_variable_path(f,map_cnf_graph_ids);
	f.close();
}



// ==================================================================
// PRINT CNF
// ==================================================================

//void GRAPH::print_cnf(std::ofstream& s_out, std::unordered_map<unsigned int,unsigned int> & map_cnf_graph_ids, bool rescale_variable_ids)
void GRAPH::print_cnf(std::string filename, std::unordered_map<unsigned int,unsigned int> & map_cnf_graph_ids, bool rescale_variable_ids)
{
	time_t my_time = time(NULL);
	std::cout << "print_cnf() : start\t\t" << ctime(&my_time);

	unsigned int nb_clauses        = 0;
	unsigned int nb_decision_vars  = 1;
	unsigned int nb_auxiliary_vars = 1;
	unsigned int max_id_var        = 0;
	std::unordered_map< unsigned int, unsigned int > mapped_decision_variable_ids;
	std::unordered_map< unsigned int, unsigned int > mapped_auxiliary_variable_ids;

	// file
	std::ofstream s_out;
	s_out.open(filename);

	for (auto& c : get_vertice_store())
	{
		if (check(*c._p1, Flag::RELATION | Flag::BINARY_DISJUNCTION))
		{
			++nb_clauses;
			unsigned int id_disjunction = *c._p0;
			chr::Logical_var_mutable< std::vector< unsigned int > > Ret_ids;
			get_all_children(id_disjunction, Ret_ids);

			for (auto& e : *Ret_ids)
			{
				chr::Logical_var< unsigned int > l_id;
				get_parent_node_with_flags(e, Flags(Flag::TRANSFORMATION), l_id);
				if (l_id.ground())
				{
					if (!rescale_variable_ids)
						s_out << e << " ";
					max_id_var = std::max(max_id_var,e);
					if (rescale_variable_ids)
					{
						// node information
						chr::Logical_var< Flags >       R_flags;
						chr::Logical_var< std::string > R_label;
						chr::Logical_var< Domain >      R_domain;
						get_vertice(e, R_flags, R_label, R_domain);

						assert( R_flags.ground() );
						if (check(*R_flags, Flag::BOOLEAN | Flag::AUXILIARY)) {
							if (mapped_auxiliary_variable_ids.insert( {e, nb_auxiliary_vars }).second)
								nb_auxiliary_vars++;
						} else {
							if (mapped_decision_variable_ids.insert( {e, nb_decision_vars }).second)
								nb_decision_vars++;
						}
					}

				} else {
					chr::Logical_var< unsigned int > l_id_pos;
					get_opposite_var(e, l_id_pos);
					if (!l_id_pos.ground()) std::cerr << "Negative of vertex " << e << " not found" << std::endl;
					unsigned int id_pos = *l_id_pos;

					if (!rescale_variable_ids)
						s_out << "-" << id_pos << " ";
					max_id_var = std::max(max_id_var,id_pos);

					if (rescale_variable_ids)
					{
						// Retrieve vertex
						chr::Logical_var<Flags>       R_flags;
						chr::Logical_var<std::string> R_label;
						chr::Logical_var<Domain>      R_domain;

						get_vertice(id_pos, R_flags, R_label, R_domain);
						assert( R_flags.ground() );
						if (check(*R_flags, Flag::BOOLEAN | Flag::AUXILIARY)) {
							if (mapped_auxiliary_variable_ids.insert( {id_pos, nb_auxiliary_vars }).second)
								nb_auxiliary_vars++;
						} else {
							if (mapped_decision_variable_ids.insert( {id_pos, nb_decision_vars }).second)
								nb_decision_vars++;
						}
					}
				}
			}
			if (!rescale_variable_ids)
				s_out << "0" << std::endl;
		}
	}
	//std::cout << std::endl;

	--nb_decision_vars;
	--nb_auxiliary_vars;

	// Commentaries + p-line
	s_out << "c =========================================================" << std::endl;
	s_out << "c " << ctime(&my_time) ; //<< std::endl;
	s_out << "c Num nodes (all): " << num_variables << " " << num_relations << " " << num_transformations << " " << _first_free_id << std::endl;

	if (!rescale_variable_ids) {
		s_out << "c =========================================================" << std::endl;
		s_out << "p cnf " << std::to_string(max_id_var) << " " << std::to_string(nb_clauses) << std::endl;
	} else {
		s_out << "c p cnf " << std::to_string(nb_decision_vars+nb_auxiliary_vars) << " " << std::to_string(nb_clauses) << " " << std::to_string(nb_decision_vars+1) << std::endl;
		s_out << "c =========================================================" << std::endl;
		s_out << "p cnf " << std::to_string(nb_decision_vars+nb_auxiliary_vars) << " " << std::to_string(nb_clauses) << std::endl;

	}

	if (rescale_variable_ids)
	{
		//FL : ADD
		for (auto& x: mapped_decision_variable_ids) {
			map_cnf_graph_ids.insert( {x.second, x.first} );
		}
		for (auto& x: mapped_auxiliary_variable_ids) {
			map_cnf_graph_ids.insert( {x.second+nb_decision_vars, x.first} );
		}

		/* print CNF-GRAPH id match (CVG 20181106/20190304)
		filename += ".cnfmatch";
		std::ofstream matchfile;
		matchfile.open(filename);
		matchfile << "c CNF GRAPH" << std::endl;
		for (auto i : map_cnf_graph_ids) {
			matchfile << "c " << i.first << " " << i.second << std::endl;
		}
		matchfile.close();
		*/

		for (auto& c : get_vertice_store())
		{
			if (check(*c._p1, Flag::RELATION | Flag::BINARY_DISJUNCTION))
			{
				unsigned int id_disjunction = *c._p0;
				chr::Logical_var_mutable< std::vector< unsigned int > > Ret_ids;
				get_all_children(id_disjunction, Ret_ids);

				for (auto& e : *Ret_ids)
				{
					chr::Logical_var< unsigned int > l_id;
					get_parent_node_with_flags(e, Flags(Flag::TRANSFORMATION), l_id);
					if (l_id.ground())
					{
						if (mapped_decision_variable_ids.find(e) != mapped_decision_variable_ids.end())
							s_out << mapped_decision_variable_ids.at(e) << " ";
						else
							s_out << nb_decision_vars + mapped_auxiliary_variable_ids.at(e) << " ";
					} else {
						chr::Logical_var< unsigned int > l_id_pos;
						get_opposite_var(e, l_id_pos);
						if (!l_id_pos.ground()) std::cerr << "Opposé au sommet " << e << " pas trouvé" << std::endl;
						if (mapped_decision_variable_ids.find(*l_id_pos) != mapped_decision_variable_ids.end())
							s_out << "-" << mapped_decision_variable_ids.at(*l_id_pos) << " ";
						else
							s_out << "-" << nb_decision_vars + mapped_auxiliary_variable_ids.at(*l_id_pos) << " ";
					}
				}
				s_out << "0" << std::endl;
			}
		}
	}

	s_out.close();

	// time --------
	my_time = time(NULL);
	std::cout << "print_cnf() : end\t\t" << ctime(&my_time);
}



//void GRAPH::print_cnf(std::string filename, std::unordered_map<unsigned int,unsigned int> & map_cnf_graph_ids, bool rescale_variable_ids)
//{
//	filename += ".cnf";
//	std::ofstream f;
//	f.open(filename);
//	print_cnf(f,map_cnf_graph_ids,rescale_variable_ids);
//	f.close();
//}



void GRAPH::print_cnf(std::ostream& s_out, bool rescale_variable_ids)
{
	unsigned int nb_clauses = 0;
	unsigned int nb_decision_vars = 1;
	unsigned int nb_auxiliary_vars = 1;
	unsigned int max_id_var = 0;
	std::unordered_map< unsigned int, unsigned int > mapped_decision_variable_ids;
	std::unordered_map< unsigned int, unsigned int > mapped_auxiliary_variable_ids;
	for (auto& c : get_vertice_store())
	{
		if (check(*c._p1, Flag::RELATION | Flag::BINARY_DISJUNCTION))
		{
			++nb_clauses;
			unsigned int id_disjunction = *c._p0;
			chr::Logical_var_mutable< std::vector< unsigned int > > Ret_ids;
			get_all_children(id_disjunction, Ret_ids);

			for (auto& e : *Ret_ids)
			{
				chr::Logical_var< unsigned int > l_id;
				get_parent_node_with_flags(e, Flags(Flag::TRANSFORMATION), l_id);
				if (l_id.ground())
				{
					if (!rescale_variable_ids)
						s_out << e << " ";
					max_id_var = std::max(max_id_var,e);
					if (rescale_variable_ids)
					{
						// Récupération du noeud
						chr::Logical_var< Flags > R_flags;
						chr::Logical_var< std::string > R_label;
						chr::Logical_var< Domain > R_domain;
						get_vertice(e, R_flags, R_label, R_domain);
						assert( R_flags.ground() );
						if (check(*R_flags, Flag::BOOLEAN | Flag::AUXILIARY))
						{
							if (mapped_auxiliary_variable_ids.insert( {e, nb_auxiliary_vars }).second)
								nb_auxiliary_vars++;
						} else {
							if (mapped_decision_variable_ids.insert( {e, nb_decision_vars }).second)
								nb_decision_vars++;
						}
					}
				} else {
					chr::Logical_var< unsigned int > l_id_pos;
					get_opposite_var(e, l_id_pos);
					if (!l_id_pos.ground()) std::cerr << "Opposé au sommet " << e << " pas trouvé" << std::endl;
					unsigned int id_pos = *l_id_pos;

					if (!rescale_variable_ids)
						s_out << "-" << id_pos << " ";
					max_id_var = std::max(max_id_var,id_pos);
					if (rescale_variable_ids)
					{
						// Récupération du noeud
						chr::Logical_var< Flags > R_flags;
						chr::Logical_var< std::string > R_label;
						chr::Logical_var< Domain > R_domain;
						get_vertice(id_pos, R_flags, R_label, R_domain);
						assert( R_flags.ground() );
						if (check(*R_flags, Flag::BOOLEAN | Flag::AUXILIARY))
						{
							if (mapped_auxiliary_variable_ids.insert( {id_pos, nb_auxiliary_vars }).second)
								nb_auxiliary_vars++;
						} else {
							if (mapped_decision_variable_ids.insert( {id_pos, nb_decision_vars }).second)
								nb_decision_vars++;
						}
					}
				}
			}
			if (!rescale_variable_ids)
				s_out << "0" << std::endl;
		}
	}
	--nb_decision_vars;
	--nb_auxiliary_vars;
	if (!rescale_variable_ids)
		s_out << "p cnf " << std::to_string(max_id_var) << " " << std::to_string(nb_clauses) << std::endl;
	else {
		s_out << "p cnf " << std::to_string(nb_decision_vars+nb_auxiliary_vars) << " " << std::to_string(nb_clauses) << " " << std::to_string(nb_decision_vars+1) << std::endl;
	}

	if (rescale_variable_ids)
	{

		for (auto& c : get_vertice_store())
		{
			if (check(*c._p1, Flag::RELATION | Flag::BINARY_DISJUNCTION))
			{
				unsigned int id_disjunction = *c._p0;
				chr::Logical_var_mutable< std::vector< unsigned int > > Ret_ids;
				get_all_children(id_disjunction, Ret_ids);

				for (auto& e : *Ret_ids)
				{
					chr::Logical_var< unsigned int > l_id;
					get_parent_node_with_flags(e, Flags(Flag::TRANSFORMATION), l_id);
					if (l_id.ground())
					{
						if (mapped_decision_variable_ids.find(e) != mapped_decision_variable_ids.end())
							s_out << mapped_decision_variable_ids.at(e) << " ";
						else
							s_out << nb_decision_vars + mapped_auxiliary_variable_ids.at(e) << " ";
					} else {
						chr::Logical_var< unsigned int > l_id_pos;
						get_opposite_var(e, l_id_pos);
						if (!l_id_pos.ground()) std::cerr << "Opposite to variable " << e << " not found." << std::endl;
						if (mapped_decision_variable_ids.find(*l_id_pos) != mapped_decision_variable_ids.end())
							s_out << "-" << mapped_decision_variable_ids.at(*l_id_pos) << " ";
						else
							s_out << "-" << nb_decision_vars + mapped_auxiliary_variable_ids.at(*l_id_pos) << " ";
					}
				}
				s_out << "0" << std::endl;
			}
		}
	}
}



// ==========================================================================
// PRINT DOT
// ==========================================================================

void GRAPH::print_graph_dot(std::ostream& s_out)
{
	// ./graph -dot | fdp -Tpdf -o graph.pdf
	// ./graph -dot | dot -Tpdf -o graph.pdf
	// ./graph -dot | neato -Tpdf -o graph.pdf
	s_out << "digraph transformation {" << std::endl;
	for (auto& c : get_vertice_store())
	{
		s_out << "  " << c._p0 << " [label=\"";
		if (check(*c._p1, Flag::VARIABLE))
				s_out << *c._p2 << "\n" << chr::TIW::to_string(*c._p3);
		else
				s_out << *c._p2;
		s_out << "\", shape=";
		if (check(*c._p1, Flag::VARIABLE))
			s_out << "box";
		else if (check(*c._p1, Flag::RELATION))
			s_out << "oval";
		else if (check(*c._p1, Flag::TRANSFORMATION))
			s_out << "diamond";
		s_out << "]" << std::endl;
	}

	for (auto& c : get_edge_store())
	{
		s_out << "  " << c._p0 << " -> " << c._p1;
		if (c._p2 != -1)
			s_out << " [label=\"" << c._p2 << "\"]";
		s_out << std::endl;
	}
	s_out << "}" << std::endl;
}




/* ==========================================================================
// PRINT variable match list
// rescaled by default
// ==========================================================================

void GRAPH::print_variable_matchlist(std::ofstream& s_out) //, std::unordered_map< unsigned int, unsigned int > & map_cnf_graph_ids, bool rescale_variable_ids)
{
	time_t my_time;
	my_time = time(NULL);
	std::cout << "print_variable_matchlist() : start\t\t" << ctime(&my_time);

	std::unordered_map<unsigned int, unsigned int> mapped_decision_var, mapped_auxiliary_var;
	mapped_decision_var.reserve(num_variables);
	mapped_auxiliary_var.reserve(num_variables);

	unsigned int max_vid = 0;				// max variable ID
	int nb_decision_vars  = 0;		// number of decision variables.
	int nb_auxiliary_vars = 0;		// number of auxiliary variables.

	for (auto& node : get_vertice_store())
	{
		if (check(*node._p1, Flag::RELATION | Flag::BINARY_DISJUNCTION))
		{
			unsigned int id_disjunction = *node._p0;
			chr::Logical_var_mutable< std::vector<unsigned int> > children_vars;
			get_all_children(id_disjunction, children_vars);

			for (auto& var : *children_vars)
			{
				chr::Logical_var< unsigned int > parent_id;
				get_parent_node_with_flags(var, Flags(Flag::TRANSFORMATION), parent_id);

				if (parent_id.ground())
				{
					max_vid = std::max(max_vid, var);

					// (variable) node attributes
					chr::Logical_var<Flags>       l_flags;
					chr::Logical_var<std::string> l_label;
					chr::Logical_var<Domain>      l_domain;
					get_vertice(var, l_flags, l_label, l_domain);

					assert(l_flags.ground());
					if (check(*l_flags, Flag::BOOLEAN | Flag::AUXILIARY)) {
						if (mapped_auxiliary_var.insert( {var, nb_auxiliary_vars }).second)
							nb_auxiliary_vars++;
					} else {
						if (mapped_decision_var.insert( {var, nb_decision_vars }).second)
							nb_decision_vars++;
					}
				}
				else
				{
					chr::Logical_var< unsigned int > nl_id;
					get_opposite_var(var, nl_id);			// Opposite literal to 'var'
					if (!nl_id.ground())
						std::cerr << "Opposite literal to " << var << " not found." << std::endl;

					unsigned int nvar_id = *nl_id;

					max_vid = std::max(max_vid, nvar_id);

					// (variable) opposite literal attributes
					chr::Logical_var< Flags >       nl_flags;
					chr::Logical_var< std::string > nl_label;
					chr::Logical_var< Domain >      nl_domain;

					get_vertice(nvar_id, nl_flags, nl_label, nl_domain);
					assert(nl_flags.ground() );
					if (check(*nl_flags, Flag::BOOLEAN | Flag::AUXILIARY)) {
						if (mapped_auxiliary_var.insert( {nvar_id, nb_auxiliary_vars }).second)
							nb_auxiliary_vars++;
					} else {
						if (mapped_decision_var.insert( {nvar_id, nb_decision_vars }).second)
							nb_decision_vars++;
					}
				}
			}
		}
	}

	std::cout << "mapped_decision_var  : " << mapped_decision_var.size() << std::endl;
	std::cout << "mapped_auxiliary_var : " << mapped_auxiliary_var.size() << std::endl;

	my_time = time(NULL);
	std::cout << "print_variable_matchlist() : end\t\t" << ctime(&my_time);

}

*/

