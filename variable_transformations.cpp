/*
 * VARIABLE TRANSFORMATION
 *
 * Encoding:
 *   direct_encoding()
 *   log_encoding()
 *   encode_variable()
 *
 * SubIntervals:
 *   add_subintervals_direct()
 *   add_subintervals_log()
 */

#include "misc_utils.hpp"
#include <graph_chr_header.hpp>

#define N_STR std::string()


// ==================================================================
// ENCODE VARIABLE ~ wrapper
//
// - When is invoked, it encode the variable on-demand, changing the
//   flag decision/auxiliary according to the encode_sequence
//   (defined in the model).
// - If the encoding requested is not DIRECT or LOG, it finishes.
// - After encoding, it checks sub-intervals and bridges.
//
// ==================================================================

//
// v1: Encodes on-demand, DV set according to 1st encoding.
//
void GRAPH::encode_variable(unsigned int id, unsigned long encoding)
{
	//std::cout << "Graph::encode_variable()" << std::endl;

	// encode "on-demand" ---------------------------------
	bool as_auxiliary = (encoding == encoding_sequence[0]) ? false : true;

	if (encoding == Flag::DIRECT_ENCODING)		build_direct_encoding_from(id, as_auxiliary);
	else if (encoding == Flag::LOG_ENCODING)	build_log_encoding_from(id, as_auxiliary);
	else if (encoding == Flag::HYBRID_ENCODING) build_hybrid_encoding(id, as_auxiliary);
	else return;


    // SubIntervals ---------------------------------------
	chr::Logical_var<Flags> flags;
	chr::Logical_var<std::string> label;
	chr::Logical_var<Domain> domain;
	get_vertice(id, flags, label, domain);

	if (check(flags, Flags(Flag::VARIABLE | Flag::PROP_INTEGER))) {
		if (encoding == Flag::DIRECT_ENCODING)		create_subintervals(id,Flag::DIRECT_ENCODING);
		else if (encoding == Flag::LOG_ENCODING)	create_subintervals(id,Flag::LOG_ENCODING);
	}


	// Bridge -----------------------------------
	chr::Logical_var_mutable< std::vector<unsigned int> > enc_list;
	get_all_children_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::DIRECT_ENCODING), enc_list);
	get_all_children_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), enc_list);

	if ((*enc_list).size() > 1) {
		link_direct_to_log_encoding(id);
		//link_log_to_direct_encoding(id);
	}

}



//
// v2: Run all encodings in 'encode_sequence'. DirectEncoding as DV and director
/*
void GRAPH::encode_variable(unsigned int id, unsigned long encoding)
{
	std::cout << "Graph::encode_variable()" << std::endl;

	// Encoding -------------------------------------------
	// 2019-12-03: Added a loop to run all encodings in the first call,
	// with the direct encoding as Decision variables; in this case,
	// parameter "encoding" is not used
	//
	for(unsigned int i=0; i<encoding_sequence.size(); ++i)
	{
		if (encoding_sequence[i] == Flag::DIRECT_ENCODING)	 build_direct_encoding_from(id, false);
		else if (encoding_sequence[i] == Flag::LOG_ENCODING) build_log_encoding_from(id, true);
	}


    // SubIntervals ---------------------------------------
    // 2019-12-03: Modified to run-all at first call
    //
	chr::Logical_var<Flags> flags;
	chr::Logical_var<std::string> label;
	chr::Logical_var<Domain> domain;
	get_vertice(id, flags, label, domain);

	if (check(flags, Flags(Flag::VARIABLE | Flag::PROP_INTEGER)))
	{
		for(unsigned int i=0; i<encoding_sequence.size(); ++i)
		{
			if (encoding_sequence[i] == Flag::DIRECT_ENCODING)	 create_subintervals(id,Flag::DIRECT_ENCODING);
			else if (encoding_sequence[i] == Flag::LOG_ENCODING) create_subintervals(id,Flag::LOG_ENCODING);
		}
	}



	// Bridge -----------------------------------
	chr::Logical_var_mutable< std::vector<unsigned int> > enc_list;
	get_all_children_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::DIRECT_ENCODING), enc_list);
	get_all_children_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), enc_list);

	if ((*enc_list).size() > 1) {
		link_direct_to_log_encoding(id);
		//link_log_to_direct_encoding(id);
	}
}
*/



// ==================================================================
// VARIABLE INTERVAL
// Creates CSP interval variables required
// ==================================================================
/*
void GRAPH::build_variable_interval(unsigned int id)
{
	// Check transformation; if it does not exist, create it
	chr::Logical_var<unsigned int> l_id;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::VAR_INTERVAL), l_id);
	if (l_id.ground())
		return;
	unsigned int id_transformation = _first_free_id++;
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::VAR_INTERVAL), N_STR, IntVarDomain()); num_transformations++;
	edge(id, id_transformation, -1);

	// Get variable domain (for min/max vars)
	chr::Logical_var<Flags>       flags;
	chr::Logical_var<std::string> label;
	chr::Logical_var<Domain>      vdomain;
	get_vertice(id, flags, label, vdomain);
	int dmin = (*vdomain).min();
	int dmax = (*vdomain).max();

	IntVarDomain domain;
	for (int i=dmin; i<=dmax; ++i)
		domain.domain.push_back(i);

	// link to the same var
	edge(id_transformation, id, 0);

	// min-var
	unsigned int minvar = _first_free_id++;
	vertice(minvar, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, domain); num_variables++;
	edge(id_transformation, minvar, 1);

	// max-var
	unsigned int maxvar = _first_free_id++;
	vertice(maxvar, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, domain); num_variables++;
	edge(id_transformation, maxvar, 2);

	// x- <= X
	unsigned int leq1 = _first_free_id++;
	vertice(leq1, Flags(Flag::RELATION | Flag::LESS_EQUAL_THAN), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, leq1, 3);
	edge(leq1, minvar,0);
	edge(leq1, id,    1);

	// X <= x+
	unsigned int leq2 = _first_free_id++;
	vertice(leq2, Flags(Flag::RELATION | Flag::LESS_EQUAL_THAN), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, leq2, 4);
	edge(leq2, id,    0);
	edge(leq2, maxvar,1);

	std::cout << "build_interval[" << id << "]\t" << "(" << minvar << " " << maxvar << ") | " << leq1 << " " << leq2 << " ";

	// INTERVAL_DOMAIN_BRANCHING -- IB(x,x-,x+)
	if ( check(flags, Flags(Flag::VARIABLE | Flag::PROP_INTEGER)) && (dmin < dmax))
	{
		unsigned int ib = _first_free_id++;
		vertice(ib, Flags(Flag::RELATION | Flag::INTERVAL_BRANCHING), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, ib, 5);
		edge(ib, id, 0);
		edge(ib, minvar, 1);
		edge(ib, maxvar, 2);
		std::cout << ib << " ";
	}
	std::cout << std::endl;
}
*/



// ==================================================================
// DIRECT ENCODING
// ==================================================================

void GRAPH::build_direct_encoding_from(unsigned int id, bool force_auxiliary)
{
	std::cout << "direct_encoding[" << id << "]";

    // Check transformation; if it does not exist, create it
	chr::Logical_var< unsigned int > l_id;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::DIRECT_ENCODING), l_id);
	if (l_id.ground()) {
		std::cout << std::endl;
		return;
	}

	unsigned int id_transformation = _first_free_id++;
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::DIRECT_ENCODING), N_STR, IntVarDomain()); num_transformations++;
	edge(id, id_transformation, -1);

	unsigned int id_disjunction = _first_free_id++;
	vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, id_disjunction, -1);

	// variable information -----------------------------------------
	chr::Logical_var<Flags>       varflags;
	chr::Logical_var<std::string> varlabel;
	chr::Logical_var<Domain>      vardomain;
	get_vertice(id, varflags, varlabel, vardomain);

	const IntVarDomain& domain = *vardomain;

	unsigned long is_aux = 0;
	if (check(*varflags, Flag::AUXILIARY) || force_auxiliary) {
		is_aux = Flag::AUXILIARY;
		std::cout << "*";
	}
	std::cout << "\t";

	// encoding (one variable per domain value) ---------------------
	std::list<unsigned int> used_ids;
	for (unsigned int i=0; i<domain.size(); ++i)
	{
		unsigned int id_noeud_not     = _first_free_id++;
		unsigned int id_noeud_positif = _first_free_id++;
		unsigned int id_noeud_negatif = _first_free_id++;
		used_ids.push_back(id_noeud_negatif);

		vertice(id_noeud_positif, Flags(Flag::VARIABLE | Flag::BOOLEAN | is_aux), N_STR, IntVarDomain({0,1})); num_variables++;
		edge(id_transformation, id_noeud_positif, i);
		edge(id_disjunction, id_noeud_positif, -1);
		vertice(id_noeud_not, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
		edge(id_noeud_not, id_noeud_positif, -1);
		edge(id_transformation, id_noeud_not, -1);
		vertice(id_noeud_negatif, Flags(Flag::VARIABLE | Flag::BOOLEAN | is_aux), N_STR, IntVarDomain({0,1})); num_variables++;
		edge(id_noeud_not, id_noeud_negatif, -1);

		std::cout << "(" << id_noeud_positif << "," << id_noeud_negatif << ") ";
	}
	std::cout << std::endl;

	// "exact-one" clauses
	auto it1 = used_ids.begin();
	auto it1_end = used_ids.end();
	for ( ; it1 != it1_end; ++it1)
	{
		auto it2 = it1;
		++it2;
		for ( ; it2 != it1_end; ++it2)
		{
			unsigned int id_disjunction = _first_free_id++;
			vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
			edge(id_transformation, id_disjunction, -1);
			edge(id_disjunction, *it1, -1);
			edge(id_disjunction, *it2, -1);
		}
	}
}



// ==========================================================================
// LOG ENCODING
// ==========================================================================

void GRAPH::build_log_encoding_from(unsigned int id, bool force_auxiliary)
{
	std::cout << "log_encoding[" << id << "]";

	// Check transformation; if it does not exist, continue
	chr::Logical_var<unsigned int> l_id1;
	chr::Logical_var<unsigned int> l_id2;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id1);
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::PROP_INTEGER | Flag::LOG_ENCODING), l_id2);
	if (l_id1.ground() || l_id2.ground()) {
		std::cout << std::endl;
		return;
	}

	// variable information -----------------------------------------
	chr::Logical_var<Flags>       varflags;
	chr::Logical_var<std::string> l_label;
	chr::Logical_var<Domain>      l_domain;
	get_vertice(id,varflags,l_label,l_domain);

	unsigned long is_aux = 0;
	if (check(*varflags, Flag::AUXILIARY) || force_auxiliary) {
		is_aux = Flag::AUXILIARY;		// CVG 20191010 It forces certain of auxiliary variables when bridges or, it follows the tag in the model.
		std::cout << "*\t";
	}
	std::cout << "    \t";

	// encoding -----------------------------------------------------

	const IntVarDomain& domain = *l_domain;
	int d_max = domain.max();
	if (d_max >= 0) ++d_max; // On compte la valeur 0 à mettre dans le domaine
	int d_min = domain.min();
	if (d_min >= 0) ++d_min; // On compte la valeur 0 à mettre dans le domaine
	unsigned int domain_bits_count = std::max(1.0, std::ceil( std::log2( std::max(std::abs(d_max), std::abs(d_min)) ) )) + 1; // 1 extra bit for the sign
	// 1 bit minimum sans le bit de signe, d'où le min(1, ....)

	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), N_STR, IntVarDomain());
	num_transformations++;

	// Création des binary variables according to the number of bits
	// necessaries to represent the maximum element in the domain
	std::vector<unsigned int> used_positive_ids, used_negative_ids;
	unsigned int i = 0;
	for (unsigned int j=0; j<domain_bits_count; ++j)
	{
		unsigned int id_noeud_positif = _first_free_id++;
		unsigned int id_noeud_negatif = _first_free_id++;
		unsigned int id_noeud_not     = _first_free_id++;
		used_positive_ids.push_back(id_noeud_positif);
		used_negative_ids.push_back(id_noeud_negatif);

		vertice(id_noeud_positif, Flags(Flag::VARIABLE | Flag::BOOLEAN | is_aux), N_STR, IntVarDomain({0,1})); num_variables++;
		edge(id_transformation, id_noeud_positif, i++);
		vertice(id_noeud_not, Flags(Flag::RELATION | Flag::BINARY_NOT), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, id_noeud_not, -1);
		vertice(id_noeud_negatif, Flags(Flag::VARIABLE | Flag::BOOLEAN | is_aux), N_STR, IntVarDomain({0,1})); num_variables++;

		edge(id_noeud_not, id_noeud_negatif, -1);
		edge(id_noeud_not, id_noeud_positif, -1);
	}

	int lower_bound = -std::exp2(domain_bits_count-1);
	int upper_bound = std::exp2(domain_bits_count-1);


	// Single-value domain (constants) ------------------------------

	if (d_min == d_max)
	{
		unsigned int int_value;
		if (d_min < 0)
		{	// for negative values, use complement-2
			int_value = static_cast<unsigned int>(abs(d_min));
			int_value = ~int_value;
			int_value += 1;
		}
		else
			int_value = static_cast<unsigned int>(abs(d_min-1));

		unsigned int bit_number = 0;
		for (unsigned int k=0; k < domain_bits_count; ++k)
		{
			unsigned int id_disjunction = _first_free_id++;
			vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
			edge(id_transformation, id_disjunction, -1);

			unsigned int bit_value = int_value & 0x01;
			if (bit_value == 1)
				edge(id_disjunction, used_positive_ids[bit_number], -1);
			else
				edge(id_disjunction, used_negative_ids[bit_number], -1);
			int_value >>= 1;
			++bit_number;
        }
	}
	else
	{
		if (domain.min() >= 0)
		{
			// If all values are POSITIVE, the sign bit (the biggest index in the vector)
			// is set to FALSE (this is, no positive literal is attached to the disjunction--> Unitary clause)
			lower_bound = 0;
			unsigned int id_disjunction = _first_free_id++;
			vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
			edge(id_transformation, id_disjunction, -1);
			edge(id_disjunction, used_negative_ids[domain_bits_count-1], -1);
		}

/*
	// Remove forbidden values that are allowed by the encoding
	for (int i=lower_bound; i < upper_bound; ++i)
	{
		auto it = std::find((*l_domain).begin(), (*l_domain).end(), i);
		if (it == (*l_domain).end())
		{
			unsigned int id_disjunction = _first_free_id++;
			vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
			edge(id_transformation, id_disjunction, -1);
			  unsigned int bit_number = 0;
			  unsigned int int_value = static_cast<unsigned int>(abs(i));
			  if (i < 0) // Pour une valeur négative du domaine, on prend le complément à deux
			  {
				  int_value = ~int_value;
				  int_value += 1;
			  }
			for (unsigned int k=0; k < domain_bits_count; ++k)
			{
				unsigned int bit_value = int_value & 0x01;
				if (bit_value == 1)
					edge(id_disjunction, used_negative_ids[bit_number], -1);
				else
					edge(id_disjunction, used_positive_ids[bit_number], -1);
				int_value >>= 1;
				++bit_number;
			}
		}
	}
*/
        // Removing forbidden values --------------------------------

        int min_value = domain.min();
        int max_value = domain.max();

        for (int i=lower_bound; i < min_value; ++i)
        {
            unsigned int id_disjunction = _first_free_id++;
            vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
            edge(id_transformation, id_disjunction, -1);

            unsigned int bit_number = 0;
            unsigned int int_value = static_cast<unsigned int>(abs(i));

            // negative values use 2-complement
            if (i < 0) {
                int_value = ~int_value;
                int_value += 1;
            }
            for (unsigned int k=0; k < domain_bits_count; ++k)
            {
                unsigned int bit_value = int_value & 0x01;
                if (bit_value == 1)
                    edge(id_disjunction, used_negative_ids[bit_number], -1);
                else
                    edge(id_disjunction, used_positive_ids[bit_number], -1);
                int_value >>= 1;
                ++bit_number;
            }
        }


        for (int i=min_value; i<max_value; ++i)
        {
            bool trouve=false;
            for (auto& e : domain.domain)
                if (e == i) {
                    trouve = true;
                    break;
                }
			if (!trouve)
			{
				unsigned int id_disjunction = _first_free_id++;
				vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
				edge(id_transformation, id_disjunction, -1);

				// negative values use 2-complement
				unsigned int bit_number = 0;
				unsigned int int_value = static_cast<unsigned int>(abs(i));
				if (i < 0) {
					int_value = ~int_value;
					int_value += 1;
				}
				for (unsigned int k=0; k < domain_bits_count; ++k)
				{
					unsigned int bit_value = int_value & 0x01;
					if (bit_value == 1)
						edge(id_disjunction, used_negative_ids[bit_number], -1);
					else
						edge(id_disjunction, used_positive_ids[bit_number], -1);
					int_value >>= 1;
					++bit_number;
				}
			}
        }

        for (int i=max_value+1; i < upper_bound; ++i)
        {
            unsigned int id_disjunction = _first_free_id++;
            vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
            edge(id_transformation, id_disjunction, -1);

            // negative values use 2-complement
            unsigned int bit_number = 0;
            unsigned int int_value = static_cast<unsigned int>(abs(i));
            if (i < 0) {
				int_value = ~int_value;
				int_value += 1;
            }
            for (unsigned int k=0; k < domain_bits_count; ++k)
            {
                unsigned int bit_value = int_value & 0x01;
                if (bit_value == 1)
                    edge(id_disjunction, used_negative_ids[bit_number], -1);
                else
                    edge(id_disjunction, used_positive_ids[bit_number], -1);
                int_value >>= 1;
                ++bit_number;
            }
        }
	}

	// print encoding result
	for (unsigned int j=0; j<used_positive_ids.size(); ++j)
		std::cout << "(" << used_positive_ids[j] << "," << used_negative_ids[j] << ") ";
	std::cout << std::endl ;

}



// ==========================================================================
// BRIDGE CLAUSES
//
// 2019-09-18:
//	DirectEnc   reacts to variable labels (decision/auxiliary)
//  LogEncoding ALWAYS auxiliary
// ==========================================================================


//
// Direct <-> Log
//
void GRAPH::link_direct_to_log_encoding(unsigned int id_var_integer)
{
    std::cout << "bridge_dl[" << id_var_integer << "]" << std::endl;

    // Check both encodings exist
	chr::Logical_var<unsigned int> l_id_trans_bin_log;

	get_child_with_flags(id_var_integer, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_bin_log);
	if (!(l_id_trans_bin_log.ground())) {
		std::cerr << "bridge_dl: LOG encoding does not exist" << std::endl;
		return;
	}

	chr::Logical_var<unsigned int> l_id_trans_bin_direct;
	get_child_with_flags(id_var_integer, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::DIRECT_ENCODING), l_id_trans_bin_direct);
	if (!(l_id_trans_bin_direct.ground())) {
		std::cerr << "bridge_dl: DIRECT encoding does not exist" << std::endl;
		return;
	}

	// integer variable information
	chr::Logical_var< Flags >       l_flags;
	chr::Logical_var< std::string > l_label;
	chr::Logical_var< Domain >      l_domain;
	get_vertice(id_var_integer,l_flags,l_label,l_domain);
	unsigned int is_aux = 0;
	if (check(*l_flags, Flag::AUXILIARY)) is_aux = Flag::AUXILIARY;
	const IntVarDomain& domain = *l_domain;

	// Get DIRECT boolean vars
	chr::Logical_var_mutable< std::vector<unsigned int> > l_children_from_transformation_direct_encoding;
	get_all_children_with_flags_ordered(*l_id_trans_bin_direct, Flags(Flag::VARIABLE | Flag::BOOLEAN | is_aux), 0, l_children_from_transformation_direct_encoding);

	std::vector<unsigned int> neg_children_from_transformation_direct_encoding;
	for (auto& x : *l_children_from_transformation_direct_encoding) {
		chr::Logical_var< unsigned int > l_id;
		get_opposite_var(x, l_id);
		if (l_id.ground()) {
			neg_children_from_transformation_direct_encoding.push_back(*l_id);
		}
	}

	// Get LOG boolean vars
	chr::Logical_var_mutable< std::vector<unsigned int> > l_children_from_transformation_log_encoding;
	//get_all_children_with_flags_ordered(*l_id_trans_bin_log, Flags(Flag::VARIABLE | Flag::BOOLEAN | is_aux), 0, l_children_from_transformation_log_encoding);	//CVG 20190918 LOG only in aux
	get_all_children_with_flags_ordered(*l_id_trans_bin_log, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_children_from_transformation_log_encoding);

	std::vector<unsigned int> neg_children_from_transformation_log_encoding;
	for (auto& x : *l_children_from_transformation_log_encoding) {
		chr::Logical_var< unsigned int > l_id;
		get_opposite_var(x, l_id);
		if (l_id.ground()) {
			neg_children_from_transformation_log_encoding.push_back(*l_id);
		}
	}

	int d_max = domain.max();
	if (d_max >= 0) ++d_max; // On compte la valeur 0 à mettre dans le domaine
	int d_min = domain.min();
	if (d_min >= 0) ++d_min; // On compte la valeur 0 à mettre dans le domaine
	unsigned int nbits = std::max(1.0, std::ceil( std::log2( std::max(std::abs(d_max), std::abs(d_min)) ) )) + 1; // +1 pour le bit de signe
	// 1 bit minimum sans le bit de signe, d'où le min(1, ....)


	// N-ARY CLAUSE : 2019-12-18
	{
		unsigned int idx_direct_var = 0;
		for (auto& e : domain.domain)
		{
			// For negative values, use complement-2
			unsigned int int_value = static_cast<unsigned int>(abs(e));
			if (e < 0) {
				int_value = ~int_value;
				int_value += 1;
			}

			unsigned int clause_nary = _first_free_id++;
			vertice(clause_nary, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
			edge(l_id_trans_bin_direct, clause_nary, -1);
			edge(l_id_trans_bin_log,    clause_nary, -1);
			edge(clause_nary, (*l_children_from_transformation_direct_encoding)[idx_direct_var], -1);
			std::cout << "\t" << clause_nary << ":\t" << (*l_children_from_transformation_direct_encoding)[idx_direct_var] << " ";

			unsigned int idx_log_var = 0;
			for (unsigned int i=0; i< nbits; ++i)
			{
				unsigned int bit_value = int_value & 0x01;
				if (bit_value == 1) {
					edge(clause_nary, neg_children_from_transformation_log_encoding[idx_log_var], -1);
					std::cout << neg_children_from_transformation_log_encoding[idx_log_var] << " ";
				} else {
					edge(clause_nary, (*l_children_from_transformation_log_encoding)[idx_log_var], -1);
					std::cout << (*l_children_from_transformation_log_encoding)[idx_log_var] << " " ;
				}

				int_value >>= 1;
				++idx_log_var;
			}
			std::cout << std::endl;
			++idx_direct_var;
		}
	}


	// BINARY CLAUSES : Post direct encoding vers log encoding règles propagations
	unsigned int idx_direct_var = 0;
	for (auto& e : domain.domain)
	{
		unsigned int idx_log_var = 0;
		unsigned int int_value = static_cast<unsigned int>(abs(e));
		if (e < 0) // For a negative value du domaine, on prend le 2-complement
		{
			int_value = ~int_value;
			int_value += 1;
		}
		for (unsigned int i=0; i< nbits; ++i)
		{
			unsigned int bit_value = int_value & 0x01;

			unsigned int id_disjunction = _first_free_id++;
			vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
			edge(l_id_trans_bin_direct, id_disjunction, -1);
			edge(l_id_trans_bin_log, id_disjunction, -1);
			edge(id_disjunction, neg_children_from_transformation_direct_encoding[idx_direct_var], -1);
			std::cout << "\t" << id_disjunction << ":\t" << neg_children_from_transformation_direct_encoding[idx_direct_var] << " ";

			if (bit_value == 1) {
				edge(id_disjunction, (*l_children_from_transformation_log_encoding)[idx_log_var], -1);
				std::cout << (*l_children_from_transformation_log_encoding)[idx_log_var] << std::endl;
			} else {
				edge(id_disjunction, (neg_children_from_transformation_log_encoding)[idx_log_var], -1);
				std::cout << neg_children_from_transformation_log_encoding[idx_log_var] << std::endl;
			}

			int_value >>= 1;
			++idx_log_var;
		}
		++idx_direct_var;
	}
}



//
// Log -> Direct
/*

void GRAPH::link_log_to_direct_encoding(unsigned int id_var_integer)
{
	std::cout << "bridge_ld[" << id_var_integer << "]" << std::endl;

	// Check encodings (log+direct) exist ---------------------------
	chr::Logical_var<unsigned int> l_id_trans_bin_log;
	get_child_with_flags(id_var_integer, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_bin_log);
	if (!(l_id_trans_bin_log.ground()))
		return;

	chr::Logical_var<unsigned int> l_id_trans_bin_direct;
	get_child_with_flags(id_var_integer, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::DIRECT_ENCODING), l_id_trans_bin_direct);
	if (!(l_id_trans_bin_direct.ground()))
		return;


	// Get CSP variable information ---------------------------------
	chr::Logical_var<Flags>       l_flags;
	chr::Logical_var<std::string> l_label;
	chr::Logical_var<Domain>      l_domain;
	get_vertice(id_var_integer,l_flags,l_label,l_domain);
	unsigned int is_aux = 0;
	if (check(*l_flags, Flag::AUXILIARY)) is_aux = Flag::AUXILIARY;
	const IntVarDomain& domain = *l_domain;


	// Get LOG boolean variables ------------------------------------
	chr::Logical_var_mutable< std::vector<unsigned int> > l_children_from_transformation_log_encoding;	// l_boolvars_log
	get_all_children_with_flags_ordered(*l_id_trans_bin_log, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_children_from_transformation_log_encoding);	//CVG 20190918 LOG only in aux

	std::vector<unsigned int> neg_children_from_transformation_log_encoding;
	for (auto& x : *l_children_from_transformation_log_encoding) {
		chr::Logical_var<unsigned int> l_id;
		get_opposite_var(x, l_id);
		if (l_id.ground()) {
			neg_children_from_transformation_log_encoding.push_back(*l_id);
		}
	}


	// Get DIRECT boolean variables ---------------------------------
	chr::Logical_var_mutable< std::vector<unsigned int> > l_children_from_transformation_direct_encoding;	// l_boolvars_direct
	get_all_children_with_flags_ordered(*l_id_trans_bin_direct, Flags(Flag::VARIABLE | Flag::BOOLEAN | is_aux), 0, l_children_from_transformation_direct_encoding);

	std::vector<unsigned int> neg_children_from_transformation_direct_encoding;
	for (auto& x : *l_children_from_transformation_direct_encoding) {
		chr::Logical_var< unsigned int > l_id;
		get_opposite_var(x, l_id);
		if (l_id.ground()) {
			neg_children_from_transformation_direct_encoding.push_back(*l_id);
		}
	}

	unsigned int count_log_children = (*l_children_from_transformation_log_encoding).size();
	std::cout << "\tcount_log_children: " << count_log_children << std::endl;


	// N-ARY Clause (allowed values) --------------------------------

	for(unsigned int i=0; i<count_log_children; ++i)
	{
		std::cout << "\tdomain: " << domain.min() << "," << domain.max() << std::endl;

		unsigned int id_disjunction = _first_free_id++;
		vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
		edge(l_id_trans_bin_direct, id_disjunction, -1);
		edge(l_id_trans_bin_log, id_disjunction, -1);

		edge(id_disjunction, neg_children_from_transformation_log_encoding[i], -1);
		std::cout << "\t(t1) " << id_disjunction << ":\t" << neg_children_from_transformation_log_encoding[i] << " ";

		unsigned int idx_direct_var = 0;
		unsigned int count = 1;

		for (auto e : domain.domain)
		{
			// for negative values, use 2-complement
			if (e < 0) {
				e = ~e;
				e += 1;
			}
			if ( ((e >> i) & 0x01) == 0x01 ) {
				edge(id_disjunction, (*l_children_from_transformation_direct_encoding)[idx_direct_var], count++);
				std::cout << (*l_children_from_transformation_direct_encoding)[idx_direct_var] << " " ;
			}
			++idx_direct_var;
		}
		std::cout << std::endl;
	}


	// BINARY clauses (forbidden values) ----------------------------

	for(unsigned int i=0; i < count_log_children; ++i)
	{
		unsigned int idx_direct_var = 0;
		for (auto e : domain.domain)
		{
			// for negative values, use 2-complement
			if (e < 0) {
				e = ~e;
				e += 1;
			}
			if ( ((e >> i) & 0x01) == 0x01 ) {
				unsigned int id_disjunction = _first_free_id++;
				vertice(id_disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
				edge(l_id_trans_bin_direct, id_disjunction, -1);
				edge(l_id_trans_bin_log, id_disjunction, -1);

				edge(id_disjunction, (*l_children_from_transformation_log_encoding)[i], -1);
				edge(id_disjunction, neg_children_from_transformation_direct_encoding[idx_direct_var], -1);
				std::cout << "\t(t2) " << id_disjunction << ":\t" << (*l_children_from_transformation_log_encoding)[i] << " " << neg_children_from_transformation_direct_encoding[idx_direct_var] << std::endl;
			}
			++idx_direct_var;
		}
	}
	std::cout << std::endl;
}
*/
void GRAPH::link_log_to_direct_encoding(unsigned int id_var_integer) {
	std::cout << std::endl;
}




// ==================================================================
// PROPAGATE LOG-ENCODED VARIABLE
// It does not encode the variable; but it adds the subinterval
// treatment, necessary for propagation
// ==================================================================
/*
void GRAPH::propagate_variable_log(unsigned int vid)
{

	// check: verify variable tag
	{
		chr::Logical_var<Flags>       flags;
		chr::Logical_var<std::string> label;
		chr::Logical_var<Domain>      domain;
		get_vertice(vid, flags, label, domain);
		if (!check(flags, Flags(Flag::VARIABLE | Flag::PROP_INTEGER))) {
				std::cerr << "propagate_variable(): Variable flag does not correspond (PROP_INTEGER required)." << std::endl;
			return;
		}
	}


	// If LOG ENCODING has not been done, call it
	chr::Logical_var<unsigned int> var_transformation;
	get_child_with_flags(vid, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), var_transformation);
	if (!var_transformation.ground()) {
		encode_variable(vid, Flag::LOG_ENCODING); 	//build_log_encoding_from(vid, false);
	}

	// Once the encode has been done, retrieve boolean vars
	chr::Logical_var_mutable< std::vector<unsigned int> > boolvars_x;
	get_all_children_with_flags_ordered(*var_transformation, Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, boolvars_x);
	get_all_children_with_flags_ordered(*var_transformation, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, boolvars_x);
	assert((*boolvars_x).size() > 0);

	// Add a TRANSFORMATION node
	unsigned int transformation_id = _first_free_id++;
	edge(vid, transformation_id, -1);
	vertice(transformation_id, Flags(Flag::TRANSFORMATION | Flag::PROP_INTEGER | Flag::INTERVAL_BRANCHING), N_STR, IntVarDomain()); num_transformations++;

	// Get negative boolean variables
	std::vector<unsigned int> negboolvars_x;
	for (unsigned int i=0; i<(*boolvars_x).size(); ++i) {
		chr::Logical_var<unsigned int> lnx;
		get_opposite_var((*boolvars_x)[i], lnx);
		negboolvars_x.push_back(lnx);
	}


	// --------------------------------------------------------------
	// CNF sub-intervals
	//
	// Activation/deactivation of sub-intervals.
	// One boolean variable per sub-interval.
	//
	// 				s 5 4 3 2 1 0
	// B1	[4,7]	0 0 0 0 1 ? ?	(~xs & ~x5 & ~x4 & ~x3 & x2)
	// B2	[8,15]	0 0 0 1 ? ? ?	(~xs & ~x5 & ~x4 & x3)
	// B3	[16,31]	0 0 1 ? ? ? ?	(~xs & ~x5 & x4)
	// B4	[32,46]	0 1 ? ? ? ? ?	(~xs & x5)
	//
	// left-side : sub-interval vars (b), they change their polarity in CNF
	// right-side: log-encoded variables to fix
    //
    // EXP:	B <=> (x & y & z)
    // CNF: (~B|x) & (~B|y) & (~B|z) & (B|~x|~y|~z)
	// --------------------------------------------------------------

	chr::Logical_var<Flags>       flags;
	chr::Logical_var<std::string> label;
	chr::Logical_var<Domain>      domain;
	get_vertice(vid, flags, label, domain);
	int vmin = (*domain).min();
	int vmax = (*domain).max();


	//std::vector<int> cutpoint = interval_cutpoints(vmin,vmax,3);
	std::vector<unsigned int> cutpoint;
	for(unsigned int i = logb(vmin)+1; pow(2,i)<vmax; ++i) {
        cutpoint.push_back(pow(2,i));
    }
	std::cout << "cutpoints: ";
	for (unsigned int i=0; i<cutpoint.size(); i++) { std::cout << cutpoint[i] << " "; }
	std::cout << std::endl;

	// build the intervals
    int lbound = vmin;
    int ubound = cutpoint[0];

    for(unsigned int i=0; i<=cutpoint.size(); ++i)
	{
        lbound = (i == 0) ? vmin : lbound;
        ubound = (i == cutpoint.size()) ? vmax : cutpoint[i]-1;
        std::cout << "b" << i << ": (" << lbound << "," << ubound << ")\t" ; //<< std::endl;

        // create variable
		std::string bname = std::string("BOX_") + std::to_string(i);
		unsigned int bvar  = _first_free_id++;
		unsigned int nbvar = _first_free_id++;
		vertice(bvar,  Flags(Flag::VARIABLE | Flag::BOOLEAN), bname, IntVarDomain({0,1}) ); num_variables++;
		vertice(nbvar, Flags(Flag::VARIABLE | Flag::BOOLEAN), bname, IntVarDomain({0,1}) ); num_variables++;
		literals(var_transformation, bvar, nbvar);
		std::cout << "(" << bvar << "," << nbvar << ")" << std::endl;

		// boundaries in binary representation
		std::vector<unsigned int> lbin = dec2binary(lbound, (*boolvars_x).size());
		std::reverse(lbin.begin(),lbin.end());
		std::vector<unsigned int> ubin = dec2binary(ubound, (*boolvars_x).size());
		std::reverse(ubin.begin(),ubin.end());

//		for (unsigned int b=lbin.size()-1; 0<=b; ++b) {std::cout << "(" << lbin[b] << "," << ubin[b] << ")\t";
//		for (unsigned int b=0; b<lbin.size(); ++b) std::cout << lbin[b];
//		std::cout << std::endl;
//		for (unsigned int b=0; b<ubin.size(); ++b) std::cout << ubin[b];
//		std::cout << std::endl;

		unsigned int nary_clause = _first_free_id++;
		vertice(nary_clause, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
		edge(nary_clause, bvar,-1);
		std::cout << "- \t" << bvar << std::endl;

		// magnitude bits
		for (int b=(lbin.size()-2); ((0<=b) && (lbin[b]==ubin[b])); --b)
		{
			unsigned int binary_clause = _first_free_id++;
			vertice(binary_clause, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
			edge(binary_clause, nbvar, -1);

			if (lbin[b] == 0) {
				edge(nary_clause,   (*boolvars_x)[b], -1);	// n-ary clause
				edge(binary_clause, negboolvars_x[b], -1);	// binary clause
				std::cout << b << "\t" << (*boolvars_x)[b] << "\t" << nbvar << " | " << negboolvars_x[b] << std::endl;
			}
			else {
				edge(nary_clause,   negboolvars_x[b], -1);	// n-ary clause
				edge(binary_clause, (*boolvars_x)[b], -1);	// binary clause
				std::cout << b << "\t" << negboolvars_x[b] << "\t" << nbvar << " | " << (*boolvars_x)[b] << std::endl;
			}
		}

		// sign bit
		{
			unsigned int binary_clause = _first_free_id++;
			vertice(binary_clause, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
			edge(binary_clause, nbvar, -1);

			if ((0<lbound) && (0<ubound))
			{
				edge(nary_clause, (*boolvars_x).back(), -1);	// n-ary clause
				edge(binary_clause, negboolvars_x.back(), -1);	// binary clause
				std::cout << "(s)\t" << (*boolvars_x).back() << "\t" << nbvar << " | " << negboolvars_x.back() << std::endl;
			}
			else if ((lbound<0) && (ubound<0))
			{
				edge(nary_clause, negboolvars_x.back(), -1);	// n-ary clause
				edge(binary_clause, (*boolvars_x).back(), -1);	// binary clause
				std::cout << "(s)\t" << negboolvars_x.back() << "\t" << nbvar << " | " << (*boolvars_x).back() << std::endl;
			}
		}

        // update lbound for the next sub-interval
        lbound = ubound+1;
        std::cout << std::endl;
    }
}
*/



// ==========================================================================
// Create SubInterval Variables
// (~ decompose_variable_domain)
//
// Creates the sub-interval variables
//
// input: Variable ID + Encoding
// Check variable is PROP_INTEGER
// ==========================================================================

void GRAPH::create_subintervals(unsigned int id, unsigned long encoding)
{
    std::cout << "Graph::create_subintervals(" << id << ")" << std::endl;

    // integer variable information ---------------------------------
    chr::Logical_var<Flags> flags;
	chr::Logical_var<std::string> label;
	chr::Logical_var<Domain> domain;
	get_vertice(id, flags, label, domain);
	int dmin = (*domain).min();
	int dmax = (*domain).max();


	// integer sub-intervals ----------------------------------------
	std::vector<Interval> dintervals = subintervals2(dmin, dmax);
	//for(unsigned int i=0; i<dintervals.size(); ++i)
	//	std::cout << "i" << i << ": " << dintervals[i].l << "," << dintervals[i].u << std::endl;


    // sub-interval transformation ----------------------------------
	chr::Logical_var<unsigned int> trans_tid;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::PROP_INTEGER | Flag::SUBINTERVAL), trans_tid);


    // sub-interval variables ---------------------------------------
    unsigned int transformation_id;
	std::vector<unsigned int> boite;
	std::vector<unsigned int> negboite;

	if (!trans_tid.ground())
	{
		std::cout << "\tcase: create des boites";
		// new transformation
		transformation_id = _first_free_id++;
		vertice(transformation_id, Flags(Flag::TRANSFORMATION | Flag::PROP_INTEGER | Flag::SUBINTERVAL), N_STR, IntVarDomain()); num_transformations++;
		edge(id, transformation_id, -1);
		std::cout << "(tid: " << transformation_id << ")" << std::endl;

		// subinterval variables
		for(unsigned int i=0; i<dintervals.size(); ++i)
		{
			std::string bname = std::string("B") + std::to_string(i);
			unsigned int bvar  = _first_free_id++;
			unsigned int nbvar = _first_free_id++;
			vertice(bvar,  Flags(Flag::VARIABLE | Flag::BOOLEAN), bname, IntVarDomain({0,1}) ); num_variables++;
			vertice(nbvar, Flags(Flag::VARIABLE | Flag::BOOLEAN), bname, IntVarDomain({0,1}) ); num_variables++;
			literals(transformation_id, bvar, nbvar,i);
			std::cout << "\t" << bname << ":\t" << "[" << dintervals[i].l << "," << dintervals[i].u << "]\t";
			std::cout << "(" << bvar << "," << nbvar << ")" << std::endl;

			boite.push_back(bvar);
			negboite.push_back(nbvar);
		}
	}
	else
	{
		std::cout << "\tcase: retrieve boites ";
		transformation_id = (*trans_tid);
		std::cout << "(tid: " << transformation_id << ")" << std::endl;

		// retrieve sub-interval variables (they are decision vars, by definition)
		chr::Logical_var_mutable< std::vector<unsigned int> > bvar;
		get_all_children_with_flags_ordered(transformation_id, Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, bvar);
		assert((*bvar).size()>0);

		// retrieve negated log-encoded literals
		for (unsigned int i=0; i<(*bvar).size(); ++i) {
			boite.push_back((*bvar)[i]);

			chr::Logical_var<unsigned int> nbvar;
			get_opposite_var((*bvar)[i], nbvar);
			assert(nbvar.ground());
			negboite.push_back(nbvar);
		}

	}

	// sub-interval clauses -----------------------------------------
	for(unsigned int i=0; i<boite.size(); ++i) {
		if (encoding == Flag::DIRECT_ENCODING)
			add_subinterval_clauses_direct(id, transformation_id, dintervals[i].l, dintervals[i].u, boite[i], negboite[i]);
		else if (encoding == Flag::LOG_ENCODING)
			add_subinterval_clauses_log(id, transformation_id, dintervals[i].l, dintervals[i].u, boite[i], negboite[i]);
	}

	std::cout << std::endl;
}



// ==================================================================
// CNF sub-intervals LOG
//
// Activation/deactivation of sub-intervals.
// One boolean variable per sub-interval.
//
// 				s 5 4 3 2 1 0
// B1	[4,7]	0 0 0 0 1 ? ?	(~xs & ~x5 & ~x4 & ~x3 & x2)
// B2	[8,15]	0 0 0 1 ? ? ?	(~xs & ~x5 & ~x4 & x3)
// B3	[16,31]	0 0 1 ? ? ? ?	(~xs & ~x5 & x4)
// B4	[32,46]	0 1 ? ? ? ? ?	(~xs & x5)
//
// left-side : sub-interval vars (b), they change their polarity in CNF
// right-side: log-encoded variables to fix
//
// EXP:	B <=> (x & y & z)
// CNF: (~B|x) & (~B|y) & (~B|z) & (B|~x|~y|~z)
// ==================================================================

void GRAPH::add_subinterval_clauses_log(unsigned int variable_id, unsigned int transformation_id, int lbound, int ubound, unsigned int bvar, unsigned int nbvar)
{
	std::cout << "Graph::add_subinterval_clauses_log(" << variable_id << "," << transformation_id << ")" << std::endl;

	// if LOG encoding does not exists, do not add clauses
	chr::Logical_var<unsigned int> log_tid;
	get_child_with_flags(variable_id, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), log_tid);
	if (!log_tid.ground()) {
		std::cout << "\tlog encoding does not exists" << std::endl;
		return;
	}
	std::cout << "\tlog_tid: " << log_tid << std::endl;

	// retrieve log-encoded (boolean) literals
	chr::Logical_var_mutable< std::vector<unsigned int> > log_vars;
	get_all_children_with_flags_ordered(log_tid, Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, log_vars);
	get_all_children_with_flags_ordered(log_tid, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, log_vars);
	assert((*log_vars).size()>0);

	// retrieve negated log-encoded literals
	std::vector<unsigned int> nlog_vars;
	for (unsigned int i=0; i<(*log_vars).size(); ++i) {
		chr::Logical_var< unsigned int> nl;
		get_opposite_var((*log_vars)[i], nl);
		assert(nl.ground());
		nlog_vars.push_back(nl);
	}

	// (lbound,ubound) in binary representation
	std::vector<unsigned int> lbin = dec2binary(lbound, (*log_vars).size());
	std::reverse(lbin.begin(),lbin.end());
	std::vector<unsigned int> ubin = dec2binary(ubound, (*log_vars).size());
	std::reverse(ubin.begin(),ubin.end());

	std::cout << "\t[" << lbound << "," << ubound << "]\t";
	for (unsigned int b=0; b<lbin.size(); ++b) std::cout << lbin[b];
	std::cout << "  ";
	for (unsigned int b=0; b<ubin.size(); ++b) std::cout << ubin[b];
	std::cout << std::endl;

	// n-ary clause
	{
		unsigned int nclause = _first_free_id++;
		vertice(nclause, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
		edge(transformation_id, nclause, -1);
		edge(nclause, bvar,-1);
		std::cout << "\t(n) " << nclause << ": " << bvar << " ";

		// magnitude bits
		for (int b=(lbin.size()-2); ((0<=b) && (lbin[b]==ubin[b])); --b) {
			if (lbin[b] == 0) {
				edge(nclause, (*log_vars)[b], -1);
				std::cout << (*log_vars)[b] << " ";
			} else {
				edge(nclause, nlog_vars[b], -1);
				std::cout << nlog_vars[b] << " ";
			}
		}

		// sign bit (+,-)
		if (0<=lbound) {
			edge(nclause, (*log_vars).back(), -1);
			std::cout << (*log_vars).back() << std::endl;
		} else if (ubound<0) {
			edge(nclause, nlog_vars.back(), -1);
			std::cout << nlog_vars.back() << std::endl;
		}
	}

	// binary clauses
	{
		// magnitude bits
		for (int b=(lbin.size()-2); ((0<=b) && (lbin[b]==ubin[b])); --b)
		{
			unsigned int bclause = _first_free_id++;
			vertice(bclause, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
			edge(transformation_id, bclause, -1);
			edge(bclause, nbvar, -1);
			std::cout << "\t(b) " << bclause << ": " << nbvar << " ";

			if (lbin[b] == 0) {
				edge(bclause, nlog_vars[b], -1);
				std::cout << nlog_vars[b] << std::endl;
			} else {
				edge(bclause, (*log_vars)[b], -1);
				std::cout << (*log_vars)[b] << std::endl;
			}
		}

		// sign bit
		unsigned int bclause = _first_free_id++;
		vertice(bclause, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
		edge(bclause, nbvar, -1);
		std::cout << "\t(b) " << bclause << ": " << nbvar << " ";

		if (0<=lbound) {
			edge(bclause, nlog_vars.back(), -1);
			std::cout << nlog_vars.back() << std::endl;
		} else if (ubound<0) {
			edge(bclause, (*log_vars).back(), -1);
			std::cout << (*log_vars).back() << std::endl;
		}
	}
	std::cout << std::endl;
}




// ==================================================================
// CNF sub-intervals DIRECT ENCODING
//
// B1 [0,1] <=> (x0|x1) & (~x2 & ~x3)
// B2 [2,3] <=> (x2|x3) & (~x0 & ~x1)
//
// left-side : sub-interval vars (b)
// right-side: log-encoded variables to fix
//
// EXP:	B <=> (x & y & z)
// CNF: (~B|x) & (~B|y) & (~B|z) & (B|~x|~y|~z)
//
// --------------------------------------------------------------
// variable_id        Variable with sub-intervals
// transformation_id  Sub-interval transformation ID
// lbound,ubound      Sub-interval boundaries
// b,nb               Sub-interval variables.
//
// ==================================================================

void GRAPH::add_subinterval_clauses_direct(unsigned int variable_id, unsigned int transformation_id, int lbound, int ubound, unsigned int b, unsigned int nb)
{
	std::cout << "GRAPH::add_subinterval_clauses_direct(" << variable_id << "," << transformation_id << ")" << std::endl;

	// if DIRECT encoding does not exists, do not add clauses
	chr::Logical_var<unsigned int> direct_tid;
	get_child_with_flags(variable_id, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::DIRECT_ENCODING), direct_tid);
	if (!direct_tid.ground()) {
		std::cout << "\tdirect encoding does not exists" << std::endl;
		return;
	}

	// direct encoding transformation_id
	std::cout << "\tdirect_tid: " << direct_tid << std::endl;

	// domain information
	int dmin; //, dmax;
	{
		chr::Logical_var<Flags> flags;
		chr::Logical_var<std::string> label;
		chr::Logical_var<Domain> domain;
		get_vertice(variable_id,flags,label,domain);
		dmin = (*domain).min();
		//dmax = (*domain).max();
	}

	// retrieve ALL boolean variables for the domain
	chr::Logical_var_mutable< std::vector<unsigned int> > direct_vars;
	get_all_children_with_flags_ordered(direct_tid, Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, direct_vars);
	get_all_children_with_flags_ordered(direct_tid, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, direct_vars);
	assert((*direct_vars).size()>0);

	std::vector<unsigned int> direct_negvars;
	for (unsigned int i=0; i<(*direct_vars).size(); i++)
	{
		chr::Logical_var< unsigned int> negvar;
		get_opposite_var((*direct_vars)[i], negvar);
		assert(negvar.ground());
		direct_negvars.push_back((*negvar));
	}

	// Clause #1: A single N-ARY clause with variable IN the sub-interval
	{
		unsigned int clause = _first_free_id++;
		vertice(clause, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
		edge(transformation_id, clause, -1);
		edge(clause, nb, -1);
		std::cout << "\t(t1) " << clause << ":\t" << nb << " ";

		for (unsigned int i=0; i<(*direct_vars).size(); i++) {
			if ((lbound <= static_cast<int>(dmin+i)) && (static_cast<int>(dmin+i) <= ubound)) {
				edge(clause, (*direct_vars)[i], -1);
				std::cout << (*direct_vars)[i] << " ";
			}
		}
		std::cout << std::endl;
	}

	// Clause #2: One BINARY clause for each variable OUT of the subinterval
	{
		for (unsigned int i=0; i<(*direct_vars).size(); i++)
		{
			if ((static_cast<int>(dmin+i) < lbound) || (ubound < static_cast<int>(dmin+i)))
			{
				unsigned int clause = _first_free_id++;
				vertice(clause, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
				edge(transformation_id, clause, -1);
				edge(clause, nb, -1);
				std::cout << "\t(t2) " << clause << ":\t" << nb << " ";

				edge(clause, direct_negvars[i], -1);
				std::cout << direct_negvars[i] << std::endl;
			}
		}
	}

	// Clause #3: For each variable IN the subinterval, add the variable + all negated variables OUT of subinterval
	{
		for (unsigned int i=0; i<(*direct_vars).size(); i++)
		{
			if ((lbound <= static_cast<int>(dmin+i)) && (static_cast<int>(dmin+i) <= ubound))
			{
				unsigned int clause = _first_free_id++;
				vertice(clause, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
				edge(transformation_id, clause, -1);
				edge(clause, b, -1);
				std::cout << "\t(t3) " << clause << ":\t" << b << " ";

				edge(clause, direct_negvars[i], -1);
				std::cout << direct_negvars[i] << " ";

				for (unsigned int j=0; j<(*direct_vars).size(); j++)
				{
					if ((static_cast<int>(dmin+j) < lbound) || (ubound < static_cast<int>(dmin+j)))
					{
						edge(clause, (*direct_vars)[j], -1);
						std::cout << (*direct_vars)[j] << " ";
					}
				}
				std::cout << std::endl;
			}
		}
	}
	std::cout << std::endl;
}




// ==========================================================================
//
// Hybrid Order-Log Encoding
//
// ==========================================================================

void GRAPH::build_hybrid_encoding(unsigned int id, bool force_auxiliary)
{
	std::cout << "hybrid_encoding[" << id << "]" << std::endl;

	// checks -------------------------------------------------------
	{
		// check if transformation exists
		chr::Logical_var<unsigned int> tid;
		get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::HYBRID_ENCODING), tid);
		if (tid.ground()) {
			std::cout << std::endl;
			return;
		}

		// encoding
		assert(HBASE > 0);
	}


	// variable -----------------------------------------------------

	chr::Logical_var<Flags>       flags;
	chr::Logical_var<std::string> label;
	chr::Logical_var<Domain>      domain;
	get_vertice(id, flags, label, domain);

	// domain (min, max, length, offset)
	int dmin = (*domain).min();
	int dmax = (*domain).max();
	unsigned int dlength = h_domain_length(dmin,dmax,HBASE);
	int doffset = h_domain_offset(dmin,dmax,HBASE);
	hvar_offset[id] = doffset;

	std::cout << "domain: [" << dmin << "," << dmax << "]\t";
	std::cout << "length: " << dlength << "\t";
	std::cout << "offset: " << doffset << "\t";

	// semantics (decision,auxiliary)
	unsigned long is_aux = 0;
	if (check(*flags, Flag::AUXILIARY) || force_auxiliary) {
		is_aux = Flag::AUXILIARY;
		std::cout << "auxiliary";
	}
	std::cout << std::endl;


	// transformation -----------------------------------------------

	unsigned int id_transformation = _first_free_id++;
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::HYBRID_ENCODING), N_STR, IntVarDomain()); num_transformations++;
	edge(id, id_transformation, -1);


	// encoding ----------------------------------------------------

	std::vector<unsigned int> positive_ids, negative_ids;
	std::cout << "vars  : ";
	for (unsigned int i=0; i<dlength; ++i)
	{
		std::string varname = std::string("X") + std::to_string(i);

		unsigned int l = _first_free_id++;
		vertice(l, Flags(Flag::VARIABLE | Flag::BOOLEAN | is_aux), varname, IntVarDomain({0,1})); num_variables++;
		positive_ids.push_back(l);

		unsigned int nl = _first_free_id++;
		vertice(nl, Flags(Flag::VARIABLE | Flag::BOOLEAN | is_aux), varname, IntVarDomain({0,1})); num_variables++;
		negative_ids.push_back(nl);

		literals(id_transformation,l,nl,i);
		std::cout << "(" << l << "," << nl << ") ";
	}
	std::cout << std::endl;


	// order-property: 1's to the left
	std::cout << "order encoding clauses..." << std::endl;
	for (unsigned int i=(HBASE+1); i<negative_ids.size(); ++i) {
		unsigned int p = clause(id_transformation, negative_ids[i], positive_ids[i-1]);
		std::cout << "(prop) " << p << ":\t" << negative_ids[i] << " " << positive_ids[i-1] << std::endl;
	}


    // single-value / no-goods --------------------------------------

	if (dmin == dmax)
	{
		std::cout << "single-value clause..." << std::endl;
		std::vector<bool> hvalue = value2hybrid(dmin,HBASE, doffset, positive_ids.size());

		std::cout << dmin << "\t";
		for (unsigned int i=0; i<hvalue.size(); i++) {
			std::cout << hvalue[i];
		}
		std::cout << std::endl;

//std::cout << "dlength: " << dlength << std::endl;
        for (unsigned int i=0; i<hvalue.size(); ++i) {
            if (hvalue[i])
                clause(id_transformation,positive_ids[i]);
            else
                clause(id_transformation,negative_ids[i]);
        }
	}
	else
	{
		// maximum reachable domain
		int emin = (dmin<0) ? bzvalue(positive_ids.size()-1,HBASE, doffset) : 0;
		int emax = (dmax<0) ? 0 : bzvalue(positive_ids.size()-1                                                                                                                                                                                                                ,HBASE, doffset) + (pow(2,HBASE)-1);

		std::cout << "removing no-goods.." << std::endl;
		for (int e=emin; e<=emax; e++) {

			if ((e<dmin) || (dmax<e)) {
				std::cout << e << "\t";
				std::vector<bool> value = value2hybrid(e,HBASE,doffset,positive_ids.size());
				for (unsigned int i=0; i<value.size(); i++)
					std::cout << value[i] ;
				std::cout << "\t";

				//std::cout << "positive.size(): " << positive_ids.size() << std::endl;
				//std::cout << "negative.size(): " << negative_ids.size() << std::endl;
				//std::cout << "value.size()   : " << value.size() << std::endl;

				// clause
				unsigned int nogood = _first_free_id++;
				vertice(nogood, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
				edge(id_transformation, nogood, -1);
				std::cout << "(ng) " << nogood << ": " ;

				for (unsigned int i=0; i<value.size(); i++) {
					if (value[i]) {
						edge(nogood,negative_ids[i],-1);
						std::cout << negative_ids[i] << " ";
					}
					else {
						edge(nogood,positive_ids[i],-1);
						std::cout << positive_ids[i] << " ";
					}
				}
				std::cout << std::endl;
			}
		}
	}

	std::cout << std::endl << std::endl;
}
