/*
 * PROPAGATIONS
 *
 * propagate_addition() : v1, v2
 *
 */

#include <graph_chr_header.hpp>
//#include "utils.hh"

#define N_STR std::string()


/*
std::vector<unsigned int> dec2binary(int n, int nbits)
{
    std::vector<unsigned int> output;
    for (int i=(nbits-1); i >= 0; i--) {
        int k = n >> i;
        if (k & 1)
            output.push_back(1);
        else
            output.push_back(0);
    }
    return output;
}*/



/* ===========================================================================
// PROPAGATE ADDITION - v1
//
// Simulates the CSP propagation by splitting the addition
// into their minimal (') and maximal (*) boundaries.
//
// input  : A+B=Z
// output : ( a'+b'=z' ) ( a*+b*=z* ) ( z'<= Z <= z* )
// ===========================================================================

void GRAPH::propagate_addition(unsigned int id)
{
	// Check --------------------------------------------------------
	{
		// transformation already exists?
		chr::Logical_var< unsigned int > l_check;
		get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::PROP_ADD_EQUAL | Flag::ADD_EQUAL), l_check);
		if (l_check.ground())
			return;

		// correct number of variables
		chr::Logical_var_mutable< std::vector<unsigned int> > l_relation_vars;
		get_all_children(id, l_relation_vars);
		assert((*l_relation_vars).size() == 3);
	}

	std::cout << std::endl << "GRAPH::propagate_addition(" << id << ")" << std::endl;

    // transformation ID
	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::PROP_ADD_EQUAL | Flag::ADD_EQUAL), N_STR, IntVarDomain()); num_transformations++;

	// get interval variables
	std::vector<unsigned int> var, minvar, maxvar;
	std::vector<int> mindom, maxdom;

	for (unsigned int i=0; i<3; ++i)
	{
		// get relation's variables (l_relation_vars)
		chr::Logical_var<unsigned int> l_rvar;
		get_child_with_order(id, i ,l_rvar);
		assert(l_rvar.ground());

		// get variable transformation
		chr::Logical_var< unsigned int > var_transformation;
		get_child_with_flags(l_rvar, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::VAR_INTERVAL), var_transformation);
		if (!var_transformation.ground()) {
			build_variable_interval(l_rvar);
			get_child_with_flags(l_rvar, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::VAR_INTERVAL), var_transformation);
		}
		assert(var_transformation.ground());

		// interval variables
		chr::Logical_var_mutable< std::vector<unsigned int> > interval_vars;
		get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), 0, interval_vars);
		//get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::PROP_INTEGER | Flag::AUXILIARY), 0, interval_vars);
		get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::INTEGER), 0, interval_vars);
		get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), 0, interval_vars);
		assert((*interval_vars).size() == 3);

		var.push_back((*interval_vars)[0]);
		minvar.push_back((*interval_vars)[1]);
		maxvar.push_back((*interval_vars)[2]);

		// get domain
		chr::Logical_var<Flags >       v_flags;
		chr::Logical_var<std::string > v_label;
		chr::Logical_var<Domain >      v_domain;
		get_vertice((*interval_vars)[0], v_flags, v_label, v_domain);
		int dmin = (*v_domain).min();
		int dmax = (*v_domain).max();
		mindom.push_back(dmin);
		maxdom.push_back(dmax);
	}


	// ----------------------------------------------------
	// A+B=Z : The original addition
	// ----------------------------------------------------

	unsigned int addition = _first_free_id++;
	vertice(addition, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, addition, -1);
	edge(addition, var[0], 0);
	edge(addition, var[1], 1);
	edge(addition, var[2], 2);
	std::cout << addition << " :\t" << var[0] << " + " << var[1] << " = " << var[2] << std::endl;
	std::cout << std::endl;


	// ----------------------------------------------------
	// Interval expressions for : A+B=Z
	// ( a'+b'=o1' ) ( a*+b*=o1* ) ( o1'<= Z <= o1* )
	// ----------------------------------------------------
	{
		unsigned int oz_min = _first_free_id++;
		unsigned int oz_max = _first_free_id++;

		IntVarDomain domain;
		//for (int i=mindom[2]; i<=maxdom[2]; ++i)
		//for (int i=-100; i<=100; ++i)
		for (int i=(mindom[0]+mindom[1]); i<=(maxdom[0]+maxdom[1]); i++)
			domain.domain.push_back(i);
		vertice(oz_min, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, domain); num_variables++;
		vertice(oz_max, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, domain); num_variables++;

		unsigned int min_addition = _first_free_id++;
		vertice(min_addition, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, min_addition, -1);
		edge(min_addition, minvar[0], 0);
		edge(min_addition, minvar[1], 1);
		edge(min_addition, oz_min,    2);
		std::cout << min_addition << " :\t" << minvar[0] << " + " << minvar[1] << " = " << oz_min << std::endl;

		unsigned int max_addition = _first_free_id++;
		vertice(max_addition, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, max_addition, -1);
		edge(max_addition, maxvar[0], 0);
		edge(max_addition, maxvar[1], 1);
		edge(max_addition, oz_max,    2);
		std::cout << max_addition << " :\t" << maxvar[0] << " + " << maxvar[1] << " = " << oz_max << std::endl;

		// OZ- <= Z
		unsigned int result_lt = _first_free_id++;
		vertice(result_lt, Flags(Flag::RELATION | Flag::LESS_EQUAL_THAN), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, result_lt, -1);
		edge(result_lt, oz_min, 0);
		edge(result_lt, var[2], 1);
		std::cout << result_lt << " :\t" << oz_min << " <= " << var[2] << std::endl;

		// Z <= OZ+
		unsigned int result_gt = _first_free_id++;
		vertice(result_gt, Flags(Flag::RELATION | Flag::LESS_EQUAL_THAN), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, result_gt, -1);
		edge(result_gt, var[2], 0);
		edge(result_gt, oz_max, 1);
		std::cout << result_gt << " :\t" << var[2] << " <= " << oz_max << std::endl;
	}
	std::cout << std::endl;


	// ----------------------------------------------------
	// Z-B=A
	// ( z'-b*=o2' ) ( z*-b*=o2* ) ( o2'<= A <= o2* )
	// ----------------------------------------------------
	{
		unsigned int oa_min = _first_free_id++;
		unsigned int oa_max = _first_free_id++;

		IntVarDomain domain;
		for (int i=(mindom[2]-maxdom[1]); i<=(maxdom[2]-mindom[1]); i++)
			domain.domain.push_back(i);
		vertice(oa_min, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, domain); num_variables++;
		vertice(oa_max, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, domain); num_variables++;

		unsigned int min_addition = _first_free_id++;
		vertice(min_addition, Flags(Flag::RELATION | Flag::SUB_EQUAL), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, min_addition, -1);
		edge(min_addition, minvar[2], 0);
		edge(min_addition, maxvar[1], 1);
		edge(min_addition, oa_min,    2);
		std::cout << min_addition << " :\t" << minvar[2] << " - " << maxvar[1] << " = " << oa_min << std::endl;

		unsigned int max_addition = _first_free_id++;
		vertice(max_addition, Flags(Flag::RELATION | Flag::SUB_EQUAL), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, max_addition, -1);
		edge(max_addition, maxvar[2], 0);
		edge(max_addition, minvar[1], 1);
		edge(max_addition, oa_max,    2);
		std::cout << max_addition << " :\t" << maxvar[2] << " - " << minvar[1] << " = " << oa_max << std::endl;

		// OA- <= A
		unsigned int result_lt = _first_free_id++;
		vertice(result_lt, Flags(Flag::RELATION | Flag::LESS_EQUAL_THAN), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, result_lt, -1);
		edge(result_lt, oa_min, 0);
		edge(result_lt, var[0], 1);
		std::cout << result_lt << " :\t" << oa_min << " <= " << var[0] << std::endl;

		// A <= OA+
		unsigned int result_gt = _first_free_id++;
		vertice(result_gt, Flags(Flag::RELATION | Flag::LESS_EQUAL_THAN), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, result_gt, -1);
		edge(result_gt, var[0], 0);
		edge(result_gt, oa_max, 1);
		std::cout << result_gt << " :\t" << var[0] << " <= " << oa_max << std::endl;
	}
	std::cout << std::endl;


	// ----------------------------------------------------
	// Z-A=B
	// ( z'-a*=o3' ) ( z*-a*=o3* ) ( o3'<= B <= o3* )
	// ----------------------------------------------------

	// output
	unsigned int ob_min = _first_free_id++;
	unsigned int ob_max = _first_free_id++;
	{
		IntVarDomain domain;
		for (int i=(mindom[2]-maxdom[0]); i<=(maxdom[2]-mindom[0]); i++)
			domain.domain.push_back(i);
		vertice(ob_min, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, domain); num_variables++;
		vertice(ob_max, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, domain); num_variables++;

		unsigned int min_addition = _first_free_id++;
		vertice(min_addition, Flags(Flag::RELATION | Flag::SUB_EQUAL), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, min_addition, -1);
		edge(min_addition, minvar[2], 0);
		edge(min_addition, maxvar[0], 1);
		edge(min_addition, ob_min,    2);
		std::cout << min_addition << " :\t" << minvar[2] << " - " << maxvar[0] << " = " << ob_min << std::endl;

		unsigned int max_addition = _first_free_id++;
		vertice(max_addition, Flags(Flag::RELATION | Flag::SUB_EQUAL), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, max_addition, -1);
		edge(max_addition, maxvar[2], 0);
		edge(max_addition, minvar[0], 1);
		edge(max_addition, ob_max,    2);
		std::cout << max_addition << " :\t" << maxvar[2] << " - " << minvar[0] << " = " << ob_max << std::endl;

		// OB- <= B
		unsigned int result_lt = _first_free_id++;
		vertice(result_lt, Flags(Flag::RELATION | Flag::LESS_EQUAL_THAN), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, result_lt, -1);
		edge(result_lt, ob_min, 0);
		edge(result_lt, var[1], 1);
		std::cout << result_lt << " :\t" << ob_min << " <= " << var[1] << std::endl;

		// B <= OB+
		unsigned int result_gt = _first_free_id++;
		vertice(result_gt, Flags(Flag::RELATION | Flag::LESS_EQUAL_THAN), N_STR, IntVarDomain()); num_relations++;
		edge(id_transformation, result_gt, -1);
		edge(result_gt, var[1], 0);
		edge(result_gt, ob_max, 1);
		std::cout << result_gt << " :\t" << var[1] << " <= " << ob_max << "" << std::endl;
	}
	std::cout << std::endl;

}
*/


// ===========================================================================
// PROPAGATE ADDITION V2
// ===========================================================================

void GRAPH::propagate_addition(unsigned int id)
{
	// Check ----------------------------------------------
	{
		// transformation already exists?
		chr::Logical_var< unsigned int > l_check;
		get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::PROP_ADD_EQUAL | Flag::ADD_EQUAL), l_check);
		if (l_check.ground())
			return;

		// correct number of variables
		chr::Logical_var_mutable< std::vector<unsigned int> > l_relation_vars;
		get_all_children(id, l_relation_vars);
		assert((*l_relation_vars).size() == 3);
	}

	std::cout << std::endl << "GRAPH::propagate_addition(" << id << ")" << std::endl;

    // transformation ID
	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::PROP_ADD_EQUAL | Flag::ADD_EQUAL), N_STR, IntVarDomain()); num_transformations++;

	// get interval variables
	std::vector<unsigned int> var;

	for (unsigned int i=0; i<3; ++i)
	{
		// get relation's variables (l_rvars)
		chr::Logical_var<unsigned int> l_rvar;
		get_child_with_order(id, i ,l_rvar);
		assert(l_rvar.ground());

		// get variable transformation
		chr::Logical_var< unsigned int > var_transformation;
		get_child_with_flags(l_rvar, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::VAR_INTERVAL), var_transformation);
		if (!var_transformation.ground()) {
			encode_variable(l_rvar, Flag::LOG_ENCODING);	//build_variable_interval(l_rvar);
			get_child_with_flags(l_rvar, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::VAR_INTERVAL), var_transformation);
		}
		assert(var_transformation.ground());

		// interval variables
		chr::Logical_var_mutable< std::vector<unsigned int> > interval_vars;
		get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), 0, interval_vars);
		get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::INTEGER), 0, interval_vars);
		get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), 0, interval_vars);
		assert((*interval_vars).size() == 3);

		var.push_back((*interval_vars)[0]);
	}


	// A+B=Z
	unsigned int add_abz = _first_free_id++;
	vertice(add_abz, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, add_abz, -1);
	edge(add_abz, var[0], 0);
	edge(add_abz, var[1], 1);
	edge(add_abz, var[2], 2);
	std::cout << add_abz << " :\t" << var[0] << " + " << var[1] << " = " << var[2] << std::endl;

	// Z-A=B
	unsigned int zab = _first_free_id++;
	vertice(zab, Flags(Flag::RELATION | Flag::SUB_EQUAL), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, zab, -1);
	edge(zab, var[2], 0);
	edge(zab, var[0], 1);
	edge(zab, var[1], 2);
	std::cout << zab << " :\t" << var[2] << " - " << var[0] << " = " << var[1] << std::endl;

	// Z-B=A
	unsigned int zba = _first_free_id++;
	vertice(zba, Flags(Flag::RELATION | Flag::SUB_EQUAL), N_STR, IntVarDomain()); num_relations++;
	edge(id_transformation, zba, -1);
	edge(zba, var[2], 0);
	edge(zba, var[1], 1);
	edge(zba, var[0], 2);
	std::cout << zab << " :\t" << var[2] << " - " << var[1] << " = " << var[0] << std::endl;

}





// ===========================================================================
// INTERVAL BRANCHING to CNF
// 20190308:
//	1) For l_rvars, replace get_all_children_with_flags_ordered();
//	   use get_child_with_order() instead.
// ===========================================================================
/*
void GRAPH::transform_interval_branching_to_cnf(unsigned int id)
{
	std::cout << std::endl << "GRAPH::transform_interval_branching_to_cnf(" << id << ") " << std::endl;
	{
		// check: does the transformation already exists?
		chr::Logical_var< unsigned int > l_check;
		get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::INTERVAL_BRANCHING | Flag::BINARY_DISJUNCTION), l_check);
		if (l_check.ground())
			return;

		// check: right number of variables (3)
		chr::Logical_var_mutable< std::vector<unsigned int> > l_relation_vars;
		get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), 0, l_relation_vars);
		get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER), 0, l_relation_vars);
		get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), 0, l_relation_vars);
		assert((*l_relation_vars).size() == 3);
	}

	// get transformation IDs for the log-encoded variables (if needed, encode them)
	std::vector<unsigned int> var_transformation;
	int vmin=0, vmax=0;
	std::cout << "variables: ";
	for (unsigned int i=0; i<3; ++i)
	{
		// relation's variables (l_rvar)
		chr::Logical_var<unsigned int> l_rvar;
		get_child_with_order(id, i ,l_rvar);
		assert(l_rvar.ground());
		std::cout << l_rvar << " ";

		// if we're observing the 1st variable (X), obtain the domain interval
		if (i==0) {
			chr::Logical_var<Flags>       flags;
			chr::Logical_var<std::string> label;
			chr::Logical_var<Domain>      domain;
			get_vertice(l_rvar, flags, label, domain);
			vmin = (*domain).min();
			vmax = (*domain).max();
			//std::cout << "[" << vmin << "," << vmax << "] " ;
		}

		// variable transformation (l_id_trans_boolean)
		chr::Logical_var< unsigned int > l_id_trans_boolean;
		get_child_with_flags(l_rvar, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		if (!l_id_trans_boolean.ground()) {
			build_log_encoding_from(l_rvar);
			get_child_with_flags(l_rvar, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), l_id_trans_boolean);
		}
		assert(l_id_trans_boolean.ground());
		var_transformation.push_back(*l_id_trans_boolean);
		std::cout << " ";
	}
	std::cout << "[" << vmin << "," << vmax << "] " ;
	std::cout << std::endl;

	// X literals (main variable)
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_x;
	get_all_children_with_flags_ordered(var_transformation[0], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_x);
	get_all_children_with_flags_ordered(var_transformation[0], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_x);
	assert((*l_boolvars_x).size() > 0);

	// XMIN literals (minimum bound variable)
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_xmin;
	get_all_children_with_flags_ordered(var_transformation[1], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_xmin);
	get_all_children_with_flags_ordered(var_transformation[1], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_xmin);
	assert((*l_boolvars_xmin).size() > 0);

	// XMAX literals (maximum bound variable)
	chr::Logical_var_mutable< std::vector<unsigned int> > l_boolvars_xmax;
	get_all_children_with_flags_ordered(var_transformation[2], Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, l_boolvars_xmax);
	get_all_children_with_flags_ordered(var_transformation[2], Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, l_boolvars_xmax);
	assert((*l_boolvars_xmax).size() > 0);


	// transformation ID
	unsigned int id_transformation = _first_free_id++;
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::INTERVAL_BRANCHING | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;
	edge(id, id_transformation, -1);
	//std::cout << "transformation: " << id_transformation << std::endl;

	// --------------------------------------------------------------
	// intervals creation (tree)
	// --------------------------------------------------------------

	std::vector<int> cutpoint = interval_cutpoints(vmin,vmax,3);
	std::cout << "cutpoints: ";
	for (unsigned int i=0; i<cutpoint.size(); i++) {
		std::cout << cutpoint[i] << " "; }
	std::cout << std::endl;

	std::vector<int> lbound;
	std::vector<int> ubound;

	lbound.push_back(vmin);
	for (unsigned int i=0; i<cutpoint.size(); i++) {
		if (cutpoint[i] == lbound.back())
			ubound.push_back(cutpoint[i]);
		else
			ubound.push_back(cutpoint[i]-1);
		lbound.push_back(cutpoint[i]);
	}
	ubound.push_back(vmax);

	// remove duplicated intervals
	std::vector<int> interval_lower;
	std::vector<int> interval_upper;

	for (unsigned int i=1; i<lbound.size(); i++) {
		if ((lbound[i-1] != lbound[i]) || (ubound[i-1] != ubound[i]) ) {
			interval_lower.push_back( lbound[i-1] );
			interval_upper.push_back( ubound[i-1] );
		}
	}
	interval_lower.push_back( lbound.back() );
	interval_upper.push_back( ubound.back() );



	// --------------------------------------------------------------
	// Path Definition (with example)
	//          0 1 2 3 4 5 6
	// [4,7]	? ? 1 0	0 0 0   (x2 & ~x3 & ~x4..) => ...
	// [8,15]	? ? ? 1 0 0 0	(x3 & ~x4 & ~x5..) => ...
	// [16,31]	? ? ? ? 1 0 0	(x4 & ~x5 & ~x6) => ...
	// [32,46]	? ? ? ? ? 1 0	(x5 & ~x6) => ...
	//
	// left-vars : decision vars (x), they change their polarity in CNF
	// right-vars: min/max interval variables (x-,x+)
	//
    // EXP   : (a & b & c) => (x & y)
    // In CNF: (~a|~b|~c|x) & (~a|~b|~c|y)
	// --------------------------------------------------------------
	for (unsigned int i=0; i<interval_lower.size(); i++)
	{
		std::cout << "interval [" << interval_lower[i] << "," << interval_upper[i] << "] \t";

		// left-side
		std::vector<unsigned int> leftside;
		{
			std::vector<unsigned int> lbin = dec2binary(interval_lower[i], (*l_boolvars_x).size()-1);
			std::reverse(lbin.begin(),lbin.end());
			std::vector<unsigned int> ubin = dec2binary(interval_upper[i], (*l_boolvars_x).size()-1);
			std::reverse(ubin.begin(),ubin.end());

			// sign
			if (interval_lower[i] * interval_upper[i] >= 0) {
				if (interval_lower[i]>=0) {
					leftside.push_back((*l_boolvars_x).back());	// positive numbers use '0'
				} else {
					chr::Logical_var<unsigned int> lnx;
					get_opposite_var((*l_boolvars_x).back(), lnx);
					leftside.push_back(lnx);
				}
			}

			// magnitude
			for (int b=(ubin.size()-1); (0<=b) && (lbin[b] == ubin[b]); b--) {
				if (lbin[b] == 0)
					leftside.push_back((*l_boolvars_x)[b]);
				else {
					chr::Logical_var<unsigned int> lnx;
					get_opposite_var((*l_boolvars_x)[b], lnx);
					leftside.push_back(lnx);
				}
			}
		}
		//std::cout << std::endl;


		// right-side
		std::vector<unsigned int> rightside;
		std::vector<unsigned int> rightside_neg;	// negation of right-side variables. Utilized
        if (leftside.size()>0) {

			std::vector<unsigned int> xmin_bin = dec2binary(interval_lower[i], (*l_boolvars_xmin).size()-1);
			std::reverse(xmin_bin.begin(),xmin_bin.end());

			// x-min : sign
			if (interval_lower[i]>=0) {
				chr::Logical_var< unsigned int> lnx;
				get_opposite_var((*l_boolvars_xmin).back(), lnx);
				rightside.push_back(lnx);
				rightside_neg.push_back((*l_boolvars_xmin).back());	// TMP
			}
			else {
				rightside.push_back((*l_boolvars_xmin).back());
				chr::Logical_var< unsigned int> lnx;				// TMP
				get_opposite_var((*l_boolvars_xmin).back(), lnx);	// TMP
				rightside_neg.push_back(lnx);						// TMP
			}
			// x-min : magnitude
			for (unsigned int b=0; b<xmin_bin.size(); b++) {
				if (xmin_bin[b] == 0) {
					chr::Logical_var< unsigned int> lnx;
					get_opposite_var((*l_boolvars_xmin)[b], lnx);
					rightside.push_back(lnx);
					rightside_neg.push_back((*l_boolvars_xmin)[b]);	// TMP
				} else {
					rightside.push_back((*l_boolvars_xmin)[b]);
					chr::Logical_var< unsigned int> lnx;			// TMP
					get_opposite_var((*l_boolvars_xmin)[b], lnx);	// TMP
					rightside_neg.push_back(lnx);					// TMP
				}
			}

			std::vector<unsigned int> xmax_bin = dec2binary(interval_upper[i], (*l_boolvars_xmax).size()-1);
			std::reverse(xmax_bin.begin(),xmax_bin.end());

			// x-max : sign
			if (interval_upper[i]>0) {
				chr::Logical_var< unsigned int> lnx;
				get_opposite_var((*l_boolvars_xmax).back(), lnx);
				rightside.push_back(lnx);
				rightside_neg.push_back((*l_boolvars_xmax).back());	// TMP
			}
			else {
				rightside.push_back((*l_boolvars_xmax).back());
				chr::Logical_var< unsigned int> lnx;				// TMP
				get_opposite_var((*l_boolvars_xmax).back(), lnx);	// TMP
				rightside_neg.push_back(lnx);						// TMP
			}
			// x-max : magnitude
			for (unsigned int b=0; b<xmax_bin.size(); b++) {
				if (xmax_bin[b] == 0) {
					chr::Logical_var< unsigned int> lnx;
					get_opposite_var((*l_boolvars_xmax)[b], lnx);
					rightside.push_back(lnx);
					rightside_neg.push_back((*l_boolvars_xmax)[b]);	// TMP
				} else {
					rightside.push_back((*l_boolvars_xmax)[b]);
					chr::Logical_var< unsigned int> lnx;			// TMP
					get_opposite_var((*l_boolvars_xmax)[b], lnx);	// TMP
					rightside_neg.push_back(lnx);					// TMP
				}
			}

			// ------------------------------------------------------
			// TMP: VARIABLES "DE BOITE" (B)
			//
			// Added to observe activation/deactivation of sub-intervals
			// One variable per sub-interval.
			// They are not involved in any other operation.
			//
			// (1) Implication rightside => B
			//   (x- & x+ => B)
			// 2019-06-27:
			//   Added a new set of clauses to convert implication in equivalence
			//   B => rightside  ((~B|r0) & (~B|r1)...)
			// ------------------------------------------------------
			{
				// literals for new variable (B)
				std::string boitename = std::string("BOX_") + std::to_string(i);
				unsigned int boite = _first_free_id++;
				unsigned int nboite = _first_free_id++;
				vertice(boite, Flags(Flag::VARIABLE | Flag::BOOLEAN), boitename, IntVarDomain({0,1}) ); num_variables++;
				vertice(nboite, Flags(Flag::VARIABLE | Flag::BOOLEAN), boitename, IntVarDomain({0,1}) ); num_variables++;
				literals(id_transformation, boite, nboite);
				std::cout << "  " << boite << "," << nboite  << std::endl ;

				// (x- & x+) => B
				unsigned int disjunction = _first_free_id++;
				vertice(disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
				edge(id_transformation, disjunction, -1);
				std::cout << "\t" << disjunction << " : " ;
				for (unsigned int r=0; r<rightside_neg.size(); r++) {
					edge(disjunction, rightside_neg[r], -1);
					std::cout << rightside_neg[r] << " " ;
				}
				edge(disjunction, boite, -1);
				std::cout << boite << std::endl ;

				// B ==> (x- & x+)
				for (unsigned int r=0; r<rightside.size(); r++) {
					unsigned int disjunction2 = _first_free_id++;
					vertice(disjunction2, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
					edge(id_transformation, disjunction2, -1);
					edge(disjunction2, nboite, -1);
					edge(disjunction2, rightside[r], -1);
					std::cout << "\t" << disjunction2 << " : " << nboite << " " << rightside[r] << std::endl;
				}
			}


			// ------------------------------------------------------
			// clause : "left => right"
			// ------------------------------------------------------
			for (unsigned int r=0; r<rightside.size(); r++)
			{
				unsigned int disjunction = _first_free_id++;
				vertice(disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
				edge(id_transformation, disjunction, -1);
				//std::cout << disjunction << " : " ;

				for(unsigned int l=0; l<leftside.size(); l++) {
					edge(disjunction, leftside[l], -1);
					//std::cout << leftside[l] << " ";
				}
				edge(disjunction, rightside[r], -1);
				//std::cout << "| " << rightside[r] << std::endl;
			}
        }
	}
}
*/




// ==========================================================================
//
// GRAPH::transform_interval_branching_to_cnf
// V2: 1 (left) to 1-2 (right) variables
//
// ==========================================================================
/*
void GRAPH::transform_interval_branching_to_cnf(unsigned int id, unsigned long int branch_type)
{
	std::cout << std::endl << "GRAPH::transform_interval_branching_to_cnf(" << id << ") " << std::endl;

	// check: does the transformation already exists?
	chr::Logical_var< unsigned int > l_check;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::INTERVAL_BRANCHING | Flag::BINARY_DISJUNCTION), l_check);
	if (l_check.ground())
		return;

	// check: right number of variables (2+)
	chr::Logical_var_mutable< std::vector<unsigned int> > l_relation_vars;
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), 0, l_relation_vars);
	get_all_children_with_flags_ordered(id, Flags(Flag::VARIABLE | Flag::INTEGER), 0, l_relation_vars);
	if (branch_type==INTERVAL_BRANCHING)
		assert((*l_relation_vars).size() == 3);
	else
		assert((*l_relation_vars).size() == 2);

	// transformation ID
	unsigned int id_transformation = _first_free_id++;
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::INTERVAL_BRANCHING | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;
	edge(id, id_transformation, -1);


	// --------------------------------------------------------------
	// Relation variables
	// --------------------------------------------------------------

	std::vector< std::vector<unsigned int> > boolvars;
	int min=0, max=0;
	std::cout << "variables: ";

	for (unsigned int i=0; i<(*l_relation_vars).size(); ++i)
	{
		// relation's variables (l_rvar)
		chr::Logical_var<unsigned int> l_rvar;
		get_child_with_order(id, i ,l_rvar);
		assert(l_rvar.ground());
		std::cout << l_rvar << " ";

		// if we're observing the 1st variable (LEFT), obtain the domain interval
		if (i==0) {
			chr::Logical_var<Flags>       flags;
			chr::Logical_var<std::string> label;
			chr::Logical_var<Domain>      domain;
			get_vertice(l_rvar, flags, label, domain);
			min = (*domain).min();
			max = (*domain).max();
			std::cout << "[" << min << "," << max << "] " ;
		}

		// variable transformation (if needed, encode them)
		chr::Logical_var<unsigned int> var_transformation;
		get_child_with_flags(l_rvar, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), var_transformation);
		if (!var_transformation.ground()) {
			build_log_encoding_from(l_rvar);
			get_child_with_flags(l_rvar, Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::LOG_ENCODING), var_transformation);
		}
		assert(var_transformation.ground());
		std::cout << "  ";

		// obtain positive literals
		chr::Logical_var_mutable< std::vector<unsigned int> > literals_x;
		get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, literals_x);
		get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, literals_x);
		assert((*literals_x).size() > 0);
		boolvars.push_back( (*literals_x) );	// store them into a matrix

	}
	std::cout << std::endl;

//	std::cout << "Boolvars: " ;
//	for (unsigned int i=0; i<boolvars.size(); i++) {
//		for (unsigned int j=0; j<boolvars[i].size(); j++) {
//			std::cout << boolvars[i][j] << " ";
//		}
//		std::cout << " | ";
//	}
//	std::cout << std::endl;



	// --------------------------------------------------------------
	// intervals creation (tree)
	// --------------------------------------------------------------

	std::vector<int> cutpoint = interval_cutpoints(min,max,3,true);
	std::cout << "cutpoint: " << cutpoint[0] << std::endl;
	std::vector<int> interval_lower;
	std::vector<int> interval_upper;
	interval_lower.push_back(min);
	for (unsigned int i=0; i<cutpoint.size(); i++) {
		interval_upper.push_back(cutpoint[i]-1);
		interval_lower.push_back(cutpoint[i]);
	}
	interval_upper.push_back(max);

std::cout << "INTERVALS done..." << std::endl;

	// --------------------------------------------------------------
	// Path Definition (with example)
	//          0 1 2 3 4 5 6
	// [4,7]	? ? 1 0	0 0 0   (x2 & ~x3 & ~x4..) => ...
	// [8,15]	? ? ? 1 0 0 0	(x3 & ~x4 & ~x5..) => ...
	// [16,31]	? ? ? ? 1 0 0	(x4 & ~x5 & ~x6) => ...
	// [32,46]	? ? ? ? ? 1 0	(x5 & ~x6) => ...
	//
	// left-vars : decision vars (x), they change their polarity in CNF
	// right-vars: min/max interval variables (x-,x+)
	//
    // Boolean Expression : (a & b & c) => (x & y)
    // In CNF             : (~a|~b|~c|x) & (~a|~b|~c|y)
	// --------------------------------------------------------------
	for (unsigned int i=0; i<interval_lower.size(); i++)
	{
		std::cout << "interval [" << interval_lower[i] << "," << interval_upper[i] << "]  ";

		// left-side
		std::vector<unsigned int> leftside;
		{
			std::vector<unsigned int> lbin = dec2binary(interval_lower[i], boolvars[0].size()-1);
			std::reverse(lbin.begin(),lbin.end());
			std::vector<unsigned int> ubin = dec2binary(interval_upper[i], boolvars[0].size()-1);
			std::reverse(ubin.begin(),ubin.end());

			// magnitude
			for (int b=(ubin.size()-1); (0<=b) && (lbin[b] == ubin[b]); b--) {
				if (lbin[b] == 0)
					leftside.push_back(boolvars[0][b]);
				else {
					chr::Logical_var<unsigned int> lnx;
					get_opposite_var(boolvars[0][b], lnx);
					leftside.push_back(lnx);
				}
			}
			// sign
			if (interval_lower[i] * interval_upper[i] >= 0) {
				if (interval_lower[i]>=0) {
					leftside.push_back(boolvars[0].back());	// positive numbers use '0'
				} else {
					chr::Logical_var<unsigned int> lnx;
					get_opposite_var(boolvars[0].back(), lnx);
					leftside.push_back(lnx);
				}
			}
		}
		//std::cout << std::endl;

		if (leftside.size() == 0) continue;

		// right-side
		std::vector<unsigned int> rightside;
		{
			for (unsigned int j=1; j<=2; j++)
			{
				int decvalue;
				if ( (j==1) && ((branch_type==INTERVAL_BRANCH_LOWER) || (branch_type==INTERVAL_BRANCHING)) ) {
					decvalue = interval_lower[i];
				} else if ( (j==1) && ((branch_type==INTERVAL_BRANCH_UPPER) || (branch_type==INTERVAL_BRANCHING)) ) {
					decvalue = interval_upper[i];
				} else if ( (j==2) && (branch_type==INTERVAL_BRANCHING) ) {
					decvalue = interval_upper[i];
				} else {
					continue;
				}
				std::cout << "branches to: " << decvalue << std::endl;

				std::vector<unsigned int> rvalue = dec2binary(decvalue, boolvars[j].size()-1);
				std::reverse(rvalue.begin(),rvalue.end());

				// magnitude
				for (unsigned int b=0; b<rvalue.size(); b++) {
					if (rvalue[b] == 0) {
						chr::Logical_var<unsigned int> lnx;
						get_opposite_var(boolvars[j][b], lnx);
						rightside.push_back(lnx);
					} else {
						rightside.push_back(boolvars[j][b]);
					}
				}
				// sign
				if (interval_lower[i] >= 0) {
					chr::Logical_var<unsigned int> lnx;
					get_opposite_var(boolvars[j].back(), lnx);
					rightside.push_back(lnx);
				}
				else {
					rightside.push_back(boolvars[j].back());
				}
			}
		}

		// clause : "left => right"
		for (unsigned int r=0; r<rightside.size(); r++)
		{
			unsigned int disjunction = _first_free_id++;
			vertice(disjunction, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
			edge(id_transformation, disjunction, -1);
			std::cout << disjunction << " : " ;

			for(unsigned int l=0; l<leftside.size(); l++) {
				edge(disjunction, leftside[l], -1);
				std::cout << leftside[l] << " ";
			}
			edge(disjunction, rightside[r], -1);
			std::cout << "| " << rightside[r] << std::endl;
		}
	}
}
*/



