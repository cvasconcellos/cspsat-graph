
#define N_STR std::string()



// ==========================================================================
// GARAM
// ==========================================================================

unsigned int create_garam_variable(GRAPH& space, std::string value, const Integer_variable_domain& domain_init)
{
	if (value == "?")
		space.vertice(space._first_free_id++, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_init);
	else
		space.vertice(space._first_free_id++, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, Integer_variable_domain( { std::stoi(value) } ));
	return (space._first_free_id-1);
}

void create_garam_op(GRAPH& space, char op, unsigned id_x, unsigned id_y, unsigned id_z)
{

	switch (op) {
		case '+':
			space.vertice(space._first_free_id++, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain() );
			break;
		case '-':
			space.vertice(space._first_free_id++, Flags(Flag::RELATION | Flag::SUB_EQUAL), N_STR, Integer_variable_domain() );
			break;
		case '*':
			space.vertice(space._first_free_id++, Flags(Flag::RELATION | Flag::MULT_EQUAL), N_STR, Integer_variable_domain() );
			break;
		default:
			assert(false);
			break;
	}
	space.edge(space._first_free_id-1,id_x,0);
	space.edge(space._first_free_id-1,id_y,1);
	space.edge(space._first_free_id-1,id_z,2);
}

void generate_garam_problem(GRAPH& space, std::string file_name)
{
	std::cout << "=== [ GARAM ] ==================" << std::endl;

	Integer_variable_domain domain_init, domain_mult_init, domain_dizaine_init;
	for (unsigned int i = 0; i < 10; ++i)
		domain_init.domain.push_back(i);
	for (unsigned int i = 0; i < 82; ++i)
		domain_mult_init.domain.push_back(i);
	for (unsigned int i = 0; i < 10; ++i)
		domain_dizaine_init.domain.push_back(i * 10);

	std::string line;
	std::ifstream file (file_name);
	if (file.is_open())
	{
		std::array< unsigned int, 44 > variable_ids;
		std::array< char, 20 > ops;
		unsigned n_var = 0;
		unsigned n_op = 0;
		std::string value;

		// Line 1
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> ops[n_op++];
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> ops[n_op++];
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		// Line 2
		file >> ops[n_op++];
		file >> ops[n_op++];
		file >> ops[n_op++];
		file >> ops[n_op++];
		// Line 3
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> ops[n_op++];
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		// Line 4
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		// Line 5
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> ops[n_op++];
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> ops[n_op++];
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		// Line 6
		file >> ops[n_op++];
		file >> ops[n_op++];
		// Line 7
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		// Line 8
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> ops[n_op++];
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> ops[n_op++];
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		// Line 9
		file >> ops[n_op++];
		file >> ops[n_op++];
		file >> ops[n_op++];
		file >> ops[n_op++];
		// Line 10
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> ops[n_op++];
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		// Line 11
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		// Line 12
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> ops[n_op++];
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> ops[n_op++];
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);
		file >> value; variable_ids[n_var++] = create_garam_variable(space,value,domain_init);


		// Contraintes de multiplication par 10
		space.vertice(space._first_free_id++, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, Integer_variable_domain( { 10 } ));
		unsigned int number_10 = space._first_free_id - 1;
		std::array< unsigned int, 8 > mult_variable_ids;
		std::array< unsigned int, 8 > dizaine_variable_ids;
		for (unsigned i = 0; i < 8; ++i)
		{
			space.vertice(space._first_free_id++, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_mult_init);
			mult_variable_ids[i] = space._first_free_id - 1;
			space.vertice(space._first_free_id++, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_dizaine_init);
			dizaine_variable_ids[i] = space._first_free_id - 1;
		}
		// Mult 1
		create_garam_op(space,'*',variable_ids[11],number_10,dizaine_variable_ids[0]);
		create_garam_op(space,'+',dizaine_variable_ids[0],variable_ids[15],mult_variable_ids[0]);
		// Mult 2
		create_garam_op(space,'*',variable_ids[12],number_10,dizaine_variable_ids[1]);
		create_garam_op(space,'+',dizaine_variable_ids[1],variable_ids[17],mult_variable_ids[1]);
		// Mult 3
		create_garam_op(space,'*',variable_ids[13],number_10,dizaine_variable_ids[2]);
		create_garam_op(space,'+',dizaine_variable_ids[2],variable_ids[18],mult_variable_ids[2]);
		// Mult 4
		create_garam_op(space,'*',variable_ids[14],number_10,dizaine_variable_ids[3]);
		create_garam_op(space,'+',dizaine_variable_ids[3],variable_ids[20],mult_variable_ids[3]);
		// Mult 5
		create_garam_op(space,'*',variable_ids[34],number_10,dizaine_variable_ids[4]);
		create_garam_op(space,'+',dizaine_variable_ids[4],variable_ids[38],mult_variable_ids[4]);
		// Mult 6
		create_garam_op(space,'*',variable_ids[35],number_10,dizaine_variable_ids[5]);
		create_garam_op(space,'+',dizaine_variable_ids[5],variable_ids[40],mult_variable_ids[5]);
		// Mult 7
		create_garam_op(space,'*',variable_ids[36],number_10,dizaine_variable_ids[6]);
		create_garam_op(space,'+',dizaine_variable_ids[6],variable_ids[41],mult_variable_ids[6]);
		// Mult 8
		create_garam_op(space,'*',variable_ids[37],number_10,dizaine_variable_ids[7]);
		create_garam_op(space,'+',dizaine_variable_ids[7],variable_ids[43],mult_variable_ids[7]);

		// Contraintes de la grille
		create_garam_op(space, ops[0], variable_ids[0], variable_ids[1], variable_ids[2]);
		create_garam_op(space, ops[1], variable_ids[3], variable_ids[4], variable_ids[5]);

		create_garam_op(space, ops[2], variable_ids[0], variable_ids[6], mult_variable_ids[0]);
		create_garam_op(space, ops[3], variable_ids[2], variable_ids[7], mult_variable_ids[1]);
		create_garam_op(space, ops[4], variable_ids[3], variable_ids[9], mult_variable_ids[2]);
		create_garam_op(space, ops[5], variable_ids[5], variable_ids[10], mult_variable_ids[3]);

		create_garam_op(space, ops[6], variable_ids[7], variable_ids[8], variable_ids[9]);

		create_garam_op(space, ops[7], variable_ids[15], variable_ids[16], variable_ids[17]);
		create_garam_op(space, ops[8], variable_ids[18], variable_ids[19], variable_ids[20]);

		create_garam_op(space, ops[9], variable_ids[16], variable_ids[21], variable_ids[24]);
		create_garam_op(space, ops[10], variable_ids[19], variable_ids[22], variable_ids[27]);

		create_garam_op(space, ops[11], variable_ids[23], variable_ids[24], variable_ids[25]);
		create_garam_op(space, ops[12], variable_ids[26], variable_ids[27], variable_ids[28]);

		create_garam_op(space, ops[13], variable_ids[23], variable_ids[29], mult_variable_ids[4]);
		create_garam_op(space, ops[14], variable_ids[25], variable_ids[30], mult_variable_ids[5]);
		create_garam_op(space, ops[15], variable_ids[26], variable_ids[32], mult_variable_ids[6]);
		create_garam_op(space, ops[16], variable_ids[28], variable_ids[33], mult_variable_ids[7]);

		create_garam_op(space, ops[17], variable_ids[30], variable_ids[31], variable_ids[32]);

		create_garam_op(space, ops[18], variable_ids[38], variable_ids[39], variable_ids[40]);
		create_garam_op(space, ops[19], variable_ids[41], variable_ids[42], variable_ids[43]);
	} else {
		std::cerr << "File " << file_name << " not found." << std::endl;
		exit(1);
	}
}




