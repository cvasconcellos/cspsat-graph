/*
 * TESTS : PROPAGATORS FOR ADDITION
 *
 */


#include <graph_chr_header.hpp>

#define N_STR std::string()



// ==========================================================================
// TEST #1
//
//   A+B=Z ,  a[10;15] b[7;9]  z[4,46]
//
//   tp1_addequal     : INTEGER      + ADD_EQUAL
//   tp1_propaddequal : INTEGER      + PROP_ADD_EQUAL
//   tp1_propall      : PROP_INTEGER + PROP_ADD_EQUAL
// ==========================================================================

//void run_testprop1()
//{
//	tp1_addequal();
//	tp1_propaddequal();
//	tp1_propall();
//}


void tp1_addequal ()
{
	GRAPH space;
	space.encoding_sequence.push_back(Flag::LOG_ENCODING);

	unsigned int a = space._first_free_id++;
	unsigned int b = space._first_free_id++;
	unsigned int z = space._first_free_id++;
	space.vertice(a, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({10,11,12,13,14,15})); space.num_variables++;
	space.vertice(b, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({7,8,9})); space.num_variables++;
	space.vertice(z, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46})); space.num_variables++;

	unsigned int sum = space._first_free_id++;
	space.vertice(sum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain()); space.num_relations++;
	space.edge(sum, a, 0);
	space.edge(sum, b, 1);
	space.edge(sum, z, 2);

	CHR_RUN( space.apply_all_transformation(); )
	std::string filename = "tp1_addequal";
	std::unordered_map<unsigned int, unsigned int> map_cnf_graph;
	space.print_cnf(filename, map_cnf_graph, true);
}


void tp1_propaddequal ()
{
	GRAPH space;
	space.encoding_sequence.push_back(Flag::LOG_ENCODING);

	unsigned int a = space._first_free_id++;
	unsigned int b = space._first_free_id++;
	unsigned int z = space._first_free_id++;
	space.vertice(a, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({10,11,12,13,14,15})); space.num_variables++;
	space.vertice(b, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({7,8,9})); space.num_variables++;
	space.vertice(z, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46})); space.num_variables++;

	unsigned int sum = space._first_free_id++;
	space.vertice(sum, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain()); space.num_relations++;
	space.edge(sum, a, 0);
	space.edge(sum, b, 1);
	space.edge(sum, z, 2);

	CHR_RUN( space.apply_all_transformation(); )
	std::string filename = "tp1_propaddequal";
	std::unordered_map<unsigned int, unsigned int> map_cnf_graph;
	space.print_cnf(filename, map_cnf_graph, true);
}


void tp1_propall ()
{
	GRAPH space;
	space.encoding_sequence.push_back(Flag::LOG_ENCODING);

	unsigned int a = space._first_free_id++;
	unsigned int b = space._first_free_id++;
	unsigned int z = space._first_free_id++;
	space.vertice(a, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, IntVarDomain({10,11,12,13,14,15})); space.num_variables++;
	space.vertice(b, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, IntVarDomain({7,8,9})); space.num_variables++;
	space.vertice(z, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, IntVarDomain({4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46})); space.num_variables++;

	unsigned int sum = space._first_free_id++;
	space.vertice(sum, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain()); space.num_relations++;
	space.edge(sum, a, 0);
	space.edge(sum, b, 1);
	space.edge(sum, z, 2);

	CHR_RUN( space.apply_all_transformation(); )
	std::string filename = "tp1_propall";
	std::unordered_map<unsigned int, unsigned int> map_cnf_graph;
	space.print_cnf(filename, map_cnf_graph, true);
}



// ==========================================================================
// TEST #2
//
// A+B=Z   A[10,15]  B[7,9]  Z[4,46]
// C+D=Z   C[9,10]   D[3,5]
//
// tp2_addequal
// tp2_propaddequal
// tp2_propall
// ==========================================================================

void tp2_addequal()
{
	GRAPH space;
	space.encoding_sequence.push_back(Flag::LOG_ENCODING);

	unsigned int a = space._first_free_id++;
	unsigned int b = space._first_free_id++;
	unsigned int z = space._first_free_id++;
	unsigned int c = space._first_free_id++;
	unsigned int d = space._first_free_id++;
	space.vertice(a, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({10,11,12,13,14,15})); space.num_variables++;
	space.vertice(b, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({7,8,9})); space.num_variables++;
	space.vertice(z, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46})); space.num_variables++;
	space.vertice(c, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({9,10})); space.num_variables++;
	space.vertice(d, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({3,4,5})); space.num_variables++;

	unsigned int sum = space._first_free_id++;
	space.vertice(sum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain()); space.num_relations++;
	space.edge(sum, a, 0);
	space.edge(sum, b, 1);
	space.edge(sum, z, 2);

	unsigned int sum2 = space._first_free_id++;
	space.vertice(sum2, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain()); space.num_relations++;
	space.edge(sum2, c, 0);
	space.edge(sum2, d, 1);
	space.edge(sum2, z, 2);

	CHR_RUN( space.apply_all_transformation(); )
	std::string filename = "tp2_addequal";
	std::unordered_map<unsigned int, unsigned int> map_cnf_graph;
	space.print_cnf(filename, map_cnf_graph, true);
}


void tp2_propaddequal()
{
	GRAPH space;
	space.encoding_sequence.push_back(Flag::LOG_ENCODING);

	unsigned int a = space._first_free_id++;
	unsigned int b = space._first_free_id++;
	unsigned int z = space._first_free_id++;
	unsigned int c = space._first_free_id++;
	unsigned int d = space._first_free_id++;
	space.vertice(a, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({10,11,12,13,14,15})); space.num_variables++;
	space.vertice(b, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({7,8,9})); space.num_variables++;
	space.vertice(z, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46})); space.num_variables++;
	space.vertice(c, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({9,10})); space.num_variables++;
	space.vertice(d, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({3,4,5})); space.num_variables++;

	unsigned int sum = space._first_free_id++;
	space.vertice(sum, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain()); space.num_relations++;
	space.edge(sum, a, 0);
	space.edge(sum, b, 1);
	space.edge(sum, z, 2);

	unsigned int sum2 = space._first_free_id++;
	space.vertice(sum2, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain()); space.num_relations++;
	space.edge(sum2, c, 0);
	space.edge(sum2, d, 1);
	space.edge(sum2, z, 2);

	CHR_RUN( space.apply_all_transformation(); )
	std::string filename = "tp2_propaddequal";
	std::unordered_map<unsigned int, unsigned int> map_cnf_graph;
	space.print_cnf(filename, map_cnf_graph, true);
}


void tp2_propall()
{
	GRAPH space;
	space.encoding_sequence.push_back(Flag::LOG_ENCODING);

	unsigned int a = space._first_free_id++;
	unsigned int b = space._first_free_id++;
	unsigned int z = space._first_free_id++;
	unsigned int c = space._first_free_id++;
	unsigned int d = space._first_free_id++;
	space.vertice(a, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, IntVarDomain({10,11,12,13,14,15})); space.num_variables++;
	space.vertice(b, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, IntVarDomain({7,8,9})); space.num_variables++;
	space.vertice(z, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, IntVarDomain({4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46})); space.num_variables++;
	space.vertice(c, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, IntVarDomain({9,10})); space.num_variables++;
	space.vertice(d, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, IntVarDomain({3,4,5})); space.num_variables++;

	unsigned int sum = space._first_free_id++;
	space.vertice(sum, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain()); space.num_relations++;
	space.edge(sum, a, 0);
	space.edge(sum, b, 1);
	space.edge(sum, z, 2);

	unsigned int sum2 = space._first_free_id++;
	space.vertice(sum2, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain()); space.num_relations++;
	space.edge(sum2, c, 0);
	space.edge(sum2, d, 1);
	space.edge(sum2, z, 2);

	CHR_RUN( space.apply_all_transformation(); )
	std::string filename = "tp2_propall";
	std::unordered_map<unsigned int, unsigned int> map_cnf_graph;
	space.print_cnf(filename, map_cnf_graph, true);
}





// ==========================================================================
// TEST #3
// Propagation at Intersection
// 2019-05-01
//
// tp3_equal   : y = x     ; aux = -2x ; y = aux + 6
// tp3_zeroadd : y = x + 0 ; aux = -2x ; y = aux + 6
//
// x[0,10]  y[0,8]  aux[-10,0]
// ==========================================================================


void tp3_equal()
{
	GRAPH space;

	// x
	IntVarDomain xdom;
	for (unsigned int i=0; i<=10; ++i) xdom.domain.push_back(i);
	unsigned int x = space._first_free_id++;
	space.vertice(x, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, xdom); space.num_variables++;
	std::cout << "x: " << x << std::endl;

	// y
	IntVarDomain ydom;
	for (unsigned int i=0; i<=8; ++i) ydom.domain.push_back(i);
	unsigned int y = space._first_free_id++;
	space.vertice(y, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, ydom); space.num_variables++;
	std::cout << "y: " << y << std::endl;

	// x = y
	unsigned int eq1 = space._first_free_id++;
	space.vertice(eq1, Flags(Flag::RELATION | Flag::EQUAL), N_STR, IntVarDomain()); space.num_relations++;
	space.edge(eq1, x, 0);
	space.edge(eq1, y, 1);
	std::cout << eq1 << ":\t" << x << " = " << y << std::endl;

	// -2
	unsigned int c2 = space._first_free_id++;
	space.vertice(c2, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, IntVarDomain({-2})); space.num_variables++;

	// aux
	IntVarDomain auxdom;
	for (int i=-10; i<=0; ++i) auxdom.domain.push_back(i);
	unsigned int aux = space._first_free_id++;
	space.vertice(aux, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, auxdom); space.num_variables++;
	std::cout << "aux: " << aux << std::endl;

	// aux = -2*x
	unsigned int mult = space._first_free_id++;
	space.vertice(mult, Flags(Flag::RELATION | Flag::MULT_EQUAL), N_STR, IntVarDomain()); space.num_relations++;
	space.edge(mult, c2,  0);
	space.edge(mult, x,   1);
	space.edge(mult, aux, 2);
	std::cout << mult << ":\t" << c2 << " * " << x << " = " << aux << std::endl;

	// 6
	unsigned int c3 = space._first_free_id++;
	space.vertice(c3, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({6})); space.num_variables++;

	// aux + 6 = y
	unsigned int eq2 = space._first_free_id++;	// 7
	space.vertice(eq2, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain()); space.num_relations++;
	space.edge(eq2, aux, 0);
	space.edge(eq2, c3,  1);
	space.edge(eq2, y,   2);
	std::cout << eq2 << ":\t" << aux << " + " << c3 << " = " << y << std::endl;

	CHR_RUN( space.apply_all_transformation(); )

	std::string filename = "tp3_equal_prop";
	std::unordered_map<unsigned int, unsigned int> map_cnf_graph;
	space.print_cnf(filename, map_cnf_graph, true);
	space.print_variable_path(filename, map_cnf_graph);
}


void tp3_zeroadd()
{
	GRAPH space;

	// x
	IntVarDomain xdom;
	for (unsigned int i=0; i<=10; ++i) xdom.domain.push_back(i);
	unsigned int x = space._first_free_id++;
	space.vertice(x, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, xdom); space.num_variables++;
	std::cout << "x: " << x << std::endl;

	// y
	IntVarDomain ydom;
	for (unsigned int i=0; i<=8; ++i) ydom.domain.push_back(i);
	unsigned int y = space._first_free_id++;
	space.vertice(y, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, ydom); space.num_variables++;
	std::cout << "y: " << y << std::endl;

	// 0
	unsigned int c0 = space._first_free_id++;
	space.vertice(c0, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, IntVarDomain({0})); space.num_variables++;

	// x + 0 = y
	unsigned int eq1 = space._first_free_id++;
	space.vertice(eq1, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain()); space.num_relations++;
	space.edge(eq1, x,  0);
	space.edge(eq1, c0, 1);
	space.edge(eq1, y,  2);
	std::cout << eq1 << ":\t" << x << " + " << c0 << " = " << y << std::endl;

	// aux
	IntVarDomain auxdom;
	for (int i=-10; i<=0; ++i) auxdom.domain.push_back(i);
	unsigned int aux = space._first_free_id++;
	space.vertice(aux, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, auxdom); space.num_variables++;
	std::cout << "aux: " << aux << std::endl;

	// -2
	unsigned int c2 = space._first_free_id++;
	space.vertice(c2, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, IntVarDomain({-2})); space.num_variables++;

	// -2*x = aux
	unsigned int mult = space._first_free_id++;
	space.vertice(mult, Flags(Flag::RELATION | Flag::MULT_EQUAL), N_STR, IntVarDomain()); space.num_relations++;
	space.edge(mult, c2,  0);
	space.edge(mult, x,   1);
	space.edge(mult, aux, 2);
	std::cout << mult << ":\t" << c2 << " * " << x << " = " << aux << std::endl;

	// 6
	unsigned int c3 = space._first_free_id++;
	space.vertice(c3, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, IntVarDomain({6})); space.num_variables++;

	// (8) aux + 6 = y
	unsigned int eq2 = space._first_free_id++;
	space.vertice(eq2, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain()); space.num_relations++;
	space.edge(eq2, aux, 0);
	space.edge(eq2, c3,  1);
	space.edge(eq2, y,   2);
	std::cout << eq2 << ":\t" << aux << " + " << c3 << " = " << y << std::endl;

	CHR_RUN( space.apply_all_transformation(); )

	// output
	std::string filename = "tp3_zeroadd";
	std::unordered_map<unsigned int, unsigned int> map_cnf_graph;
	space.print_cnf(filename, map_cnf_graph, true);
	space.print_variable_path(filename, map_cnf_graph);
}


