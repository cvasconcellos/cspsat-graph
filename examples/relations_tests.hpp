#define N_STR std::string()


const int POWERSET_MAX_SIZE = 10;
using Powerset_stack = std::array<int, POWERSET_MAX_SIZE >;
using Powerset = std::vector< Integer_variable_domain >;


void powerset_rec(Powerset_stack& s, unsigned int k, int m, int n, Powerset& powerset)
{
	if (m <= n) {
		s[k] = m;

		// Copy result
		Integer_variable_domain set;
		std::for_each(s.begin(), s.begin() + k + 1, [&set](int i){
			set.domain.push_back(i);
		});
		powerset.push_back( set );

		powerset_rec(s, k+1, m+1, n, powerset);		// with m
		if (k == 0)									// To dismiss non contiguous domains
			powerset_rec(s, k, m+1, n, powerset);	// without m
	}
}

void create_contiguous_powerset(unsigned int min, unsigned int max, Powerset& powerset)
{
	assert( (max - min + 1) < POWERSET_MAX_SIZE );
	Powerset_stack stack;
	powerset_rec(stack,0,min,max,powerset);
}




// ==========================================================================
// TEST DECOMPOSITION LINEAR EQ SEQ
// CVG 2019-10-08
// ==========================================================================

void test_decomp_linear_seq()
{
	GRAPH space;
	unsigned int linear = space._first_free_id++;
	space.vertice(linear, Flags(Flag::RELATION | Flag::LINEAR_EQ_SEQ), N_STR, Integer_variable_domain()); space.num_relations++;

    std::cout << linear << ":\t" ;
	for (unsigned int i=0; i<5; i++)
	{
		unsigned int var = space._first_free_id++;
        space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, Integer_variable_domain({2,5})); space.num_variables++;
        space.edge(linear,var, i);
        std::cout << var << " ";
    }
    std::cout << std::endl;

    //space.decompose_linear_seq(linear);
	CHR_RUN( space.apply_all_transformation(); )
}





// ==========================================================================
// TEST : ADD_EQUAL + COMPARATOR
// March 01, 2019
// Goal : Evaluate LESS_EQUAL_THAN when numbers differ in their number of bits
// ==========================================================================

void test_add_comparator()
{
	GRAPH space;

	// a+b=z
	unsigned int a = space._first_free_id++;
	space.vertice(a, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, Integer_variable_domain({10,11,12,13,14,15})); space.num_variables++;
	unsigned int b = space._first_free_id++;
	space.vertice(b, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, Integer_variable_domain({7,8,9})); space.num_variables++;
	unsigned int z = space._first_free_id++;
	space.vertice(z, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, Integer_variable_domain({4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25})); space.num_variables++;
	unsigned int sum = space._first_free_id++;
	space.vertice(sum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain()); space.num_relations++;
	space.edge(sum, a, 0);
	space.edge(sum, b, 1);
	space.edge(sum, z, 2);

	// z > zmin
	unsigned int zmin = space._first_free_id++;
	space.vertice(zmin, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, Integer_variable_domain({4})); space.num_variables++;
	unsigned int lt_zmin = space._first_free_id++;
	space.vertice(lt_zmin, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, Integer_variable_domain()); space.num_relations++;
	space.edge(lt_zmin, z, 0);
	space.edge(lt_zmin, zmin, 1);

	// CNF transformations
	space.collect_graph_stats(N_STR);
	CHR_RUN( space.apply_all_transformation(); )
	space.collect_graph_stats(N_STR);

	// print cnf
	std::unordered_map<unsigned int,unsigned int> map_cnf_graph;
	space.print_cnf(std::string("lala.cnf"), map_cnf_graph, true);
	space.print_variable_path(std::string("lala"), map_cnf_graph);

}



// ========================================================
// TEST : LESS_THAN
// ========================================================

void test_lessthan() {
	GRAPH space;
//	unsigned int x = space._first_free_id++;
//	space.vertice(x, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, Integer_variable_domain({0,1,2,3})); space.num_variables++;
//	unsigned int y = space._first_free_id++;
//	space.vertice(y, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, Integer_variable_domain({5})); space.num_variables++;
//	unsigned int rel = space._first_free_id++;
//	space.vertice(rel, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, Integer_variable_domain()); space.num_relations++;
//	space.edge(rel, x, 0);
//	space.edge(rel, y, 1);


	// x<5, x in {0,16}
	unsigned int x = space._first_free_id++;
	Integer_variable_domain aux_domain;
	for (unsigned int i=0; i<=11; ++i)
		aux_domain.domain.push_back(i);
	space.vertice(x, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, aux_domain); space.num_variables++;
	unsigned int y = space._first_free_id++;
	space.vertice(y, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, Integer_variable_domain({5})); space.num_variables++;
	unsigned int rel = space._first_free_id++;
	space.vertice(rel, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, Integer_variable_domain()); space.num_relations++;
	space.edge(rel, x, 0);
	space.edge(rel, y, 1);

	space.collect_graph_stats(N_STR);
	CHR_RUN( space.apply_all_transformation(); )
	space.collect_graph_stats(N_STR);

    // print scaled-CNF
    std::unordered_map<unsigned int,unsigned int> map_cnf_graph;
//    std::ofstream ofile;
//    ofile.open("output.cnf");
//    space.print_cnf(ofile, map_cnf_graph, true);
//    ofile.close();
	space.print_cnf(std::string("test_lessthan.cnf"), map_cnf_graph, true);
}


// ========================================================
// TEST CARDINALITY
// x1+x2+x3=D
// ========================================================

void test_cardinality()
{
	GRAPH space;

	unsigned int card = space._first_free_id++;
	unsigned int d    = space._first_free_id++;
	unsigned int x1   = space._first_free_id++;
	unsigned int x2   = space._first_free_id++;
	unsigned int x3   = space._first_free_id++;

	space.vertice(card, Flags(Flag::RELATION | Flag::CARDINALITY_LEQ), N_STR, Integer_variable_domain()); space.num_relations++;
	space.vertice(d,  Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, Integer_variable_domain({2})); space.num_variables++;
	space.vertice(x1, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, Integer_variable_domain({1})); space.num_variables++;
	space.vertice(x2, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, Integer_variable_domain({1})); space.num_variables++;
	space.vertice(x3, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, Integer_variable_domain({0,1})); space.num_variables++;
	space.edge(card,  d, 0);
    space.edge(card, x1, 1);
    space.edge(card, x2, 2);
    space.edge(card, x3, 3);

	space.collect_graph_stats(N_STR);
	CHR_RUN( space.apply_all_transformation(); )
	space.collect_graph_stats(N_STR);

    // print scaled-CNF
    std::unordered_map<unsigned int,unsigned int> map_cnf_graph;
//    std::ofstream ofile;
//    ofile.open("output.cnf");
//    space.print_cnf(ofile, map_cnf_graph, true);
//    ofile.close();
	space.print_cnf(std::string("test_cardinality"), map_cnf_graph, true);
}

void create_add_equal_benchmark_files()
{
	unsigned int n_file = 0;
	std::ofstream os_file;
	Powerset powerset;
	create_contiguous_powerset(-2,4,powerset);
	for (Integer_variable_domain domain_x : powerset)
		for (Integer_variable_domain domain_y : powerset)
			for (Integer_variable_domain domain_z : powerset)
			{
				os_file.open("chr_graph_benchmark_" + std::to_string(n_file++) + ".cnf");
				// On calcule le résultat de l'addition
				int r_inf = domain_x.min() + domain_y.min();
				int r_sup = domain_x.max() + domain_y.max();
				// On vérifie qu'on a une intersection avec le domaine d'arrivé
				if ((r_sup < domain_z.min()) || (domain_z.max() < r_inf))
					os_file << "c UNSAT" << std::endl;
				else
					os_file << "c SAT" << std::endl;

				if (os_file.is_open())
				{
					os_file << "c x :"; for (int i : domain_x.domain) os_file << " " << i; os_file << std::endl;
					os_file << "c y :"; for (int i : domain_y.domain) os_file << " " << i; os_file << std::endl;
					os_file << "c z :"; for (int i : domain_z.domain) os_file << " " << i; os_file << std::endl;
					GRAPH space;
					CHR_RUN(
							space.vertice(0, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_x );
							space.vertice(1, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_y );
							space.vertice(2, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_z );
							space.vertice(3, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain() ); space.num_relations++;
							space.edge(3,0,0);
							space.edge(3,1,1);
							space.edge(3,2,2);
							space._first_free_id = 4;
							space.transform_add_equal_to_disjunction(3);
					)
					space.print_cnf(os_file,true);
					os_file.close();
				}
			}
}

void create_sub_equal_benchmark_files()
{
	unsigned int n_file = 0;
	std::ofstream os_file;
	Powerset powerset;
	create_contiguous_powerset(-2,4,powerset);
	for (Integer_variable_domain domain_x : powerset)
		for (Integer_variable_domain domain_y : powerset)
			for (Integer_variable_domain domain_z : powerset)
			{
				os_file.open("chr_graph_benchmark_" + std::to_string(n_file++) + ".cnf");
				// On calcule le résultat de l'addition
				int r_inf = domain_x.min() - domain_y.max();
				int r_sup = domain_x.max() - domain_y.min();
				// On vérifie qu'on a une intersection avec le domaine d'arrivé
				if ((r_sup < domain_z.min()) || (domain_z.max() < r_inf))
					os_file << "c UNSAT" << std::endl;
				else
					os_file << "c SAT" << std::endl;

				if (os_file.is_open())
				{
					os_file << "c x :"; for (int i : domain_x.domain) os_file << " " << i; os_file << std::endl;
					os_file << "c y :"; for (int i : domain_y.domain) os_file << " " << i; os_file << std::endl;
					os_file << "c z :"; for (int i : domain_z.domain) os_file << " " << i; os_file << std::endl;
					GRAPH space;
					CHR_RUN(
							space.vertice(0, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_x );
							space.vertice(1, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_y );
							space.vertice(2, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_z );
							space.vertice(3, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain() ); space.num_relations++;
							space.edge(3,0,0);
							space.edge(3,1,1);
							space.edge(3,2,2);
							space._first_free_id = 4;
							space.transform_sub_equal_to_disjunction(3);
					)
					space.print_cnf(os_file,true);
					os_file.close();
				}
			}
}

void create_mult_equal_benchmark_files()
{
	unsigned int n_file = 0;
	std::ofstream os_file;
	Powerset powerset;
	create_contiguous_powerset(-2,4,powerset);
	for (Integer_variable_domain domain_x : powerset)
		for (Integer_variable_domain domain_y : powerset)
			for (Integer_variable_domain domain_z : powerset)
			{
				os_file.open("chr_graph_benchmark_" + std::to_string(n_file++) + ".cnf");
				// On calcule le résultat de la multiplication
				bool is_sat = false;
				for (auto x : domain_x.domain)
				{
					for (auto y : domain_y.domain)
					{
						auto res = std::find(domain_z.domain.begin(), domain_z.domain.end(), x*y);
						if (res != domain_z.domain.end()) {
							is_sat = true;
							break;
						}
					}
					if (is_sat) break;
				}
				if (is_sat)
					os_file << "c SAT" << std::endl;
				else
					os_file << "c UNSAT" << std::endl;

				if (os_file.is_open())
				{
					os_file << "c x :"; for (int i : domain_x.domain) os_file << " " << i; os_file << std::endl;
					os_file << "c y :"; for (int i : domain_y.domain) os_file << " " << i; os_file << std::endl;
					os_file << "c z :"; for (int i : domain_z.domain) os_file << " " << i; os_file << std::endl;
					GRAPH space;
					CHR_RUN(
							space.vertice(0, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_x );
							space.vertice(1, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_y );
							space.vertice(2, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_z );
							space.vertice(3, Flags(Flag::RELATION | Flag::MULT_EQUAL), N_STR, Integer_variable_domain() ); space.num_relations++;
							space.edge(3,0,0);
							space.edge(3,1,1);
							space.edge(3,2,2);
							space._first_free_id = 4;
							space.transform_mult_equal_to_disjunction(3);
					)
					space.print_cnf(os_file,true);
					os_file.close();
				}
			}
}


void create_equal_benchmark_files()
{
	unsigned int n_file = 0;
	std::ofstream os_file;
	Powerset powerset;
	create_contiguous_powerset(-2,4,powerset);
	for (Integer_variable_domain domain_x : powerset)
		for (Integer_variable_domain domain_y : powerset)
		    {
			    os_file.open("chr_graph_benchmark_equal_" + std::to_string(n_file++) + ".cnf");
			    // On calcule le résultat
				bool is_sat = false;
				for (auto x : domain_x.domain) {
					for (auto y : domain_y.domain) {
						if (x == y) {
							is_sat = true;
							break;
						}
					}
					if (is_sat) break;
				}
				if (is_sat)
					os_file << "c SAT" << std::endl;
				else
					os_file << "c UNSAT" << std::endl;

				if (os_file.is_open())
				{
					os_file << "c x :"; for (int i : domain_x.domain) os_file << " " << i; os_file << std::endl;
					os_file << "c y :"; for (int i : domain_y.domain) os_file << " " << i; os_file << std::endl;
					GRAPH space;
					CHR_RUN(
							space.vertice(0, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_x );
							space.vertice(1, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_y );
							space.vertice(2, Flags(Flag::RELATION | Flag::EQUAL), N_STR, Integer_variable_domain() );
							space.edge(2,0,0);
							space.edge(2,1,1);
							space._first_free_id = 3;
							space.transform_equal_to_disjunction(2);
					)
					space.print_cnf(os_file,true);
					os_file.close();
				}
		    }
}

void create_condition_benchmark_files()
{
	unsigned int nfile = 0;
	std::ofstream os_file;
	Powerset powerset;
	create_contiguous_powerset(0,5,powerset);
	for (Integer_variable_domain domain_x : powerset)
		for (Integer_variable_domain domain_y : powerset)
		    {
			    os_file.open("benchmark_condition_" + std::to_string(nfile++) + ".cnf");
				bool is_sat = false;
				for (auto x : domain_x.domain) {
					for (auto y : domain_y.domain) {
						if (x == y) {
							is_sat = true;
							break;
						}
					}
					if (is_sat) break;
				}
				if (is_sat)
					os_file << "c SAT" << std::endl;
				else
					os_file << "c UNSAT" << std::endl;

				if (os_file.is_open())
				{
					os_file << "c x :"; for (int i : domain_x.domain) os_file << " " << i; os_file << std::endl;
					os_file << "c y :"; for (int i : domain_y.domain) os_file << " " << i; os_file << std::endl;
					GRAPH space;
					CHR_RUN(
							space.vertice(0, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_x );
							space.vertice(1, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_y );
							space.vertice(2, Flags(Flag::RELATION | Flag::EQUAL), N_STR, Integer_variable_domain() );
							space.edge(2,0,0);
							space.edge(2,1,1);
							space.vertice(3, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_x );
							space._first_free_id = 3;
							space.transform_equal_to_disjunction(2);
					)
					space.print_cnf(os_file,true);
					os_file.close();
				}
		    }
}



// ==================================================================
// COMPARATORS (<, >, <=, >=, ==)
// ==================================================================

void create_less_than_benchmark_files()
{
	unsigned int n_file = 0;
	std::ofstream os_file;
	Powerset powerset;
	create_contiguous_powerset(-2,4,powerset);
	for (Integer_variable_domain domain_x : powerset)
		for (Integer_variable_domain domain_y : powerset)
		    {
			    os_file.open("chr_graph_benchmark_less_than_" + std::to_string(n_file++) + ".cnf");
			    // On calcule le résultat
				bool is_sat = domain_y.max() > domain_x.min();
				if (is_sat)
					os_file << "c SAT" << std::endl;
				else
					os_file << "c UNSAT" << std::endl;

				if (os_file.is_open())
				{
					os_file << "c x :"; for (int i : domain_x.domain) os_file << " " << i; os_file << std::endl;
					os_file << "c y :"; for (int i : domain_y.domain) os_file << " " << i; os_file << std::endl;
					GRAPH space;
					CHR_RUN(
					      space.vertice(0, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_x );
					      space.vertice(1, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_y );
					      space.vertice(2, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, Integer_variable_domain() );
					      space.edge(2,0,0);
					      space.edge(2,1,1);
					      space._first_free_id = 3;
					      //space.transform_less_than_to_disjunction(2);
					      space.less_greater_equal_to_disjunction(2,Flag::LESS_THAN);
					)
					space.print_cnf(os_file,true);
					os_file.close();
				}
		    }
}

void create_greater_than_benchmark_files()
{
	unsigned int n_file = 0;
	std::ofstream os_file;
	Powerset powerset;
	create_contiguous_powerset(-2,4,powerset);
	for (Integer_variable_domain domain_x : powerset) {
		for (Integer_variable_domain domain_y : powerset) {
			os_file.open("chr_graph_benchmark_greater_than_" + std::to_string(n_file++) + ".cnf");
			// On calcule le résultat
			bool is_sat = domain_y.min() < domain_x.max();
			if (is_sat)
				os_file << "c SAT" << std::endl;
			else
				os_file << "c UNSAT" << std::endl;

			if (os_file.is_open())
			{
				os_file << "c x :"; for (int i : domain_x.domain) os_file << " " << i; os_file << std::endl;
				os_file << "c y :"; for (int i : domain_y.domain) os_file << " " << i; os_file << std::endl;
				GRAPH space;
				CHR_RUN(
					  space.vertice(0, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_x );
					  space.vertice(1, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_y );
					  space.vertice(2, Flags(Flag::RELATION | Flag::GREATER_THAN), N_STR, Integer_variable_domain() );
					  space.edge(2,0,0);
					  space.edge(2,1,1);
					  space._first_free_id = 3;
					  space.transform_greater_than_to_disjunction(2);
				)
				space.print_cnf(os_file,true);
				os_file.close();
			}
		}
	}
}


void create_less_equal_than_benchmark_files()
{
	unsigned int n_file = 0;
	std::ofstream os_file;
	Powerset powerset;
	create_contiguous_powerset(-2,4,powerset);
	for (Integer_variable_domain domain_x : powerset)
		for (Integer_variable_domain domain_y : powerset)
		    {
			    os_file.open("chr_graph_benchmark_less_equal_than_" + std::to_string(n_file++) + ".cnf");
			    // On calcule le résultat
				bool is_sat = domain_y.max() >= domain_x.min();
				if (is_sat)
					os_file << "c SAT" << std::endl;
				else
					os_file << "c UNSAT" << std::endl;

				if (os_file.is_open())
				{
					os_file << "c x :"; for (int i : domain_x.domain) os_file << " " << i; os_file << std::endl;
					os_file << "c y :"; for (int i : domain_y.domain) os_file << " " << i; os_file << std::endl;
					GRAPH space;
					CHR_RUN(
					      space.vertice(0, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_x );
					      space.vertice(1, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_y );
					      space.vertice(2, Flags(Flag::RELATION | Flag::LESS_EQUAL_THAN), N_STR, Integer_variable_domain() );
					      space.edge(2,0,0);
					      space.edge(2,1,1);
					      space._first_free_id = 3;
					      space.transform_less_equal_than_to_disjunction(2);
					)
					space.print_cnf(os_file,true);
					os_file.close();
				}
		    }
}

void create_greater_equal_than_benchmark_files()
{
	unsigned int n_file = 0;
	std::ofstream os_file;
	Powerset powerset;
	create_contiguous_powerset(-2,4,powerset);
	for (Integer_variable_domain domain_x : powerset)
		for (Integer_variable_domain domain_y : powerset)
		    {
			    os_file.open("chr_graph_benchmark_greater_equal_than_" + std::to_string(n_file++) + ".cnf");
			    // On calcule le résultat
				bool is_sat = domain_y.min() <= domain_x.max();
				if (is_sat)
					os_file << "c SAT" << std::endl;
				else
					os_file << "c UNSAT" << std::endl;

				if (os_file.is_open())
				{
					os_file << "c x :"; for (int i : domain_x.domain) os_file << " " << i; os_file << std::endl;
					os_file << "c y :"; for (int i : domain_y.domain) os_file << " " << i; os_file << std::endl;
					GRAPH space;
					CHR_RUN(
					      space.vertice(0, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_x );
					      space.vertice(1, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_y );
					      space.vertice(2, Flags(Flag::RELATION | Flag::GREATER_EQUAL_THAN), N_STR, Integer_variable_domain() );
					      space.edge(2,0,0);
					      space.edge(2,1,1);
					      space._first_free_id = 3;
					      space.transform_greater_equal_than_to_disjunction(2);
					)
					space.print_cnf(os_file,true);
					os_file.close();
				}
		    }
}


// ==================================================================
// ELEMENT
// ==================================================================

void create_element_benchmark_files()
{
	unsigned int n_file = 0;
	std::ofstream os_file;
	Powerset powerset_x;
	create_contiguous_powerset(-2,4,powerset_x);
	Powerset powerset_index;
	create_contiguous_powerset(-1,3,powerset_index);
	Powerset powerset_tab;
	create_contiguous_powerset(-3,2,powerset_tab);

	for (Integer_variable_domain domain_x : powerset_x)
		for (Integer_variable_domain domain_index : powerset_index)
			for (Integer_variable_domain domain_tab : powerset_tab)
			{
			os_file.open("chr_graph_benchmark_element_" + std::to_string(n_file++) + ".cnf");

			// On calcule le résultat
			bool is_sat = false;
			if (domain_index.max()<0) {
				is_sat = false;
			} else {
				for (auto x : domain_x.domain) {
					for (auto y : domain_tab.domain) {
						if (x == y) {
							is_sat = true;
							break;
						}
					}
					if (is_sat) break;
				}
			}
			if (is_sat)
				os_file << "c SAT" << std::endl;
			else
				os_file << "c UNSAT" << std::endl;

			if (os_file.is_open())
			{
				os_file << "c x :"; for (int i : domain_x.domain) os_file << " " << i; os_file << std::endl;
				os_file << "c index :"; for (int i : domain_index.domain) os_file << " " << i; os_file << std::endl;
				GRAPH space;
				if (domain_index.max()>=0)
				{
					for (unsigned int i=0;i< static_cast<unsigned int> (domain_index.max())+1;++i)
					{
						os_file << "c tab[" << i << "] :";
						for (int j : domain_tab.domain)
							os_file << " " << j;
						os_file << std::endl;
					}
				CHR_RUN(
					space.vertice(0, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_x );
					space.vertice(1, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_index );
					for (unsigned int i=0; i<static_cast<unsigned int> (domain_index.max())+1; ++i)
						space.vertice(2+i, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_tab );

					unsigned int node_element = 2+domain_index.max();
					space.vertice(node_element, Flags(Flag::RELATION | Flag::ELEMENT), N_STR, Integer_variable_domain() );
					space.edge(node_element,0,0); //x
					space.edge(node_element,1,1); //index

					for (unsigned int i=0; i<static_cast<unsigned int> (domain_index.max())+1; ++i)
						space.edge(node_element,2+i,2+i); //tab
					space._first_free_id = 2+domain_index.max();
					space.transform_element_to_disjunction(node_element);
				)

			} else //tous les index sont inférieurs à 0
			{
				CHR_RUN(
					space.vertice(0, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_x );
					space.vertice(1, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_index );
					space.vertice(2, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain_tab ); //fake tab
					unsigned int node_element = 3;
					space.vertice(node_element, Flags(Flag::RELATION | Flag::ELEMENT), N_STR, Integer_variable_domain() );
					space.edge(node_element,0,0); //x
					space.edge(node_element,1,1); //index
					space.edge(node_element,2,2); //fake tab
					space._first_free_id = 3;
					space.transform_element_to_disjunction(node_element);
				)
			}
			space.print_cnf(os_file,true);
			os_file.close();
			}
			}
}


// ==========================================================================
// TEST : STORED EQUAL
// (x == 3) + (x == 4) = 1
// ==========================================================================

void test_stored_equal()
{
	GRAPH space;
	/* // test#1 SAT : (x==2)+(y==3)=2
	unsigned int x  = space._first_free_id++;	// the variable
	unsigned int n1 = space._first_free_id++;	// a random number
	unsigned int a1 = space._first_free_id++;	// an auxiliary (to store result)
	space.vertice(x, Flags(Flag::VARIABLE | Flag::INTEGER),  std::string("x"), Integer_variable_domain({0,1,2,3,4,5}));
	space.vertice(n1, Flags(Flag::VARIABLE | Flag::INTEGER), std::string("n1"), Integer_variable_domain({2}));
	space.vertice(a1, Flags(Flag::VARIABLE | Flag::INTEGER), std::string("a1"), Integer_variable_domain({0,1}));

	unsigned int eq1 = space._first_free_id++;
	space.vertice(eq1, Flags(Flag::RELATION | Flag::STORED_EQUAL), std::string("ST_EQUAL"), Integer_variable_domain() );
	space.edge(eq1, x, 0);
	space.edge(eq1, n1, 1);
	space.edge(eq1, a1, 2);

	unsigned int y  = space._first_free_id++;	// the variable
	unsigned int n2 = space._first_free_id++;
	unsigned int a2 = space._first_free_id++;
	space.vertice(y, Flags(Flag::VARIABLE | Flag::INTEGER),  std::string("y"), Integer_variable_domain({0,1,2,3,4,5}));
	space.vertice(n2, Flags(Flag::VARIABLE | Flag::INTEGER), std::string("n2"), Integer_variable_domain({3}));
	space.vertice(a2, Flags(Flag::VARIABLE | Flag::INTEGER), std::string("a2"), Integer_variable_domain({0,1}));

	unsigned int eq2 = space._first_free_id++;
	space.vertice(eq2, Flags(Flag::RELATION | Flag::STORED_EQUAL), std::string("ST_EQUAL"), Integer_variable_domain() );
	space.edge(eq2, y, 0);
	space.edge(eq2, n2, 1);
	space.edge(eq2, a2, 2);

	unsigned int s  = space._first_free_id++;	// the sum result (=1)
	space.vertice(s, Flags(Flag::VARIABLE | Flag::INTEGER),  std::string("s"), Integer_variable_domain({1}));

	unsigned int addition = space._first_free_id++;
	space.vertice(addition, Flags(Flag::RELATION | Flag::ADD_EQUAL), std::string("SUM"), Integer_variable_domain() );
	space.edge(addition, a1, 0);
	space.edge(addition, a2, 1);
	space.edge(addition, s, 2);
	*/

	// ----------------------------------------------------
	// Test#2
	// variables are "column-sum" and compared to D values
	// ----------------------------------------------------

	// VARIABLES : A 3x2 binary matrix
	std::cout << "X : " << std::endl;
	unsigned int N=3;
	unsigned int C=2;
	std::vector< std::vector<unsigned int> > x;
	for(unsigned int i=0; i<N; i++) {
		std::vector<unsigned int> xrow;
		for(unsigned int j=0; j<C; j++) {
			unsigned int var = space._first_free_id++;
			space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER),N_STR, Integer_variable_domain({0,1}));
			xrow.push_back(var);
			std::cout << var << " ";
		}
		x.push_back(xrow);
		std::cout << std::endl;
	}

	// CONSTANT : the cardinality values
	std::cout << "D : " ;
	std::vector<unsigned int> d{1,2};	// OBS: their sum cant be bigger than sequence
	std::vector<unsigned int> D;
	for(unsigned int j=0; j<C; j++) {
		unsigned int var = space._first_free_id++;
		space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER),N_STR, Integer_variable_domain({static_cast <int>(d[j])}));
		D.push_back(var);
		std::cout << var << " ";
	}
	std::cout << std::endl;

	// aux_domain
	Integer_variable_domain aux_domain;
	for (unsigned int i=0; i<N; i++)
		aux_domain.domain.push_back(i);

	// CONSTRAINT: the sum
	for(unsigned int j=0; j<C; j++) {
		unsigned int xvar = x[0][j];

		for(unsigned int i=1; i<N; i++) {
			unsigned int yvar = x[i][j];
			unsigned int zvar;

			if (i == N-1) {
				zvar = D[j];
			} else {
				zvar = space._first_free_id++;
				space.vertice(zvar, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain );
			}
			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain());
			space.edge(current_add, xvar,0);
			space.edge(current_add, yvar,1);
			space.edge(current_add, zvar,2);
			std::cout << xvar << " + " << yvar << " = " << zvar << std::endl;
			xvar = zvar;
		}
	}

	//space.print_graph_dot(std::cout);

	CHR_RUN(
		space.apply_all_transformation();
	)
//	std::cout << std::endl;
//	space.print_graph_dot(std::cout);
	space.print_store(std::cout);

	// print
	std::unordered_map<unsigned int,unsigned int> map_cnf_graph;
//	std::ofstream ofile;
//	ofile.open("test.cnf");
//	space.print_cnf(ofile, map_cnf_graph, true);
//	ofile.close();
	space.print_cnf(std::string("test_stored_equal.cnf"), map_cnf_graph, true);

}

