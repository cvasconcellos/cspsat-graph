// ===========================================================================
//
// GOLOMB RULERS
// Oct 2019
//
// Variables: N variables to indicate the position of cuts.
//   x in {0,m}
//
// Constraints:
//   C1: All cuts are ordered
//   C2: The distances between all cuts (c2-c1=d) must be different
//
// http://www.csplib.org/Problems/prob006/
// ===========================================================================

#include <iostream>
#include <vector>

#define N_STR std::string()


void run_golomb(GRAPH& space, unsigned int N)
{
	// Model
	space.encoding_sequence.push_back(Flag::LOG_ENCODING);
	space.encoding_sequence.push_back(Flag::DIRECT_ENCODING);

	// Variables ------------------------------------------

	IntVarDomain domain;
	for (unsigned int i=0; i<=6; i++) domain.domain.push_back(i);

	std::cout << "variables: ";
	std::vector<unsigned int> cuts;
    for (unsigned int i=0; i<N; ++i) {
		unsigned int var = space._first_free_id++;
		if (i==0)	// to do the first cut in 0
			space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({0}));
		else
			space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain);
        space.num_variables++;
		cuts.push_back(var);
		std::cout << var << " ";
    }
    std::cout << std::endl;

    // ----------------------------------------------------
    // constraint: Cut order
    // c1 < c2 < .. < cN
    // ----------------------------------------------------
    {
    	unsigned int c1 = cuts[0];
    	for (unsigned int i=1; i<cuts.size(); ++i) {
			unsigned int c2 = cuts[i];
			unsigned int lessthan = space._first_free_id++;
			space.vertice(lessthan, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, IntVarDomain());
			space.edge(lessthan, c1, 0);
			space.edge(lessthan, c2, 1);
			std::cout << lessthan << ":\t" << c1 << " < " << c2 << std::endl;
			c1 = c2;
    	}
    }
    std::cout << std::endl;

    // ----------------------------------------------------
	// constraint: all distances between cuts are different
	// (c2-c1 = d)   <==>   d+c1=c2
	// ----------------------------------------------------
	{
		unsigned int alldiff = space._first_free_id++;
		space.vertice(alldiff, Flags(Flag::RELATION | Flag::ALL_DIFFERENT), N_STR, IntVarDomain());

		for (int i=N-1; 0<i; --i) {
			unsigned int c2 = cuts[i];			// cut2

			for (int j=(i-1); 0<=j; --j) {
				unsigned int c1 = cuts[j];		// cut1
				unsigned int distance = space._first_free_id++;	// distance
				space.vertice(distance, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, domain);

				unsigned int addition = space._first_free_id++;
				space.vertice(addition, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain());
				space.edge(addition, distance, 0);
				space.edge(addition, c1, 1);
				space.edge(addition, c2, 2);
				std::cout << addition << ":\t" << distance << " + " << c1 << " = " << c2 << std::endl;

				space.edge(alldiff, distance,-1);
			}
		}
	}
	std::cout << std::endl;

}

