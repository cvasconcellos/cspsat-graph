/*
 *
 * TESTS.hpp
 *
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <graph_chr_header.hpp>

#define N_STR std::string()



// =======================================================================
// run_test()
//
// Parses a model-file.
// =======================================================================

void run_test(GRAPH& space, const std::string& filename)
{
	// model
	space.encoding_sequence.push_back(Flag::LOG_ENCODING);


	int nvars, nconstraints;		// model parameters
	std::map<std::string,int> vars;	// variables (graph_id,name)

	std::ifstream inputfile(filename);
	if (!inputfile) {
		std::cerr << "run_test(): no file." << std::endl;
		return;
	}

	std::string line;
	unsigned int numline = 0;
	while (std::getline(inputfile, line))
	{
		numline++;
		std::istringstream linestream(line);
		std::string aux;
		linestream >> aux;	// skips 1st element

		// -- PARAMETERS
		if (line[0] == 'p') {
			linestream >> nvars;
			linestream >> nconstraints;
		}
		// -- VARIABLES
		else if (line[0] == 'v') {
			std::string varname;
			linestream >> varname;

			int lb, ub;		// boundaries
			linestream >> lb;
			linestream >> ub;

			if (ub < lb) {
				std::cerr << "run_test(): Variable domain error" << std::endl;
				return;
			}

			// domain
			IntVarDomain dom;
			for (int i=lb; i<=ub; i++) dom.domain.push_back(i);

			// create variable
			unsigned int x = space._first_free_id++;
			space.vertice(x, Flags(Flag::VARIABLE | Flag::INTEGER), varname, dom); space.num_variables++;

			// add variable to map
			vars[varname] = x;

		}
		// -- OPERATIONS / CONSTRAINTS
		else if (line[0] == 'o') {
			std::string op, x, y, z;
			linestream >> op;
			linestream >> x;
			linestream >> y;
			linestream >> z;

			if (op.length() * x.length() * y.length() * z.length() == 0) {
				std::cerr << "run_test(): Operation error (line " << numline << ")" << std::endl;
				return;
			}

			// create operation
			unsigned int addition = space._first_free_id++;
			space.vertice(addition, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain()); space.num_relations++;
			space.edge(addition, vars[x], 0);
			space.edge(addition, vars[y], 1);
			space.edge(addition, vars[z], 2);
		}
	}
	inputfile.close();


	// -------------------------
	std::cout << "graph size: " << space._first_free_id << std::endl;
	return;

}




// ==========================================================================
// Test #1 HYBRID
// 2020-02-02
//
// Sums 2 numbers (constants)
// ==========================================================================


void test_hybrid(GRAPH& space)
{
	std::cout << "test_hybrid()" << std::endl;
	space.encoding_sequence.push_back(Flag::HYBRID_ENCODING);
	space.HBASE = 3;

	unsigned int x = space._first_free_id++;
	unsigned int y = space._first_free_id++;
	unsigned int z = space._first_free_id++;

	/* 21+10=31	(no carry, both log+order)
	space.vertice(x, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({21}));
	space.vertice(y, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({10}));
	space.vertice(z, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}));
	*/

	/* 17+6=23	(no carry, y is only log)
	space.vertice(x, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({17}));
	space.vertice(y, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({6}));
	space.vertice(z, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}));
	*/

	// 30+13=[0,45]	(carry, both log+order)
	space.vertice(x, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({30}));
	space.vertice(y, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({13}));
	space.vertice(z, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45}));


	/* -2+15=[0,24]
	space.vertice(x, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({-2}));
	space.vertice(y, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({15}));
	//space.vertice(z, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}));
	space.vertice(z, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}));
    */

	/* [0,2]+[0,9]=[0,12]	--> 18 solutions (NOT TESTED)
	space.vertice(x, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({0,1,2}));
	space.vertice(y, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({0,1,2,3,4,5,6,7,8,9}));
	space.vertice(z, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({0,1,2,3,4,5,6,7,8,9,10,11,12}));
    */

	unsigned int add = space._first_free_id++;
	space.vertice(add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain());
	space.edge(add, x, 0);
	space.edge(add, y, 1);
	space.edge(add, z, 2);
}



void test_addition_hybrid(GRAPH& space)
{
	std::cout << "test_addition_hybrid()" << std::endl;
	space.encoding_sequence.push_back(Flag::HYBRID_ENCODING);
	space.HBASE = 3;

/*	// test #1 ------------------------------------------------
	unsigned int M = space._first_free_id++;
	space.vertice(M , Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({15}));
	unsigned int x1 = space._first_free_id++;
	space.vertice(x1, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({1,2,3,4,5,6,7,8,9}));
	unsigned int x2 = space._first_free_id++;
	space.vertice(x2, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({1,2,3,4,5,6,7,8,9}));

	unsigned int sum1 = space._first_free_id++;
	space.vertice(sum1, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain());
	space.edge(sum1,x1, 0);
	space.edge(sum1,x2, 1);
	space.edge(sum1, M, 2);
	std::cout << "[1,9] + [1,9] = 15" << std::endl;
*/

	// test #2 ------------------------------------------------
	unsigned int M = space._first_free_id++;
	space.vertice(M , Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({15}));
	unsigned int x1 = space._first_free_id++;
	space.vertice(x1, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({1,2,3,4,5,6,7,8,9}));
	unsigned int x2 = space._first_free_id++;
	space.vertice(x2, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({1,2,3,4,5,6,7,8,9}));
	unsigned int x3 = space._first_free_id++;
	space.vertice(x3, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({1,2,3,4,5,6,7,8,9}));
	unsigned int a = space._first_free_id++;
	space.vertice(a , Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({3,4,5,6,7,8,9,10,11,12,13,14}));

	unsigned int sum1 = space._first_free_id++;
	space.vertice(sum1, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain());
	space.edge(sum1, x1, 0);
	space.edge(sum1, x2, 1);
	space.edge(sum1, a , 2);
	std::cout << "[1,9] + [1,9] = [3,14]" << std::endl;

	unsigned int sum2 = space._first_free_id++;
	space.vertice(sum2, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain());
	space.edge(sum2, a , 0);
	space.edge(sum2, x3, 1);
	space.edge(sum2, M , 2);
	std::cout << "[3,14] + [1,9] = 15" << std::endl;


	std::cout << std::endl;
}




