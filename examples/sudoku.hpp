// ===========================================================================
//
// SUDOKU
//
// ===========================================================================

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>

#define N_STR std::string()


struct SudokuInstance
{
    std::vector<unsigned int> board;
    unsigned int order;

    SudokuInstance() {}

    //
    // Constructor for the Gordon Royle's instances
    //
    SudokuInstance(const std::string& filename, const unsigned int pb)
    {
		// open file
		std::ifstream inputfile(filename);
		if (!inputfile) {
			std::cerr << "sudoku: no file" << std::endl;
			return;
		}

		// read line by line until find the instance required
		unsigned int current_pb = 0;
		std::string line;

		while (std::getline(inputfile, line)) {
			if (current_pb == pb) {
				for (unsigned int i=0; i<line.size();i++)
					board.push_back( line[i]-'0' );
				inputfile.close();
			}
			current_pb++;
		}
		inputfile.close();

		// problem size/order
		//N     = sqrt(board.size());
		order = sqrt(sqrt(board.size()));
		return;
	}

//    SudokuInstance(const unsigned int pb)
//    {
//        for (unsigned int i=0; i<instances[pb].size(); i++) {
//            if (instances[pb] == ".")
//                board.push_back( '0' );
//            else
//                board.push_back( line[i]-'0' );
//        }
//
//		// problem size/order
//		//N     = sqrt(board.size());
//		order = sqrt(sqrt(board.size()));
//		return;
//	}

    //
    // Sudoku::print()
    //
	void print()
	{
		const unsigned int n = order*order;
		std::cout << "board: " << std::endl;

		for (unsigned int i=0; i<n*n; ++i) {
			if (board[i] == 0) std::cout << "- ";
			else std::cout << board[i] << " ";

			if ((i+1) % n == 0)          std::cout << std::endl;
			else if ((i+1) % order == 0) std::cout << "| ";
		}
		std::cout << std::endl;
	}


//    const int* instance[] = {
//        ".................................................................................",    // 0
//
//        // GordonRoyle
//        ".......1.4.........2...........5.4.7..8...3....1.9....3..4..2...5.1........8.6...",    // 1
//        ".......1.4.........2...........5.6.4..8...3....1.9....3..4..2...5.1........8.7...",    // 2
//        ".......12....35......6...7.7.....3.....4..8..1...........12.....8.....4..5....6..",    // 3
//        ".......12..36..........7...41..2.......5..3..7.....6..28.....4....3..5...........",    // 4
//        ".......12..8.3...........4.12.5..........47...6.......5.7...3.....62.......1.....",    // 5
//
//        // MagicSquare
//    }

};



// ============================================================================
// SUDOKU (generic)
// ============================================================================

void run_sudoku(GRAPH& space, std::string filename, unsigned int instance)
{
    // read instance
    SudokuInstance s(filename, instance);
    s.print();
    const unsigned int N = s.order*s.order;

	// domain
	IntVarDomain domain;
	for (unsigned int i=1; i<=N; i++) domain.domain.push_back(i);

    // variables
    for (unsigned int i=0; i<N*N; ++i) {
        if (s.board[i] == 0)
            space.vertice(space._first_free_id++, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain);
        else
            space.vertice(space._first_free_id++, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({static_cast <int>(s.board[i])}));
        space.num_variables++;
    }

    // alldiff rows
    for (unsigned int i=0; i<N; ++i) {
        space.vertice(space._first_free_id++, Flags(Flag::RELATION | Flag::ALL_DIFFERENT), N_STR, IntVarDomain()); space.num_relations++;
        for (unsigned int j=0; j<N; ++j)
            space.edge(space._first_free_id-1, i*N + j,0);
    }
    // alldiff columns
    for (unsigned int i=0; i<N; ++i) {
        space.vertice(space._first_free_id++, Flags(Flag::RELATION | Flag::ALL_DIFFERENT), N_STR, IntVarDomain()); space.num_relations++;
        for (unsigned int j=0; j<N; ++j)
            space.edge(space._first_free_id-1, i + j*N ,0);
    }
    // alldiff sub-grids
    for (unsigned int i=0; i<N; ++i) {
        space.vertice(space._first_free_id++, Flags(Flag::RELATION | Flag::ALL_DIFFERENT), N_STR, IntVarDomain()); space.num_relations++;
        for (unsigned int j=0; j<N; ++j)
            space.edge(space._first_free_id-1,((j/s.order) * N) + j % s.order,0);
    }
}



// ============================================================================
// MAGIC SUDOKU
//
// Sudoku + MagicSquare3
// It adds MS constraints to the 1st grid
// www.funwithpuzzles.com/2014/05/magic-square-sudoku-daily-sudoku-league.html
//
// ============================================================================

void run_magic_sudoku(GRAPH& space, std::string filename, unsigned int instance)
{
    // read instance
    SudokuInstance s(filename, instance);
    s.print();
    const unsigned int N = s.order*s.order;

	// domain
	IntVarDomain domain;
	for (unsigned int i=1; i<=N; ++i) domain.domain.push_back(i);

    // variables
    for (unsigned int i=0; i<N*N; ++i) {
        if (s.board[i] == 0)
            space.vertice(space._first_free_id++, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, domain);
        else
            space.vertice(space._first_free_id++, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, IntVarDomain({static_cast <int>(s.board[i])}));
        space.num_variables++;
    }

    // Sudoku constraints -------------------------------------------

    // alldiff rows
    for (unsigned int i=0; i<N; ++i) {
        space.vertice(space._first_free_id++, Flags(Flag::RELATION | Flag::ALL_DIFFERENT), N_STR, IntVarDomain()); space.num_relations++;
        for (unsigned int j=0; j<N; ++j)
            space.edge(space._first_free_id-1, i*N + j,0);
    }
    // alldiff columns
    for (unsigned int i=0; i<N; ++i) {
        space.vertice(space._first_free_id++, Flags(Flag::RELATION | Flag::ALL_DIFFERENT), N_STR, IntVarDomain()); space.num_relations++;
        for (unsigned int j=0; j<N; ++j)
            space.edge(space._first_free_id-1, i + j*N ,0);
    }
    // alldiff sub-grids
    for (unsigned int i=0; i<N; ++i) {
        space.vertice(space._first_free_id++, Flags(Flag::RELATION | Flag::ALL_DIFFERENT), N_STR, IntVarDomain()); space.num_relations++;
        for (unsigned int j=0; j<N; ++j)
            space.edge(space._first_free_id-1,((j/s.order) * N) + (j % s.order),0);
    }


    // MagicSquare constraints --------------------------------------

    // magic number (order3: 15)
    const unsigned int M = ((s.order*s.order*s.order)+s.order)/2;
	unsigned int magic_var = space._first_free_id++;
    space.vertice(magic_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, IntVarDomain({ static_cast<int>(M) })); space.num_variables++;
    std::cout << "M = " << M << "\t(id: " << magic_var << ")" << std::endl;

    // domain for auxiliary vars [1..M]
    IntVarDomain aux_domain;
    for (unsigned int i=1; i<=M; i++) aux_domain.domain.push_back(i);

    std::vector<std::vector< unsigned int >> matrix = {{57,58,59},{66,67,68},{75,76,77}};

    // row sum
    {
		for (unsigned int i=0; i<s.order; ++i)
		{
			unsigned int prev_var = matrix[i][0];
			for (unsigned int j=1; j<s.order; ++j)
			{
//				// full propagation
//				unsigned int curr_var = matrix[i][j];
//				unsigned int next_var;
//				if (j == (s.order-1)) {
//					next_var = magic_var;
//				} else {
//					next_var = space._first_free_id++;
//					space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain); space.num_variables++;
//				}
//				unsigned int addition = space._first_free_id++;
//				space.vertice(addition, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
//				space.edge(addition, prev_var, 0);
//				space.edge(addition, curr_var, 1);
//				space.edge(addition, next_var, 2);
//				std::cout << addition << ":\t" << prev_var << " + " << curr_var << " = " << next_var << std::endl;
//				prev_var = next_var;


				// selective propagation (last sum of sequence)
				unsigned int curr_var = matrix[i][j];
				unsigned int next_var;
				unsigned int addition = space._first_free_id++;
				if (j == (s.order-1)) {
					next_var = magic_var;
					space.vertice(addition, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
				} else {
					next_var = space._first_free_id++;
					space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain); space.num_variables++;
					space.vertice(addition, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
				}
				space.edge(addition, prev_var, 0);
				space.edge(addition, curr_var, 1);
				space.edge(addition, next_var, 2);
				std::cout << addition << ":\t" << prev_var << " + " << curr_var << " = " << next_var << std::endl;
				prev_var = next_var;
			}
		}
		std::cout << std::endl;
    }

	// column sum
    {
		for (unsigned int j=0; j<s.order; ++j)
		{
			unsigned int previous_var = matrix[0][j];
			for (unsigned int i=1; i<s.order; ++i)
			{
				// full propagation
				unsigned int current_var = matrix[i][j];
				unsigned int next_var;
				if (i == (s.order-1)) {
					next_var = magic_var;
				} else {
					next_var = space._first_free_id++;
					space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain); space.num_variables++;
				}
				unsigned int current_add = space._first_free_id++;
				space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
				space.edge(current_add, previous_var,0);
				space.edge(current_add, current_var, 1);
				space.edge(current_add, next_var,    2);
				std::cout << current_add << " :\t" << previous_var << " + " << current_var << " = " << next_var << std::endl;
				previous_var = next_var;

//				// selective propagation
//				unsigned int current_var = matrix[i][j];
//				unsigned int next_var;
//				unsigned int current_add = space._first_free_id++;
//				if (i == (s.order-1)) {
//					next_var = magic_var;
//					space.vertice(current_add, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
//				} else {
//					next_var = space._first_free_id++;
//					space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain); space.num_variables++;
//					space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
//				}
//				space.edge(current_add,previous_var,0);
//				space.edge(current_add,current_var, 1);
//				space.edge(current_add,next_var,    2);
//				std::cout << current_add << " :\t" << previous_var << " + " << current_var << " = " << next_var << std::endl;
//				previous_var = next_var;
			}
		}
		std::cout << std::endl;
    }

    // diagonals
    {
		unsigned int d1_prev_var = matrix[0][0];
		unsigned int d2_prev_var = matrix[0][s.order-1];

		for (unsigned int i=1; i<s.order; ++i)
		{
			// principal + all propagation
			unsigned int d1_curr_var = matrix[i][i];
			unsigned int d1_next_var;
			if (i == (s.order-1)) {
				d1_next_var = magic_var;
			} else {
				d1_next_var = space._first_free_id++;
				space.vertice(d1_next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain); space.num_variables++;
			}
			unsigned int d1_sum = space._first_free_id++;
			space.vertice(d1_sum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(d1_sum, d1_prev_var,0);
			space.edge(d1_sum, d1_curr_var,1);
			space.edge(d1_sum, d1_next_var,2);
			d1_prev_var = d1_next_var;

//			// principal + selective propagation
//			unsigned int d1_curr_var = matrix[i][i];
//			unsigned int d1_next_var;
//			unsigned int d1_sum = space._first_free_id++;
//			if (i == (s.order-1)) {
//				d1_next_var = magic_var;
//				space.vertice(d1_sum, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, Integer_variable_domain() ); space.num_relations++;
//			} else {
//				d1_next_var = space._first_free_id++;
//				space.vertice(d1_next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain); space.num_variables++;
//				space.vertice(d1_sum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
//			}
//			space.edge(d1_sum, d1_prev_var,0);
//			space.edge(d1_sum, d1_curr_var,1);
//			space.edge(d1_sum, d1_next_var,2);
//			d1_prev_var = d1_next_var;


			// secondary + full propagation
			unsigned int d2_curr_var = matrix[i][s.order-1-i];
			unsigned int d2_next_var;
			if (i == (s.order-1)) {
				d2_next_var = magic_var;
			} else {
				d2_next_var = space._first_free_id++;
				space.vertice(d2_next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain ); space.num_variables++;
			}
			unsigned int d2_sum = space._first_free_id++;
			space.vertice(d2_sum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(d2_sum, d2_prev_var,0);
			space.edge(d2_sum, d2_curr_var,1);
			space.edge(d2_sum, d2_next_var,2);
			d2_prev_var = d2_next_var;

//			// secondary + selective propagation
//			unsigned int d2_curr_var = matrix[i][s.order-1-i];
//			unsigned int d2_next_var;
//			unsigned int d2_sum = space._first_free_id++;
//			if (i == (s.order-1)) {
//				d2_next_var = magic_var;
//				space.vertice(d2_sum, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
//			} else {
//				d2_next_var = space._first_free_id++;
//				space.vertice(d2_next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain ); space.num_variables++;
//				space.vertice(d2_sum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
//			}
//			space.edge(d2_sum, d2_prev_var,0);
//			space.edge(d2_sum, d2_curr_var,1);
//			space.edge(d2_sum, d2_next_var,2);
//			d2_prev_var = d2_next_var;

		}
    }
}
