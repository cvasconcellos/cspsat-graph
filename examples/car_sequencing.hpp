// ===========================================================================
// CAR SEQUENCING
//
// Demand  : The number of cars per class to sequence
// Link    :
// Capacity: The stations' capacity can not be exceeded
//
// (csplib)
// 1st line: number of cars; number of options; number of classes.
// 2nd line: for each option, the max. number of cars with that option in a block.
// 3rd line: for each option, the block size to which the maximum number refers.
// Then for each class: index, demand; which option are required (1) or not (0).
// ===========================================================================

#define N_STR std::string()

struct CarSequencingInstance
{
    unsigned int                            n, num_options, num_classes;
    std::vector<unsigned int>               capacity, blocksize, demand;
    std::vector< std::vector<unsigned int>> classoption;

	// constructors
    CarSequencingInstance() : n(0) {}

    CarSequencingInstance(const std::string& filename)
    {
	    std::ifstream inputfile(filename);
		if (!inputfile) {
    	    std::cerr << "carsequencing : no file" << std::endl;
			return;
    	}

	    std::vector<unsigned int> opvector;
		std::string line;
    	unsigned int i=1;

	    while (std::getline(inputfile, line))
    	{
			std::istringstream linestream(line);
		    int aux;
			while(linestream >> aux) {
				if (i==1) {
					n = aux;
				} else if (i == 2) {
					num_options = aux;
				} else if (i == 3) {
					num_classes = aux;
				} else if ((3 < i) && (i<= 3+num_options)) {
					capacity.push_back(aux);
				} else if ((3+num_options < i) && (i< 3+2*num_options+1)) {
					blocksize.push_back(aux);
				} else {
					opvector.push_back(aux);
				}
				i++;
			}
    	}
    	inputfile.close();

		// demand, optionclass
		for(unsigned int r=0; r<num_classes; r++)
		{
			demand.push_back(opvector[r*(num_options+2)+1]);

			std::vector<unsigned int> oprow;
			for (unsigned int c=0; c<num_options; c++) {
				unsigned int i = r*(num_options+2)+(c+2);
				oprow.push_back(opvector[i]);
			}
			classoption.push_back(oprow);
		}
	    return;
    }

	// destructor
	~CarSequencingInstance()
    {
		capacity.clear();
		blocksize.clear();
		demand.clear();
		classoption.clear();
	}
};





// ===========================================================================
// CARSEQUENCING (original code)
// ===========================================================================

void generate_car_sequencing(GRAPH& space, std::string instance)
{
    std::cout << "carseq : start" << std::endl;

	CarSequencingInstance pb(instance);
	// TEST -------------------------------------
	std::cout << "Instance" << std::endl;
	std::cout << pb.n << " " << pb.num_options << " " << pb.num_classes << std::endl;
	for (unsigned int i=0; i<pb.num_options; i++)
		std::cout << pb.capacity[i] << " " ;
	std::cout << std::endl;
	for (unsigned int i=0; i<pb.num_options; i++)
		std::cout << pb.blocksize[i] << " " ;
	std::cout << std::endl;
	for (unsigned int i=0; i<pb.num_classes; i++) {
		std::cout << i << " " << pb.demand[i] << " ";
		for (unsigned int j=0; j<pb.num_options; j++)
			std::cout << pb.classoption[i][j] << " ";
		std::cout << std::endl;
	}
	std::cout << std::endl;


    std::vector<unsigned int>                capacity, blocksize, demand; //, classes, options;
    std::vector< std::vector<unsigned int> > sequenceclass, sequenceoptions, classoptions;

	// SequenceClass, SequenceOptions ---------------------
	for (unsigned int i=0; i<pb.n; i++) {
		std::vector<unsigned int> srow;
		for (unsigned int j=0; j<pb.num_classes; j++) {
			unsigned int var = space._first_free_id++;
			space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, Integer_variable_domain({0,1})); space.num_variables++;
			srow.push_back(var);
		}
		sequenceclass.push_back(srow);
	}
	for (unsigned int i=0; i<pb.n; i++) {
		std::vector<unsigned int> srow;
		for (unsigned int j=0; j<pb.num_options; j++) {
			unsigned int var = space._first_free_id++;
			space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, Integer_variable_domain({0,1})); space.num_variables++;
			srow.push_back(var);
		}
		sequenceoptions.push_back(srow);
	}

	// Capacities, Demand, Classes, Options & ClassOptions ------------------
	for (unsigned int i=0; i<pb.num_options; i++) {
		unsigned int var = space._first_free_id++;
		// +1 for the LESS_THAN constraint
		//space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, Integer_variable_domain({static_cast <int>(pb.capacity[i]+1)})); space.num_variables++;
		space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, Integer_variable_domain({static_cast <int>(pb.capacity[i])})); space.num_variables++;
		capacity.push_back(var);
	}
	for (unsigned int i=0; i<pb.num_classes; i++) {
		unsigned int var = space._first_free_id++;
		space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, Integer_variable_domain({static_cast <int>(pb.demand[i])})); space.num_variables++;
		demand.push_back(var);
	}
	for (unsigned int i=0; i<pb.num_classes; i++) {
		std::vector<unsigned int> co_row;
		for (unsigned int j=0; j<pb.num_options; j++) {
			unsigned int var = space._first_free_id++;
			space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, Integer_variable_domain({static_cast <int>(pb.classoption[i][j])})); space.num_variables++;
			co_row.push_back(var);
		}
		classoptions.push_back(co_row);
	}


	// TEST -------------------------------------
	std::cout << "SequenceClass : " << std::endl ;
	for (unsigned int i=0; i<pb.n; i++) {
		for(unsigned int j=0; j<pb.num_classes; j++) {
            std::cout << sequenceclass[i][j] << " " ;
		}
		std::cout << std::endl ;
    }
	std::cout << std::endl ;
    std::cout << "SequenceOptions : " << std::endl ;
	for (unsigned int i=0; i<pb.n; i++) {
		for(unsigned int k=0; k<pb.num_options; k++) {
            std::cout << sequenceoptions[i][k] << " " ;
		}
		std::cout << std::endl ;
    }
    std::cout << std::endl ;
	std::cout << "Capacity : " << std::endl ;
	for(unsigned int j=0; j<pb.num_options; j++) {
		std::cout << capacity[j] << " " ;
    }
	std::cout << std::endl << std::endl;
	std::cout << "Demand : " << std::endl ;
	for(unsigned int j=0; j<pb.num_classes; j++) {
		std::cout << demand[j] << " " ;
    }
	std::cout << std::endl << std::endl;
    std::cout << "ClassOptions : " << std::endl ;
	for(unsigned int j=0; j<pb.num_classes; j++) {
        for(unsigned int k=0; k<pb.num_options; k++) {
            std::cout << classoptions[j][k] << " " ;
		}
		std::cout << std::endl ;
    }
    std::cout << std::endl;


	// ----------------------------------------------------
	// DEMAND-1
	// ----------------------------------------------------
/*	for(unsigned int j=0; j<pb.num_classes; j++) {
		unsigned int xvar = sequenceclass[0][j];

		for(unsigned int i=1; i<pb.n; i++) {
			unsigned int yvar = sequenceclass[i][j];

			unsigned int zvar;
			if (i == (pb.n-1)) {
				zvar = demand[j];
			} else {

				Integer_variable_domain zdom;
				for (unsigned int d=0; d<=pb.demand[j]; d++)
					zdom.domain.push_back(d);

				zvar = space._first_free_id++;
				space.vertice(zvar, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, zdom ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain()); space.num_relations++;
			space.edge(current_add, xvar,0);
			space.edge(current_add, yvar,1);
			space.edge(current_add, zvar,2);
			//std::cout << xvar << " + " << yvar << " = " << zvar << std::endl;
			xvar = zvar;
		}
	}
	//std::cout << std::endl;
*/

	// ----------------------------------------------------
	// DEMAND-2
	// ----------------------------------------------------
/*	//std::vector<unsigned int> optiondemand;

	for(unsigned int k=0; k<pb.num_options; k++) {

		// identify the classes requiring the option-k
		unsigned int max_demand = 0;				// the value
		std::vector<unsigned int> optionclasses;	// the variables involved

		//std::cout << "option " << k << ": ";

		for(unsigned int j=0; j<pb.num_classes; j++) {
			if (pb.classoption[j][k] == 1) {
				optionclasses.push_back(demand[j]);
				max_demand += pb.demand[j];
				//std::cout << demand[j] << " ";
			}
		}

		// store the maximum demand
		unsigned int max_demand_var = space._first_free_id++;
		space.vertice(max_demand_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, Integer_variable_domain({static_cast <int>(max_demand)})); space.num_variables++;
		//optiondemand.push_back(max_demand_var);

		//std::cout << "\t(" << max_demand << " - " << max_demand_var << ")"<< std::endl;

		// sum-up the class demands
		unsigned int xvar = optionclasses[0];

		for(unsigned int i=1; i<optionclasses.size(); i++) {
			unsigned int yvar = optionclasses[i];

			unsigned int zvar;
			if (i == (optionclasses.size()-1)) {
				zvar = max_demand_var;
			} else {

				Integer_variable_domain zdom;
				for (unsigned int d=0; d<=max_demand; d++)
					zdom.domain.push_back(d);

				zvar = space._first_free_id++;
				space.vertice(zvar, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, zdom ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain()); space.num_relations++;
			space.edge(current_add, xvar,0);
			space.edge(current_add, yvar,1);
			space.edge(current_add, zvar,2);
			//std::cout << xvar << " + " << yvar << " = " << zvar << std::endl;
			xvar = zvar;
		}

		std::cout << std::endl;

		// sum-up across the SequenceOption
		xvar = sequenceoptions[0][k];

		for(unsigned int i=1; i<pb.n; i++) {
			unsigned int yvar = sequenceoptions[i][k];

			unsigned int zvar;
			if (i == (pb.n-1)) {
				// variable to store the demands' sum
				zvar = max_demand_var;
			} else {

				Integer_variable_domain zdom;
				for (unsigned int d=0; d<=(i+1); d++)
					zdom.domain.push_back(d);

				zvar = space._first_free_id++;
				space.vertice(zvar, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, zdom ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain()); space.num_relations++;
			space.edge(current_add, xvar,0);
			space.edge(current_add, yvar,1);
			space.edge(current_add, zvar,2);
			//std::cout << xvar << " + " << yvar << " = " << zvar << std::endl;
			xvar = zvar;
		}
	}
*/


	// ----------------------------------------------------
	// DEMAND-3 (Demand1 written with SequenceCounter)
	// ----------------------------------------------------
	for(unsigned int j=0; j<pb.num_classes; j++) {
		unsigned int card = space._first_free_id++;
		space.vertice(card, Flags(Flag::RELATION | Flag::CARDINALITY), N_STR, Integer_variable_domain()); space.num_relations++;
		space.edge(card, demand[j], 0);
		//std::cout << "cardinality " << card <<  ": " << demand[j] << " " ;

		for(unsigned int i=0; i<pb.n; i++) {
			space.edge(card, sequenceclass[i][j], i+1);
			//std::cout << sequenceclass[i][j] << " ";
		}
		//std::cout << std::endl;
	}
	//std::cout << std::endl;



	// ----------------------------------------------------
	// DEMAND4 : Demand2 with SequenceCounter
	// ----------------------------------------------------

	for(unsigned int k=0; k<pb.num_options; k++) {
		// identify the classes requiring the option-k
		unsigned int total_demand = 0;				// the value

		for(unsigned int j=0; j<pb.num_classes; j++) {
			if (pb.classoption[j][k] == 1) {
				total_demand += pb.demand[j];
			}
		}

		// store the total demand (per option)
		unsigned int max_demand_var = space._first_free_id++;
		space.vertice(max_demand_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, Integer_variable_domain({static_cast <int>(total_demand)})); space.num_variables++;

		// cardinality over options' usage
		unsigned int card = space._first_free_id++;
		space.vertice(card, Flags(Flag::RELATION | Flag::CARDINALITY), N_STR, Integer_variable_domain()); space.num_relations++;
		space.edge(card, max_demand_var, 0);

		for(unsigned int i=0; i<pb.n; i++) {
			space.edge(card, sequenceoptions[i][k], i+1);
		}
	}



	// ----------------------------------------------------
	// LINK1 : Sequence to stations usage
	// IF SequenceClass[i,j] THEN SequenceOptions[i,k] = ClassOptions[j,k]
	// ----------------------------------------------------

	for (unsigned int i=0; i<pb.n; i++) {
		for(unsigned int j=0; j<pb.num_classes; j++) {
			for(unsigned int k=0; k<pb.num_options; k++) {

				unsigned int ifthen = space._first_free_id++;
				space.vertice(ifthen, Flags(Flag::RELATION | Flag::IF_THEN), N_STR, Integer_variable_domain()); space.num_relations++;
				space.edge(ifthen, sequenceclass[i][j], 0);
				space.edge(ifthen, sequenceoptions[i][k], 1);
				space.edge(ifthen, classoptions[j][k], 2);
				//std::cout << sequenceclass[i][j] << " => (" << sequenceoptions[i][k] << " = " << classoptions[j][k] << ")" << "   ";
			}
			//std::cout << std::endl;
		}
	}


	// ----------------------------------------------------
	// LINK2 : options => OR(classes..)
	// ----------------------------------------------------
	for (unsigned int i=0; i<pb.n; i++) {
		for (unsigned int k=0; k<pb.num_options; k++) {

			unsigned int ifthenor = space._first_free_id++;
			space.vertice(ifthenor, Flags(Flag::RELATION | Flag::IF_THEN_OR), N_STR, Integer_variable_domain()); space.num_relations++;
			unsigned int v = 0;
			space.edge(ifthenor, sequenceoptions[i][k], v++);
			//std::cout << sequenceoptions[i][k] << " ==> ";

			for (unsigned int j=0; j<pb.num_classes; j++) {
				if (pb.classoption[j][k] == 1) {
					//std::cout << sequenceclass[i][j] << " " ;
					space.edge(ifthenor, sequenceclass[i][j], v++);
				}
			}
			//std::cout << std::endl;
		}
	}


    // ----------------------------------------------------
    // UNIQUE1 : One car per slot
    // ----------------------------------------------------
/*	unsigned int one = space._first_free_id++;
    space.vertice(one, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, Integer_variable_domain({1})); space.num_variables++;

    for (unsigned int i=0; i<pb.n; i++) {
        unsigned int xvar = sequenceclass[i][0];

        for(unsigned int j=1; j<pb.num_classes; j++) {
            unsigned int yvar = sequenceclass[i][j];

			unsigned int zvar;
			if (j == (pb.num_classes-1)) {
				zvar = one;
			} else {
				zvar = space._first_free_id++;
				space.vertice(zvar, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, Integer_variable_domain({0,1}) ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain()); space.num_relations++;
			space.edge(current_add, xvar,0);
			space.edge(current_add, yvar,1);
			space.edge(current_add, zvar,2);
			//std::cout << xvar << " + " << yvar << " = " << zvar << std::endl;
			xvar = zvar;
        }
    }
*/


    // ----------------------------------------------------
    // UNIQUE2 : AtLeast + AtMost
    // ----------------------------------------------------
	for (unsigned int i=0; i<pb.n; i++) {
		unsigned int atleast = space._first_free_id++;
		space.vertice(atleast, Flags(Flag::RELATION | Flag::AT_LEAST), N_STR, Integer_variable_domain()); space.num_relations++;

		unsigned int atmost = space._first_free_id++;
		space.vertice(atmost, Flags(Flag::RELATION | Flag::AT_MOST), N_STR, Integer_variable_domain()); space.num_relations++;

		//std::cout << "atmost " << atmost <<  ": " ;

		for (unsigned int j=0; j<pb.num_classes; j++) {
			space.edge(atleast, sequenceclass[i][j], -1);
			space.edge(atmost, sequenceclass[i][j], -1);
			//std::cout << sequenceclass[i][j] << " ";
		}
		//std::cout << std::endl;
	}


	// ----------------------------------------------------
	// CAPACITY-1 : ADDER(NotFactorized)
	// ----------------------------------------------------
/*	for(unsigned int j=0; j<pb.num_options; j++) {								// for all options
		for(unsigned int i=0; i<(pb.n-pb.blocksize[j]+1) ; i++) {				// for all vehicles
			unsigned int xvar = sequenceoptions[i][j];

			for (unsigned int s=(i+1); s<(i+pb.blocksize[j]); s++) {			//a series of sums 'x+y=z' across the block
				unsigned int yvar = sequenceoptions[s][j];

				Integer_variable_domain zdom;
				for (unsigned int d=0; d<=(pb.capacity[j]+1); d++)
					zdom.domain.push_back(d);

				unsigned int zvar = space._first_free_id++;
				space.vertice(zvar, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, zdom); space.num_variables++;

				unsigned int current_add = space._first_free_id++;
				space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain()); space.num_relations++;
				space.edge(current_add, xvar,0);
				space.edge(current_add, yvar,1);
				space.edge(current_add, zvar,2);
				//std::cout << xvar << " + " << yvar << " = " << zvar << std::endl;
				xvar = zvar;

				if (s == (i+pb.blocksize[j]-1)) {	// at the block's end
					// zvar <= capacity
					unsigned int lessthan = space._first_free_id++;
					space.vertice(lessthan, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, Integer_variable_domain()); space.num_relations++;
					space.edge(lessthan, zvar,0);
					space.edge(lessthan, capacity[j],1);
					//std::cout << zvar << " <= " << capacity[j] << std::endl;
				}

			}
		}
	}
	//std::cout << std::endl ;
*/

	// ----------------------------------------------------
	// CAPACITY-2 : ADDER-FACTORIZED (20181115: not tested yet)
	// ----------------------------------------------------
/*
	// 2a) auxiliary vars for addition
	std::vector< std::vector<unsigned int> > auxiliary_adds;
	for(unsigned int i=0; i<(pb.n-1) ; i++) {
		std::vector<unsigned int> auxadd;

		for(unsigned int k=0; k<pb.num_options ; k++) {
			unsigned int auxvar = space._first_free_id++;

			unsigned int addition = space._first_free_id++;
			space.vertice(addition, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain()); space.num_relations++;
			space.edge(addition, sequenceoptions[i][k],0);
			space.edge(addition, sequenceoptions[i+1][k],0);
			space.edge(addition, auxvar,0);

			auxadd.push_back(auxvar);
		}
		auxiliary_adds.push_back(auxadd);
	}

	// 2b) constraints
	for(unsigned int k=0; k<pb.num_options; k++) {					// options
		for(unsigned int i=0; i<(pb.n-pb.blocksize[k]+1) ; i++) {	// sequence

			if (pb.blocksize[k] == 2) {
//				unsigned int equality = space._first_free_id++;
//				space.vertice(equality, Flags(Flag::RELATION | Flag::EQUAL), N_STR, Integer_variable_domain()); space.num_relations++;
//				space.edge(equality, auxiliary_adds[i][j],-1);
//				space.edge(equality, capacity[j],-1);
				std::cout << auxiliary_adds[i][k] << " == " << capacity[k] << std::endl;
			} else {
				unsigned int xvar = auxiliary_adds[i][k];

				for (unsigned int w=(i+2); w<(i+pb.blocksize[k]); w+=2)
				{
					unsigned int yvar, zvar;
					if (w == (i+pb.blocksize[k]-1)) {
						yvar = sequenceoptions[w][k];
						zvar = capacity[k];
					}
					else if ((w+1) == (i+pb.blocksize[k]-1)) {
						yvar = auxiliary_adds[w][k];
						zvar = capacity[k];
					}
					else {

						Integer_variable_domain zdom;
						for (unsigned int d=0; d<=(pb.capacity[k]+1); d++)
							zdom.domain.push_back(d);

						yvar = auxiliary_adds[w][k];
						zvar = space._first_free_id++;
						space.vertice(zvar, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, zdom); space.num_variables++;
					}

//					unsigned int current_add = space._first_free_id++;
//					space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain()); space.num_relations++;
//					space.edge(current_add, xvar,0);
//					space.edge(current_add, yvar,1);
//					space.edge(current_add, zvar,2);
					std::cout << xvar << " + " << yvar << " = " << zvar << std::endl;
					xvar = zvar;
				}

			}
		}
	}
*/


	// ----------------------------------------------------
	// CAPACITY-3 : Capacity1 with SequentialCounter
	// ----------------------------------------------------
	for(unsigned int k=0; k<pb.num_options; k++) {								// for all options
		for(unsigned int i=0; i<(pb.n-pb.blocksize[k]+1) ; i++) {				// for all vehicles

			unsigned int card = space._first_free_id++;
			space.vertice(card, Flags(Flag::RELATION | Flag::CARDINALITY_LEQ), N_STR, Integer_variable_domain()); space.num_relations++;
			unsigned int j=0;
			space.edge(card, capacity[k], j++);

			for (unsigned int s=i; s<(i+pb.blocksize[k]); s++) {
				space.edge(card, sequenceoptions[s][k], j++);
			}
		}
	}


	// ----------------------------------------------------
	// SYM : Symmetry breakers
	// ----------------------------------------------------
	for (unsigned int i=1; i<pb.num_classes; i++)
	{
		unsigned int symbreaker = space._first_free_id++;
		space.vertice(symbreaker, Flags(Flag::RELATION | Flag::IFTHEN_GREATER), N_STR, Integer_variable_domain()); space.num_relations++;
		space.edge(symbreaker, sequenceclass[0][i], 0);
		//std::cout << sequenceclass[0][i] << " => " ;

		for (unsigned int j=0; j<i; j++) {
			space.edge(symbreaker, sequenceclass[(pb.n-1)][j], j+1);
			//std::cout << sequenceclass[(pb.n-1)][j] << " ";
		}
		//std::cout << std::endl;
	}


}



// ===========================================================================
// CARSEQUENCING (M1)
//
// ===========================================================================

void car_sequencing_m1(GRAPH& space, std::string instance)
{
    std::cout << "Car Seqencing M1: start" << std::endl;

	CarSequencingInstance pb(instance);
	// TEST -------------------------------------
	std::cout << "Instance" << std::endl;
	std::cout << pb.n << " " << pb.num_options << " " << pb.num_classes << std::endl;
	for (unsigned int i=0; i<pb.num_options; i++)
		std::cout << pb.capacity[i] << " " ;
	std::cout << std::endl;
	for (unsigned int i=0; i<pb.num_options; i++)
		std::cout << pb.blocksize[i] << " " ;
	std::cout << std::endl;
	for (unsigned int i=0; i<pb.num_classes; i++) {
		std::cout << i << " " << pb.demand[i] << " ";
		for (unsigned int j=0; j<pb.num_options; j++)
			std::cout << pb.classoption[i][j] << " ";
		std::cout << std::endl;
	}
	std::cout << std::endl;


    std::vector<unsigned int>                capacity, blocksize, demand; //, classes, options;
    std::vector< std::vector<unsigned int> > sequenceclass, sequenceoptions, classoptions;

	// SequenceClass, SequenceOptions ---------------------
	for (unsigned int i=0; i<pb.n; i++) {
		std::vector<unsigned int> srow;
		for (unsigned int j=0; j<pb.num_classes; j++) {
			unsigned int var = space._first_free_id++;
			space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, Integer_variable_domain({0,1})); space.num_variables++;
			srow.push_back(var);
		}
		sequenceclass.push_back(srow);
	}
	for (unsigned int i=0; i<pb.n; i++) {
		std::vector<unsigned int> srow;
		for (unsigned int j=0; j<pb.num_options; j++) {
			unsigned int var = space._first_free_id++;
			space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, Integer_variable_domain({0,1})); space.num_variables++;
			srow.push_back(var);
		}
		sequenceoptions.push_back(srow);
	}

	// Capacities, Demand, Classes, Options & ClassOptions ------------------
	for (unsigned int i=0; i<pb.num_options; i++) {
		unsigned int var = space._first_free_id++;
		// +1 for the LESS_THAN constraint
		//space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, Integer_variable_domain({static_cast <int>(pb.capacity[i]+1)})); space.num_variables++;
		space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, Integer_variable_domain({static_cast <int>(pb.capacity[i])})); space.num_variables++;
		capacity.push_back(var);
	}
	for (unsigned int i=0; i<pb.num_classes; i++) {
		unsigned int var = space._first_free_id++;
		space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, Integer_variable_domain({static_cast <int>(pb.demand[i])})); space.num_variables++;
		demand.push_back(var);
	}
	for (unsigned int i=0; i<pb.num_classes; i++) {
		std::vector<unsigned int> co_row;
		for (unsigned int j=0; j<pb.num_options; j++) {
			unsigned int var = space._first_free_id++;
			space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, Integer_variable_domain({static_cast <int>(pb.classoption[i][j])})); space.num_variables++;
			co_row.push_back(var);
		}
		classoptions.push_back(co_row);
	}


	// TEST -------------------------------------
	std::cout << "SequenceClass : " << std::endl ;
	for (unsigned int i=0; i<pb.n; i++) {
		for(unsigned int j=0; j<pb.num_classes; j++) {
            std::cout << sequenceclass[i][j] << " " ;
		}
		std::cout << std::endl ;
    }
	std::cout << std::endl ;
    std::cout << "SequenceOptions : " << std::endl ;
	for (unsigned int i=0; i<pb.n; i++) {
		for(unsigned int k=0; k<pb.num_options; k++) {
            std::cout << sequenceoptions[i][k] << " " ;
		}
		std::cout << std::endl ;
    }
    std::cout << std::endl ;
	std::cout << "Capacity : " << std::endl ;
	for(unsigned int j=0; j<pb.num_options; j++) {
		std::cout << capacity[j] << " " ;
    }
	std::cout << std::endl << std::endl;
	std::cout << "Demand : " << std::endl ;
	for(unsigned int j=0; j<pb.num_classes; j++) {
		std::cout << demand[j] << " " ;
    }
	std::cout << std::endl << std::endl;
    std::cout << "ClassOptions : " << std::endl ;
	for(unsigned int j=0; j<pb.num_classes; j++) {
        for(unsigned int k=0; k<pb.num_options; k++) {
            std::cout << classoptions[j][k] << " " ;
		}
		std::cout << std::endl ;
    }
    std::cout << std::endl;


	// ----------------------------------------------------
	// DEMAND-1
	// ----------------------------------------------------
/*	for(unsigned int j=0; j<pb.num_classes; j++) {
		unsigned int xvar = sequenceclass[0][j];

		for(unsigned int i=1; i<pb.n; i++) {
			unsigned int yvar = sequenceclass[i][j];

			unsigned int zvar;
			if (i == (pb.n-1)) {
				zvar = demand[j];
			} else {

				Integer_variable_domain zdom;
				for (unsigned int d=0; d<=pb.demand[j]; d++)
					zdom.domain.push_back(d);

				zvar = space._first_free_id++;
				space.vertice(zvar, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, zdom ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain()); space.num_relations++;
			space.edge(current_add, xvar,0);
			space.edge(current_add, yvar,1);
			space.edge(current_add, zvar,2);
			//std::cout << xvar << " + " << yvar << " = " << zvar << std::endl;
			xvar = zvar;
		}
	}
	//std::cout << std::endl;
*/

	// ----------------------------------------------------
	// DEMAND-2
	// ----------------------------------------------------
/*	//std::vector<unsigned int> optiondemand;

	for(unsigned int k=0; k<pb.num_options; k++) {

		// identify the classes requiring the option-k
		unsigned int max_demand = 0;				// the value
		std::vector<unsigned int> optionclasses;	// the variables involved

		//std::cout << "option " << k << ": ";

		for(unsigned int j=0; j<pb.num_classes; j++) {
			if (pb.classoption[j][k] == 1) {
				optionclasses.push_back(demand[j]);
				max_demand += pb.demand[j];
				//std::cout << demand[j] << " ";
			}
		}

		// store the maximum demand
		unsigned int max_demand_var = space._first_free_id++;
		space.vertice(max_demand_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, Integer_variable_domain({static_cast <int>(max_demand)})); space.num_variables++;
		//optiondemand.push_back(max_demand_var);

		//std::cout << "\t(" << max_demand << " - " << max_demand_var << ")"<< std::endl;

		// sum-up the class demands
		unsigned int xvar = optionclasses[0];

		for(unsigned int i=1; i<optionclasses.size(); i++) {
			unsigned int yvar = optionclasses[i];

			unsigned int zvar;
			if (i == (optionclasses.size()-1)) {
				zvar = max_demand_var;
			} else {

				Integer_variable_domain zdom;
				for (unsigned int d=0; d<=max_demand; d++)
					zdom.domain.push_back(d);

				zvar = space._first_free_id++;
				space.vertice(zvar, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, zdom ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain()); space.num_relations++;
			space.edge(current_add, xvar,0);
			space.edge(current_add, yvar,1);
			space.edge(current_add, zvar,2);
			//std::cout << xvar << " + " << yvar << " = " << zvar << std::endl;
			xvar = zvar;
		}

		std::cout << std::endl;

		// sum-up across the SequenceOption
		xvar = sequenceoptions[0][k];

		for(unsigned int i=1; i<pb.n; i++) {
			unsigned int yvar = sequenceoptions[i][k];

			unsigned int zvar;
			if (i == (pb.n-1)) {
				// variable to store the demands' sum
				zvar = max_demand_var;
			} else {

				Integer_variable_domain zdom;
				for (unsigned int d=0; d<=(i+1); d++)
					zdom.domain.push_back(d);

				zvar = space._first_free_id++;
				space.vertice(zvar, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, zdom ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain()); space.num_relations++;
			space.edge(current_add, xvar,0);
			space.edge(current_add, yvar,1);
			space.edge(current_add, zvar,2);
			//std::cout << xvar << " + " << yvar << " = " << zvar << std::endl;
			xvar = zvar;
		}
	}
*/


	// ----------------------------------------------------
	// DEMAND-3 (Demand1 written with SequenceCounter)
	// ----------------------------------------------------
	for(unsigned int j=0; j<pb.num_classes; j++) {
		unsigned int card = space._first_free_id++;
		space.vertice(card, Flags(Flag::RELATION | Flag::CARDINALITY), N_STR, Integer_variable_domain()); space.num_relations++;
		space.edge(card, demand[j], 0);
		//std::cout << "cardinality " << card <<  ": " << demand[j] << " " ;

		for(unsigned int i=0; i<pb.n; i++) {
			space.edge(card, sequenceclass[i][j], i+1);
			//std::cout << sequenceclass[i][j] << " ";
		}
		//std::cout << std::endl;
	}
	//std::cout << std::endl;



	// ----------------------------------------------------
	// DEMAND4 : Demand2 with SequenceCounter
	// ----------------------------------------------------

	for(unsigned int k=0; k<pb.num_options; k++) {
		// identify the classes requiring the option-k
		unsigned int total_demand = 0;				// the value

		for(unsigned int j=0; j<pb.num_classes; j++) {
			if (pb.classoption[j][k] == 1) {
				total_demand += pb.demand[j];
			}
		}

		// store the total demand (per option)
		unsigned int max_demand_var = space._first_free_id++;
		space.vertice(max_demand_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, Integer_variable_domain({static_cast <int>(total_demand)})); space.num_variables++;

		// cardinality over options' usage
		unsigned int card = space._first_free_id++;
		space.vertice(card, Flags(Flag::RELATION | Flag::CARDINALITY), N_STR, Integer_variable_domain()); space.num_relations++;
		space.edge(card, max_demand_var, 0);

		for(unsigned int i=0; i<pb.n; i++) {
			space.edge(card, sequenceoptions[i][k], i+1);
		}
	}



	// ----------------------------------------------------
	// LINK1 : Sequence to stations usage
	// IF SequenceClass[i,j] THEN SequenceOptions[i,k] = ClassOptions[j,k]
	// ----------------------------------------------------

	for (unsigned int i=0; i<pb.n; i++) {
		for(unsigned int j=0; j<pb.num_classes; j++) {
			for(unsigned int k=0; k<pb.num_options; k++) {

				unsigned int ifthen = space._first_free_id++;
				space.vertice(ifthen, Flags(Flag::RELATION | Flag::IF_THEN), N_STR, Integer_variable_domain()); space.num_relations++;
				space.edge(ifthen, sequenceclass[i][j], 0);
				space.edge(ifthen, sequenceoptions[i][k], 1);
				space.edge(ifthen, classoptions[j][k], 2);
				//std::cout << sequenceclass[i][j] << " => (" << sequenceoptions[i][k] << " = " << classoptions[j][k] << ")" << "   ";
			}
			//std::cout << std::endl;
		}
	}


	// ----------------------------------------------------
	// LINK2 : options => OR(classes..)
	// ----------------------------------------------------
	for (unsigned int i=0; i<pb.n; i++) {
		for (unsigned int k=0; k<pb.num_options; k++) {

			unsigned int ifthenor = space._first_free_id++;
			space.vertice(ifthenor, Flags(Flag::RELATION | Flag::IF_THEN_OR), N_STR, Integer_variable_domain()); space.num_relations++;
			unsigned int v = 0;
			space.edge(ifthenor, sequenceoptions[i][k], v++);
			//std::cout << sequenceoptions[i][k] << " ==> ";

			for (unsigned int j=0; j<pb.num_classes; j++) {
				if (pb.classoption[j][k] == 1) {
					//std::cout << sequenceclass[i][j] << " " ;
					space.edge(ifthenor, sequenceclass[i][j], v++);
				}
			}
			//std::cout << std::endl;
		}
	}


    // ----------------------------------------------------
    // UNIQUE1 : One car per slot
    // ----------------------------------------------------
/*	unsigned int one = space._first_free_id++;
    space.vertice(one, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, Integer_variable_domain({1})); space.num_variables++;

    for (unsigned int i=0; i<pb.n; i++) {
        unsigned int xvar = sequenceclass[i][0];

        for(unsigned int j=1; j<pb.num_classes; j++) {
            unsigned int yvar = sequenceclass[i][j];

			unsigned int zvar;
			if (j == (pb.num_classes-1)) {
				zvar = one;
			} else {
				zvar = space._first_free_id++;
				space.vertice(zvar, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, Integer_variable_domain({0,1}) ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain()); space.num_relations++;
			space.edge(current_add, xvar,0);
			space.edge(current_add, yvar,1);
			space.edge(current_add, zvar,2);
			//std::cout << xvar << " + " << yvar << " = " << zvar << std::endl;
			xvar = zvar;
        }
    }
*/


    // ----------------------------------------------------
    // UNIQUE2 : AtLeast + AtMost
    // ----------------------------------------------------
	for (unsigned int i=0; i<pb.n; i++) {
		unsigned int atleast = space._first_free_id++;
		space.vertice(atleast, Flags(Flag::RELATION | Flag::AT_LEAST), N_STR, Integer_variable_domain()); space.num_relations++;

		unsigned int atmost = space._first_free_id++;
		space.vertice(atmost, Flags(Flag::RELATION | Flag::AT_MOST), N_STR, Integer_variable_domain()); space.num_relations++;

		//std::cout << "atmost " << atmost <<  ": " ;

		for (unsigned int j=0; j<pb.num_classes; j++) {
			space.edge(atleast, sequenceclass[i][j], -1);
			space.edge(atmost, sequenceclass[i][j], -1);
			//std::cout << sequenceclass[i][j] << " ";
		}
		//std::cout << std::endl;
	}


	// ----------------------------------------------------
	// CAPACITY-1 : ADDER(NotFactorized)
	// ----------------------------------------------------
/*	for(unsigned int j=0; j<pb.num_options; j++) {								// for all options
		for(unsigned int i=0; i<(pb.n-pb.blocksize[j]+1) ; i++) {				// for all vehicles
			unsigned int xvar = sequenceoptions[i][j];

			for (unsigned int s=(i+1); s<(i+pb.blocksize[j]); s++) {			//a series of sums 'x+y=z' across the block
				unsigned int yvar = sequenceoptions[s][j];

				Integer_variable_domain zdom;
				for (unsigned int d=0; d<=(pb.capacity[j]+1); d++)
					zdom.domain.push_back(d);

				unsigned int zvar = space._first_free_id++;
				space.vertice(zvar, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, zdom); space.num_variables++;

				unsigned int current_add = space._first_free_id++;
				space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain()); space.num_relations++;
				space.edge(current_add, xvar,0);
				space.edge(current_add, yvar,1);
				space.edge(current_add, zvar,2);
				//std::cout << xvar << " + " << yvar << " = " << zvar << std::endl;
				xvar = zvar;

				if (s == (i+pb.blocksize[j]-1)) {	// at the block's end
					// zvar <= capacity
					unsigned int lessthan = space._first_free_id++;
					space.vertice(lessthan, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, Integer_variable_domain()); space.num_relations++;
					space.edge(lessthan, zvar,0);
					space.edge(lessthan, capacity[j],1);
					//std::cout << zvar << " <= " << capacity[j] << std::endl;
				}

			}
		}
	}
	//std::cout << std::endl ;
*/

	// ----------------------------------------------------
	// CAPACITY-2 : ADDER-FACTORIZED (20181115: not tested yet)
	// ----------------------------------------------------
/*
	// 2a) auxiliary vars for addition
	std::vector< std::vector<unsigned int> > auxiliary_adds;
	for(unsigned int i=0; i<(pb.n-1) ; i++) {
		std::vector<unsigned int> auxadd;

		for(unsigned int k=0; k<pb.num_options ; k++) {
			unsigned int auxvar = space._first_free_id++;

			unsigned int addition = space._first_free_id++;
			space.vertice(addition, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain()); space.num_relations++;
			space.edge(addition, sequenceoptions[i][k],0);
			space.edge(addition, sequenceoptions[i+1][k],0);
			space.edge(addition, auxvar,0);

			auxadd.push_back(auxvar);
		}
		auxiliary_adds.push_back(auxadd);
	}

	// 2b) constraints
	for(unsigned int k=0; k<pb.num_options; k++) {					// options
		for(unsigned int i=0; i<(pb.n-pb.blocksize[k]+1) ; i++) {	// sequence

			if (pb.blocksize[k] == 2) {
//				unsigned int equality = space._first_free_id++;
//				space.vertice(equality, Flags(Flag::RELATION | Flag::EQUAL), N_STR, Integer_variable_domain()); space.num_relations++;
//				space.edge(equality, auxiliary_adds[i][j],-1);
//				space.edge(equality, capacity[j],-1);
				std::cout << auxiliary_adds[i][k] << " == " << capacity[k] << std::endl;
			} else {
				unsigned int xvar = auxiliary_adds[i][k];

				for (unsigned int w=(i+2); w<(i+pb.blocksize[k]); w+=2)
				{
					unsigned int yvar, zvar;
					if (w == (i+pb.blocksize[k]-1)) {
						yvar = sequenceoptions[w][k];
						zvar = capacity[k];
					}
					else if ((w+1) == (i+pb.blocksize[k]-1)) {
						yvar = auxiliary_adds[w][k];
						zvar = capacity[k];
					}
					else {

						Integer_variable_domain zdom;
						for (unsigned int d=0; d<=(pb.capacity[k]+1); d++)
							zdom.domain.push_back(d);

						yvar = auxiliary_adds[w][k];
						zvar = space._first_free_id++;
						space.vertice(zvar, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, zdom); space.num_variables++;
					}

//					unsigned int current_add = space._first_free_id++;
//					space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, Integer_variable_domain()); space.num_relations++;
//					space.edge(current_add, xvar,0);
//					space.edge(current_add, yvar,1);
//					space.edge(current_add, zvar,2);
					std::cout << xvar << " + " << yvar << " = " << zvar << std::endl;
					xvar = zvar;
				}

			}
		}
	}
*/


	// ----------------------------------------------------
	// CAPACITY-3 : Capacity1 with SequentialCounter
	// ----------------------------------------------------
	for(unsigned int k=0; k<pb.num_options; k++) {								// for all options
		for(unsigned int i=0; i<(pb.n-pb.blocksize[k]+1) ; i++) {				// for all vehicles

			unsigned int card = space._first_free_id++;
			space.vertice(card, Flags(Flag::RELATION | Flag::CARDINALITY_LEQ), N_STR, Integer_variable_domain()); space.num_relations++;
			unsigned int j=0;
			space.edge(card, capacity[k], j++);

			for (unsigned int s=i; s<(i+pb.blocksize[k]); s++) {
				space.edge(card, sequenceoptions[s][k], j++);
			}
		}
	}


	// ----------------------------------------------------
	// SYM : Symmetry breakers
	// ----------------------------------------------------
	for (unsigned int i=1; i<pb.num_classes; i++)
	{
		unsigned int symbreaker = space._first_free_id++;
		space.vertice(symbreaker, Flags(Flag::RELATION | Flag::IFTHEN_GREATER), N_STR, Integer_variable_domain()); space.num_relations++;
		space.edge(symbreaker, sequenceclass[0][i], 0);
		//std::cout << sequenceclass[0][i] << " => " ;

		for (unsigned int j=0; j<i; j++) {
			space.edge(symbreaker, sequenceclass[(pb.n-1)][j], j+1);
			//std::cout << sequenceclass[(pb.n-1)][j] << " ";
		}
		//std::cout << std::endl;
	}


}





