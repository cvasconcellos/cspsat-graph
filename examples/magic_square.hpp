/*
 *
 * MAGIC SQUARE implementations
 *
 *
 * m0: generate_magic_square_problem()	MagicSquare v1
 * m1: run_magic_square_propagated()	MS propagated, PROP_INTEGER + PROP_ADD_EQUAL + SymmetryBreakers
 * m2: run_magicsquare_v2()				MS with LINEAR_EQ_SEQ (Sequential Linear constraint)
 *
 * generate_bimagic_square_problem()	BimagicSquare v1
 *
 */


#include <cmath>

#define N_STR std::string()



// ==================================================================
// MAGIC SQUARE v1
// ==================================================================

void generate_magic_square_problem(GRAPH& space, unsigned int pb_size, unsigned int domain_size, bool consecutive=false)
{
    time_t my_time = time(NULL);
	std::cout << "ms_model()  : start\t\t" << ctime(&my_time);

	// model -------------------------
	space.encoding_sequence.push_back(Flag::LOG_ENCODING);

	IntVarDomain domain, aux_domain;
	unsigned int max_aux_value, old_max_aux_value,  real_max_aux_value;

	if (consecutive)
	    domain_size = pb_size*pb_size;
	for (unsigned int i=1; i <= domain_size; ++i)
		domain.domain.push_back(i);

	// On cherche la première puissance de 2 qui puisse contenir le domain aux
	std::vector< std::vector< unsigned int > > matrix;

	// Sum var
	unsigned int sum_var = space._first_free_id++;
	if (consecutive) {
		space.vertice(sum_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({ static_cast<int>(pb_size*(pb_size*pb_size+1)/2) }));
		space.num_variables++;

	} else {
		max_aux_value = 1;
		while (max_aux_value <= (pb_size*domain_size)-(pb_size-1)*(pb_size)/2)
			max_aux_value *=2;
		for (unsigned int i=1; i < max_aux_value; ++i)
			aux_domain.domain.push_back(i);

		//CVG: space.vertice(sum_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain);
		space.vertice(sum_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, aux_domain); space.num_variables++;
	}

	// Problem
	// Variables creation + sum for lines
	for (unsigned int i = 0; i < pb_size; ++i)
	{
		std::vector< unsigned int > matrix_line;

		// Line sum
		//std::cerr << "Sum for lines" << std::endl;
		unsigned int previous_var = space._first_free_id++;

		//SOL
		//std::cerr << "val= " << sol[idx_sol] << std::endl;
		//space.vertice(previous_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({ sol[idx_sol++] })); space.num_variables++;
		space.vertice(previous_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain); space.num_variables++;
		matrix_line.push_back(previous_var);

		max_aux_value = 1;
		real_max_aux_value = domain_size;
		while (max_aux_value <= domain_size)
		      max_aux_value *=2;
		aux_domain.domain.clear();
		for (unsigned int i=1; i < max_aux_value; ++i)
		      aux_domain.domain.push_back(i);

		for (unsigned int j=1; j<pb_size; ++j)
		{
			unsigned int current_var = space._first_free_id++;
			space.vertice(current_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain); space.num_variables++;
			matrix_line.push_back(current_var);

			unsigned int next_var;
			if (j == (pb_size-1))
			{
				next_var = sum_var;
			} else {
				old_max_aux_value = max_aux_value;
				real_max_aux_value += domain_size;
				while (max_aux_value <= real_max_aux_value)
				      max_aux_value *=2;
				for (unsigned int i=old_max_aux_value; i < max_aux_value; ++i)
				      aux_domain.domain.push_back(i);
				next_var = space._first_free_id++;
				//CVG: space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain );
				space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, aux_domain ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(current_add,previous_var,0);
			space.edge(current_add,current_var,1);
			space.edge(current_add,next_var,2);
			previous_var = next_var;
		}
		matrix.push_back(matrix_line);
 	}

	// Sum for columns
	for (unsigned int j=0; j<pb_size; ++j)
	{
		max_aux_value = 1;
		real_max_aux_value = domain_size;
		while (max_aux_value <= domain_size)
			max_aux_value *=2;
		aux_domain.domain.clear();
		for (unsigned int i=1; i < max_aux_value; ++i)
		      aux_domain.domain.push_back(i);
		// Column sum
		unsigned int previous_var = matrix[0][j];
		for (unsigned int i = 1; i < pb_size; ++i)
		{
			unsigned int current_var = matrix[i][j];

			unsigned int next_var;
			if (i == (pb_size-1))
			{
				next_var = sum_var;
			} else {
				old_max_aux_value   = max_aux_value;
				real_max_aux_value += domain_size;
				while (max_aux_value <= real_max_aux_value)
				      max_aux_value *=2;
				for (unsigned int i=old_max_aux_value; i < max_aux_value; ++i)
				      aux_domain.domain.push_back(i);
				next_var = space._first_free_id++;
				space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain );
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(current_add,previous_var,0);
			space.edge(current_add,current_var,1);
			space.edge(current_add,next_var,2);

			previous_var = next_var;
		}
	}

	// Sum for diags
	{
		max_aux_value = 1;
		real_max_aux_value = domain_size;
		while (max_aux_value <= domain_size)
		      max_aux_value *=2;
		aux_domain.domain.clear();
		for (unsigned int i=1; i < max_aux_value; ++i)
		      aux_domain.domain.push_back(i);
		unsigned int diag1_previous_var = matrix[0][0];
		unsigned int diag2_previous_var = matrix[0][pb_size-1];
		for (unsigned int i = 1; i < pb_size; ++i)
		{
			// Diag1
			unsigned int diag1_current_var = matrix[i][i];
			unsigned int diag1_next_var;
			if (i == (pb_size-1))
			{
				diag1_next_var = sum_var;
			} else {
				old_max_aux_value = max_aux_value;
				real_max_aux_value += domain_size;
				while (max_aux_value <= real_max_aux_value)
				      max_aux_value *=2;
				for (unsigned int i=old_max_aux_value; i < max_aux_value; ++i)
				      aux_domain.domain.push_back(i);
				diag1_next_var = space._first_free_id++;
				space.vertice(diag1_next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain );
			}
			unsigned int diag1_current_add = space._first_free_id++;
			space.vertice(diag1_current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(diag1_current_add,diag1_previous_var,0);
			space.edge(diag1_current_add,diag1_current_var,1);
			space.edge(diag1_current_add,diag1_next_var,2);
			diag1_previous_var = diag1_next_var;

			// Diag2
			unsigned int diag2_current_var = matrix[i][pb_size-1-i];
			unsigned int diag2_next_var;
			if (i == (pb_size-1)) {
				diag2_next_var = sum_var;
			} else {
				diag2_next_var = space._first_free_id++;
				space.vertice(diag2_next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain ); space.num_variables++;
			}
			unsigned int diag2_current_add = space._first_free_id++;
			space.vertice(diag2_current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(diag2_current_add,diag2_previous_var,0);
			space.edge(diag2_current_add,diag2_current_var,1);
			space.edge(diag2_current_add,diag2_next_var,2);
			diag2_previous_var = diag2_next_var;
		}
	}

	// alldiff
	unsigned int alldiff = space._first_free_id++;
	space.vertice(alldiff, Flags(Flag::RELATION | Flag::ALL_DIFFERENT), N_STR, IntVarDomain() ); space.num_relations++;
	for (unsigned int i = 0; i < pb_size; ++i)
		for (unsigned int j = 0; j < pb_size; ++j)
			space.edge(alldiff,matrix[i][j],-1);

    my_time = time(NULL);
	std::cout << "ms_model()  : end\t\t" << ctime(&my_time);
}




// ===========================================================================
// MAGIC SQUARE 4 PROPAGATED
// ===========================================================================

void run_magic_square_propagated(GRAPH& space, unsigned int N)
{
    time_t my_time = time(NULL);
	std::cout << "magic_square(): " << ctime(&my_time) << std::endl;

	// magic constant -------------------------------------

	const unsigned int M = ((N*N*N)+N)/2;
	unsigned int magic_var = space._first_free_id++;
    space.vertice(magic_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({ static_cast<int>(M) })); space.num_variables++;
	std::cout << "N: " << N << std::endl;
	std::cout << "M: " << M << std::endl;

    // domains ------------------------------------------------------
	IntVarDomain var_domain;		// variables
	for (unsigned int i=1; i<=(N*N); i++)
		var_domain.domain.push_back(i);

    IntVarDomain max_aux_domain;	// auxiliary sum   auxsum_domain
    for (unsigned int i=3; i<=M; i++)
        max_aux_domain.domain.push_back(i);
    std::cout << "max_aux_domain: [" << max_aux_domain.domain.front() << "," << max_aux_domain.domain.back() << "]\n";


    // variables ----------------------------------------------------
    std::cout << "variables: " << std::endl;
	std::vector< std::vector<unsigned int> > matrix;

	for (unsigned int i=0; i<N; i++)
	{
        std::vector<unsigned int> matrix_row;
        for (unsigned int j=0; j<N; j++) {
            unsigned int var = space._first_free_id++;
            space.vertice(var, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, var_domain); space.num_variables++;
            matrix_row.push_back(var);
            std::cout << var << " ";
        }
        matrix.push_back(matrix_row);
        std::cout << std::endl;
    }

/*
	// solution -----------------------------------------------------
	std::vector< std::vector<unsigned int> > solution;
    solution = {{8,11,14,1},{13,2,7,12},{3,16,9,6},{10,5,4,15}};

	for (unsigned int i=0; i<N; i++)
	{
		std::vector<unsigned int> matrix_row;
		for (unsigned int j=0; j<N; j++) {
			unsigned int var = space._first_free_id++;
			if ((i*4+j) < 4) {	// variables: 0,1,2,3
				space.vertice(var, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, var_domain); space.num_variables++;
			} else {
				space.vertice(var, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, IntVarDomain({ static_cast<int>(solution[i][j]) })); space.num_variables++;
			}
			matrix_row.push_back(var);
			std::cout << var << " ";
		}
        matrix.push_back(matrix_row);
        std::cout << std::endl;
    }
*/
	std::cout << std::endl;

    // ROWS ---------------------------------------------------------

	// arborescent decomposition
	for (unsigned int i=0; i<N; i++)
	{
		// all vars to queue
		std::queue<int> varqueue;
		for (unsigned int j=0; j<N; j++) {
			varqueue.push(matrix[i][j]);
		}

		// pop and sum until length<=2
		while (varqueue.size() > 2) {
			unsigned int x1 = varqueue.front(); varqueue.pop();
			unsigned int x2 = varqueue.front(); varqueue.pop();

			// 20190705 : new auxiliary domain
			chr::Logical_var<Flags>       pflags;
			chr::Logical_var<std::string> plabel;
			chr::Logical_var<Domain>      pdomain;
			space.get_vertice(x1, pflags, plabel, pdomain);
			int x1_dmin = (*pdomain).min();
			int x1_dmax = (*pdomain).max();

			chr::Logical_var<Flags>       cflags;
			chr::Logical_var<std::string> clabel;
			chr::Logical_var<Domain>      cdomain;
			space.get_vertice(x2, cflags, clabel, cdomain);
			int x2_dmin = (*cdomain).min();
			int x2_dmax = (*cdomain).max();

			int dmin = std::max((x1_dmin+x2_dmin), max_aux_domain.domain.front());
			int dmax = std::min((x1_dmax+x2_dmax), max_aux_domain.domain.back());

			std::cout << "[" <<  x1_dmin << "," << x2_dmax << "] + ";
			std::cout << "[" <<  x2_dmin << "," << x2_dmax << "] = ";
			std::cout << "[" <<  dmin << "," << dmax << "] \t ";

			IntVarDomain new_aux_domain;
			for (int k=dmin; k<=dmax; k++)
				new_aux_domain.domain.push_back(k);


			unsigned int aux = space._first_free_id++;
			unsigned int auxsum = space._first_free_id++;

			// 20190705: Selective propagation
			if ( (x1 <= N*N) && (x2 <= N*N)) {
                space.vertice(aux, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, new_aux_domain); space.num_variables++;
                space.vertice(auxsum, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			} else {
                space.vertice(aux, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, new_aux_domain); space.num_variables++;
                space.vertice(auxsum, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
            }

//			// 20190711: Selective propagation v2 (only addends at last sum)
//			if (varqueue.size() == 1 || varqueue.size() == 2) {
//				space.vertice(aux, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, new_aux_domain); space.num_variables++;
//			} else {
//				space.vertice(aux, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, new_aux_domain); space.num_variables++;
//			}
//			space.vertice(auxsum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;

			space.edge(auxsum, x1, 0);
			space.edge(auxsum, x2, 1);
			space.edge(auxsum, aux, 2);
			std::cout << auxsum << ": " << x1 << " + " << x2 << " = " << aux << std::endl;

			varqueue.push(aux);
		}

		// final sum
		unsigned int x1 = varqueue.front(); varqueue.pop();
		unsigned int x2 = varqueue.front(); varqueue.pop();
		unsigned int auxsum = space._first_free_id++;
        space.vertice(auxsum, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
        space.edge(auxsum, x1, 0);
        space.edge(auxsum, x2, 1);
        space.edge(auxsum, magic_var,  2);
        std::cout << auxsum << ": " << x1 << " + " << x2 << " = " << magic_var << std::endl;
	}
/*
	// sequential sum decomposition
	for (unsigned int i=0; i<N; ++i)
	{
		unsigned int previous_var = matrix[i][0];
		for (unsigned int j=1; j<N; ++j)
		{
			unsigned int current_var = matrix[i][j];
			unsigned int next_var;
			if (j == (N-1)){
				next_var = magic_var;
			} else {
				next_var = space._first_free_id++;

				// 20190705 : new auxiliary domain
                chr::Logical_var<Flags>       pflags;
                chr::Logical_var<std::string> plabel;
                chr::Logical_var<Domain>      pdomain;
                space.get_vertice(previous_var, pflags, plabel, pdomain);
                int previous_dmin = (*pdomain).min();
                int previous_dmax = (*pdomain).max();

                chr::Logical_var<Flags>       cflags;
                chr::Logical_var<std::string> clabel;
                chr::Logical_var<Domain>      cdomain;
                space.get_vertice(current_var, cflags, clabel, cdomain);
                int current_dmin = (*cdomain).min();
                int current_dmax = (*cdomain).max();

                int dmin = std::max((previous_dmin+current_dmin), max_aux_domain.domain.front());
				int dmax = std::min((previous_dmax+current_dmax), max_aux_domain.domain.back());

                std::cout << "[" <<  previous_dmin << "," << previous_dmax << "] + ";
                std::cout << "[" <<  current_dmin << "," << current_dmax << "] = ";
                std::cout << "[" <<  dmin << "," << dmax << "] \t ";

                IntVarDomain new_aux_domain;
                for (int k=dmin; k<=dmax; k++)
                    new_aux_domain.domain.push_back(k);

				space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, new_aux_domain ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(current_add, previous_var, 0);
			space.edge(current_add, current_var,  1);
			space.edge(current_add, next_var,     2);
			std::cout << current_add << ": " << previous_var << " + " << current_var << " = " << next_var << std::endl;

			previous_var = next_var;
		}
	}

	std::cout << std::endl;
*/

    // COLUMNS ------------------------------------------------------

	// arborescent decomposition
	for (unsigned int j=0; j<N; j++)
	{
		// enqueue vars
		std::queue<int> varqueue;
		for (unsigned int i=0; i<N; i++) { varqueue.push(matrix[i][j]); }

		// pop and sum until length<=2
		while (varqueue.size() > 2) {
			unsigned int x1 = varqueue.front(); varqueue.pop();
			unsigned int x2 = varqueue.front(); varqueue.pop();

			// 20190705 : new auxiliary domain
			chr::Logical_var<Flags>       pflags;
			chr::Logical_var<std::string> plabel;
			chr::Logical_var<Domain>      pdomain;
			space.get_vertice(x1, pflags, plabel, pdomain);
			int x1_dmin = (*pdomain).min();
			int x1_dmax = (*pdomain).max();

			chr::Logical_var<Flags>       cflags;
			chr::Logical_var<std::string> clabel;
			chr::Logical_var<Domain>      cdomain;
			space.get_vertice(x2, cflags, clabel, cdomain);
			int x2_dmin = (*cdomain).min();
			int x2_dmax = (*cdomain).max();

			int dmin = std::max((x1_dmin+x2_dmin), max_aux_domain.domain.front());
			int dmax = std::min((x1_dmax+x2_dmax), max_aux_domain.domain.back());

			std::cout << "[" <<  x1_dmin << "," << x2_dmax << "] + ";
			std::cout << "[" <<  x2_dmin << "," << x2_dmax << "] = ";
			std::cout << "[" <<  dmin << "," << dmax << "] \t ";

			IntVarDomain new_aux_domain;
			for (int k=dmin; k<=dmax; k++)
				new_aux_domain.domain.push_back(k);

			unsigned int aux= space._first_free_id++;
			unsigned int auxsum = space._first_free_id++;
			space.vertice(aux, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, new_aux_domain); space.num_variables++;
			space.vertice(auxsum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;

//			// 20190705: Propagate only if the sum involves two DECISION VARS
//			if ( (x1 <= N*N) && (x2 <= N*N)) {
//                space.vertice(aux, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, new_aux_domain); space.num_variables++;
//                space.vertice(auxsum, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
//			} else {
//                space.vertice(aux, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, new_aux_domain); space.num_variables++;
//                space.vertice(auxsum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
//            }
			space.edge(auxsum, x1, 0);
			space.edge(auxsum, x2, 1);
			space.edge(auxsum, aux, 2);
			std::cout << auxsum << ": " << x1 << " + " << x2 << " = " << aux << std::endl;

			varqueue.push(aux);
		}

		// final sum
		unsigned int x1 = varqueue.front(); varqueue.pop();
		unsigned int x2 = varqueue.front(); varqueue.pop();

		unsigned int auxsum = space._first_free_id++;
        space.vertice(auxsum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
        space.edge(auxsum, x1, 0);
        space.edge(auxsum, x2, 1);
        space.edge(auxsum, magic_var, 2);
        std::cout << auxsum << ": " << x1 << " + " << x2 << " = " << magic_var << std::endl;
	}

/*	// sequential sum decomposition
	for (unsigned int j=0; j<N; ++j)
	{
		unsigned int previous_var = matrix[0][j];
		for (unsigned int i=1; i<N; ++i)
		{
			unsigned int current_var = matrix[i][j];
			unsigned int next_var;
			if (i == (N-1)){
				next_var = magic_var;
			} else {
				next_var = space._first_free_id++;

				// 20190705 : new auxiliary domain
                chr::Logical_var<Flags>       pflags;
                chr::Logical_var<std::string> plabel;
                chr::Logical_var<Domain>      pdomain;
                space.get_vertice(previous_var, pflags, plabel, pdomain);
                int previous_dmin = (*pdomain).min();
                int previous_dmax = (*pdomain).max();

                chr::Logical_var<Flags>       cflags;
                chr::Logical_var<std::string> clabel;
                chr::Logical_var<Domain>      cdomain;
                space.get_vertice(current_var, cflags, clabel, cdomain);
                int current_dmin = (*cdomain).min();
                int current_dmax = (*cdomain).max();

                int dmin = std::max((previous_dmin+current_dmin), max_aux_domain.domain.front());
				int dmax = std::min((previous_dmax+current_dmax), max_aux_domain.domain.back());

                std::cout << "[" <<  previous_dmin << "," << previous_dmax << "] + ";
                std::cout << "[" <<  current_dmin << "," << current_dmax << "] = ";
                std::cout << "[" <<  dmin << "," << dmax << "] \t ";

                IntVarDomain new_aux_domain;
                for (int k=dmin; k<=dmax; k++)
                    new_aux_domain.domain.push_back(k);

				space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, new_aux_domain ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(current_add,previous_var,0);
			space.edge(current_add,current_var,1);
			space.edge(current_add,next_var,2);
			std::cout << current_add << ": " << previous_var << " + " << current_var << " = " << next_var << std::endl;

			previous_var = next_var;
		}
	}
*/
	std::cout << std::endl;


    // DIAGONAL 1 ---------------------------------------------------
	{
		// enqueue vars
		std::queue<int> varqueue;
		for (unsigned int i=0; i<N; i++) { varqueue.push(matrix[i][i]); }

		// pop and sum until length<=2
		while (varqueue.size() > 2) {
			unsigned int x1 = varqueue.front(); varqueue.pop();
			unsigned int x2 = varqueue.front(); varqueue.pop();

			// 20190705 : new auxiliary domain
			chr::Logical_var<Flags>       pflags;
			chr::Logical_var<std::string> plabel;
			chr::Logical_var<Domain>      pdomain;
			space.get_vertice(x1, pflags, plabel, pdomain);
			int x1_dmin = (*pdomain).min();
			int x1_dmax = (*pdomain).max();

			chr::Logical_var<Flags>       cflags;
			chr::Logical_var<std::string> clabel;
			chr::Logical_var<Domain>      cdomain;
			space.get_vertice(x2, cflags, clabel, cdomain);
			int x2_dmin = (*cdomain).min();
			int x2_dmax = (*cdomain).max();

			int dmin = std::max((x1_dmin+x2_dmin), max_aux_domain.domain.front());
			int dmax = std::min((x1_dmax+x2_dmax), max_aux_domain.domain.back());

			std::cout << "[" <<  x1_dmin << "," << x2_dmax << "] + ";
			std::cout << "[" <<  x2_dmin << "," << x2_dmax << "] = ";
			std::cout << "[" <<  dmin << "," << dmax << "] \t ";

			IntVarDomain new_aux_domain;
			for (int k=dmin; k<=dmax; k++)
				new_aux_domain.domain.push_back(k);

			unsigned int aux = space._first_free_id++;
			unsigned int auxsum = space._first_free_id++;

			// 20190705: Propagate only if the sum involves two DECISION VARS
//			if ( (x1 <= N*N) && (x2 <= N*N)) {
//                space.vertice(aux, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, new_aux_domain); space.num_variables++;
//                space.vertice(auxsum, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
//			} else {
                space.vertice(aux, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, new_aux_domain); space.num_variables++;
                space.vertice(auxsum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
//            }

			space.edge(auxsum, x1, 0);
			space.edge(auxsum, x2, 1);
			space.edge(auxsum, aux, 2);
			std::cout << auxsum << ": " << x1 << " + " << x2 << " = " << aux << std::endl;

			varqueue.push(aux);
		}

		// final D1 sum
		unsigned int x1 = varqueue.front(); varqueue.pop();
		unsigned int x2 = varqueue.front(); varqueue.pop();

		unsigned int auxsum = space._first_free_id++;
		space.vertice(auxsum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(auxsum, x1, 0);
		space.edge(auxsum, x2, 1);
		space.edge(auxsum, magic_var,  2);
		std::cout << auxsum << ": " << x1 << " + " << x2 << " = " << magic_var << std::endl;
	}
	std::cout << std::endl;


    // DIAGONAL 2 ---------------------------------------------------
	{
		// enqueue vars
		std::queue<int> varqueue;
		for (unsigned int i=0; i<N; i++) { varqueue.push(matrix[N-i-1][i]); }

		// pop and sum until length<=2
		while (varqueue.size() > 2) {
			unsigned int x1 = varqueue.front(); varqueue.pop();
			unsigned int x2 = varqueue.front(); varqueue.pop();

			// 20190705 : new auxiliary domain
			chr::Logical_var<Flags>       pflags;
			chr::Logical_var<std::string> plabel;
			chr::Logical_var<Domain>      pdomain;
			space.get_vertice(x1, pflags, plabel, pdomain);
			int x1_dmin = (*pdomain).min();
			int x1_dmax = (*pdomain).max();

			chr::Logical_var<Flags>       cflags;
			chr::Logical_var<std::string> clabel;
			chr::Logical_var<Domain>      cdomain;
			space.get_vertice(x2, cflags, clabel, cdomain);
			int x2_dmin = (*cdomain).min();
			int x2_dmax = (*cdomain).max();

			int dmin = std::max((x1_dmin+x2_dmin), max_aux_domain.domain.front());
			int dmax = std::min((x1_dmax+x2_dmax), max_aux_domain.domain.back());

			std::cout << "[" <<  x1_dmin << "," << x2_dmax << "] + ";
			std::cout << "[" <<  x2_dmin << "," << x2_dmax << "] = ";
			std::cout << "[" <<  dmin << "," << dmax << "] \t ";

			IntVarDomain new_aux_domain;
			for (int k=dmin; k<=dmax; k++)
				new_aux_domain.domain.push_back(k);

			unsigned int aux = space._first_free_id++;
			unsigned int auxsum = space._first_free_id++;

			// 20190705: Propagate only if the sum involves two DECISION VARS
//			if ( (x1 <= N*N) && (x2 <= N*N)) {
//                space.vertice(aux, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, new_aux_domain); space.num_variables++;
//                space.vertice(auxsum, Flags(Flag::RELATION | Flag::PROP_ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
//			} else {
                space.vertice(aux, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, new_aux_domain); space.num_variables++;
                space.vertice(auxsum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
//            }
			space.edge(auxsum, x1, 0);
			space.edge(auxsum, x2, 1);
			space.edge(auxsum, aux, 2);
			std::cout << auxsum << ": " << x1 << " + " << x2 << " = " << aux << std::endl;

			varqueue.push(aux);
		}

		// final D2 sum
		unsigned int x1 = varqueue.front(); varqueue.pop();
		unsigned int x2 = varqueue.front(); varqueue.pop();

		unsigned int auxsum = space._first_free_id++;
		space.vertice(auxsum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(auxsum, x1, 0);
		space.edge(auxsum, x2, 1);
		space.edge(auxsum, magic_var,  2);
		std::cout << auxsum << ": " << x1 << " + " << x2 << " = " << magic_var << std::endl;
	}

/*
	// DIAGONALS 1&2 (sequential) -----------------------------------
	{
		unsigned int d1_previous_var = matrix[0][0];
		unsigned int d2_previous_var = matrix[0][N-1];

		for (unsigned int i=1; i<N; ++i)
		{
			// Diagonal 1
			unsigned int d1_current_var = matrix[i][i];
			unsigned int d1_next_var;
			if (i == (N-1)){
				d1_next_var = magic_var;
			} else {
				d1_next_var = space._first_free_id++;

				// 20190705 : new auxiliary domain
                chr::Logical_var<Flags>       pflags;
                chr::Logical_var<std::string> plabel;
                chr::Logical_var<Domain>      pdomain;
                space.get_vertice(d1_previous_var, pflags, plabel, pdomain);
                int previous_dmin = (*pdomain).min();
                int previous_dmax = (*pdomain).max();

                chr::Logical_var<Flags>       cflags;
                chr::Logical_var<std::string> clabel;
                chr::Logical_var<Domain>      cdomain;
                space.get_vertice(d1_current_var, cflags, clabel, cdomain);
                int current_dmin = (*cdomain).min();
                int current_dmax = (*cdomain).max();

                int dmin = std::max((previous_dmin+current_dmin), max_aux_domain.domain.front());
				int dmax = std::min((previous_dmax+current_dmax), max_aux_domain.domain.back());

                std::cout << "[" <<  previous_dmin << "," << previous_dmax << "] + ";
                std::cout << "[" <<  current_dmin << "," << current_dmax << "] = ";
                std::cout << "[" <<  dmin << "," << dmax << "]  " << std::endl;

                IntVarDomain new_aux_domain;
                for (int k=dmin; k<=dmax; k++)
                    new_aux_domain.domain.push_back(k);

				space.vertice(d1_next_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, new_aux_domain ); space.num_variables++;
			}
			unsigned int d1_current_add = space._first_free_id++;
			space.vertice(d1_current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(d1_current_add, d1_previous_var,0);
			space.edge(d1_current_add, d1_current_var,1);
			space.edge(d1_current_add, d1_next_var,2);
			d1_previous_var = d1_next_var;

			// Diagonal 2
			unsigned int d2_current_var = matrix[i][N-1-i];
			unsigned int d2_next_var;
			if (i == (N-1)) {
				d2_next_var = magic_var;
			} else {
				d2_next_var = space._first_free_id++;

				// 20190705 : new auxiliary domain
                chr::Logical_var<Flags>       pflags;
                chr::Logical_var<std::string> plabel;
                chr::Logical_var<Domain>      pdomain;
                space.get_vertice(d2_previous_var, pflags, plabel, pdomain);
                int previous_dmin = (*pdomain).min();
                int previous_dmax = (*pdomain).max();

                chr::Logical_var<Flags>       cflags;
                chr::Logical_var<std::string> clabel;
                chr::Logical_var<Domain>      cdomain;
                space.get_vertice(d2_current_var, cflags, clabel, cdomain);
                int current_dmin = (*cdomain).min();
                int current_dmax = (*cdomain).max();

                int dmin = std::max((previous_dmin+current_dmin), max_aux_domain.domain.front());
				int dmax = std::min((previous_dmax+current_dmax), max_aux_domain.domain.back());

                std::cout << "[" <<  previous_dmin << "," << previous_dmax << "] + ";
                std::cout << "[" <<  current_dmin << "," << current_dmax << "] = ";
                std::cout << "[" <<  dmin << "," << dmax << "]  " << std::endl;

                IntVarDomain new_aux_domain;
                for (int k=dmin; k<=dmax; k++)
                    new_aux_domain.domain.push_back(k);

				space.vertice(d2_next_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, new_aux_domain ); space.num_variables++;
			}
			unsigned int d2_current_add = space._first_free_id++;
			space.vertice(d2_current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(d2_current_add, d2_previous_var,0);
			space.edge(d2_current_add, d2_current_var,1);
			space.edge(d2_current_add, d2_next_var,2);
			d2_previous_var = d2_next_var;
		}
	}
*/
	std::cout << std::endl;


	// alldiff --------------------------------------------
	unsigned int alldiff = space._first_free_id++;
	space.vertice(alldiff, Flags(Flag::RELATION | Flag::ALL_DIFFERENT), N_STR, IntVarDomain() ); space.num_relations++;
	for (unsigned int i=0; i<N; ++i)
		for (unsigned int j=0; j<N; ++j)
			space.edge(alldiff,matrix[i][j],-1);
	std::cout << alldiff << ": alldiff" << std::endl << std::endl;


    // SYMMETRY BREAKERS ----------------------------------
    {
		unsigned int sb_row = space._first_free_id++;
		space.vertice(sb_row, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(sb_row, matrix[0][0], 0);
		space.edge(sb_row, matrix[N-1][0], 1);
		std::cout << sb_row << ": " << matrix[0][0] << " < " << matrix[N-1][0] << std::endl;

		unsigned int sb_col = space._first_free_id++;
		space.vertice(sb_col, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(sb_col, matrix[0][0],0);
		space.edge(sb_col, matrix[0][N-1],1);
		std::cout << sb_col << ": " << matrix[0][0] << " < " << matrix[0][N-1] << std::endl;

		unsigned int sb_d1 = space._first_free_id++;
		space.vertice(sb_d1, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(sb_d1, matrix[0][0], 0);
		space.edge(sb_d1, matrix[N-1][N-1], 1);
		std::cout << sb_d1 << ": " << matrix[0][0] << " < " << matrix[N-1][N-1] << std::endl;

		unsigned int sb_d2 = space._first_free_id++;
		space.vertice(sb_d2, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(sb_d2, matrix[0][N-1], 0);
		space.edge(sb_d2, matrix[N-1][0], 1);
		std::cout << sb_d2 << ": " << matrix[0][N-1] << " < " << matrix[N-1][0] << std::endl;
    }
    std::cout << std::endl;

}





// ==================================================================
// m2: Magic Square + LINEAR_EQ_SEQ
// CVG 2019-10-09
// Assuming: MS consecutive, classic w/ symmetry breakers
// ==================================================================

void run_magic_square_m2(GRAPH& space, unsigned int N)
{
    time_t my_time = time(NULL);
	std::cout << "magic_square()  : start\t\t" << ctime(&my_time);

	// Model ----------------------------------------------
	space.encoding_sequence.push_back(Flag::DIRECT_ENCODING);
	space.encoding_sequence.push_back(Flag::LOG_ENCODING);

	// DOMAIN ---------------------------------------------
	unsigned int domain_size = N*N;
	IntVarDomain domain;
	for (unsigned int i=1; i <= domain_size; ++i)
		domain.domain.push_back(i);

	// CONSTANTS (the magic number) -----------------------
	int M = N*(N*N+1)/2;
	std::cout << "M: " << M << std::endl;
	unsigned int magic_var = space._first_free_id++;
	space.vertice(magic_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, IntVarDomain({M})); space.num_variables++;

	// VARIABLES ------------------------------------------
	std::vector< std::vector<unsigned int> > matrix;

	for (unsigned int i=0; i<N; ++i)
	{
		std::vector<unsigned int> var_row;
		for (unsigned int j=0; j<N; ++j) {
			unsigned int var = space._first_free_id++;
			space.vertice(var, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, domain); space.num_variables++;
			var_row.push_back(var);
		}
		matrix.push_back(var_row);
	}

	// CONSTRAINTS ----------------------------------------

	// row
	for (unsigned int i=0; i<N; ++i)
	{
		unsigned int addition = space._first_free_id++;
		space.vertice(addition, Flags(Flag::RELATION | Flag::LINEAR_EQ_SEQ), N_STR, IntVarDomain()); space.num_relations++;
		space.edge(addition, magic_var, 0);	// the result
		std::cout << addition << ":\t" ;

		for (unsigned int j=0; j<N; ++j) {
			space.edge(addition, matrix[i][j], j+1);
			std::cout << matrix[i][j] << " + ";
		}
		std::cout << std::endl;
	}

	// columns
	for (unsigned int j=0; j<N; ++j)
	{
		unsigned int addition = space._first_free_id++;
		space.vertice(addition, Flags(Flag::RELATION | Flag::LINEAR_EQ_SEQ), N_STR, IntVarDomain()); space.num_relations++;
		space.edge(addition, magic_var, 0);
		std::cout << addition << ":\t" ;

		for (unsigned int i=0; i<N; ++i) {
			space.edge(addition, matrix[i][j], i+1);
			std::cout << matrix[i][j] << " + ";
		}
		std::cout << std::endl;
	}

	// diagonals
	unsigned int d1_addition = space._first_free_id++;
	space.vertice(d1_addition, Flags(Flag::RELATION | Flag::LINEAR_EQ_SEQ), N_STR, IntVarDomain()); space.num_relations++;
	space.edge(d1_addition, magic_var, 0);

	unsigned int d2_addition = space._first_free_id++;
	space.vertice(d2_addition, Flags(Flag::RELATION | Flag::LINEAR_EQ_SEQ), N_STR, IntVarDomain()); space.num_relations++;
	space.edge(d2_addition, magic_var, 0);

	for (unsigned int i=0; i<N; ++i)
	{
		space.edge(d1_addition, matrix[i][i], i+1);
		space.edge(d2_addition, matrix[i][N-1-i], i+1);
	}

	// all different
	unsigned int alldiff = space._first_free_id++;
	space.vertice(alldiff, Flags(Flag::RELATION | Flag::ALL_DIFFERENT), N_STR, IntVarDomain() ); space.num_relations++;

	for (unsigned int i=0; i<N; ++i) {
		for (unsigned int j=0; j<N; ++j)
			space.edge(alldiff, matrix[i][j], -1);
	}

    // SYMMETRY BREAKERS ----------------------------------
    {
		unsigned int sb_row = space._first_free_id++;
		space.vertice(sb_row, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(sb_row, matrix[0][0], 0);
		space.edge(sb_row, matrix[N-1][0], 1);
		std::cout << sb_row << ":\t" << matrix[0][0] << " < " << matrix[N-1][0] << std::endl;

		unsigned int sb_col = space._first_free_id++;
		space.vertice(sb_col, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(sb_col, matrix[0][0],0);
		space.edge(sb_col, matrix[0][N-1],1);
		std::cout << sb_col << ":\t" << matrix[0][0] << " < " << matrix[0][N-1] << std::endl;

		unsigned int sb_d1 = space._first_free_id++;
		space.vertice(sb_d1, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(sb_d1, matrix[0][0], 0);
		space.edge(sb_d1, matrix[N-1][N-1], 1);
		std::cout << sb_d1 << ":\t" << matrix[0][0] << " < " << matrix[N-1][N-1] << std::endl;

		unsigned int sb_d2 = space._first_free_id++;
		space.vertice(sb_d2, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(sb_d2, matrix[0][N-1], 0);
		space.edge(sb_d2, matrix[N-1][0], 1);
		std::cout << sb_d2 << ":\t" << matrix[0][N-1] << " < " << matrix[N-1][0] << std::endl;
    }
    std::cout << std::endl;


    my_time = time(NULL);
	std::cout << "ms_model()  : end\t\t" << ctime(&my_time);
}




























// ===========================================================================
// MAGIC SQUARE (m4)
// Sequential Sum Decomposition
// ===========================================================================

void run_magic_square_m4(GRAPH& space, unsigned int N)
{
    time_t my_time = time(NULL);
	std::cout << "magic_square_m4(): " << ctime(&my_time) << std::endl;

	// magic constant -------------------------------------

	const unsigned int M = ((N*N*N)+N)/2;
	unsigned int magic_var = space._first_free_id++;
    space.vertice(magic_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({ static_cast<int>(M) })); space.num_variables++;
	std::cout << "N: " << N << std::endl;
	std::cout << "M: " << M << std::endl;

    // domains ------------------------------------------------------
	IntVarDomain var_domain;
	for (unsigned int i=1; i<=(N*N); i++)
		var_domain.domain.push_back(i);

    IntVarDomain max_aux_domain;
    for (unsigned int i=3; i<=M; i++)
        max_aux_domain.domain.push_back(i);
    std::cout << "max_aux_domain: [" << max_aux_domain.domain.front() << "," << max_aux_domain.domain.back() << "]\n";


    // variables ----------------------------------------------------
    std::cout << "variables: " << std::endl;
	std::vector< std::vector<unsigned int> > matrix;

	for (unsigned int i=0; i<N; i++)
	{
        std::vector<unsigned int> matrix_row;
        for (unsigned int j=0; j<N; j++) {
            unsigned int var = space._first_free_id++;
            space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, var_domain); space.num_variables++;
            matrix_row.push_back(var);
            std::cout << var << " ";
        }
        matrix.push_back(matrix_row);
        std::cout << std::endl;
    }

/*
	// solution -----------------------------------------------------
	std::vector< std::vector<unsigned int> > solution;
    solution = {{8,11,14,1},{13,2,7,12},{3,16,9,6},{10,5,4,15}};

	for (unsigned int i=0; i<N; i++)
	{
		std::vector<unsigned int> matrix_row;
		for (unsigned int j=0; j<N; j++) {
			unsigned int var = space._first_free_id++;
			if ((i*4+j) < 4) {	// variables: 0,1,2,3
				space.vertice(var, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, var_domain); space.num_variables++;
			} else {
				space.vertice(var, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, IntVarDomain({ static_cast<int>(solution[i][j]) })); space.num_variables++;
			}
			matrix_row.push_back(var);
			std::cout << var << " ";
		}
        matrix.push_back(matrix_row);
        std::cout << std::endl;
    }
*/
	std::cout << std::endl;

    // ROWS ---------------------------------------------------------

	for (unsigned int i=0; i<N; ++i)
	{
		unsigned int previous_var = matrix[i][0];
		for (unsigned int j=1; j<N; ++j)
		{
			unsigned int current_var = matrix[i][j];
			unsigned int next_var;
			if (j == (N-1)){
				next_var = magic_var;
			} else {
				next_var = space._first_free_id++;

				// 20190705 : new auxiliary domain
                chr::Logical_var<Flags>       pflags;
                chr::Logical_var<std::string> plabel;
                chr::Logical_var<Domain>      pdomain;
                space.get_vertice(previous_var, pflags, plabel, pdomain);
                int previous_dmin = (*pdomain).min();
                int previous_dmax = (*pdomain).max();

                chr::Logical_var<Flags>       cflags;
                chr::Logical_var<std::string> clabel;
                chr::Logical_var<Domain>      cdomain;
                space.get_vertice(current_var, cflags, clabel, cdomain);
                int current_dmin = (*cdomain).min();
                int current_dmax = (*cdomain).max();

                int dmin = std::max((previous_dmin+current_dmin), max_aux_domain.domain.front());
				int dmax = std::min((previous_dmax+current_dmax), max_aux_domain.domain.back());

                //std::cout << "[" <<  previous_dmin << "," << previous_dmax << "] + ";
                //std::cout << "[" <<  current_dmin << "," << current_dmax << "] = ";
                //std::cout << "[" <<  dmin << "," << dmax << "] \t ";

                IntVarDomain new_aux_domain;
                for (int k=dmin; k<=dmax; k++)
                    new_aux_domain.domain.push_back(k);

				space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, new_aux_domain ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(current_add, previous_var, 0);
			space.edge(current_add, current_var,  1);
			space.edge(current_add, next_var,     2);
			std::cout << current_add << ": " << previous_var << " + " << current_var << " = " << next_var << std::endl;

			previous_var = next_var;
		}
	}
	std::cout << std::endl;


    // COLUMNS ------------------------------------------------------

	for (unsigned int j=0; j<N; ++j)
	{
		unsigned int previous_var = matrix[0][j];
		for (unsigned int i=1; i<N; ++i)
		{
			unsigned int current_var = matrix[i][j];
			unsigned int next_var;
			if (i == (N-1)){
				next_var = magic_var;
			} else {
				next_var = space._first_free_id++;

				// 20190705 : new auxiliary domain
                chr::Logical_var<Flags>       pflags;
                chr::Logical_var<std::string> plabel;
                chr::Logical_var<Domain>      pdomain;
                space.get_vertice(previous_var, pflags, plabel, pdomain);
                int previous_dmin = (*pdomain).min();
                int previous_dmax = (*pdomain).max();

                chr::Logical_var<Flags>       cflags;
                chr::Logical_var<std::string> clabel;
                chr::Logical_var<Domain>      cdomain;
                space.get_vertice(current_var, cflags, clabel, cdomain);
                int current_dmin = (*cdomain).min();
                int current_dmax = (*cdomain).max();

                int dmin = std::max((previous_dmin+current_dmin), max_aux_domain.domain.front());
				int dmax = std::min((previous_dmax+current_dmax), max_aux_domain.domain.back());

                //std::cout << "[" <<  previous_dmin << "," << previous_dmax << "] + ";
                //std::cout << "[" <<  current_dmin << "," << current_dmax << "] = ";
                //std::cout << "[" <<  dmin << "," << dmax << "] \t ";

                IntVarDomain new_aux_domain;
                for (int k=dmin; k<=dmax; k++)
                    new_aux_domain.domain.push_back(k);

				space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, new_aux_domain ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(current_add,previous_var,0);
			space.edge(current_add,current_var,1);
			space.edge(current_add,next_var,2);
			std::cout << current_add << ": " << previous_var << " + " << current_var << " = " << next_var << std::endl;

			previous_var = next_var;
		}
	}
	std::cout << std::endl;


	// DIAGONALS 1&2 ------------------------------------------------
	{
		unsigned int d1_previous_var = matrix[0][0];
		unsigned int d2_previous_var = matrix[0][N-1];

		for (unsigned int i=1; i<N; ++i)
		{
			// Diagonal 1
			unsigned int d1_current_var = matrix[i][i];
			unsigned int d1_next_var;
			if (i == (N-1)){
				d1_next_var = magic_var;
			} else {
				d1_next_var = space._first_free_id++;

				// 20190705 : new auxiliary domain
                chr::Logical_var<Flags>       pflags;
                chr::Logical_var<std::string> plabel;
                chr::Logical_var<Domain>      pdomain;
                space.get_vertice(d1_previous_var, pflags, plabel, pdomain);
                int previous_dmin = (*pdomain).min();
                int previous_dmax = (*pdomain).max();

                chr::Logical_var<Flags>       cflags;
                chr::Logical_var<std::string> clabel;
                chr::Logical_var<Domain>      cdomain;
                space.get_vertice(d1_current_var, cflags, clabel, cdomain);
                int current_dmin = (*cdomain).min();
                int current_dmax = (*cdomain).max();

                int dmin = std::max((previous_dmin+current_dmin), max_aux_domain.domain.front());
				int dmax = std::min((previous_dmax+current_dmax), max_aux_domain.domain.back());

                //std::cout << "[" <<  previous_dmin << "," << previous_dmax << "] + ";
                //std::cout << "[" <<  current_dmin << "," << current_dmax << "] = ";
                //std::cout << "[" <<  dmin << "," << dmax << "]  " << std::endl;

                IntVarDomain new_aux_domain;
                for (int k=dmin; k<=dmax; k++)
                    new_aux_domain.domain.push_back(k);

				space.vertice(d1_next_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, new_aux_domain ); space.num_variables++;
			}
			unsigned int d1_current_add = space._first_free_id++;
			space.vertice(d1_current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(d1_current_add, d1_previous_var,0);
			space.edge(d1_current_add, d1_current_var,1);
			space.edge(d1_current_add, d1_next_var,2);
			std::cout << d1_current_add << ": " << d1_previous_var << " + " << d1_current_var << " = " << d1_next_var << std::endl;
			d1_previous_var = d1_next_var;

			// diagonal 2
			unsigned int d2_current_var = matrix[i][N-1-i];
			unsigned int d2_next_var;
			if (i == (N-1)) {
				d2_next_var = magic_var;
			} else {
				d2_next_var = space._first_free_id++;

				// 20190705 : new auxiliary domain
                chr::Logical_var<Flags>       pflags;
                chr::Logical_var<std::string> plabel;
                chr::Logical_var<Domain>      pdomain;
                space.get_vertice(d2_previous_var, pflags, plabel, pdomain);
                int previous_dmin = (*pdomain).min();
                int previous_dmax = (*pdomain).max();

                chr::Logical_var<Flags>       cflags;
                chr::Logical_var<std::string> clabel;
                chr::Logical_var<Domain>      cdomain;
                space.get_vertice(d2_current_var, cflags, clabel, cdomain);
                int current_dmin = (*cdomain).min();
                int current_dmax = (*cdomain).max();

                int dmin = std::max((previous_dmin+current_dmin), max_aux_domain.domain.front());
				int dmax = std::min((previous_dmax+current_dmax), max_aux_domain.domain.back());

                //std::cout << "[" <<  previous_dmin << "," << previous_dmax << "] + ";
                //std::cout << "[" <<  current_dmin << "," << current_dmax << "] = ";
                //std::cout << "[" <<  dmin << "," << dmax << "]  " << std::endl;

                IntVarDomain new_aux_domain;
                for (int k=dmin; k<=dmax; k++)
                    new_aux_domain.domain.push_back(k);

				space.vertice(d2_next_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, new_aux_domain ); space.num_variables++;
			}
			unsigned int d2_current_add = space._first_free_id++;
			space.vertice(d2_current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(d2_current_add, d2_previous_var,0);
			space.edge(d2_current_add, d2_current_var,1);
			space.edge(d2_current_add, d2_next_var,2);
			std::cout << d2_current_add << ": " << d2_previous_var << " + " << d2_current_var << " = " << d2_next_var << std::endl;
			d2_previous_var = d2_next_var;
		}
	}
	std::cout << std::endl;


	// alldiff --------------------------------------------
	unsigned int alldiff = space._first_free_id++;
	space.vertice(alldiff, Flags(Flag::RELATION | Flag::ALL_DIFFERENT), N_STR, IntVarDomain() ); space.num_relations++;
	for (unsigned int i=0; i<N; ++i)
		for (unsigned int j=0; j<N; ++j)
			space.edge(alldiff,matrix[i][j],-1);
	std::cout << alldiff << ": alldiff" << std::endl << std::endl;


    /* SYMMETRY BREAKERS ----------------------------------
    {
		unsigned int sb_row = space._first_free_id++;
		space.vertice(sb_row, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(sb_row, matrix[0][0], 0);
		space.edge(sb_row, matrix[N-1][0], 1);
		std::cout << sb_row << ": " << matrix[0][0] << " < " << matrix[N-1][0] << std::endl;

		unsigned int sb_col = space._first_free_id++;
		space.vertice(sb_col, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(sb_col, matrix[0][0],0);
		space.edge(sb_col, matrix[0][N-1],1);
		std::cout << sb_col << ": " << matrix[0][0] << " < " << matrix[0][N-1] << std::endl;

		unsigned int sb_d1 = space._first_free_id++;
		space.vertice(sb_d1, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(sb_d1, matrix[0][0], 0);
		space.edge(sb_d1, matrix[N-1][N-1], 1);
		std::cout << sb_d1 << ": " << matrix[0][0] << " < " << matrix[N-1][N-1] << std::endl;

		unsigned int sb_d2 = space._first_free_id++;
		space.vertice(sb_d2, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(sb_d2, matrix[0][N-1], 0);
		space.edge(sb_d2, matrix[N-1][0], 1);
		std::cout << sb_d2 << ": " << matrix[0][N-1] << " < " << matrix[N-1][0] << std::endl;
    }
    std::cout << std::endl;
*/
}


















// --------------------------------------------------------
// MagicSquare "discontinuous"
// --------------------------------------------------------
/*
void run_magicsquare_discontinuous(GRAPH& space, unsigned int N, unsigned int domsize)
{
    time_t my_time = time(NULL);
	std::cout << "ms_model()  : start\t\t" << ctime(&my_time);

	// Magic Number ---------------------------------------
	unsigned int M = N*(N*N+1)/2;
	unsigned int magic_var = space._first_free_id++;
	space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({ static_cast<int>(M) }));


	// Variables ------------------------------------------

	IntVarDomain domain;
	for (unsigned int i=1; i<=domsize; ++i)
		domain.domain.push_back(i);

	std::vector< std::vector<unsigned int> > varmatrix;

	for (unsigned int i=0; i<N; ++i) {
        std::vector<unsigned int> vrow;

        for (unsigned int j=0; j<N; ++j) {
            unsigned int var = space._first_free_id++;
            space.vertice(var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain);
            space.num_variables++;
            vrow.push_back(var);
        }
        varmatrix.push_back(vrow);
	}

	// Row constraints ------------------------------------

	// Column constraints ---------------------------------


	IntVarDomain domain, aux_domain;
	unsigned int max_aux_value, old_max_aux_value, real_max_aux_value;

	if (consecutive)
	    domain_size = pb_size*pb_size;
	for (unsigned int i=1; i <= domain_size; ++i)
		domain.domain.push_back(i);

	// On cherche la première puissance de 2 qui puisse contenir le domain aux
	std::vector< std::vector< unsigned int > > matrix;

	// Sum var
	unsigned int sum_var = space._first_free_id++;
	if (consecutive) {
		space.vertice(sum_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({ static_cast<int>(pb_size*(pb_size*pb_size+1)/2) }));
		space.num_variables++;

	} else {
		max_aux_value = 1;
		while (max_aux_value <= (pb_size*domain_size)-(pb_size-1)*(pb_size)/2)
			max_aux_value *=2;
		for (unsigned int i=1; i < max_aux_value; ++i)
			aux_domain.domain.push_back(i);

		//CVG: space.vertice(sum_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain);
		space.vertice(sum_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, aux_domain); space.num_variables++;
	}

	// Problem
	// Variables creation + sum for lines
	for (unsigned int i = 0; i < pb_size; ++i)
	{
		std::vector< unsigned int > matrix_line;

		// Line sum
		//std::cerr << "Sum for lines" << std::endl;
		unsigned int previous_var = space._first_free_id++;

		//SOL
		//std::cerr << "val= " << sol[idx_sol] << std::endl;
		//space.vertice(previous_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, IntVarDomain({ sol[idx_sol++] })); space.num_variables++;
		space.vertice(previous_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain); space.num_variables++;
		matrix_line.push_back(previous_var);

		max_aux_value = 1;
		real_max_aux_value = domain_size;
		while (max_aux_value <= domain_size)
		      max_aux_value *=2;
		aux_domain.domain.clear();
		for (unsigned int i=1; i < max_aux_value; ++i)
		      aux_domain.domain.push_back(i);

		for (unsigned int j=1; j<pb_size; ++j)
		{
			unsigned int current_var = space._first_free_id++;
			space.vertice(current_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain); space.num_variables++;
			matrix_line.push_back(current_var);

			unsigned int next_var;
			if (j == (pb_size-1))
			{
				next_var = sum_var;
			} else {
				old_max_aux_value = max_aux_value;
				real_max_aux_value += domain_size;
				while (max_aux_value <= real_max_aux_value)
				      max_aux_value *=2;
				for (unsigned int i=old_max_aux_value; i < max_aux_value; ++i)
				      aux_domain.domain.push_back(i);
				next_var = space._first_free_id++;
				//CVG: space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain );
				space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, aux_domain ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(current_add,previous_var,0);
			space.edge(current_add,current_var,1);
			space.edge(current_add,next_var,2);
			previous_var = next_var;
		}
		matrix.push_back(matrix_line);
 	}

	// Sum for columns
	for (unsigned int j=0; j<pb_size; ++j)
	{
		max_aux_value = 1;
		real_max_aux_value = domain_size;
		while (max_aux_value <= domain_size)
			max_aux_value *=2;
		aux_domain.domain.clear();
		for (unsigned int i=1; i < max_aux_value; ++i)
		      aux_domain.domain.push_back(i);
		// Column sum
		unsigned int previous_var = matrix[0][j];
		for (unsigned int i = 1; i < pb_size; ++i)
		{
			unsigned int current_var = matrix[i][j];

			unsigned int next_var;
			if (i == (pb_size-1))
			{
				next_var = sum_var;
			} else {
				old_max_aux_value   = max_aux_value;
				real_max_aux_value += domain_size;
				while (max_aux_value <= real_max_aux_value)
				      max_aux_value *=2;
				for (unsigned int i=old_max_aux_value; i < max_aux_value; ++i)
				      aux_domain.domain.push_back(i);
				next_var = space._first_free_id++;
				//CVG: space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain );
				space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, aux_domain ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(current_add,previous_var,0);
			space.edge(current_add,current_var,1);
			space.edge(current_add,next_var,2);

			previous_var = next_var;
		}
	}

	// Sum for diags
	{
		max_aux_value = 1;
		real_max_aux_value = domain_size;
		while (max_aux_value <= domain_size)
		      max_aux_value *=2;
		aux_domain.domain.clear();
		for (unsigned int i=1; i < max_aux_value; ++i)
		      aux_domain.domain.push_back(i);
		unsigned int diag1_previous_var = matrix[0][0];
		unsigned int diag2_previous_var = matrix[0][pb_size-1];
		for (unsigned int i = 1; i < pb_size; ++i)
		{
			// Diag1
			unsigned int diag1_current_var = matrix[i][i];
			unsigned int diag1_next_var;
			if (i == (pb_size-1))
			{
				diag1_next_var = sum_var;
			} else {
				old_max_aux_value = max_aux_value;
				real_max_aux_value += domain_size;
				while (max_aux_value <= real_max_aux_value)
				      max_aux_value *=2;
				for (unsigned int i=old_max_aux_value; i < max_aux_value; ++i)
				      aux_domain.domain.push_back(i);
				diag1_next_var = space._first_free_id++;
				//CVG: space.vertice(diag1_next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain );
				space.vertice(diag1_next_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, aux_domain ); space.num_variables++;
			}
			unsigned int diag1_current_add = space._first_free_id++;
			space.vertice(diag1_current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(diag1_current_add,diag1_previous_var,0);
			space.edge(diag1_current_add,diag1_current_var,1);
			space.edge(diag1_current_add,diag1_next_var,2);
			diag1_previous_var = diag1_next_var;

			// Diag2
			unsigned int diag2_current_var = matrix[i][pb_size-1-i];
			unsigned int diag2_next_var;
			if (i == (pb_size-1)) {
				diag2_next_var = sum_var;
			} else {
				diag2_next_var = space._first_free_id++;
				space.vertice(diag2_next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain ); space.num_variables++;
			}
			unsigned int diag2_current_add = space._first_free_id++;
			space.vertice(diag2_current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(diag2_current_add,diag2_previous_var,0);
			space.edge(diag2_current_add,diag2_current_var,1);
			space.edge(diag2_current_add,diag2_next_var,2);
			diag2_previous_var = diag2_next_var;
		}
	}


	// alldiff
	unsigned int all_diff = space._first_free_id++;
	space.vertice(all_diff, Flags(Flag::RELATION | Flag::ALL_DIFFERENT), N_STR, IntVarDomain() ); space.num_relations++;
	for (unsigned int i = 0; i < pb_size; ++i)
		for (unsigned int j = 0; j < pb_size; ++j)
			space.edge(all_diff,matrix[i][j],-1);

    my_time = time(NULL);
	std::cout << "ms_model()  : end\t\t" << ctime(&my_time);
}

*/





// ===========================================================================
// BIMAGIC SQUARE
// if (consecutive), then: domain_size=N^2
// ===========================================================================
/*
void generate_bimagic_square_problem(GRAPH& space, unsigned int N, unsigned int domain_size, bool consecutive=false)
{
	std::cout << "=== [ BIMAGIC SQUARE ] ==================" << std::endl;

	time_t my_time = time(NULL);
	std::cout << "bms_model() : start\t\t" << ctime(&my_time);

	IntVarDomain domain, aux_domain, aux_square_domain;
	unsigned int max_aux_value, old_max_aux_value, real_max_aux_value;
	unsigned int square_max_aux_value, old_square_max_aux_value, real_square_max_aux_value;

	// domains --------------------------------------------

	if (consecutive)
		domain_size = N*N;
	for (unsigned int i=1; i <= domain_size; ++i)
		domain.domain.push_back(i);

	// --------------------------------
	// Magic numbers
	// TODO why the adjustments (N*domain_size) and (N*domain_size*domain_size) ???
	// --------------------------------
	const int S1 = N*(N*N+1)/2;				//(N*domain_size)-(N-1)*(N)/2;
	const int S2 = N*(N*N+1)*(2*N*N+1)/6;	//(N*domain_size*domain_size)-(N-1)*(N)*(2*(N-1)+1)/6;
	unsigned int s1_var = space._first_free_id++;
	unsigned int s2_var = space._first_free_id++;

	if (consecutive) {
		space.vertice(s1_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, IntVarDomain({ static_cast<int>(S1) })); space.num_variables++;
		space.vertice(s2_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, IntVarDomain({ static_cast<int>(S2) })); space.num_variables++;

	} else {
		max_aux_value = pow(2, ceil(log(S1)/log(2)));
		for (unsigned int i=1; i < max_aux_value; ++i)
			aux_domain.domain.push_back(i);

		square_max_aux_value = pow(2, ceil(log(S2)/log(2)));
		for (unsigned int i=1; i < square_max_aux_value; ++i)
			aux_square_domain.domain.push_back(i);

		space.vertice(s1_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain); space.num_variables++;
		space.vertice(s2_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_square_domain); space.num_variables++;
	}

	// --------------------------------
	// magic square constraints
	// --------------------------------

	std::vector< std::vector< unsigned int > > matrix;

	// Variables creation + ROW sum
	for (unsigned int i=0; i<N; ++i)
	{
		std::vector< unsigned int > matrix_line;

		unsigned int previous_var = space._first_free_id++;
		space.vertice(previous_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain); space.num_variables++;
		matrix_line.push_back(previous_var);

		real_max_aux_value = domain_size;
		max_aux_value      = pow(2,logb(domain_size)+1);

		aux_domain.domain.clear();
		for (unsigned int i=1; i < max_aux_value; ++i)
			aux_domain.domain.push_back(i);

		for (unsigned int j=1; j<N; ++j) {
			unsigned int current_var = space._first_free_id++;
			space.vertice(current_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain); space.num_variables++;
			matrix_line.push_back(current_var);

			unsigned int next_var;
			if (j == (N-1)) {
				next_var = s1_var;

			} else {
				old_max_aux_value = max_aux_value;
				real_max_aux_value += domain_size;
				max_aux_value = pow(2,logb(real_max_aux_value)+1);

				for (unsigned int i=old_max_aux_value; i < max_aux_value; ++i)
					aux_domain.domain.push_back(i);
				next_var = space._first_free_id++;
				space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(current_add,previous_var,0);
			space.edge(current_add,current_var,1);
			space.edge(current_add,next_var,2);
			previous_var = next_var;
		}
		matrix.push_back(matrix_line);
 	}

	// COLUMN sum
	for (unsigned int j=0; j<N; ++j)
	{
		real_max_aux_value = domain_size;
		max_aux_value = pow(2,logb(domain_size)+1);

		aux_domain.domain.clear();
		for (unsigned int i=1; i < max_aux_value; ++i)
			aux_domain.domain.push_back(i);

		unsigned int previous_var = matrix[0][j];
		for (unsigned int i=1; i<N; ++i) {

			unsigned int current_var = matrix[i][j];
			unsigned int next_var;

			if (i == (N-1)) {
				next_var = s1_var;
			} else {
				old_max_aux_value = max_aux_value;
				real_max_aux_value += domain_size;
				max_aux_value = pow(2, ceil(log(real_max_aux_value)/log(2)));

				for (unsigned int i=old_max_aux_value; i < max_aux_value; ++i) {
					aux_domain.domain.push_back(i);
				}
				next_var = space._first_free_id++;
				space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(current_add,previous_var,0);
			space.edge(current_add,current_var,1);
			space.edge(current_add,next_var,2);
			std::cout << current_add << ":\t" << previous_var << " + " << current_var << " = " << next_var << std::endl;

			previous_var = next_var;
		}
	}

	// DIAGONALS sum
	{
		real_max_aux_value = domain_size;
		max_aux_value = pow(2, ceil(log(domain_size)/log(2)));

		aux_domain.domain.clear();
		for (unsigned int i=1; i < max_aux_value; ++i) {
			aux_domain.domain.push_back(i);
		}
		unsigned int diag1_previous_var = matrix[0][0];
		unsigned int diag2_previous_var = matrix[0][N-1];
		for (unsigned int i = 1; i < N; ++i)
		{
			// Diag1
			unsigned int diag1_current_var = matrix[i][i];
			unsigned int diag1_next_var;
			if (i == (N-1)) {
				diag1_next_var = s1_var;

			} else {
				old_max_aux_value = max_aux_value;
				real_max_aux_value += domain_size;

				max_aux_value = pow(2, ceil(log(real_max_aux_value)/log(2)));
				for (unsigned int i=old_max_aux_value; i < max_aux_value; ++i)
					aux_domain.domain.push_back(i);

				diag1_next_var = space._first_free_id++;
				space.vertice(diag1_next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain ); space.num_variables++;
			}
			unsigned int diag1_current_add = space._first_free_id++;
			space.vertice(diag1_current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() );  space.num_relations++;
			space.edge(diag1_current_add,diag1_previous_var,0);
			space.edge(diag1_current_add,diag1_current_var,1);
			space.edge(diag1_current_add,diag1_next_var,2);
			std::cout << diag1_current_add << ":\t" << diag1_previous_var << " + " << diag1_current_var << " = " << diag1_next_var << std::endl;

			diag1_previous_var = diag1_next_var;

			// Diag2
			unsigned int diag2_current_var = matrix[i][N-1-i];
			unsigned int diag2_next_var;
			if (i == (N-1)) {
				diag2_next_var = s1_var;

			} else {
				diag2_next_var = space._first_free_id++;
				space.vertice(diag2_next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain ); space.num_variables++;
			}
			unsigned int diag2_current_add = space._first_free_id++;
			space.vertice(diag2_current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(diag2_current_add,diag2_previous_var,0);
			space.edge(diag2_current_add,diag2_current_var,1);
			space.edge(diag2_current_add,diag2_next_var,2);
			std::cout << diag2_current_add << ":\t" << diag2_previous_var << " + " << diag2_current_var << " = " << diag2_next_var << std::endl;

			diag2_previous_var = diag2_next_var;
		}
	}


	// ------------------------------------------
	// BIMAGIC constraints : Rows
	// ------------------------------------------

	std::vector< std::vector<unsigned int> > sqvars_ids;

	for (unsigned int i = 0; i < N; ++i)
	{
		std::vector<unsigned int> sqvars_row;

		// domains
		real_square_max_aux_value = domain_size*domain_size;
		square_max_aux_value      = pow(2, ceil(log(domain_size*domain_size)/log(2)));

		aux_square_domain.domain.clear();
		for (unsigned int j=1; j < square_max_aux_value; ++j)
		      aux_square_domain.domain.push_back(j);

		// 1st multiplication
		unsigned int previous_var = space._first_free_id++;
		space.vertice(previous_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_square_domain ); space.num_variables++;
		sqvars_row.push_back(previous_var);	//CVG

		unsigned int first_aux_mult = space._first_free_id++;
		space.vertice(first_aux_mult, Flags(Flag::RELATION | Flag::MULT_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(first_aux_mult,matrix[i][0],0);
		space.edge(first_aux_mult,matrix[i][0],1);
		space.edge(first_aux_mult,previous_var,2);
		std::cout << first_aux_mult << ":\t" << matrix[i][0] << " * " << matrix[i][0] << " = " << previous_var << std::endl;

		for (unsigned int j=1; j < N; ++j)
		{
			// 2nd multiplication
			unsigned int current_var = space._first_free_id++;
			space.vertice(current_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_square_domain ); space.num_variables++;
			sqvars_row.push_back(current_var);	//CVG

			unsigned int aux_mult = space._first_free_id++;
			space.vertice(aux_mult, Flags(Flag::RELATION | Flag::MULT_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(aux_mult,matrix[i][j],0);
			space.edge(aux_mult,matrix[i][j],1);
			space.edge(aux_mult,current_var,2);
			std::cout << aux_mult << ":\t" << matrix[i][j] << " * " << matrix[i][j] << " = " << current_var << std::endl;

			unsigned int next_var;
			if (j == (N-1)) {	//if it's the last cell in the row, link to the magic number
				next_var = s2_var;
			} else {
				old_square_max_aux_value   = square_max_aux_value;
				real_square_max_aux_value += domain_size*domain_size;
				square_max_aux_value       = pow(2, ceil(log(real_square_max_aux_value)/log(2)));
				for (unsigned int i=old_square_max_aux_value; i < square_max_aux_value; ++i)
				      aux_square_domain.domain.push_back(i);
				next_var = space._first_free_id++;
				space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_square_domain ); space.num_variables++;
			}


			// SUM up the 2 multiplications
			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(current_add,previous_var,0);
			space.edge(current_add,current_var,1);
			space.edge(current_add,next_var,2);
			std::cout << current_add << ":\t" << previous_var << " + " << current_var << " = " << next_var << std::endl;

			previous_var = next_var;
		}

		sqvars_ids.push_back(sqvars_row);

	}


	// TEST sqvars_ids
	//for(unsigned int i=0; i<N; i++) {
	//	for(unsigned int j=0; j<N; j++) {
	//		std::cout << sqvars_ids[i][j] << " ";
	//	}
	//	std::cout << "\n";
	//}


	// Square sum for COLUMNS
	for (unsigned int j=0; j<N; ++j)
	{
		real_square_max_aux_value = domain_size*domain_size;
		square_max_aux_value      = pow(2, ceil(log(domain_size*domain_size)/log(2)));

		aux_square_domain.domain.clear();
		for (unsigned int i=1; i < square_max_aux_value; ++i) {
            aux_square_domain.domain.push_back(i);
        }

		// Square column sum
		unsigned int previous_var = sqvars_ids[0][j];

		for (unsigned int i=1; i < N; ++i)
		{
			unsigned int current_var = sqvars_ids[i][j];

			unsigned int next_var;
			if (i == (N-1)) {
				next_var = s2_var;
			} else {
				old_square_max_aux_value   = square_max_aux_value;
				real_square_max_aux_value += domain_size*domain_size;
				square_max_aux_value       = pow(2, ceil(log(real_square_max_aux_value)/log(2)));

				for (unsigned int i=old_square_max_aux_value; i < square_max_aux_value; ++i)
				      aux_square_domain.domain.push_back(i);
				next_var = space._first_free_id++;
				space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_square_domain ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(current_add,previous_var,0);
			space.edge(current_add,current_var,1);
			space.edge(current_add,next_var,2);
			std::cout << current_add << ":\t" << previous_var << " + " << current_var << " = " << next_var << std::endl;

			previous_var = next_var;
		}
	}

	// ------------------------------------------
	// BIMAGIC constraint: Diagonals
	// ------------------------------------------
	{
		real_square_max_aux_value = domain_size*domain_size;
		square_max_aux_value      = pow(2, ceil(log(domain_size*domain_size)/log(2)));

		aux_square_domain.domain.clear();
		for (unsigned int i=1; i < square_max_aux_value; ++i) {
            aux_square_domain.domain.push_back(i);
        }

		unsigned int diag1_previous_var = sqvars_ids[0][0];
		unsigned int diag2_previous_var = sqvars_ids[0][N-1];

		for (unsigned int i = 1; i < N; ++i)
		{
			// first diagonal
			unsigned int diag1_current_var = sqvars_ids[i][i];
			unsigned int diag1_next_var;

			if (i == (N-1)) {
				diag1_next_var = s2_var;
			} else {
				old_square_max_aux_value   = square_max_aux_value;
				real_square_max_aux_value += domain_size*domain_size;
				while (square_max_aux_value <= real_square_max_aux_value)
				      square_max_aux_value *=2;
				for (unsigned int i=old_square_max_aux_value; i < square_max_aux_value; ++i)
				      aux_square_domain.domain.push_back(i);
				diag1_next_var = space._first_free_id++;
				space.vertice(diag1_next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_square_domain ); space.num_variables++;
			}
			unsigned int d1_current_add = space._first_free_id++;
			space.vertice(d1_current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(d1_current_add, diag1_previous_var,0);
			space.edge(d1_current_add, diag1_current_var,1);
			space.edge(d1_current_add, diag1_next_var,2);
			std::cout << d1_current_add << ":\t" << diag1_previous_var << " + " << diag1_current_var << " = " << diag1_next_var << std::endl;

			diag1_previous_var = diag1_next_var;

			// second diagonal
			unsigned int diag2_current_var = sqvars_ids[i][N-1-i];
			unsigned int diag2_next_var;

			if (i == (N-1)) {
				diag2_next_var = s2_var;
			} else {
				diag2_next_var = space._first_free_id++;
				space.vertice(diag2_next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_square_domain ); space.num_variables++;
			}
			unsigned int diag2_current_add = space._first_free_id++;
			space.vertice(diag2_current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(diag2_current_add,diag2_previous_var,0);
			space.edge(diag2_current_add,diag2_current_var,1);
			space.edge(diag2_current_add,diag2_next_var,2);
			std::cout << diag2_current_add << ":\t" << diag2_previous_var << " + " << diag2_current_var << " = " << diag2_next_var << std::endl;

			diag2_previous_var = diag2_next_var;
		}
	}

	// alldiff
	unsigned int alldiff = space._first_free_id++;
	space.vertice(alldiff, Flags(Flag::RELATION | Flag::ALL_DIFFERENT), N_STR, IntVarDomain() ); space.num_relations++;
	for (unsigned int i=0; i<N; ++i) {
		for (unsigned int j=0; j<N; ++j)
			space.edge(alldiff, matrix[i][j], -1);
	}

	my_time = time(NULL);
	std::cerr << "bms_model() : end\t\t" << ctime(&my_time);

}
*/




// ===========================================================================
// BIMAGIC SQUARE v2
//
// if (consecutive) => domain_size=N^2
//
// S1: Magic number for 'simple' variables (x1+x2.. xn = S1)
// S2: Magic number for squared variables  (x1^2+x2^2.. xn^2 = S2)
// ===========================================================================

bool run_bimagic_square(GRAPH& space, unsigned int N, bool consecutive=true, unsigned int domain_size=0)
{
	time_t my_time = time(NULL);
	std::cout << "bms_model() : start\t\t" << ctime(&my_time);

	// Checks ---------------------------------------------
	if (!consecutive && domain_size==0) {
		std::cout << "ERROR: domain_size=0" << std::endl;
		return false;
	}
	if (N<3) {
		std::cout << "ERROR: N<3" << std::endl;
		return false;
	}

	// Model ----------------------------------------------

	space.encoding_sequence.push_back(Flag::LOG_ENCODING);

	//IntVarDomain aux_domain, aux_square_domain;
	//unsigned int max_aux_value, old_max_aux_value, real_max_aux_value;
	//unsigned int square_max_aux_value, old_square_max_aux_value, real_square_max_aux_value;


	// Constants (S1, S2) ---------------------------------
	//   consecutive: S1, S2 are constants.
	//   distinct   : s1[1,r2(S1)], s2[1,r2(S2)]
	//   (*) r2: Round up to next power of 2.

	const int S1 = N*(N*N+1)/2;
	const int S2 = N*(N*N+1)*(2*N*N+1)/6;
	unsigned int s1_var = space._first_free_id++;
	unsigned int s2_var = space._first_free_id++;

	if (consecutive) {
		space.vertice(s1_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, IntVarDomain({ static_cast<int>(S1) })); space.num_variables++;
		space.vertice(s2_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, IntVarDomain({ static_cast<int>(S2) })); space.num_variables++;
		std::cout << "S1: " << S1 << "\tS2: " << S2 << std::endl;

	} else {
		IntVarDomain domain_s1;
		for (unsigned int i=1; i<pow(2, ceil(log(S1)/log(2))); ++i)
			domain_s1.domain.push_back(i);

		IntVarDomain domain_s2;
		for (unsigned int i=1; i<pow(2,ceil(log(S2)/log(2))); ++i)
			domain_s2.domain.push_back(i);

		space.vertice(s1_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, domain_s1); space.num_variables++;
		space.vertice(s2_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, domain_s2); space.num_variables++;

		std::cout << "S1: " << S1 << " (" << pow(2, ceil(log(S1)/log(2))) << ")\t";
		std::cout << "S2: " << S2 << " (" << pow(2, ceil(log(S2)/log(2))) << std::endl;
	}
	std::cout << std::endl;


	// Domains --------------------------------------------

	IntVarDomain domain, sqdomain; //, aux_domain, aux_square_domain;

	if (consecutive) {
		domain_size = N*N;
		for (unsigned int i=1; i<=(N*N); ++i)		domain.domain.push_back(i);
		for (unsigned int i=1; i<=(N*N*N*N); ++i)	sqdomain.domain.push_back(i);
	}


	// Variables ------------------------------------------

	std::vector< std::vector<unsigned int> > vars;

	for (unsigned int i=0; i<N; ++i) {
		std::vector<unsigned int> matrix_line;

		for (unsigned int j=0; j<N; ++j) {
			unsigned int x = space._first_free_id++;
			space.vertice(x, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain); space.num_variables++;
			matrix_line.push_back(x);
		}
		vars.push_back(matrix_line);
	}
	std::cout << "variables:" << std::endl;
	for (unsigned int i=0; i<N; ++i) {
		for (unsigned int j=0; j<N; ++j) {
			std::cout << vars[i][j] << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;

	// squared variables x^2
	std::vector< std::vector<unsigned int> > sqvars;

	for (unsigned int i=0; i<N; ++i) {
		std::vector<unsigned int> matrix_line;

		for (unsigned int j=0; j<N; ++j) {
			unsigned int x = space._first_free_id++;
			space.vertice(x, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, sqdomain); space.num_variables++;
			matrix_line.push_back(x);
		}
		sqvars.push_back(matrix_line);
	}
	std::cout << "squared variables:" << std::endl;
	for (unsigned int i=0; i<N; ++i) {
		for (unsigned int j=0; j<N; ++j) {
			std::cout << sqvars[i][j] << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;


	/* MAGIC-ROW constraints ------------------------------

	for (unsigned int i=0; i<N; ++i)
	{
		unsigned int prev_var = vars[i][0];

		for (unsigned int j=1; j<N; ++j) {

		}
	}
*/


	return false;

/*	// --------------------------------
	// magic square constraints
	// --------------------------------

IntVarDomain aux_domain, aux_square_domain;


	for (unsigned int i=0; i<N; ++i)
	{
		unsigned int prev_var = vars[i][0];


		IntvarDomain auxdom;
		for (unsigned int i=1; i < max_aux_value; ++i)
			aux_domain.domain.push_back(i);

		for (unsigned int j=1; j<N; ++j) {
			unsigned int current_var = space._first_free_id++;
			space.vertice(current_var, Flags(Flag::VARIABLE | Flag::INTEGER), N_STR, domain); space.num_variables++;
			matrix_line.push_back(current_var);

			unsigned int next_var;
			if (j == (N-1)) {
				next_var = s1_var;

			} else {

				// auxiliary var
				unsigned int rounded_dmax = pow(2,logb(domain_size)+1);


				old_max_aux_value = max_aux_value;
				real_max_aux_value += domain_size;
				max_aux_value = pow(2,logb(real_max_aux_value)+1);

				for (unsigned int i=old_max_aux_value; i < max_aux_value; ++i)
					aux_domain.domain.push_back(i);
				next_var = space._first_free_id++;
				space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(current_add,previous_var,0);
			space.edge(current_add,current_var,1);
			space.edge(current_add,next_var,2);
			previous_var = next_var;
		}
 	}

	// COLUMN sum
	for (unsigned int j=0; j<N; ++j)
	{
		real_max_aux_value = domain_size;
		max_aux_value = pow(2,logb(domain_size)+1);

		aux_domain.domain.clear();
		for (unsigned int i=1; i < max_aux_value; ++i)
			aux_domain.domain.push_back(i);

		unsigned int previous_var = matrix[0][j];
		for (unsigned int i=1; i<N; ++i) {

			unsigned int current_var = matrix[i][j];
			unsigned int next_var;

			if (i == (N-1)) {
				next_var = s1_var;
			} else {
				old_max_aux_value = max_aux_value;
				real_max_aux_value += domain_size;
				max_aux_value = pow(2, ceil(log(real_max_aux_value)/log(2)));

				for (unsigned int i=old_max_aux_value; i < max_aux_value; ++i) {
					aux_domain.domain.push_back(i);
				}
				next_var = space._first_free_id++;
				space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(current_add,previous_var,0);
			space.edge(current_add,current_var,1);
			space.edge(current_add,next_var,2);
			std::cout << current_add << ":\t" << previous_var << " + " << current_var << " = " << next_var << std::endl;

			previous_var = next_var;
		}
	}

	// DIAGONALS sum
	{
		real_max_aux_value = domain_size;
		max_aux_value = pow(2, ceil(log(domain_size)/log(2)));

		aux_domain.domain.clear();
		for (unsigned int i=1; i < max_aux_value; ++i) {
			aux_domain.domain.push_back(i);
		}
		unsigned int diag1_previous_var = matrix[0][0];
		unsigned int diag2_previous_var = matrix[0][N-1];
		for (unsigned int i = 1; i < N; ++i)
		{
			// Diag1
			unsigned int diag1_current_var = matrix[i][i];
			unsigned int diag1_next_var;
			if (i == (N-1)) {
				diag1_next_var = s1_var;

			} else {
				old_max_aux_value = max_aux_value;
				real_max_aux_value += domain_size;

				max_aux_value = pow(2, ceil(log(real_max_aux_value)/log(2)));
				for (unsigned int i=old_max_aux_value; i < max_aux_value; ++i)
					aux_domain.domain.push_back(i);

				diag1_next_var = space._first_free_id++;
				space.vertice(diag1_next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain ); space.num_variables++;
			}
			unsigned int diag1_current_add = space._first_free_id++;
			space.vertice(diag1_current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() );  space.num_relations++;
			space.edge(diag1_current_add,diag1_previous_var,0);
			space.edge(diag1_current_add,diag1_current_var,1);
			space.edge(diag1_current_add,diag1_next_var,2);
			std::cout << diag1_current_add << ":\t" << diag1_previous_var << " + " << diag1_current_var << " = " << diag1_next_var << std::endl;

			diag1_previous_var = diag1_next_var;

			// Diag2
			unsigned int diag2_current_var = matrix[i][N-1-i];
			unsigned int diag2_next_var;
			if (i == (N-1)) {
				diag2_next_var = s1_var;

			} else {
				diag2_next_var = space._first_free_id++;
				space.vertice(diag2_next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_domain ); space.num_variables++;
			}
			unsigned int diag2_current_add = space._first_free_id++;
			space.vertice(diag2_current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(diag2_current_add,diag2_previous_var,0);
			space.edge(diag2_current_add,diag2_current_var,1);
			space.edge(diag2_current_add,diag2_next_var,2);
			std::cout << diag2_current_add << ":\t" << diag2_previous_var << " + " << diag2_current_var << " = " << diag2_next_var << std::endl;

			diag2_previous_var = diag2_next_var;
		}
	}


	// ------------------------------------------
	// BIMAGIC constraints : Rows
	// ------------------------------------------

	std::vector< std::vector<unsigned int> > sqvars_ids;

	for (unsigned int i = 0; i < N; ++i)
	{
		std::vector<unsigned int> sqvars_row;

		// domains
		real_square_max_aux_value = domain_size*domain_size;
		square_max_aux_value      = pow(2, ceil(log(domain_size*domain_size)/log(2)));

		aux_square_domain.domain.clear();
		for (unsigned int j=1; j < square_max_aux_value; ++j)
		      aux_square_domain.domain.push_back(j);

		// 1st multiplication
		unsigned int previous_var = space._first_free_id++;
		space.vertice(previous_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_square_domain ); space.num_variables++;
		sqvars_row.push_back(previous_var);	//CVG

		unsigned int first_aux_mult = space._first_free_id++;
		space.vertice(first_aux_mult, Flags(Flag::RELATION | Flag::MULT_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(first_aux_mult,matrix[i][0],0);
		space.edge(first_aux_mult,matrix[i][0],1);
		space.edge(first_aux_mult,previous_var,2);
		std::cout << first_aux_mult << ":\t" << matrix[i][0] << " * " << matrix[i][0] << " = " << previous_var << std::endl;

		for (unsigned int j=1; j < N; ++j)
		{
			// 2nd multiplication
			unsigned int current_var = space._first_free_id++;
			space.vertice(current_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_square_domain ); space.num_variables++;
			sqvars_row.push_back(current_var);	//CVG

			unsigned int aux_mult = space._first_free_id++;
			space.vertice(aux_mult, Flags(Flag::RELATION | Flag::MULT_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(aux_mult,matrix[i][j],0);
			space.edge(aux_mult,matrix[i][j],1);
			space.edge(aux_mult,current_var,2);
			std::cout << aux_mult << ":\t" << matrix[i][j] << " * " << matrix[i][j] << " = " << current_var << std::endl;

			unsigned int next_var;
			if (j == (N-1)) {	//if it's the last cell in the row, link to the magic number
				next_var = s2_var;
			} else {
				old_square_max_aux_value   = square_max_aux_value;
				real_square_max_aux_value += domain_size*domain_size;
				square_max_aux_value       = pow(2, ceil(log(real_square_max_aux_value)/log(2)));
				for (unsigned int i=old_square_max_aux_value; i < square_max_aux_value; ++i)
				      aux_square_domain.domain.push_back(i);
				next_var = space._first_free_id++;
				space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_square_domain ); space.num_variables++;
			}


			// SUM up the 2 multiplications
			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(current_add,previous_var,0);
			space.edge(current_add,current_var,1);
			space.edge(current_add,next_var,2);
			std::cout << current_add << ":\t" << previous_var << " + " << current_var << " = " << next_var << std::endl;

			previous_var = next_var;
		}

		sqvars_ids.push_back(sqvars_row);

	}


	// TEST sqvars_ids
	//for(unsigned int i=0; i<N; i++) {
	//	for(unsigned int j=0; j<N; j++) {
	//		std::cout << sqvars_ids[i][j] << " ";
	//	}
	//	std::cout << "\n";
	//}


	// Square sum for COLUMNS
	for (unsigned int j=0; j<N; ++j)
	{
		real_square_max_aux_value = domain_size*domain_size;
		square_max_aux_value      = pow(2, ceil(log(domain_size*domain_size)/log(2)));

		aux_square_domain.domain.clear();
		for (unsigned int i=1; i < square_max_aux_value; ++i) {
            aux_square_domain.domain.push_back(i);
        }

		// Square column sum
		unsigned int previous_var = sqvars_ids[0][j];

		for (unsigned int i=1; i < N; ++i)
		{
			unsigned int current_var = sqvars_ids[i][j];

			unsigned int next_var;
			if (i == (N-1)) {
				next_var = s2_var;
			} else {
				old_square_max_aux_value   = square_max_aux_value;
				real_square_max_aux_value += domain_size*domain_size;
				square_max_aux_value       = pow(2, ceil(log(real_square_max_aux_value)/log(2)));

				for (unsigned int i=old_square_max_aux_value; i < square_max_aux_value; ++i)
				      aux_square_domain.domain.push_back(i);
				next_var = space._first_free_id++;
				space.vertice(next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_square_domain ); space.num_variables++;
			}

			unsigned int current_add = space._first_free_id++;
			space.vertice(current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(current_add,previous_var,0);
			space.edge(current_add,current_var,1);
			space.edge(current_add,next_var,2);
			std::cout << current_add << ":\t" << previous_var << " + " << current_var << " = " << next_var << std::endl;

			previous_var = next_var;
		}
	}

	// ------------------------------------------
	// BIMAGIC constraint: Diagonals
	// ------------------------------------------
	{
		real_square_max_aux_value = domain_size*domain_size;
		square_max_aux_value      = pow(2, ceil(log(domain_size*domain_size)/log(2)));

		aux_square_domain.domain.clear();
		for (unsigned int i=1; i < square_max_aux_value; ++i) {
            aux_square_domain.domain.push_back(i);
        }

		unsigned int diag1_previous_var = sqvars_ids[0][0];
		unsigned int diag2_previous_var = sqvars_ids[0][N-1];

		for (unsigned int i = 1; i < N; ++i)
		{
			// first diagonal
			unsigned int diag1_current_var = sqvars_ids[i][i];
			unsigned int diag1_next_var;

			if (i == (N-1)) {
				diag1_next_var = s2_var;
			} else {
				old_square_max_aux_value   = square_max_aux_value;
				real_square_max_aux_value += domain_size*domain_size;
				while (square_max_aux_value <= real_square_max_aux_value)
				      square_max_aux_value *=2;
				for (unsigned int i=old_square_max_aux_value; i < square_max_aux_value; ++i)
				      aux_square_domain.domain.push_back(i);
				diag1_next_var = space._first_free_id++;
				space.vertice(diag1_next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_square_domain ); space.num_variables++;
			}
			unsigned int d1_current_add = space._first_free_id++;
			space.vertice(d1_current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(d1_current_add, diag1_previous_var,0);
			space.edge(d1_current_add, diag1_current_var,1);
			space.edge(d1_current_add, diag1_next_var,2);
			std::cout << d1_current_add << ":\t" << diag1_previous_var << " + " << diag1_current_var << " = " << diag1_next_var << std::endl;

			diag1_previous_var = diag1_next_var;

			// second diagonal
			unsigned int diag2_current_var = sqvars_ids[i][N-1-i];
			unsigned int diag2_next_var;

			if (i == (N-1)) {
				diag2_next_var = s2_var;
			} else {
				diag2_next_var = space._first_free_id++;
				space.vertice(diag2_next_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, aux_square_domain ); space.num_variables++;
			}
			unsigned int diag2_current_add = space._first_free_id++;
			space.vertice(diag2_current_add, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
			space.edge(diag2_current_add,diag2_previous_var,0);
			space.edge(diag2_current_add,diag2_current_var,1);
			space.edge(diag2_current_add,diag2_next_var,2);
			std::cout << diag2_current_add << ":\t" << diag2_previous_var << " + " << diag2_current_var << " = " << diag2_next_var << std::endl;

			diag2_previous_var = diag2_next_var;
		}
	}

	// alldiff
	unsigned int alldiff = space._first_free_id++;
	space.vertice(alldiff, Flags(Flag::RELATION | Flag::ALL_DIFFERENT), N_STR, IntVarDomain() ); space.num_relations++;
	for (unsigned int i=0; i<N; ++i) {
		for (unsigned int j=0; j<N; ++j)
			space.edge(alldiff, matrix[i][j], -1);
	}
*/
	my_time = time(NULL);
	std::cerr << "bms_model() : end\t\t" << ctime(&my_time);

}





























// =============================================================
// MagicSquare M3
// not factorized (arborescent decomposition)
// =============================================================

void run_magic_square_m3(GRAPH& space, unsigned int N)
{
    //time_t my_time = time(NULL);
	//std::cout << "magic_square_m3(): " << ctime(&my_time) << std::endl;
	std::cout << std::endl;
	std::cout << "Magic Square (model 3)" << std::endl << std::endl;


	// model ----------------------------------------------
	space.encoding_sequence.push_back(Flag::DIRECT_ENCODING);
	space.encoding_sequence.push_back(Flag::LOG_ENCODING);


	// magic constant -------------------------------------
	const unsigned int M = ((N*N*N)+N)/2;
	unsigned int magic_var = space._first_free_id++;
    space.vertice(magic_var, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, IntVarDomain({ static_cast<int>(M) })); space.num_variables++;
	std::cout << "N: " << N << "\t";
	std::cout << "M: " << M << std::endl << std::endl;

    // domains ------------------------------------------------------
	IntVarDomain var_domain;
	for (unsigned int i=1; i<=(N*N); i++) var_domain.domain.push_back(i);

    IntVarDomain max_aux_domain;	// auxiliary sum
    for (unsigned int i=3; i<=M; i++)
        max_aux_domain.domain.push_back(i);
    //std::cout << "max_aux_domain: [" << max_aux_domain.domain.front() << "," << max_aux_domain.domain.back() << "]\n";


    // variables ----------------------------------------------------
    std::cout << "vars: " << std::endl << "\t";
	std::vector< std::vector<unsigned int> > matrix;

	for (unsigned int i=0; i<N; i++)
	{
        std::vector<unsigned int> matrix_row;
        for (unsigned int j=0; j<N; j++) {
            unsigned int var = space._first_free_id++;
            space.vertice(var, Flags(Flag::VARIABLE | Flag::PROP_INTEGER), N_STR, var_domain); space.num_variables++;
            matrix_row.push_back(var);
            std::cout << var << " ";
        }
        matrix.push_back(matrix_row);
        std::cout << std::endl << "\t";
    }
	std::cout << std::endl;


    // ROWS ---------------------------------------------------------
	for (unsigned int i=0; i<N; i++)
	{
		// all vars to queue
		std::queue<int> varqueue;
		for (unsigned int j=0; j<N; j++) {
			varqueue.push(matrix[i][j]);
		}

		// pop and sum until length<=2
		while (varqueue.size() > 2)
		{
			// x1
			unsigned int x1 = varqueue.front(); varqueue.pop();
			chr::Logical_var<Flags>       pflags;
			chr::Logical_var<std::string> plabel;
			chr::Logical_var<Domain>      pdomain;
			space.get_vertice(x1, pflags, plabel, pdomain);
			int x1min = (*pdomain).min();
			int x1max = (*pdomain).max();

			// x2
			unsigned int x2 = varqueue.front(); varqueue.pop();
			chr::Logical_var<Flags>       cflags;
			chr::Logical_var<std::string> clabel;
			chr::Logical_var<Domain>      cdomain;
			space.get_vertice(x2, cflags, clabel, cdomain);
			int x2min = (*cdomain).min();
			int x2max = (*cdomain).max();

			// 20190705 : new auxiliary domain
			int dmin = std::max((x1min+x2min), max_aux_domain.domain.front());
			int dmax = std::min((x1max+x2max), max_aux_domain.domain.back());

			IntVarDomain new_aux_domain;
			for (int k=dmin; k<=dmax; k++)
				new_aux_domain.domain.push_back(k);


			unsigned int aux = space._first_free_id++;
			space.vertice(aux, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, new_aux_domain); space.num_variables++;
			unsigned int auxsum = space._first_free_id++;
			space.vertice(auxsum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;

			space.edge(auxsum, x1, 0);
			space.edge(auxsum, x2, 1);
			space.edge(auxsum, aux, 2);
			std::cout << auxsum << ": " << x1 << " + " << x2 << " = " << aux << "\t";
			std::cout << "[" <<  x1min << "," << x2max << "] + ";
			std::cout << "[" <<  x2min << "," << x2max << "] = ";
			std::cout << "[" <<  dmin << "," << dmax << "] " << std::endl;

			varqueue.push(aux);
		}

		// final sum
		unsigned int x1 = varqueue.front(); varqueue.pop();
		unsigned int x2 = varqueue.front(); varqueue.pop();
		unsigned int auxsum = space._first_free_id++;
        space.vertice(auxsum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
        space.edge(auxsum, x1, 0);
        space.edge(auxsum, x2, 1);
        space.edge(auxsum, magic_var,  2);
        std::cout << auxsum << ": " << x1 << " + " << x2 << " = " << magic_var << std::endl;
	}
	std::cout << std::endl;


    // COLUMNS ------------------------------------------------------

	for (unsigned int j=0; j<N; j++)
	{
		// enqueue vars
		std::queue<int> varqueue;
		for (unsigned int i=0; i<N; i++) { varqueue.push(matrix[i][j]); }

		// pop and sum until length<=2
		while (varqueue.size() > 2)
		{
			unsigned int x1 = varqueue.front(); varqueue.pop();
			chr::Logical_var<Flags>       pflags;
			chr::Logical_var<std::string> plabel;
			chr::Logical_var<Domain>      pdomain;
			space.get_vertice(x1, pflags, plabel, pdomain);
			int x1min = (*pdomain).min();
			int x1max = (*pdomain).max();

			unsigned int x2 = varqueue.front(); varqueue.pop();
			chr::Logical_var<Flags>       cflags;
			chr::Logical_var<std::string> clabel;
			chr::Logical_var<Domain>      cdomain;
			space.get_vertice(x2, cflags, clabel, cdomain);
			int x2min = (*cdomain).min();
			int x2max = (*cdomain).max();

			// 20190705 : new auxiliary domain
			int dmin = std::max((x1min+x2min), max_aux_domain.domain.front());
			int dmax = std::min((x1max+x2max), max_aux_domain.domain.back());

			IntVarDomain new_aux_domain;
			for (int k=dmin; k<=dmax; k++)
				new_aux_domain.domain.push_back(k);

			unsigned int aux= space._first_free_id++;
			unsigned int auxsum = space._first_free_id++;
			space.vertice(aux, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, new_aux_domain); space.num_variables++;
			space.vertice(auxsum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;

			space.edge(auxsum, x1, 0);
			space.edge(auxsum, x2, 1);
			space.edge(auxsum, aux, 2);
			std::cout << auxsum << ": " << x1 << " + " << x2 << " = " << aux << "\t";
			std::cout << "[" <<  x1min << "," << x2max << "] + ";
			std::cout << "[" <<  x2min << "," << x2max << "] = ";
			std::cout << "[" <<  dmin << "," << dmax << "] " << std::endl;

			varqueue.push(aux);
		}

		// final sum
		unsigned int x1 = varqueue.front(); varqueue.pop();
		unsigned int x2 = varqueue.front(); varqueue.pop();

		unsigned int auxsum = space._first_free_id++;
        space.vertice(auxsum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
        space.edge(auxsum, x1, 0);
        space.edge(auxsum, x2, 1);
        space.edge(auxsum, magic_var, 2);
        std::cout << auxsum << ": " << x1 << " + " << x2 << " = " << magic_var << std::endl;
	}

	std::cout << std::endl;


    // DIAGONAL 1 ---------------------------------------------------
	{
		// enqueue vars
		std::queue<int> varqueue;
		for (unsigned int i=0; i<N; i++) { varqueue.push(matrix[i][i]); }

		// pop and sum until length<=2
		while (varqueue.size() > 2)
		{
			unsigned int x1 = varqueue.front(); varqueue.pop();
			chr::Logical_var<Flags>       pflags;
			chr::Logical_var<std::string> plabel;
			chr::Logical_var<Domain>      pdomain;
			space.get_vertice(x1, pflags, plabel, pdomain);
			int x1_dmin = (*pdomain).min();
			int x1_dmax = (*pdomain).max();

			unsigned int x2 = varqueue.front(); varqueue.pop();
			chr::Logical_var<Flags>       cflags;
			chr::Logical_var<std::string> clabel;
			chr::Logical_var<Domain>      cdomain;
			space.get_vertice(x2, cflags, clabel, cdomain);
			int x2_dmin = (*cdomain).min();
			int x2_dmax = (*cdomain).max();

			// 20190705 : new auxiliary domain
			int dmin = std::max((x1_dmin+x2_dmin), max_aux_domain.domain.front());
			int dmax = std::min((x1_dmax+x2_dmax), max_aux_domain.domain.back());

			IntVarDomain new_aux_domain;
			for (int k=dmin; k<=dmax; k++)
				new_aux_domain.domain.push_back(k);

			unsigned int aux = space._first_free_id++;
			space.vertice(aux, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, new_aux_domain); space.num_variables++;
			unsigned int auxsum = space._first_free_id++;
			space.vertice(auxsum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;

			space.edge(auxsum, x1, 0);
			space.edge(auxsum, x2, 1);
			space.edge(auxsum, aux, 2);
			std::cout << auxsum << ": " << x1 << " + " << x2 << " = " << aux << "\t";
			std::cout << "[" <<  x1_dmin << "," << x2_dmax << "] + ";
			std::cout << "[" <<  x2_dmin << "," << x2_dmax << "] = ";
			std::cout << "[" <<  dmin << "," << dmax << "] " << std::endl;

			varqueue.push(aux);
		}

		// final D1 sum
		unsigned int x1 = varqueue.front(); varqueue.pop();
		unsigned int x2 = varqueue.front(); varqueue.pop();

		unsigned int auxsum = space._first_free_id++;
		space.vertice(auxsum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(auxsum, x1, 0);
		space.edge(auxsum, x2, 1);
		space.edge(auxsum, magic_var,  2);
		std::cout << auxsum << ": " << x1 << " + " << x2 << " = " << magic_var << std::endl;
	}
	std::cout << std::endl;


    // DIAGONAL 2 ---------------------------------------------------
	{
		// enqueue vars
		std::queue<int> varqueue;
		for (unsigned int i=0; i<N; i++) { varqueue.push(matrix[N-i-1][i]); }

		// pop and sum until length<=2
		while (varqueue.size() > 2)
		{
			unsigned int x1 = varqueue.front(); varqueue.pop();
			chr::Logical_var<Flags>       pflags;
			chr::Logical_var<std::string> plabel;
			chr::Logical_var<Domain>      pdomain;
			space.get_vertice(x1, pflags, plabel, pdomain);
			int x1min = (*pdomain).min();
			int x1max = (*pdomain).max();

			unsigned int x2 = varqueue.front(); varqueue.pop();
			chr::Logical_var<Flags>       cflags;
			chr::Logical_var<std::string> clabel;
			chr::Logical_var<Domain>      cdomain;
			space.get_vertice(x2, cflags, clabel, cdomain);
			int x2min = (*cdomain).min();
			int x2max = (*cdomain).max();

			// 20190705 : new auxiliary domain
			int dmin = std::max((x1min+x2min), max_aux_domain.domain.front());
			int dmax = std::min((x1max+x2max), max_aux_domain.domain.back());

			IntVarDomain new_aux_domain;
			for (int k=dmin; k<=dmax; k++)
				new_aux_domain.domain.push_back(k);

			unsigned int aux = space._first_free_id++;
			space.vertice(aux, Flags(Flag::VARIABLE | Flag::INTEGER | Flag::AUXILIARY), N_STR, new_aux_domain); space.num_variables++;
			unsigned int auxsum = space._first_free_id++;
			space.vertice(auxsum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;

			space.edge(auxsum, x1, 0);
			space.edge(auxsum, x2, 1);
			space.edge(auxsum, aux, 2);
			std::cout << auxsum << ": " << x1 << " + " << x2 << " = " << aux << "\t";
			std::cout << "[" <<  x1min << "," << x2max << "] + ";
			std::cout << "[" <<  x2min << "," << x2max << "] = ";
			std::cout << "[" <<  dmin << "," << dmax << "] " << std::endl;


			varqueue.push(aux);
		}

		// final D2 sum
		unsigned int x1 = varqueue.front(); varqueue.pop();
		unsigned int x2 = varqueue.front(); varqueue.pop();

		unsigned int auxsum = space._first_free_id++;
		space.vertice(auxsum, Flags(Flag::RELATION | Flag::ADD_EQUAL), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(auxsum, x1, 0);
		space.edge(auxsum, x2, 1);
		space.edge(auxsum, magic_var,  2);
		std::cout << auxsum << ": " << x1 << " + " << x2 << " = " << magic_var << std::endl;
	}
	std::cout << std::endl;


	// alldiff --------------------------------------------
	unsigned int alldiff = space._first_free_id++;
	space.vertice(alldiff, Flags(Flag::RELATION | Flag::ALL_DIFFERENT), N_STR, IntVarDomain() ); space.num_relations++;
	for (unsigned int i=0; i<N; ++i)
		for (unsigned int j=0; j<N; ++j)
			space.edge(alldiff,matrix[i][j],-1);
	std::cout << alldiff << ": alldiff" << std::endl << std::endl;


    // SYMMETRY BREAKERS ----------------------------------
    {
		unsigned int sb_row = space._first_free_id++;
		space.vertice(sb_row, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(sb_row, matrix[0][0], 0);
		space.edge(sb_row, matrix[N-1][0], 1);
		std::cout << sb_row << ": " << matrix[0][0] << " < " << matrix[N-1][0] << std::endl;

		unsigned int sb_col = space._first_free_id++;
		space.vertice(sb_col, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(sb_col, matrix[0][0],0);
		space.edge(sb_col, matrix[0][N-1],1);
		std::cout << sb_col << ": " << matrix[0][0] << " < " << matrix[0][N-1] << std::endl;

		unsigned int sb_d1 = space._first_free_id++;
		space.vertice(sb_d1, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(sb_d1, matrix[0][0], 0);
		space.edge(sb_d1, matrix[N-1][N-1], 1);
		std::cout << sb_d1 << ": " << matrix[0][0] << " < " << matrix[N-1][N-1] << std::endl;

		unsigned int sb_d2 = space._first_free_id++;
		space.vertice(sb_d2, Flags(Flag::RELATION | Flag::LESS_THAN), N_STR, IntVarDomain() ); space.num_relations++;
		space.edge(sb_d2, matrix[0][N-1], 0);
		space.edge(sb_d2, matrix[N-1][0], 1);
		std::cout << sb_d2 << ": " << matrix[0][N-1] << " < " << matrix[N-1][0] << std::endl;
    }
    std::cout << std::endl;


}
