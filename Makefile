# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.5

# Default target executed when no arguments are given to make.
default_target: all

.PHONY : default_target

# Allow only one "make -f Makefile2" at a time, but pass parallelism.
.NOTPARALLEL:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Produce verbose output by default.
VERBOSE = 1

# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/claudia/Bureau/cspsat-graph

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/claudia/Bureau/cspsat-graph

#=============================================================================
# Targets provided globally by CMake.

# Special rule for the target install
install: preinstall
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Install the project..."
	/usr/bin/cmake -P cmake_install.cmake
.PHONY : install

# Special rule for the target install
install/fast: preinstall/fast
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Install the project..."
	/usr/bin/cmake -P cmake_install.cmake
.PHONY : install/fast

# Special rule for the target rebuild_cache
rebuild_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake to regenerate build system..."
	/usr/bin/cmake -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : rebuild_cache

# Special rule for the target rebuild_cache
rebuild_cache/fast: rebuild_cache

.PHONY : rebuild_cache/fast

# Special rule for the target list_install_components
list_install_components:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Available install components are: \"Unspecified\""
.PHONY : list_install_components

# Special rule for the target list_install_components
list_install_components/fast: list_install_components

.PHONY : list_install_components/fast

# Special rule for the target install/strip
install/strip: preinstall
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Installing the project stripped..."
	/usr/bin/cmake -DCMAKE_INSTALL_DO_STRIP=1 -P cmake_install.cmake
.PHONY : install/strip

# Special rule for the target install/strip
install/strip/fast: install/strip

.PHONY : install/strip/fast

# Special rule for the target install/local
install/local: preinstall
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Installing only the local directory..."
	/usr/bin/cmake -DCMAKE_INSTALL_LOCAL_ONLY=1 -P cmake_install.cmake
.PHONY : install/local

# Special rule for the target install/local
install/local/fast: install/local

.PHONY : install/local/fast

# Special rule for the target edit_cache
edit_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "No interactive CMake dialog available..."
	/usr/bin/cmake -E echo No\ interactive\ CMake\ dialog\ available.
.PHONY : edit_cache

# Special rule for the target edit_cache
edit_cache/fast: edit_cache

.PHONY : edit_cache/fast

# The main all target
all: cmake_check_build_system
	$(CMAKE_COMMAND) -E cmake_progress_start /home/claudia/Bureau/cspsat-graph/CMakeFiles /home/claudia/Bureau/cspsat-graph/CMakeFiles/progress.marks
	$(MAKE) -f CMakeFiles/Makefile2 all
	$(CMAKE_COMMAND) -E cmake_progress_start /home/claudia/Bureau/cspsat-graph/CMakeFiles 0
.PHONY : all

# The main clean target
clean:
	$(MAKE) -f CMakeFiles/Makefile2 clean
.PHONY : clean

# The main clean target
clean/fast: clean

.PHONY : clean/fast

# Prepare targets for installation.
preinstall: all
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall

# Prepare targets for installation.
preinstall/fast:
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall/fast

# clear depends
depend:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 1
.PHONY : depend

#=============================================================================
# Target rules for targets named graph_unity

# Build rule for target.
graph_unity: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 graph_unity
.PHONY : graph_unity

# fast build rule for target.
graph_unity/fast:
	$(MAKE) -f CMakeFiles/graph_unity.dir/build.make CMakeFiles/graph_unity.dir/build
.PHONY : graph_unity/fast

#=============================================================================
# Target rules for targets named all_unity

# Build rule for target.
all_unity: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 all_unity
.PHONY : all_unity

# fast build rule for target.
all_unity/fast:
	$(MAKE) -f CMakeFiles/all_unity.dir/build.make CMakeFiles/all_unity.dir/build
.PHONY : all_unity/fast

#=============================================================================
# Target rules for targets named clean_cotire

# Build rule for target.
clean_cotire: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 clean_cotire
.PHONY : clean_cotire

# fast build rule for target.
clean_cotire/fast:
	$(MAKE) -f CMakeFiles/clean_cotire.dir/build.make CMakeFiles/clean_cotire.dir/build
.PHONY : clean_cotire/fast

#=============================================================================
# Target rules for targets named graph_pch_pre

# Build rule for target.
graph_pch_pre: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 graph_pch_pre
.PHONY : graph_pch_pre

# fast build rule for target.
graph_pch_pre/fast:
	$(MAKE) -f CMakeFiles/graph_pch_pre.dir/build.make CMakeFiles/graph_pch_pre.dir/build
.PHONY : graph_pch_pre/fast

#=============================================================================
# Target rules for targets named graph

# Build rule for target.
graph: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 graph
.PHONY : graph

# fast build rule for target.
graph/fast:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/build
.PHONY : graph/fast

#=============================================================================
# Target rules for targets named graph_pch

# Build rule for target.
graph_pch: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 graph_pch
.PHONY : graph_pch

# fast build rule for target.
graph_pch/fast:
	$(MAKE) -f CMakeFiles/graph_pch.dir/build.make CMakeFiles/graph_pch.dir/build
.PHONY : graph_pch/fast

#=============================================================================
# Target rules for targets named all_pch

# Build rule for target.
all_pch: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 all_pch
.PHONY : all_pch

# fast build rule for target.
all_pch/fast:
	$(MAKE) -f CMakeFiles/all_pch.dir/build.make CMakeFiles/all_pch.dir/build
.PHONY : all_pch/fast

apply_transformations.o: apply_transformations.cpp.o

.PHONY : apply_transformations.o

# target to build an object file
apply_transformations.cpp.o:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/apply_transformations.cpp.o
.PHONY : apply_transformations.cpp.o

apply_transformations.i: apply_transformations.cpp.i

.PHONY : apply_transformations.i

# target to preprocess a source file
apply_transformations.cpp.i:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/apply_transformations.cpp.i
.PHONY : apply_transformations.cpp.i

apply_transformations.s: apply_transformations.cpp.s

.PHONY : apply_transformations.s

# target to generate assembly for a file
apply_transformations.cpp.s:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/apply_transformations.cpp.s
.PHONY : apply_transformations.cpp.s

cotire/graph_CXX_unity.o: cotire/graph_CXX_unity.cxx.o

.PHONY : cotire/graph_CXX_unity.o

# target to build an object file
cotire/graph_CXX_unity.cxx.o:
	$(MAKE) -f CMakeFiles/graph_unity.dir/build.make CMakeFiles/graph_unity.dir/cotire/graph_CXX_unity.cxx.o
.PHONY : cotire/graph_CXX_unity.cxx.o

cotire/graph_CXX_unity.i: cotire/graph_CXX_unity.cxx.i

.PHONY : cotire/graph_CXX_unity.i

# target to preprocess a source file
cotire/graph_CXX_unity.cxx.i:
	$(MAKE) -f CMakeFiles/graph_unity.dir/build.make CMakeFiles/graph_unity.dir/cotire/graph_CXX_unity.cxx.i
.PHONY : cotire/graph_CXX_unity.cxx.i

cotire/graph_CXX_unity.s: cotire/graph_CXX_unity.cxx.s

.PHONY : cotire/graph_CXX_unity.s

# target to generate assembly for a file
cotire/graph_CXX_unity.cxx.s:
	$(MAKE) -f CMakeFiles/graph_unity.dir/build.make CMakeFiles/graph_unity.dir/cotire/graph_CXX_unity.cxx.s
.PHONY : cotire/graph_CXX_unity.cxx.s

graph_chr_GRAPH.o: graph_chr_GRAPH.cpp.o

.PHONY : graph_chr_GRAPH.o

# target to build an object file
graph_chr_GRAPH.cpp.o:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/graph_chr_GRAPH.cpp.o
.PHONY : graph_chr_GRAPH.cpp.o

graph_chr_GRAPH.i: graph_chr_GRAPH.cpp.i

.PHONY : graph_chr_GRAPH.i

# target to preprocess a source file
graph_chr_GRAPH.cpp.i:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/graph_chr_GRAPH.cpp.i
.PHONY : graph_chr_GRAPH.cpp.i

graph_chr_GRAPH.s: graph_chr_GRAPH.cpp.s

.PHONY : graph_chr_GRAPH.s

# target to generate assembly for a file
graph_chr_GRAPH.cpp.s:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/graph_chr_GRAPH.cpp.s
.PHONY : graph_chr_GRAPH.cpp.s

main.o: main.cpp.o

.PHONY : main.o

# target to build an object file
main.cpp.o:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/main.cpp.o
.PHONY : main.cpp.o

main.i: main.cpp.i

.PHONY : main.i

# target to preprocess a source file
main.cpp.i:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/main.cpp.i
.PHONY : main.cpp.i

main.s: main.cpp.s

.PHONY : main.s

# target to generate assembly for a file
main.cpp.s:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/main.cpp.s
.PHONY : main.cpp.s

output_print.o: output_print.cpp.o

.PHONY : output_print.o

# target to build an object file
output_print.cpp.o:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/output_print.cpp.o
.PHONY : output_print.cpp.o

output_print.i: output_print.cpp.i

.PHONY : output_print.i

# target to preprocess a source file
output_print.cpp.i:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/output_print.cpp.i
.PHONY : output_print.cpp.i

output_print.s: output_print.cpp.s

.PHONY : output_print.s

# target to generate assembly for a file
output_print.cpp.s:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/output_print.cpp.s
.PHONY : output_print.cpp.s

relation_transformations.o: relation_transformations.cpp.o

.PHONY : relation_transformations.o

# target to build an object file
relation_transformations.cpp.o:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/relation_transformations.cpp.o
.PHONY : relation_transformations.cpp.o

relation_transformations.i: relation_transformations.cpp.i

.PHONY : relation_transformations.i

# target to preprocess a source file
relation_transformations.cpp.i:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/relation_transformations.cpp.i
.PHONY : relation_transformations.cpp.i

relation_transformations.s: relation_transformations.cpp.s

.PHONY : relation_transformations.s

# target to generate assembly for a file
relation_transformations.cpp.s:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/relation_transformations.cpp.s
.PHONY : relation_transformations.cpp.s

variable_transformations.o: variable_transformations.cpp.o

.PHONY : variable_transformations.o

# target to build an object file
variable_transformations.cpp.o:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/variable_transformations.cpp.o
.PHONY : variable_transformations.cpp.o

variable_transformations.i: variable_transformations.cpp.i

.PHONY : variable_transformations.i

# target to preprocess a source file
variable_transformations.cpp.i:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/variable_transformations.cpp.i
.PHONY : variable_transformations.cpp.i

variable_transformations.s: variable_transformations.cpp.s

.PHONY : variable_transformations.s

# target to generate assembly for a file
variable_transformations.cpp.s:
	$(MAKE) -f CMakeFiles/graph.dir/build.make CMakeFiles/graph.dir/variable_transformations.cpp.s
.PHONY : variable_transformations.cpp.s

# Help Target
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... depend"
	@echo "... install"
	@echo "... rebuild_cache"
	@echo "... graph_unity"
	@echo "... all_unity"
	@echo "... clean_cotire"
	@echo "... graph_pch_pre"
	@echo "... graph"
	@echo "... list_install_components"
	@echo "... install/strip"
	@echo "... install/local"
	@echo "... graph_pch"
	@echo "... all_pch"
	@echo "... edit_cache"
	@echo "... apply_transformations.o"
	@echo "... apply_transformations.i"
	@echo "... apply_transformations.s"
	@echo "... cotire/graph_CXX_unity.o"
	@echo "... cotire/graph_CXX_unity.i"
	@echo "... cotire/graph_CXX_unity.s"
	@echo "... graph_chr_GRAPH.o"
	@echo "... graph_chr_GRAPH.i"
	@echo "... graph_chr_GRAPH.s"
	@echo "... main.o"
	@echo "... main.i"
	@echo "... main.s"
	@echo "... output_print.o"
	@echo "... output_print.i"
	@echo "... output_print.s"
	@echo "... relation_transformations.o"
	@echo "... relation_transformations.i"
	@echo "... relation_transformations.s"
	@echo "... variable_transformations.o"
	@echo "... variable_transformations.i"
	@echo "... variable_transformations.s"
.PHONY : help



#=============================================================================
# Special targets to cleanup operation of make.

# Special rule to run CMake to check the build system integrity.
# No rule that depends on this can have commands that come from listfiles
# because they might be regenerated.
cmake_check_build_system:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 0
.PHONY : cmake_check_build_system

