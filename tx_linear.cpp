#include "misc_utils.hpp"
#include <algorithm>
#include <graph_chr_header.hpp>

#define N_STR std::string()


// ===========================================================================
// ADD_EQUAL to CNF (wrapper)
// ===========================================================================

void GRAPH::transform_add_equal_cnf(unsigned int id)
{
    if (encoding_sequence[0] == Flag::LOG_ENCODING)
		encode_constraint_different_direct(id);
    else if (encoding_sequence[0] == Flag::HYBRID_ENCODING)
		encode_add_equal_cnf_hybrid(id);
	else
		std::cerr << "ADD_EQUAL: Transformation not available." << std::endl;
	return;
}



// ===========================================================================
// ADD_EQUAL to CNF ~ Hybrid encoding
// ===========================================================================

void GRAPH::encode_add_equal_cnf_hybrid(unsigned int id)
{
    std::cout << std::endl << "GRAPH::add_equal_cnf_hybrid(" << id << ")" << std::endl;

    // check if transformation already exists
	chr::Logical_var<unsigned int> l_id_trans;
	get_child_with_flags(id, Flags(Flag::TRANSFORMATION | Flag::ADD_EQUAL | Flag::BINARY_DISJUNCTION), l_id_trans);
	if (l_id_trans.ground()) {
		std::cerr << "Transformation already exists." << std::endl;
        return;
	}

	// check if relation has 3 children
	chr::Logical_var_mutable< std::vector<unsigned int> > relation_children;
	get_all_children(id, relation_children);
	assert((*relation_children).size() == 3);

	unsigned int id_transformation = _first_free_id++;
	edge(id, id_transformation, -1);
	vertice(id_transformation, Flags(Flag::TRANSFORMATION | Flag::ADD_EQUAL | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_transformations++;

	std::vector<unsigned int> boolvars_x;
	std::vector<unsigned int> boolvars_y;
	std::vector<unsigned int> boolvars_z;
	int ox=0, oy=0, oz=0; 						// offsets

	for (unsigned int i=0; i<3; i++)
	{
		chr::Logical_var<unsigned int> rvar;
		get_child_with_order(id, i, rvar);

		// variable information
		chr::Logical_var<Flags> flags;
		chr::Logical_var<std::string> label;
		chr::Logical_var<Domain> domain;
		get_vertice((*rvar), flags, label, domain);

		// get transformation associated to variable encoding
		chr::Logical_var<unsigned int> var_transformation;
		get_child_with_flags((*rvar), Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::HYBRID_ENCODING), var_transformation);
		if (!var_transformation.ground()) {
			encode_variable((*rvar), Flag::HYBRID_ENCODING);
			get_child_with_flags((*rvar), Flags(Flag::TRANSFORMATION | Flag::INTEGER | Flag::HYBRID_ENCODING), var_transformation);
		}

		// get boolean variables
		chr::Logical_var_mutable< std::vector<unsigned int> > boolvars;

		get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::BOOLEAN | Flag::AUXILIARY), 0, boolvars);
		if ((*boolvars).size()==0)
			get_all_children_with_flags_ordered(var_transformation, Flags(Flag::VARIABLE | Flag::BOOLEAN), 0, boolvars);
		assert((*boolvars).size()>0);

		for(unsigned int j=0; j<(*boolvars).size(); ++j) {
			if (i==0) {
				boolvars_x.push_back( (*boolvars)[j] );
				ox = hvar_offset[(*rvar)];
			}
			if (i==1) {
				boolvars_y.push_back( (*boolvars)[j] );
				oy = hvar_offset[(*rvar)];
			}
			if (i==2) {
				boolvars_z.push_back( (*boolvars)[j] );
				oz = hvar_offset[(*rvar)];
			}
		}
	}




	// LOG addition -------------------------------------------------

	std::cout << "-- log encoding " << std::endl;
	//std::cout << "HBASE: " << HBASE << std::endl << std::endl;


    long carry, n_carry, previous_x, previous_y, previous_z;
	full_add(id_transformation, boolvars_x[0], boolvars_y[0], -1, -1, boolvars_z[0], carry, n_carry);
	std::cout << "full_adder(" << boolvars_x[0] << "," << boolvars_y[0] << ",-1,-1," << boolvars_z[0] << "," << carry << "," << n_carry << ")\t\t";
	std::cout << "c: " << carry << "\t nc: " << n_carry << std::endl;
	previous_x = boolvars_x[0];
	previous_y = boolvars_y[0];
	previous_z = boolvars_z[0];

	for (unsigned int i=1; i<HBASE; ++i)
	{
		long x = (i < boolvars_x.size()) ? boolvars_x[i] : previous_x;
		long y = (i < boolvars_y.size()) ? boolvars_y[i] : previous_y;
		long z = (i < boolvars_z.size()) ? boolvars_z[i] : previous_z;
		full_add(id_transformation, x, y, carry, n_carry, z, carry, n_carry);
		std::cout << "full_adder(" << x << "," << y << "," << carry << "," << n_carry << "," << z << "," << carry << "," << n_carry << ")\t";
		std::cout << "c: " << carry << "\t nc: " << n_carry << std::endl;
		previous_x = x;
		previous_y = y;
		previous_z = z;
	}

	/* log-carry management
	// If (x,y) signs are different, the final carry is not relevant
	// If (x,y) signs are equal, then final carry must have the same sign as z-sign
	// (prev_x == prev_y) -> (carry == prev_z)

	chr::Logical_var<unsigned int> nx, ny, nz;
	get_opposite_var(previous_x, nx);
	assert(nx.ground());

	get_opposite_var(previous_y, ny);
	assert(ny.ground());

	get_opposite_var(previous_z, nz);
	assert(nz.ground());

	clause(id_transformation, previous_x,previous_y,carry,*nz);
	clause(id_transformation, previous_x,previous_y,n_carry,previous_z);
	clause(id_transformation, *nx, *ny, carry, *nz);
	clause(id_transformation, *nx, *ny, n_carry,previous_z);
	*/
	std::cout << std::endl;


	// ORDER addition -----------------------------------------------

	std::cout << "-- order encoding " << std::endl;

//	// length of order-encoded variables
//	unsigned int lox = std::max(0, static_cast<int>(boolvars_x.size()-HBASE));
//	unsigned int loy = std::max(0, static_cast<int>(boolvars_y.size()-HBASE));
////	unsigned int loz = std::max(0, static_cast<int>(boolvars_z.size()-HBASE));
////	std::cout << "lox: " << lox << "\t";
////	std::cout << "loy: " << loy << "\t";
////	std::cout << "loz: " << loz << std::endl << std::endl;

/*
	// case: no order part (lox=0,loy=0)
	if ((lox==0) && (loy==0)) {
		std::cout << "no order-addition (lx=ly=0)" << std::endl;
		return;
	}
	// case: order-part only for X
	else if ((lox>0) && (loy==0))
	{
//		//for (unsigned int i=0; i<lox; i++) {
//			chr::Logical_var<unsigned int> nx;
//			get_opposite_var(boolvars_x[i+HBASE], nx);
//
//			int xvalue = bzvalue(i+HBASE,HBASE,ox);
//			int zindex = bzindex(xvalue,HBASE,oz);
//			std::cout << "i: " << i << "\t";
//			std::cout << "xvalue: " << xvalue << "\t";
//			std::cout << "zindex: " << zindex << "\n";
//
//			unsigned int d = _first_free_id++;
//			vertice(d, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
//			edge(d, nx,-1);
//			edge(d, boolvars_z[zindex],-1);
//			std::cout << "(y0) " << d << ":\t" << nx << " " << boolvars_z[zindex] << std::endl;
//		}


		for (unsigned int i=HBASE; i<boolvars_x.size(); i++) {
            chr::Logical_var<unsigned int> nx;
			get_opposite_var(boolvars_x[i], nx);

			int xvalue = bzvalue(i,HBASE,ox);
			int zindex = bzindex(xvalue,HBASE,oz);

            std::cout << "xi:" << i << "\txv:" << xvalue << "\t";
			std::cout << "\t\t";
			std::cout << "zi:" << zindex << "\tzv:" << bzvalue(zindex,HBASE,oz) << std::endl;

			unsigned int d = clause(id_transformation, nx, boolvars_z[zindex]);
			std::cout << "(y0) " << d << ":\t" << nx << " " << boolvars_z[zindex] << std::endl;
		}
		return;
	}
	// case: order-part only for Y
	else if ((lox==0) && (loy>0))
	{
//		// for (unsigned int j=0; j<loy; j++) {
//			chr::Logical_var<unsigned int> ny;
//			get_opposite_var(boolvars_y[j+HBASE], ny);
//
//			int yvalue = bzvalue(j+HBASE,HBASE,oy);
//			int zindex = bzindex(yvalue,HBASE,oz);
//			std::cout << "j: " << j << "\t";
//			std::cout << "yvalue: " << yvalue << "\t";
//			std::cout << "zindex: " << zindex << "\n";
//
//			unsigned int d = _first_free_id++;
//			vertice(d, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
//			edge(d, ny,-1);
//			edge(d, boolvars_z[zindex],-1);
//			std::cout << "(x0) " << d << ":\t" << ny << " " << boolvars_z[zindex] << std::endl;
//		}

		for (unsigned int j=HBASE; j<boolvars_y.size(); j++) {
            chr::Logical_var<unsigned int> ny;
			get_opposite_var(boolvars_y[j], ny);

			int yvalue = bzvalue(j,HBASE,oy);
			int zindex = bzindex(yvalue,HBASE,oz);

			std::cout << "\t\t";
			std::cout << "yi:" << j << "\tyv:" << yvalue << "\t";
			std::cout << "zi:" << zindex << "\tzv:" << bzvalue(zindex,HBASE,oz) << std::endl;

			unsigned int d = clause(id_transformation, ny, boolvars_z[zindex]);
			std::cout << "(x0) " << d << ":\t" << ny << " " << boolvars_z[zindex] << std::endl;
		}
		return;
	}


	// addition of 1st b-zaine
	for (unsigned int j=HBASE; j<boolvars_y.size(); j++)
	{
		chr::Logical_var<unsigned int> ny;
		get_opposite_var(boolvars_y[j+oy], ny);

		int yvalue = bzvalue(j+HBASE,HBASE,oy);
			int zindex = bzindex(yvalue,HBASE,oz);
			std::cout << "j: " << j << "\t";
			std::cout << "yvalue: " << yvalue << "\t";
			std::cout << "zindex: " << zindex << "\n";

		unsigned int d1 = _first_free_id++;
		vertice(d1, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
		edge(d1, boolvars_x[HBASE],-1);
		edge(d1, ny,-1);
		edge(d1, boolvars_z[j+oz],-1);
		std::cout << "(1x) " << d1 << ":\t" << boolvars_x[HBASE+1] << " " << ny << " " << boolvars_z[j] << std::endl;
	}

	for (unsigned int i=(HBASE+1); i<boolvars_x.size(); i++)
	{
		chr::Logical_var<unsigned int> nx;
		get_opposite_var(boolvars_x[i], nx);

		unsigned int d1 = _first_free_id++;
		vertice(d1, Flags(Flag::RELATION | Flag::BINARY_DISJUNCTION), N_STR, IntVarDomain()); num_relations++;
		edge(d1, nx,-1);
		edge(d1, boolvars_y[HBASE+1],-1);
		edge(d1, boolvars_z[i],-1);
		std::cout << "(1y) " << d1 << ":\t" << nx << " " << boolvars_y[HBASE+1] << " " << boolvars_z[i] << std::endl;
	}
	std::cout << std::endl;
    */

    // bzaines addition
	int zvalue_min = bzvalue(HBASE,HBASE,oz);
	int zvalue_max = bzvalue(boolvars_z.size()-1,HBASE,oz);
	std::cout << "zvalue IN " << zvalue_min << "," << zvalue_max << std::endl << std::endl;

    // addition of first bzaine
    {
        chr::Logical_var<unsigned int> nx;
		get_opposite_var(boolvars_x[HBASE], nx);
        unsigned int xzclause = clause(id_transformation, nx, boolvars_z[HBASE]);       // x -> z
        std::cout << "(fbx)\t" << xzclause << ": " << nx << " " << boolvars_z[HBASE] << std::endl;

        chr::Logical_var<unsigned int> ny;
		get_opposite_var(boolvars_y[HBASE], ny);
        unsigned int yzclause = clause(id_transformation, ny, boolvars_z[HBASE]);       // y -> z
        std::cout << "(fby)\t" << yzclause << ": " << ny << " " << boolvars_z[HBASE] << std::endl;

        unsigned int rzclause = clause(id_transformation, n_carry, boolvars_z[HBASE]);  // r -> z
        std::cout << "(fbr)\t" << rzclause << ": " << n_carry << " " << boolvars_z[HBASE] << std::endl;

        // not-clause
        chr::Logical_var<unsigned int> nz;
        get_opposite_var(boolvars_z[HBASE], nz);
        unsigned int notclause = clause(id_transformation, boolvars_x[HBASE], boolvars_y[HBASE], carry, nz);    // (~x & ~y & ~r ) => ~z
        std::cout << "(fbn)\t" << notclause << ": " << boolvars_x[HBASE] << " " << boolvars_y[HBASE] << " " << carry << " " << nz << std::endl;
    }

	for (unsigned int i=HBASE; i<boolvars_x.size(); i++)
	{
		chr::Logical_var<unsigned int> nx;
		get_opposite_var(boolvars_x[i], nx);

		int xvalue = bzvalue(i,HBASE,ox);

		for (unsigned int j=HBASE; j<boolvars_y.size(); j++)
		{
			chr::Logical_var<unsigned int> ny;
			get_opposite_var(boolvars_y[j], ny);

			int yvalue = bzvalue(j,HBASE,oy);

			int k = bzindex(xvalue+yvalue,HBASE,oz);
			int kvalue = bzvalue(k,HBASE,oz);

			std::cout << "i: " << i << "~" << xvalue << "\t";
			std::cout << "j: " << j << "~" << yvalue << "\t";
			std::cout << "k: " << k << "~" << kvalue << std::endl;

			//
			// GENERAL CASES
			//

			if ( (zvalue_min < kvalue) && (kvalue <= zvalue_max) ) //if ( (zvalue_min < k) && (k <= zvalue_max) ) //if (k < boolvars_z.size())
			{
				chr::Logical_var<unsigned int> nz;
				get_opposite_var(boolvars_z[k], nz);

				// cg1: (xi & yj) -> z(i+j)
				unsigned int c1 = clause(id_transformation, nx, ny, boolvars_z[k]);
				std::cout << "(cg1) " << c1 << ":\t" << nx << " " << ny << " " << boolvars_z[k] << std::endl;

				// cg3: (~xi & ~yj) -> ~z(i+j)
				unsigned int c3 = clause(id_transformation, boolvars_x[i], boolvars_y[j], nz);
				std::cout << "(cg3) " << c3 << ":\t" << boolvars_x[i] << " " << boolvars_y[j] << " " << nz << std::endl;

                //max_zindex = (k > max_zindex) ? k : max_zindex;
			}

            int B = pow(2,HBASE);
			if ( (zvalue_min < kvalue+B) && (kvalue+B <= zvalue_max) )//if ( (zvalue_min < k+1) && (k+1 <= zvalue_max) )    //if (k+1 < boolvars_z.size())
			{
				// c2: (xi & yj & r) -> z(i+j+1)
				unsigned int c2 = clause(id_transformation, nx, ny, n_carry, boolvars_z[k+1]);
				std::cout << "(cg2) " << c2 << ":\t" << nx << " " << ny << " " << n_carry << " " << boolvars_z[k+1] << std::endl;
				//max_zindex = (k+1 > max_zindex) ? k+1 : max_zindex;
			}

			if ( (zvalue_min < kvalue-B) && (kvalue-B <= zvalue_max) )//if ( (zvalue_min < k-1) && (k-1 <= zvalue_max) ) //if ( (HBASE < k-1) && (k-1 < boolvars_z.size()) )
			{
                // cg4: (¬xi & ¬yj & ¬r) -> ¬z(i+j-1)
                chr::Logical_var<unsigned int> nz;
				get_opposite_var(boolvars_z[k-1], nz);

                unsigned int c4 = clause(id_transformation, boolvars_x[i], boolvars_y[j], carry, nz);
                std::cout << "(cg4) " << c4 << ":\t" << boolvars_x[i] << " " << boolvars_y[j] << " " << carry << " " << nz << std::endl;
			}


			//
			// PARTICULAR CASES (min/max bornes)
			//

			if (zvalue_max < kvalue)
			{   // cp1: ~(xi & yj)
                unsigned int cp1 = clause(id_transformation, nx, ny);
                std::cout << "(cp1) " << cp1 << ":\t" << nx << " " << ny << std::endl;

                // TEST
                //unsigned int cp = clause(id_transformation, nx, ny, n_carry);
                //std::cout << "(cp) " << cp << ":\t" << nx << " " << ny << " " << n_carry << std::endl;
			}

			if ((zvalue_max-B < kvalue) && (kvalue <= zvalue_max))
			{   // cp2: ~(xi & yj & r)
                unsigned int cp2 = clause(id_transformation, nx, ny, n_carry);
                std::cout << "(cp2) " << cp2 << ":\t" << nx << " " << ny << " " << n_carry << std::endl;
			}

            // TEST
			if (xvalue == zvalue_max) {
                unsigned int cpa = clause(id_transformation, nx, n_carry);
                std::cout << "(cpa) " << cpa << ":\t" << nx << " " << n_carry << std::endl;
			}
			if (yvalue == zvalue_max) {
                unsigned int cpb = clause(id_transformation, ny, n_carry);
                std::cout << "(cpb) " << cpb << ":\t" << ny << " " << n_carry << std::endl;
			}

			if (kvalue < zvalue_min-B)
			{   // cp3
                unsigned int cp3 = clause(id_transformation, nx, ny);
                std::cout << "(cp3) " << cp3 << ":\t" << nx << " " << ny << std::endl;
			}

			if ((zvalue_min-B <= kvalue) && (kvalue < zvalue_min))
			{   // cp4
                unsigned int cp4 = clause(id_transformation, nx, ny, carry);
                std::cout << "(cp4) " << cp4 << ":\t" << nx << " " << ny << " " << carry <<std::endl;
			}
		}
		std::cout << std::endl;
	}

//	// clauses for optimized encoding (if required)
//	if (max_zindex+1 < boolvars_z.size()) {
//		std::cout << "max_zindex: " << max_zindex << std::endl;
//		chr::Logical_var<unsigned int> nz;
//		get_opposite_var(boolvars_z[max_zindex+1], nz);
//		unsigned int copt = clause(id_transformation, nz);
//		std::cout << "(copt) " << copt << ":\t" << nz << std::endl;
//	}


	std::cout << std::endl;
}






