#ifndef __CSPSAT_UTILS__
#define __CSPSAT_UTILS__

//#include <iostream>
#include <cmath>
#include <vector>



// =======================================================================
// Hybrid Encoding functions
// =======================================================================

int h_offset(int x, unsigned int base);
int h_domain_offset(int l, int u, unsigned int base);
unsigned int h_domain_length(int lower, int upper, int base);

int bzvalue(unsigned int index, unsigned int base, int offset);	// bzaine value
unsigned int bzindex(int value, unsigned int base, int offset);	// bzaine index

int hybrid2value(std::string s, unsigned int base, unsigned int offset);
//std::vector<bool> value2hybrid(int value, unsigned int base);
std::vector<bool> value2hybrid(int value, unsigned int base, int offset, unsigned int length);



// =======================================================================
// Intervals
// =======================================================================

struct Interval {
	int l, u;
	Interval(int a,int b) : l(a), u(b) {}
};

//std::vector<int> interval_cutpoints(int a, int b, unsigned int ncuts, bool all_levels=true);
//std::vector<int> interval_cutpoints(int a, int b, unsigned int ncuts);
std::vector<Interval> subintervals2(int a, int b);



// =======================================================================
// misc. functions
// =======================================================================

std::vector<unsigned int> dec2binary(int n, int nbits);
//unsigned int power_ceil(unsigned int num, int base=2);



#endif // __CSPSAT_UTILS__

